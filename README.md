# Hi-C single-cell deconvolution through Markov chain Monte Carlo sampling #

This program is part of the **CHROMATIX** chromatin folding software suite.

If using this software, please cite:

* Perez-Rathke, Alan, Qiu Sun, Boshen Wang, Valentina Boeva, Zhifeng Shao, and Jie Liang. "CHROMATIX: computing the functional landscape of many-body chromatin interactions in transcriptionally active loci from deconvolved single cells." Genome Biology 21, no. 1 (2020): 1-17. [https://doi.org/10.1186/s13059-019-1904-z](https://doi.org/10.1186/s13059-019-1904-z).

See also additional **CHROMATIX** software:

* [CHR-FOLDER](https://bitbucket.org/aperezrathke/chr-folder/) - 3-D chromatin folder using fractal Monte Carlo
* [MBI-MAM](https://bitbucket.org/aperezrathke/chr-loc-mbi-mam/) - Application of CHROMATIX for detection of **m**any-**b**ody **i**nteractions in the **mam**malian GM12878 cell line

Application framework is based on [SKELeton](https://bitbucket.org/aperezrathke/skel).

![spop](img/spop.svg)

## Setup ##

### Windows ###

A visual studio 2019 project is supplied. It assumes the existence of the following environment variable(s):

* `BOOST_ROOT` - location of boost directory containing headers and binaries

The provided [LAPACK binaries](https://bitbucket.org/aperezrathke/vs-lapack) are compatible with visual studio 2019.

### Linux ###

CMake build scripts are provided. They have been tested under linux CentOs and Ubuntu.

First, to initialize the build system, run the script `cmake_init.sh`. Then, run the script `cmake_build.sh` to compile the resulting configurations. The compiled executables can be found within the `CMakeBuild` folder.

## License ##

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
