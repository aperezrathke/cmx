#!/bin/bash

# Script for compiling on linux with g++, threading-enabled, tests-enabled,
# asserts are not force-enabled and only enabled on debug builds by default
#
# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo "Running build on $HOSTNAME"

# Generate make files
$SCRIPT_DIR/../cmake_init.sh -tests 1 -threads 1

# Compile
$SCRIPT_DIR/../cmake_build.sh -tests 1 -threads 1

echo "Finished build."
