#!/bin/bash

# Script for compiling on linux with g++, threading-enabled, and acceptance
#   tracking enabled
#
# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

echo "Running build on $HOSTNAME"

# Generate make files
$SCRIPT_DIR/../cmake_init.sh -threads 1 -trackaccept 1

# Compile
$SCRIPT_DIR/../cmake_build.sh -threads 1 -trackaccept 1

echo "Finished build."
