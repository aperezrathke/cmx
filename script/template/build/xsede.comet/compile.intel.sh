#!/bin/bash

# Script for compiling on XSEDE comet

# Directory containing this script
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# To check available software:
# https://portal.xsede.org/software#/

# Load modules
module load gnu
module load cmake/3.3.2
module load boost/1.55.0
module load intel/2016.3.210

$SCRIPT_DIR/../cmake_init.sh -intel 1

# Compile scripts
$SCRIPT_DIR/../cmake_build.sh

echo "Finished XSEDE build."
