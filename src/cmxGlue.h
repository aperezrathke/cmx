//****************************************************************************
// cmxGlue.h
//****************************************************************************

/**
 * @brief Macros and utilities for declaring and defining glue constructs
 */

/**
 * 'Glue' classes for Bayesian hierarchical modeling. The Glue class must
 * define the following typedefs:
 *  1a.) Core simulation utilities
 *      - sim_t: Parent simulation type
 *      - sim_shared_t: Parent simulation-level shared data
 *      - model_t: This model type
 *      - model_shared_t: Model-level shared data
 *  1b.) Controller simulation utilities
 *      - controller_t: Base controller type
 *      - model_batch_runner_t: Utility for batch updating models
 *  1c.) Observer simulation utilities
 *      - obs_t: Base observer type
 *  2.) Model components supporting the "Moco" interface:
 *      - Dp_stick_alpha_t: Dirichlet process stick-breaking alpha parameter;
 *          may be a fixed deterministic value or a prior such as Gamma or
 *          Uniform
 *      - Dp_stick_t: Dirichlet process stick-breaking component
 *      - Bnet_dag_kappa_t: Bayesian network DAG edge prior
 *      - Bnet_cpt_alpha_t: Bayesian network conditional probability table
 *          (CPT) prior
 *      - Bnet_infeas_lambda: Hyper-parameter for prior on infeasible samples
 *      - Bnet_t: Bayesian network tuple (DAG, CPT)
 *      - Clust_t: Cluster association indicator variables
 *      - Pseudopop_t: Pseudo-population
 *      - Contact_freqs_alpha_t: Alpha parameter for contact frequency matrix
 *          may be a fixed deterministic value or a prior such as Uniform
 *      - Contact_freqs_t: Linearized contact matrix, observed Hi-C, defines
 *          likelihood of contact frequency matrix given pseudo-population
 *
 * See model component declarations for further information.
 */

#ifndef cmxGlue_h
#define cmxGlue_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxSmartPtr.h"

//****************************************************************************
// Macros
//****************************************************************************

/**
 * Macro for injecting Glue simulation core typedef's into enclosing class
 */
#define CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue)         \
    typedef t_Glue glue_t;                            \
    typedef typename t_Glue::sim_t sim_t;             \
    typedef typename t_Glue::shared_ro_t shared_ro_t; \
    typedef typename t_Glue::shared_mu_t shared_mu_t; \
    typedef typename t_Glue::model_t model_t;

/**
 * Macro injects typedefs for simulation controller handles.
 *
 * Requires glue to define base controller type, e.g.:
 *  class t_Glue { public:
 *      ... typedef cmxController<t_Glue> controller_t; ... };
 */
#define CMX_GLUE_INJECT_CONTROLLER_TYPEDEFS(t_Glue)                            \
    typedef typename t_Glue::controller_t controller_t;                        \
    typedef cmxSmartPtr::shared_ptr<controller_t> sp_controller_t;             \
    typedef cmxSmartPtr::shared_ptr<const controller_t> sp_const_controller_t; \
    typedef cmxSmartPtr::weak_ptr<controller_t> wp_controller_t;               \
    typedef cmxSmartPtr::weak_ptr<const controller_t> wp_const_controller_t;   \
    typedef typename t_Glue::model_batch_runner_t model_batch_runner_t

/**
 * Macro injects typedefs for simulation observer handles.
 *
 * Requires glue to define base observer type, e.g.:
 *  class t_Glue { public: ... typedef cmxObs<t_Glue> obs_t; ... };
 */
#define CMX_GLUE_INJECT_OBS_TYPEDEFS(t_Glue)                     \
    typedef typename t_Glue::obs_t obs_t;                        \
    typedef cmxSmartPtr::shared_ptr<obs_t> sp_obs_t;             \
    typedef cmxSmartPtr::shared_ptr<const obs_t> sp_const_obs_t; \
    typedef cmxSmartPtr::weak_ptr<obs_t> wp_obs_t;               \
    typedef cmxSmartPtr::weak_ptr<const obs_t> wp_const_obs_t

/**
 * Macro injects typedefs relevant for simulation management.
 *
 * Requires glue to define base controller and base observer types, e.g.:
 *  class t_Glue { public:
 *      ... typedef cmxController<t_Glue> controller_t;
 *          typedef cmxObs<t_Glue> obs_t; ... };
 */
#define CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue)     \
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);       \
    CMX_GLUE_INJECT_CONTROLLER_TYPEDEFS(t_Glue); \
    CMX_GLUE_INJECT_OBS_TYPEDEFS(t_Glue)

#endif  // cmxGlue_h
