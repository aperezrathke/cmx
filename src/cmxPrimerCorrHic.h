//****************************************************************************
// cmxPrimerCorrHic.h
//****************************************************************************

/**
 * A primer is a pre-burn utility which can perform the following:
 *   - randomize models
 *   - prime models: models are randomized and simulated for a fixed number of
 *       steps and then checked for some criteria (e.g. Hi-C correlation) - any
 *       models which fail criteria are re-primed until criteria is met or the
 *       maximum attempt threshold is reached
 */

#ifndef cmxPrimerCorrHic_h
#define cmxPrimerCorrHic_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxMcmcGibbsRev.h"
#include "cmxObsCorrHicSnap.h"
#include "cmxOpts.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// Defines
//****************************************************************************

// @TODO - Should we move these defines to cmxBuild.h?

/**
 * Default number of steps to prime a model before checking criteria
 */
#define CMX_DEFAULT_PRIMER_STEPS CMX_TO_UINT(1000)

/**
 * Maximum number of prime attempts
 */
#define CMX_DEFAULT_PRIMER_ATTEMPTS CMX_TO_UINT(100)

/**
 * Default primer target Hi-C correlation criteria
 */
#define CMX_DEFAULT_PRIMER_CORR_HIC_THRESH CMX_TO_REAL(0.8)

/**
 * Maximum allowable threshold for primer target Hi-C correlation
 */
#define CMX_MAX_PRIMER_CORR_HIC_THRESH CMX_TO_REAL(0.9)

//****************************************************************************
// cmxPrimerCorrHic
//****************************************************************************

/**
 * Attempts to prime models such that their Hi-C correlation meets target
 *  threshold
 */
template <typename t_Glue>
class cmxPrimerCorrHic {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Hi-C correlation snapshot observer type
     */
    typedef cmxObsCorrHicSnap<t_Glue> obs_corr_hic_snap_t;

private:
    /**
     * Primer core configuration
     */
    typedef struct {
        // Steps alloted for priming a model
        cmxUInt steps;
        // Maximum attempts to prime a model
        cmxUInt attempts;
        // Target empirical Hi-C correlation for primed models
        cmxReal corr_hic_thresh;
    } core_t;

public:
    /**
     * Primes simulation models searching for states satisfying target Hi-C
     *  correlation threshold
     * @param sim - Simulation containing models to prime
     * @param config - User configuration
     */
    static void prime(sim_t& sim, cmxSpConstConfig_t config) {
        // Initialize prime core
        core_t core;
        init_core(core, config);
        // Randomize from prior distributions
        sim.randomize(config);
        // Record initial randomization
        cmxLogf("Finished randomization of simulation.\n");
        sim.observers_force_update(CMX_TO_UINT(0));
        // Early out if zero prime attempts or steps
        if ((core.attempts < CMX_TO_UINT(1)) || (core.steps < CMX_TO_UINT(1))) {
            return;
        }
        cmxAssert(core.attempts > CMX_TO_UINT(0));
        cmxAssert(core.steps > CMX_TO_UINT(0));
        // Empirical Hi-C frequencies
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();
        // Copy pre-primed models
        const std::vector<model_t> revert(sim.get_models());
        const size_t num_models = revert.size();
        // Handle to set of output models
        std::vector<model_t>& models = sim.get_models();
        // Set of primed models
        std::vector<model_t> primed;
        primed.reserve(num_models);
        // Correlations at each model - must be pre-sized!
        std::vector<cmxReal> corrs(num_models, CMX_TO_REAL(0.0));
        // Prime models!
        for (cmxUInt i_attempt = CMX_TO_UINT(0); i_attempt < core.attempts;
             ++i_attempt) {
            cmxAssert(primed.size() < num_models);
            // Reset models
            models = revert;
            // Randomize
            cmxLogf("%d: Primer randomize.\n", (int)i_attempt);
            sim.randomize(config);
            // Update
            cmxLogf("%d: Primer update.\n", (int)i_attempt);
            model_batch_runner_t::run(
                core.steps, models, sim CMX_MAIN_THREAD_ID_ARG);
            // Compute criteria
            obs_corr_hic_snap_t::calc_corr_hic(corrs, models, hic);
            // Check criteria
            for (size_t i_model = 0; i_model < num_models; ++i_model) {
                cmxAssertBounds(i_model, 0, corrs.size());
                const cmxReal corr = corrs[i_model];
                cmxAssertRealBoundsInc(
                    corr, CMX_TO_REAL(-1.0), CMX_TO_REAL(1.0));
                if (corr >= core.corr_hic_thresh) {
                    // Model primed!
                    cmxAssertBounds(i_model, 0, models.size());
                    primed.push_back(models[i_model]);
                    if (primed.size() >= num_models) {
                        break;
                    }  // end inner check if we've primed enough models
                }      // end check if model correlation meets target
            }          // end iteration over model correlations
            // Status
            cmxLogf(
                "%d: Primed %d models.\n", (int)i_attempt, (int)primed.size());
            if (primed.size() >= num_models) {
                break;
            }  // end outer check if we've primed enough models
        }      // end iteration over attempts
        // Emit primed models
        const size_t num_primed = primed.size();
        cmxAssert(num_primed <= models.size());
        for (size_t i_primed = 0; i_primed < num_primed; ++i_primed) {
            const model_t& out = primed[i_primed];
            cmxAssertBounds(i_primed, 0, models.size());
            models[i_primed] = out;
        }
        cmxAssert(models.size() == num_models);
        cmxLogf("Finished priming of simulation.\n");
        sim.observers_force_update(CMX_TO_UINT(0));
    }

private:
    /**
     * Initialize primer core configuration
     * @param core - Output primer configuration
     * @param config - User configuration options
     */
    static void init_core(core_t& core, cmxSpConstConfig_t config) {
        // Determine number of steps
        int steps = static_cast<int>(CMX_DEFAULT_PRIMER_STEPS);
        config->read_into(steps, cmxOpt_primer_steps);
        steps = std::max(steps, 0);
        core.steps = CMX_TO_UINT(steps);
        // Determine number of attempts
        int attempts = static_cast<int>(CMX_DEFAULT_PRIMER_ATTEMPTS);
        config->read_into(attempts, cmxOpt_primer_attempts);
        attempts = std::max(attempts, 0);
        core.attempts = CMX_TO_UINT(attempts);
        // Determine target Hi-C correlation
        core.corr_hic_thresh = CMX_DEFAULT_PRIMER_CORR_HIC_THRESH;
        config->read_into(core.corr_hic_thresh, cmxOpt_primer_corr_hic_thresh);
        if (core.corr_hic_thresh > CMX_MAX_PRIMER_CORR_HIC_THRESH) {
            cmxLogf(
                "Warning: primer Hi-C correlation threshold %f exceeds max %f, "
                "capping to %f.\n",
                (double)core.corr_hic_thresh,
                (double)CMX_MAX_PRIMER_CORR_HIC_THRESH,
                (double)CMX_MAX_PRIMER_CORR_HIC_THRESH);
            core.corr_hic_thresh = CMX_MAX_PRIMER_CORR_HIC_THRESH;
        }
        cmxAssert(core.corr_hic_thresh <= CMX_MAX_PRIMER_CORR_HIC_THRESH);
    }
};

#endif  // cmxPrimerCorrHic_h
