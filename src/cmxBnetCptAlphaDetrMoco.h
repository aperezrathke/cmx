//****************************************************************************
// cmxBnetCptAlphaDetrMoco.h
//****************************************************************************

/**
 * @brief Real-valued scalar hyper-parameter for Dirichlet (Beta) prior on
 *  Bayesian network conditional probability table (CPT) parameters.
 *      Detr -> 'Deterministic', this component is non-random
 */

#ifndef cmxBnetCptAlphaDetrMoco_h
#define cmxBnetCptAlphaDetrMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxScalarDetrMoco.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetCptAlphaDetrMoco
//****************************************************************************

/**
 * Simulation-level read-only shared data, provides user configured alpha
 * (a hyper-parameter intended for prior on CPT parameters)
 */
class cmxBnetCptAlphaDetrSharcoRo {
public:
    /**
     * Default constructor
     */
    cmxBnetCptAlphaDetrSharcoRo() : m_alpha(CMX_DEFAULT_BNET_CPT_ALPHA) {}

    /**
     * Initializes shared resource(s) - captures user-supplied CPT alpha
     * @param config - user options
     */
    void init(cmxSpConstConfig_t config) {
        m_alpha = CMX_DEFAULT_BNET_CPT_ALPHA;
        config->read_into(m_alpha, cmxOpt_Bnet_cpt_alpha);
        if (m_alpha <= CMX_TO_REAL(0.0)) {
            cmxLogf(
                "Warning: Bayesian network CPT alpha must be > 0.\n\t-Setting "
                "to default: %f\n",
                ((double)(CMX_DEFAULT_BNET_CPT_ALPHA)));
            m_alpha = CMX_DEFAULT_BNET_CPT_ALPHA;
        }
        cmxAssert(m_alpha > CMX_TO_REAL(0.0));
    }

    /**
     * Clear (reset) read-only resources
     */
    void clear() { m_alpha = CMX_DEFAULT_BNET_CPT_ALPHA; }

    /**
     * @return user configured alpha scalar
     */
    inline cmxReal get_scalar() const { return m_alpha; }

private:
    /**
     * CPT pseudo-count hyper-parameter
     */
    cmxReal m_alpha;
};

/**
 * Utility for querying global read-only Bayesian network CPT alpha parameter
 */
template <typename t_Glue>
class cmxBnetCptAlphaDetrSharedQuery {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);
    /**
     * @param sim - outer simulation
     * @return real-valued scalar hyper-parameter
     */
    static inline cmxReal get_scalar(const sim_t& sim) {
        const cmxReal alpha =
            sim.get_shared_ro().get_Bnet_cpt_alpha().get_scalar();
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        return alpha;
    }
};

/**
 * Root-level deterministic scalar node representing pseudo-count argument for
 * Dirichlet (Beta) prior on Bayesian network CPTs, template arguments:
 *  - t_Glue: Glue object as defined by cmxGlue
 */
template <typename t_Glue>
class cmxBnetCptAlphaDetrMoco
    : public cmxScalarDetrMoco<t_Glue,
                               cmxBnetCptAlphaDetrSharedQuery<t_Glue> > {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);
};

#endif  // cmxBnetCptAlphaDetrMoco_h
