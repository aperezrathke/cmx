//****************************************************************************
// cmxObsCorrHicTrac.h
//****************************************************************************

/**
 * @brief Trace plot observer for model empirical Hi-C correlation with latent
 *  pseudo-population. Tracer observers observers record a history of the
 *  tracked value for trace plot generation and typically rely on Snap
 *  ('Snapshot') observers.
 */

#ifndef cmxObsCorrHicTrac_h
#define cmxObsCorrHicTrac_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxExportCore.h"
#include "cmxFilesystem.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObs.h"
#include "cmxObsCorrHicSnap.h"
#include "cmxSmartPtr.h"

#include <fstream>
#include <string>

//****************************************************************************
// cmxObsCorrHicTrac
//****************************************************************************

/**
 * Tracer observer - captures history at a regular, user-defined interval.
 * Depends on snapshot observer
 */
template <typename t_Glue>
class cmxObsCorrHicTrac : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Hi-C correlation snapshot observer type
     */
    typedef cmxObsCorrHicSnap<t_Glue> obs_corr_hic_snap_t;

    /**
     * Defaults
     */
    enum { default_period = 1, default_start_iter = 0 };

    /**
     * Default constructor
     */
    cmxObsCorrHicTrac() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read optional trace plot export path
        m_fpath = cmxExport::get_fpath(cmxOpt_obs_corr_hic_trac_export,
                                       config,
                                       "",               /*subdir*/
                                       "trace.corr.hic", /*file_id*/
                                       "csv");           /*ext*/
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_corr_hic_trac_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Read start iteration
        config->read_into(m_start_iter, cmxOpt_obs_corr_hic_trac_start_iter);
        // Extract snapshot observer - must occur before tracer observer
        if (!obs_t::find_raw(&mp_snap, sim, index)) {
            cmxLogf(
                "Error: Hi-C correlation tracer observer unable to find handle "
                "to snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
        // Allocate history buffers
        const size_t num_corrs = mp_snap->get_num_corrs();
        cmxAssertPosEq(num_corrs, sim.get_num_models());
        m_vals.resize(num_corrs);
    }

    /**
     * Reset to default state
     */
    void clear() {
        mp_snap = NULL;
        m_fpath.clear();
        m_period = default_period;
        m_start_iter = default_start_iter;
        m_vals.clear();
        m_iters.clear();
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, records history of
     * current model state for tracked parameter
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if ((iter < m_start_iter) || ((iter % m_period) != CMX_TO_UINT(0))) {
            // Early out if not yet at sampling interval
            return;
        }
        // Store latest snapshot
        cmxAssert(mp_snap);
        const std::vector<cmxReal>& corrs = mp_snap->get_corrs();
        const size_t num_corrs = corrs.size();
        cmxAssertPosEq(num_corrs, m_vals.size());
        m_iters.push_back(iter);
        for (size_t i = 0; i < num_corrs; ++i) {
            m_vals[i].push_back(corrs[i]);
            cmxAssertPosEq(m_iters.size(), m_vals[i].size());
        }
    }

    /**
     * Callback for when simulation has finished running, writes trace history
     * to disk
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        if (m_fpath.empty() || m_iters.empty() || m_vals.empty()) {
            // Early out if user did not specify file path or no data present
            return;
        }
        cmxLogf("Exporting Hi-C correlation trace: %s\n", m_fpath.c_str());
        // Make sure output directory exists
        cmxFs_create_parent_dirs(m_fpath);
        std::ofstream ofs(m_fpath.c_str());
        if (ofs.good()) {
            // Write CSV format with first column being iteration number and
            // each chain occupying a separate column

            // Write CSV header
            const size_t NUM_CHAINS = m_vals.size();
            ofs << "ITER";
            for (size_t i = 0; i < NUM_CHAINS; ++i) {
                ofs << ",CHAIN" << i;
            }
            ofs << "\n";

            // Write content
            const size_t NUM_ROWS = m_iters.size();
            for (size_t i = 0; i < NUM_ROWS; ++i) {
                ofs << m_iters[i];
                for (size_t j = 0; j < NUM_CHAINS; ++j) {
                    cmxAssertBounds(i, 0, m_vals[j].size());
                    ofs << "," << m_vals[j][i];
                }
                ofs << "\n";
            }
        } else {
            // Inform user that export path is invalid
            cmxLogf(
                "Warning: Hi-C correlation trace unable to export to path: "
                "%s\n",
                m_fpath.c_str());
        }
        ofs.close();
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_corr_hic_trac)

private:
    /**
     * Handle to snapshot observer - raw pointer is used as lifetime of
     * pointer does not exceed that of parent simulation's shared_ptr
     */
    const obs_corr_hic_snap_t* mp_snap;

    /**
     * Trace plot export path
     */
    std::string m_fpath;

    /**
     * History is recorded on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Iteration at which to begin recording history
     */
    cmxUInt m_start_iter;

    /**
     * History buffers for each simulation model
     */
    std::vector<std::vector<cmxReal> > m_vals;

    /**
     * Track which iterations were recorded, all vectors should be parallel
     * in size to 'm_vals'
     */
    std::vector<cmxUInt> m_iters;
};

#endif  // cmxObsCorrHicTrac_h
