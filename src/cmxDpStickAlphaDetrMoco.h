//****************************************************************************
// cmxDpStickAlphaDetrMoco
//****************************************************************************

/**
 * Deterministic (user defined) alpha concentration parameter (> 0) for
 * generalized Dirichlet stick-breaking process (truncated) approximation to
 * infinite Dirichlet process.
 *
 * From:
 *
 * Murugiah, Siva, and Trevor Sweeting. "Selecting the precision parameter
 *  prior in Dirichlet process mixture models." Journal of Statistical
 *  Planning and Inference 142, no. 7 (2012): 1947-1959.
 *
 * which cites:
 *
 * Antoniak, Charles E. "Mixtures of Dirichlet processes with applications to
 *  Bayesian nonparametric problems." The annals of statistics (1974):
 *  1152-1174.
 *
 * These sources state:
 *
 *  For concentration alpha and pseudo-population of size M, the expected
 *  number of clusters K is:
 *
 *  E(K|M,alpha) (approx)= alpha * ln{(M+alpha)/alpha}
 *
 * Therefore, the expected number of clusters is monotonic relative to alpha.
 *
 * See also:
 *
 *  Sudderth, Erik Blaine. "Graphical models for visual object recognition
 *      and tracking." PhD diss., Massachusetts Institute of Technology, 2006.
 *
 * which states: K -> alpha * log{M} as M -> infinity (Ch.2, pg. 108)
 *
 * According to MCMC approximation for Dirichlet process:
 *
 *  Ishwaran, Hemant, and Mahmoud Zarepour. "Markov chain Monte Carlo in
 *      approximate Dirichlet and beta two-parameter process hierarchical
 *      models." Biometrika 87, no. 2 (2000): 371-390.
 *
 *  A stick-breaking truncation of size N = 50 is sufficient for simulating
 *  alphas in range (0, 10] (pg. 385 and Fig. 5).
 */

#ifndef cmxDpStickAlphaDetrMoco_h
#define cmxDpStickAlphaDetrMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxScalarDetrMoco.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxDpStickAlphaDetrMoco
//****************************************************************************

/**
 * Simulation-level read-only shared data, provides user configured alpha
 * which is a positive scalar that is monotonic to the expected number of
 * clusters.
 */
class cmxDpStickAlphaDetrSharcoRo {
public:
    /**
     * Default constructor
     */
    cmxDpStickAlphaDetrSharcoRo() : m_alpha(CMX_DEFAULT_DP_STICK_ALPHA) {}

    /**
     * Initializes shared resource(s) - captures user-supplied alpha
     * @param config - user options
     */
    void init(cmxSpConstConfig_t config) {
        // Reset state
        this->clear();
        cmxAssert(m_alpha == CMX_DEFAULT_DP_STICK_ALPHA);
        // Read user configuration
        config->read_into(m_alpha, cmxOpt_Dp_stick_alpha);
        // Error if non-positive alpha encountered
        if (m_alpha <= CMX_TO_REAL(0.0)) {
            cmxLogf("Error: Dp stick alpha must be > 0! Exiting.\n");
            exit(cmxExitCode_invalid_dp_stick_alpha);
        }
    }

    /**
     * Reset read-only state
     */
    void clear() { m_alpha = CMX_DEFAULT_DP_STICK_ALPHA; }

    /**
     * @return user configured alpha concentration parameter
     */
    inline cmxReal get_scalar() const {
        cmxAssert(m_alpha > CMX_TO_REAL(0.0));
        return m_alpha;
    }

private:
    /**
     * Positive concentration parameter, monotonic with expected number of
     * clusters. Recommended range (0, 10] according to Ishwaran paper
     */
    cmxReal m_alpha;
};

/**
 * Utility for querying global read-only Dirichlet process alpha
 *  concentration parameter
 */
template <typename t_Glue>
class cmxDpStickAlphaDetrSharedQuery {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);
    /**
     * @param sim - outer simulation
     * @return real-valued scalar hyper-parameter
     */
    static inline cmxReal get_scalar(const sim_t& sim) {
        const cmxReal alpha =
            sim.get_shared_ro().get_Dp_stick_alpha().get_scalar();
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        return alpha;
    }
};

/**
 * Root-level deterministic scalar node representing alpha concentration
 * hyper-parameter for (generalized) Dirichlet stick-breaking process which
 * is monotonic to expected to expected number of clusters.
 */
template <typename t_Glue>
class cmxDpStickAlphaDetrMoco
    : public cmxScalarDetrMoco<t_Glue,
                               cmxDpStickAlphaDetrSharedQuery<t_Glue> > {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);
};

#endif  // cmxDpStickAlphaDetrMoco_h
