//****************************************************************************
// cmxConfig.cpp
//
// Implementation for reading and storing user configuration options
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxCmdOptsMap.h"  // for parsing command line
#include "cmxConfigFile.h"  // for parsing INI files
#include "cmxLogf.h"
#include "cmxOpts.h"
#include "cmxStringUtils.h"

#include <limits>
#include <stdio.h>

//****************************************************************************
// Anonymous namespace utilities
//****************************************************************************

namespace {
// REFERENCE CODE FOR WORKING WITH VECTOR INI SETTINGS
#if 0
        /**
         * Utility to assign a value at a parameter index even if index exceeds
         * the current array bounds. If this is the case, then vector will be
         * resized so that the operation becomes legal.
         *
         * @param vec - vector which will have element at index overwritten by val
         * @param index - 0-based index to assign val to
         * @param val - value to assign to vec[index]
         */
        void assign_at(std::vector<unsigned int> &vec,
                       const size_t index,
                       const unsigned int val)
        {
            if (vec.size() <= index)
            {
                vec.resize(index + 1, 0);
            }
            vec[index] = val;
        }
#endif  // 0

/**
 * Utility to merge command line options. If should_clear is cmxTRUE, then
 * out_opts = in_opts (out_opts will be exact copy of in_opts). Else, if
 * should_clear is cmxFALSE, then all existing in_opts keys override any
 * corresponding keys in out_opts; however keys unique to out_opts will
 * retain their values.
 *
 * @param out_opts - user options to override
 * @param in_opts - options provider to override out_opts with
 * @param should_clear - If cmxTRUE, then out_opts = in_opts. Else, out_opts
 *  will be merged with in_opts with in_opts values overriding any common
 *  keys.
 */
void override_config(cmxCmdOptsMap& out_opts,
                     const cmxCmdOptsMap& in_opts,
                     const cmxBool should_clear) {
    if (should_clear) {
        // Completely overwrite
        out_opts = in_opts;
    } else {
        // Override common keys
        std::vector<std::string> keys;
        std::string key;
        std::string val;
        in_opts.get_keys(keys);
        for (std::vector<std::string>::iterator it = keys.begin();
             it != keys.end();
             it++) {
            key = *it;
            cmxVerify(in_opts.read_into(val, key));
            out_opts.set_option(key, val);
        }
    }
}

/**
 * Determines if archetype exists in INI file or command-line (optional) - if
 *  so, then loads archetype INI file. If command-line is valid and specifies
 *  archetype, it will take priority over INI file archetype
 * @param arch - Output archetype configuration file (WILL ALWAYS BE CLEARED!)
 * @param ini - Already initialized configuration file (MUST NOT BE SAME
 *  MEMORY LOCATION AS 'arch'!)
 * @param cmd - Optional command-line options
 * @return TRUE if archetype found and loaded, FALSE o/w
 */
cmxBool load_arch(cmxConfigFile& arch,
                  const cmxConfigFile& ini,
                  const cmxCmdOptsMap* const cmd = NULL) {
    // Assert addresses are unique
    cmxAssert(((void*)&arch) != ((void*)&ini));
    // Check if archetype path is specified, defer to command-line if present
    std::string arch_path;
    // Note, "short-circuit evaluation" applies here: if option is present on
    // command-line, then the INI path resolution will not be called
    // https://en.wikipedia.org/wiki/Short-circuit_evaluation
    const cmxBool result =
        (cmd &&
         cmd->resolve_path(arch_path, cmxOpt_get_cmd_switch(cmxOpt_arch))) ||
        ini.resolve_path(arch_path, cmxOpt_get_ini_key(cmxOpt_arch));
    if (result && !arch_path.empty()) {
        // Archetype path found!
        arch = cmxConfigFile(arch_path);
        return cmxTRUE;
    }
    // Archetype path not found
    arch = cmxConfigFile();
    return cmxFALSE;
}

}  // namespace

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Clears configuration options
 */
void cmxConfig::clear() {
    this->m_cmd_opts = cmxCmdOptsMap();
    this->m_cfg_file = cmxConfigFile();
    this->m_cfg_arch = cmxConfigFile();
    this->m_parent.reset();
    this->m_child.reset();
}

/**
 * Initializes from command line
 *
 * @param cmd_opts - command line options
 * @param should_clear - If TRUE, erases existing state in this object
 */
void cmxConfig::init(const cmxCmdOptsMap& cmd_opts,
                     const cmxBool should_clear) {
    // Reset configuration
    if (should_clear) {
        this->clear();
    }

    // Stores complete file path to INI file
    std::string ini_fpath;

    // Parse parameter INI file if specified,
    if (cmd_opts.read_into(ini_fpath, cmxOpt_get_cmd_switch(cmxOpt_conf))) {
        m_cfg_file = cmxConfigFile(ini_fpath);
        load_arch(m_cfg_arch, m_cfg_file, &cmd_opts);
    }

    // Store command line options
    override_config(m_cmd_opts, cmd_opts, should_clear);

    // See if a child config needs to be initialized
    conditional_init_child();
}

/**
 * Overwrite child configuration
 * @param child - child configuration
 */
void cmxConfig::set_child(cmxSpConfig_t child) {
    m_child = child;
    child->m_parent = shared_from_this();
}

/**
 * Checks if child configuration should be initialized
 */
void cmxConfig::conditional_init_child() {
    std::string child_ini_fpath;
    // Make sure to avoid checking the parent for a child config - else if
    // parent contains a child, we will infinitely recurse!
    if (this->resolve_path(
            child_ini_fpath, cmxOpt_conf_child, cmxFALSE /*check_parent*/)) {
        m_child = cmxSmartPtr::make_shared<cmxConfig>();
        m_child->init(shared_from_this(), cmxConfigFile(child_ini_fpath));
    }
}

/**
 * Actual initialization for a child configuration
 * @param parent - The parent configuration
 * @param cfg_file - Child INI files
 * Note: child command line options are extracted from parent command line
 */
void cmxConfig::init(cmxSpConstConfig_t parent, const cmxConfigFile& cfg_file) {
    clear();
    m_parent = parent;
    // Initialize CMD options
    parent->m_cmd_opts.get_child_cmd(m_cmd_opts);
    // Initialize INI options
    m_cfg_file = cfg_file;
    load_arch(m_cfg_arch, m_cfg_file, &m_cmd_opts);
    // Initialize child configuration if present
    conditional_init_child();
}

/**
 * Outputs configuration options to <stdout>
 */
void cmxConfig::print() const { this->print(0); }

/**
 * Utility to print configuration <key,val> pairs
 */
template <typename t_opts>
static void print_keys(const t_opts& opts) {
    std::vector<std::string> keys;
    opts.get_keys(keys);
    std::string key;
    std::string val;
    for (std::vector<std::string>::iterator it = keys.begin(); it != keys.end();
         it++) {
        key = *it;
        cmxVerify(opts.read_into(val, key));
        cmxLogf("\t%s: %s\n", key.c_str(), val.c_str());
    }
}

/**
 * Internal print adjusted for child level
 */
void cmxConfig::print(const cmxUInt level) const {
    // Keyed options: command line
    cmxLogf("===============CMD OPTS (%u)===============\n", level);
    print_keys(m_cmd_opts);

    // Keyed options: INI config
    cmxLogf("---------------INI OPTS (%u)---------------\n", level);
    print_keys(m_cfg_file);

    // Keyed options: INI archetype
    if (!m_cfg_arch.empty()) {
        cmxLogf("---------------INI ARCH OPTS (%u)---------------\n", level);
        print_keys(m_cfg_arch);
    }

    // Print child
    if (m_child) {
        m_child->print(level + 1);
    }
}

/**
 * @WARNING - DOES NOT PERFORM PATH RESOLUTION!
 * @out_val - Searches for first parameter value mapped to a matching key
 *  in 'ixs' array where all command line matches have precedence over INI
 * @param ixs - Set of option keys to search for mapped value, the key
 *  priority is as follows: All command line variants have higher priority
 *  than INI, then earlier indices within array have priority over later
 * @param check_parent - If true, then parent configuration (if valid) will
 *  be recursively checked for the set of option keys
 * @return index of option read or ixs.size() if not found
 */
size_t cmxConfig::read_into(std::string& out_val,
                            const std::vector<cmxOptE>& ixs,
                            const cmxBool check_parent) const {
    // Flag no path path resolution
    std::vector<cmxBool> should_resolve(ixs.size(), cmxFALSE);
    // Defer to common routine
    return this->read_into(out_val, ixs, should_resolve, check_parent);
}

/**
 * PERFORMS PATH RESOLUTION!
 * @out_val - Searches for first parameter value mapped to a matching key
 *  in 'ixs' array where all command line matches have precedence over INI
 * @param ixs - Set of option keys to search for mapped value, the key
 *  priority is as follows: All command line variants have higher priority
 *  than INI, then earlier indices within array have priority over later
 * @param should_resolve - Parallel vector of same size as 'ixs' with i-th
 *  element TRUE if option is a path that must be resolved, FALSE o/w
 * @param check_parent - If true, then parent configuration (if valid) will
 *  be recursively checked for the set of option keys
 * @return index of option read or ixs.size() if not found
 */
size_t cmxConfig::read_into(std::string& out_val,
                            const std::vector<cmxOptE>& ixs,
                            const std::vector<cmxBool>& should_resolve,
                            const cmxBool check_parent) const {
    cmxAssert(should_resolve.size() == ixs.size());
    const size_t num_keys = ixs.size();
    // Check command line first
    for (size_t i = 0; i < num_keys; ++i) {
        const cmxOptE ix = ixs[i];
        if (should_resolve[i]) {
            if (m_cmd_opts.resolve_path(out_val, cmxOpt_get_cmd_switch(ix))) {
                return i;
            }
        } else {
            if (m_cmd_opts.read_into(out_val, cmxOpt_get_cmd_switch(ix))) {
                return i;
            }
        }
    }  // end command line check

    // Else check INI file
    for (size_t i = 0; i < num_keys; ++i) {
        const cmxOptE ix = ixs[i];
        if (should_resolve[i]) {
            if (m_cfg_file.resolve_path(out_val, cmxOpt_get_ini_key(ix))) {
                return i;
            } else if (m_cfg_arch.resolve_path(out_val,
                                               cmxOpt_get_ini_key(ix))) {
                return i;
            }
        } else {
            if (m_cfg_file.read_into(out_val, cmxOpt_get_ini_key(ix))) {
                return i;
            } else if (m_cfg_arch.read_into(out_val, cmxOpt_get_ini_key(ix))) {
                return i;
            }
        }
    }  // end INI check

    // Else check the parent configuration
    if (check_parent) {
        cmxSpConstConfig_t sptr_parent = m_parent.lock();
        if (sptr_parent) {
            return sptr_parent->read_into(
                out_val, ixs, should_resolve, cmxTRUE /*check_parent*/);
        }
    }

    // Not found, return ixs.size()
    return num_keys;
}

/**
 * Obtain path value resolved according to the rules (in order of precedence)
 *  - if not found, path argument is unmodified
 *  - if from command line, output path is unmodified
 *  - if from config file and absolute, output path is unmodified
 *  - if from config file and relative, output path is resolved relative
 *      to config file directory
 *
 * Assumes parse() has been called for internal command line and ini file
 * maps.
 *
 * @param path - Output path, unmodified if key not found
 * @param ix - Option identifier
 * @return TRUE if key exists, FALSE otherwise. If key exists and value
 *  is from command line, then output path argument is same as command
 *  line. However, if key exists and value is from configuration file,
 *  then relative paths are resolved to be relative to the configuration
 *  file directory. Note that command line values override any
 *  configuration file values.
 */
cmxBool cmxConfig::resolve_path(std::string& path,
                                const cmxOptE ix,
                                const cmxBool check_parent) const {
    // Check command line first
    if (m_cmd_opts.resolve_path(path, cmxOpt_get_cmd_switch(ix))) {
        return cmxTRUE;
    }
    // Else check INI file
    else if (m_cfg_file.resolve_path(path, cmxOpt_get_ini_key(ix))) {
        return cmxTRUE;
    }
    // Else check INI archetype
    else if (m_cfg_arch.resolve_path(path, cmxOpt_get_ini_key(ix))) {
        return cmxTRUE;
    }
    // Else check the parent configuration
    else if (check_parent) {
        cmxSpConstConfig_t sptr_parent = m_parent.lock();
        if (sptr_parent) {
            return sptr_parent->resolve_path(
                path, ix, cmxTRUE /*check_parent*/);
        }
    }

    // Key not found, return false
    return cmxFALSE;
}

/**
 * Resolve path for vectorized options
 * @param path - Output path obtained using resolve_path() semantics
 * @param ix - Option identifier
 * @param off - The option offset
 * @return TRUE if key exists, FALSE o/w
 */
cmxBool cmxConfig::resolve_path_vec(std::string& path,
                                    const cmxOptE ix,
                                    const cmxUInt off,
                                    const cmxBool check_parent) const {
    // Check command line first
    const std::string suffix(std::string("_") + cmx2Str(off));
    if (m_cmd_opts.resolve_path(path, cmxOpt_get_cmd_switch(ix) + suffix)) {
        return cmxTRUE;
    }
    // Else check INI file
    /* else */
    if (m_cfg_file.resolve_path(path, cmxOpt_get_ini_key(ix) + suffix)) {
        return cmxTRUE;
    }
    // Else check INI archetype
    /* else */
    if (m_cfg_arch.resolve_path(path, cmxOpt_get_ini_key(ix) + suffix)) {
        return cmxTRUE;
    }
    // Else check the parent configuration
    /* else */
    if (check_parent) {
        cmxSpConstConfig_t sptr_parent = m_parent.lock();
        if (sptr_parent) {
            return sptr_parent->resolve_path_vec(
                path, ix, off, cmxTRUE /*check_parent*/);
        }
    }

    // Key not found, return false
    return cmxFALSE;
}
