//****************************************************************************
// cmxObsLogscoreSnap.h
//****************************************************************************

/**
 * @brief Snapshot observer for model log-score, where the log-score is
 *  the log of the possibly unnormalized posterior density. Snapshot observers
 *  compute the value of some statistic according to their sampling frequency
 *  and should only store the most recent value. Other observer types such
 *  as Cons ('Console') and Trac ('Tracer') can use the snapshot value to log
 *  to stdout and generate trace plots respectively.
 */

#ifndef cmxObsLogscoreSnap_h
#define cmxObsLogscoreSnap_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxCaptLogscoreParallel.h"
#include "cmxGlue.h"
#include "cmxObs.h"
#include "cmxThread.h"

//****************************************************************************
// cmxObsLogscoreSnap
//****************************************************************************

/**
 * Log-score snapshot observer - stores latest snapshot of log-score at each
 * model. The snapshot is updated at a regular, user-defined interval.
 */
template <typename t_Glue, typename t_Capt = cmxCaptLogscoreParallel<t_Glue> >
class cmxObsLogscoreSnap : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Batch capture utility type
     */
    typedef t_Capt capt_t;

    /**
     * Default snapshot period
     */
    enum { default_period = 1 };

    /**
     * Default constructor
     */
    cmxObsLogscoreSnap() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_logscore_snap_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Allocate snapshots buffer
        cmxAssert(sim.get_num_models() > 0);
        m_logscores.resize(sim.get_num_models(), CMX_TO_REAL(0.0));
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_period = default_period;
        m_logscores.clear();
    }

    /**
     * @return TRUE if update is scheduled for parameter iteration, FALSE o/w
     */
    inline cmxBool should_update(const cmxUInt iter) const {
        return ((iter % m_period) == CMX_TO_UINT(0));
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, captures posterior
     * log-score for all models if iteration matches period
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if (!should_update(iter)) {
            // Early out if not yet at sampling interval
            return;
        }
        // Defer to batch capture utility
        capt_t::capture(m_logscores, sim CMX_MAIN_THREAD_ID_ARG);
    }

    /**
     * @return read-only view of model log-score snapshots
     */
    inline const std::vector<cmxReal>& get_logscores() const {
        return m_logscores;
    }

    /**
     * @return number of tracked log-scores
     */
    inline size_t get_num_logscores() const { return m_logscores.size(); }

    /**
     * Other [dependent] observers may be interested in period of snapshot
     *  observer
     * @return period of this observer
     */
    inline cmxUInt get_period() const { return m_period; }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_logscore_snap)

private:
    /**
     * Log-score snapshot is updated on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Snapshot log-scores at each model
     */
    std::vector<cmxReal> m_logscores;
};

#endif  // cmxObsLogscoreSnap_h
