//****************************************************************************
// cmxPseudopopSharcoMu.h
//****************************************************************************

/**
 * @brief Mutable (modifiable) shared data (buffers) for pseudo-population.
 */

#ifndef cmxPseudopopSharcoMu_h
#define cmxPseudopopSharcoMu_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxShufflerFisherYates.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxPseudopopSharcoMu
//****************************************************************************

/**
 * Pseudo-population mutable shared resources
 */
class cmxPseudopopSharcoMu {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     * @param sim - outer simulation
     */
    template <typename sim_t>
    void init(cmxSpConstConfig_t config, const sim_t& sim) {
        m_block_shuffler.init(cmxConfigUtils::get_pseudopop_size(config));
    }

    /**
     * Clear shared buffer(s)
     */
    void clear() { m_rowsums_buf.clear(); }

    /**
     * @return Mutable shuffler for block indices
     */
    inline cmxShufflerFisherYates<cmxUIVecCol>& get_block_shuffler() const {
        return m_block_shuffler;
    }

    /**
     * @return Mutable buffer for aggregating pseudo-population contacts
     */
    inline cmxUIVecCol& get_rowsums_buf() const { return m_rowsums_buf; }

private:
    /**
     * Buffer for storing randomly selected block indices
     */
    mutable cmxShufflerFisherYates<cmxUIVecCol> m_block_shuffler;

    /**
     * Buffer for storing aggregate row sums
     */
    mutable cmxUIVecCol m_rowsums_buf;
};

#endif  // cmxPseudopopSharcoMu_h
