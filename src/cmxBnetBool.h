//****************************************************************************
// cmxBnetBool.h
//****************************************************************************

/**
 * @brief Bayesian network for both generating samples and for evaluating the
 *  probability of a sample under restriction that all network nodes are
 *  boolean (0|1) random variables.
 */

#ifndef cmxBnetBool_h
#define cmxBnetBool_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetBoolCt.h"
#include "cmxBnetThinDag.h"
#include "cmxRand.h"
#include "cmxTypes.h"

#include <math.h>
#include <vector>

//****************************************************************************
// cmxBnetBool
//****************************************************************************

class cmxBnetBool {
private:
    /**
     * Visitor for generating a random sample from underlying Boolean network
     */
    class sample_visitor {
    public:
        /**
         * Constructor
         * @param rng_ - random number generator
         * @param out_bits_ - output bit vector, pre-sized
         */
        sample_visitor(cmxRand& rng_, cmxBool* const out_bits_)
            : rng(rng_), out_bits(out_bits_) {}
        /**
         * Visitor callback - generates random value at i_node index
         * @param i_node - index of node to generate value at
         * @param distrib - boolean distribution at i_node given its parent
         * state(s)
         */
        inline void operator()(const cmxUInt i_node,
                               const cmxBnetBoolCt::const_handle distrib) {
            // Randomly generate value for this node
            const cmxReal p_false = distrib(cmxBnetBoolCt::row_false);
            out_bits[i_node] = (p_false < rng.unif_real()) ? cmxFALSE : cmxTRUE;
        }

    private:
        cmxRand& rng;
        cmxBool* const out_bits;
    };

    /**
     * Visitor for determining log probability of a sample from underlying
     * Boolean network
     */
    class logprob_visitor {
    public:
        /**
         * Constructor
         * @param bits_ - bit vector, pre-sized
         */
        explicit logprob_visitor(const cmxBool* const bits_)
            : bits(bits_), logp(CMX_TO_REAL(0.0)) {}
        /**
         * Visitor callback - evaluates log probability at i_node
         * @param i_node - index of node to evaluate log probability at
         * @param distrib - boolean distribution at i_node given its parent
         * state(s)
         */
        inline void operator()(const cmxUInt i_node,
                               const cmxBnetBoolCt::const_handle distrib) {
            cmxAssert((bits[i_node] == cmxFALSE) || (bits[i_node] == cmxTRUE));
            const cmxReal p =
                distrib(static_cast<cmxBnetBoolCt::eRowIx>(bits[i_node]));
            logp += log(p);
        }
        /**
         * @return log-probability from graph traversal
         */
        inline cmxReal get_logp() const { return logp; }

    private:
        const cmxBool* const bits;
        cmxReal logp;
    };

public:
    /**
     * Generate a bit vector from underlying Boolean network
     * @param rng - random number generator
     * @param out_bits - output bit vector, assumed to be properly sized
     */
    void sample(cmxRand& rng, cmxBool* const out_bits) const {
        sample_visitor visitor(rng, out_bits);
        traverse(visitor, out_bits);
    }

    /**
     * Compute log-probability of parameter bit vector
     * @param bits - properly sized bit vector
     * @return log probability of bit vector given underlying Bayesian network
     */
    cmxReal logprob(const cmxBool* const bits) const {
        logprob_visitor visitor(bits);
        traverse(visitor, bits);
        return visitor.get_logp();
    }

    // [Mutable] accessors

    /**
     * @return [Const] DAG - directed acyclic graph, edge representation
     */
    inline cmxBnetThinDag& get_dag() { return m_dag; }
    inline const cmxBnetThinDag& get_dag() const { return m_dag; }

    /**
     * @return [Const] buffer containing a topological sort of the Bayesian
     *  network nodes
     */
    inline std::vector<cmxUInt>& get_order() { return m_order; }
    inline const std::vector<cmxUInt>& get_order() const { return m_order; }

    /**
     * @return [Const] conditional probability table at each node
     */
    inline std::vector<cmxBnetBoolCt>& get_cpts() { return m_cpts; }
    inline const std::vector<cmxBnetBoolCt> get_cpts() const { return m_cpts; }

    /**
     * Utility to check elements form a probability mass function!
     */
    static cmxBool check_is_pmf(const cmxReal prob_false,
                                const cmxReal prob_true) {
        cmxAssertBounds(prob_false, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        cmxAssertBounds(prob_true, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        cmxAssertRealEq(prob_false + prob_true, CMX_TO_REAL(1.0));
        return cmxTRUE;
    }

private:
    /**
     * Visits each node in the boolean network according to parent states in bit
     * vector, calls vistor(...) on each visited node
     * @param visitor - visitor callback for each visited node
     * @param bits - defines which nodes to visit
     */
    template <typename t_visitor>
    void traverse(t_visitor& visitor, const cmxBool* const bits) const {
        cmxAssert(bits);
        cmxAssertPosEq(m_dag.get_v_num(), CMX_TO_UINT(m_order.size()));
        cmxAssertPosEq(m_dag.get_v_num(), CMX_TO_UINT(m_cpts.size()));
        cmxAssert(this->internal_check_order());
        // Number of nodes in network
        const cmxUInt num_v = m_dag.get_v_num();
        // Mapping from child to its parent nodes
        const cmxBnetDagAdjMap_t& dst2src = m_dag.get_dst2src_map();
        cmxAssertPosEq(num_v, CMX_TO_UINT(dst2src.size()));
        // Buffer for storing values at a node's parents
        std::vector<cmxBool> parents;
        for (cmxUInt itr_v = 0; itr_v < num_v; ++itr_v) {
            // Obtain identifier of node to sample
            cmxAssertBounds(itr_v, 0, CMX_TO_UINT(m_order.size()));
            const cmxUInt i_node = m_order[itr_v];
            // Obtain node's list of parent identifiers
            cmxAssertBounds(i_node, 0, CMX_TO_UINT(dst2src.size()));
            const std::vector<cmxUInt>& i_parents = dst2src[i_node];
            // Extract parent states
            const cmxUInt num_parents = CMX_TO_UINT(i_parents.size());
            cmxAssertBounds(num_parents, 0, num_v);
            parents.resize(num_parents);
            for (cmxUInt itr_par = 0; itr_par < num_parents; ++itr_par) {
                const cmxUInt i_parent = i_parents[itr_par];
                cmxAssertBounds(i_parent, 0, num_v);
                cmxAssert(i_parent != i_node);
                parents[itr_par] = bits[i_parent];
                cmxAssert((parents[itr_par] == cmxFALSE) ||
                          (parents[itr_par] == cmxTRUE));
            }
            // Obtain node's CPT
            cmxAssertBounds(i_node, 0, num_v);
            cmxAssertBounds(i_node, 0, CMX_TO_UINT(m_cpts.size()));
            const cmxBnetBoolCt& cpt = m_cpts[i_node];
            // Obtain conditional distribution of node given parent states
            cmxBnetBoolCt::const_handle distrib = cpt(parents);
            cmxAssert(check_is_pmf(distrib(cmxBnetBoolCt::row_false),
                                   distrib(cmxBnetBoolCt::row_true)));
            // Visitor callback
            visitor(i_node, distrib);
        }
    }

#ifdef CMX_BUILD_ENABLE_ASSERT
    /**
     * @return TRUE if cached order is valid, FALSE o/w
     */
    cmxBool internal_check_order() const {
#   ifdef CMX_BUILD_ENABLE_BNET_CHECK_ORDER
        std::vector<cmxUInt> test_order;
        std::vector<cmxUInt> buff_a;
        std::vector<cmxUInt> buff_b;
        m_dag.topo_sort(test_order, buff_a, buff_b);
        return test_order == m_order;
#   else
        return cmxTRUE;
#   endif  // CMX_BUILD_ENABLE_BNET_CHECK_ORDER
    }
#endif  // CMX_BUILD_ENABLE_ASSERT

    /**
     * DAG = directed acyclic graph, edge representation
     */
    cmxBnetThinDag m_dag;

    /**
     * Buffer containing a topological sort of the Bayesian network nodes
     */
    std::vector<cmxUInt> m_order;

    /**
     * Conditional probability table at each node
     */
    std::vector<cmxBnetBoolCt> m_cpts;
};

#endif  // cmxBnetBool_h
