//****************************************************************************
// cmxCaptMixEntSerial.h
//****************************************************************************

/**
 * @brief Serial (non-parallel) batch capture of model mixture entropies
 */

#ifndef cmxCaptMixEntSerial_h
#define cmxCaptMixEntSerial_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxCaptMixEntSerial
//****************************************************************************

/**
 * Performs batched capture of model mixture entropies (serial version)
 */
template <typename t_Glue>
class cmxCaptMixEntSerial {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Capture sorted mixture entropies at each simulated model
     * @param out - vector of output mixture entropies, will correspond to
     *  each simulated model
     * @param sim - Parent simulation
     */
    static void capture(cmxVecCol& out, const sim_t& sim CMX_THREAD_ID_PARAM) {
        const std::vector<model_t>& models = sim.get_models();
        const size_t NUM_MODELS = models.size();
        out.set_size(NUM_MODELS);
        cmxAssertPosEq(models.size(), out.size());
        // Iterate over models
        for (size_t i_model = 0; i_model < NUM_MODELS; ++i_model) {
            const model_t& model = models[i_model];
            out.at(CMX_TO_MAT_SZ_T(i_model)) =
                model.get_Dp_stick().get_mix_ent();
        }  // End iteration over models
    }
};

#endif  // cmxCaptMixEntSerial_h
