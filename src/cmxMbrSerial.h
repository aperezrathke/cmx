//****************************************************************************
// cmxMbrSerial.h
//****************************************************************************

/**
 * @brief Serial (non-parallel) model batch runner - performs batched Gibbs
 *  MCMC updating for a set of models. Mbr -> *M*odel *b*atch *r*unner
 */

#ifndef cmxMbrSerial_h
#define cmxMbrSerial_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxGlue.h"
#include "cmxMcmcGibbsRev.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxMbrSerial
//****************************************************************************

/**
 * Performs batched Gibbs MCMC updating for set of models (serial version)
 */
template <typename t_Glue>
class cmxMbrSerial {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Performs Gibbs sampling MCMC for parameter set of models
     * @param num_steps - Number of Gibbs update steps to run for each model
     * @param models - Set of models to be Gibbs updated
     * @param sim - Outer simulation
     */
    static void run(const cmxUInt num_steps,
                    std::vector<model_t>& models,
                    sim_t& sim CMX_THREAD_ID_PARAM) {
        const size_t num_models = models.size();
        for (size_t i = 0; i < num_models; ++i) {
            model_t& model = models[i];
            cmxMcmcGibbsRev::run(num_steps, model, sim CMX_THREAD_ID_ARG);
        }
    }
};

#endif  // cmxMbrSerial_h
