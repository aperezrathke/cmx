//****************************************************************************
// cmxSim.inl
//****************************************************************************

// Not meant to be included more than once
#ifdef cmxSim_inl
#error "Cmx Sim implementation included multiple times!"
#endif  // cmxSim_inl
#define cmxSim_inl

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxAssert.h"
#include "cmxConfigUtils.h"
#include "cmxControllerFactory.h"
#include "cmxObsBuilder.h"

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Initialize from user configuration
 * @param config - user configuration
 */
template <typename t_Glue>
void cmxSim<t_Glue>::init(cmxSpConstConfig_t config) {
    this->clear();
    m_config = config;
    // Initialize shared data common to all model chains
    m_shared_ro.init(config);
    CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM(m_shared_mu, init, config, *this);
    // Initialize models
    const size_t num_models = (size_t)cmxConfigUtils::get_num_models(config);
    cmxAssert(num_models > 0);
    m_models.resize(num_models);
    for (size_t i = 0; i < num_models; ++i) {
        model_t& model = m_models[i];
        model.init(config, *this CMX_MAIN_THREAD_ID_ARG);
    }
    // Initialize observers and reactors
    cmxObsBuilder<t_Glue>::init(config, *this);
    // Create controller
    mp_controller = cmxControllerFactory<t_Glue>::create(config);
    // https://stackoverflow.com/questions/7580009/gcc-error-cannot-convert-const-shared-ptr-to-bool-in-return
    cmxAssert(static_cast<bool>(mp_controller));
    // Initialize controller
    mp_controller->init(config, *this);
}

/**
 * Randomize all model chains - can only be called after init. Currently,
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * @param config - user configuration
 */
template <typename t_Glue>
void cmxSim<t_Glue>::randomize(cmxSpConstConfig_t config) {
    // @TODO - if performance is an issue, consider parallel implementation
    const size_t num_mods = m_models.size();
    for (size_t i = 0; i < num_mods; ++i) {
        model_t& model = m_models[i];
        model.randomize(config, *this CMX_MAIN_THREAD_ID_ARG);
    }
}

/**
 * Reset to default state
 */
template <typename t_Glue>
void cmxSim<t_Glue>::clear() {
    m_config.reset();
    m_shared_ro.clear();
    m_shared_mu.clear();
    m_models.clear();
    mp_controller.reset();
    m_observers.clear();
    m_reactors.clear();
}

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Runs entire simulation to completion.
 * Assumes simulation has been initialized.
 * @return cmxTRUE if simulation successful, cmxFALSE o/w
 */
template <typename t_Glue>
cmxBool cmxSim<t_Glue>::run() {
    // Defer to controller
    cmxAssert(static_cast<bool>(mp_controller));
    return mp_controller->run(*this);
}

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Notifies 'active' observers that iteration 'iter' has completed, note that
 * 'passive' observers (aka'reactors') are *not* notified!
 * @param iter - 0-based iteration counter that has completed
 */
template <typename t_Glue>
void cmxSim<t_Glue>::observers_update(const cmxUInt iter) {
    const size_t num_obs = m_observers.size();
    for (size_t i = 0; i < num_obs; ++i) {
        cmxAssert(static_cast<bool>(m_observers[i]));
        m_observers[i]->update(iter, *this);
    }
}

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Marks active observers 'stale' and then updates
 * @param iter - 0-based iteration counter that has completed
 */
template <typename t_Glue>
void cmxSim<t_Glue>::observers_force_update(const cmxUInt iter) {
    this->observers_mark_stale();
    this->observers_update(iter);
}

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Notifies 'active' observers that they should set their current state
 *  as stale without clearing history
 */
template <typename t_Glue>
void cmxSim<t_Glue>::observers_mark_stale() {
    const size_t num_obs = m_observers.size();
    for (size_t i = 0; i < num_obs; ++i) {
        cmxAssert(static_cast<bool>(m_observers[i]));
        m_observers[i]->mark_stale();
    }
}

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Notifies *both* 'active' and 'passive' observers (aka 'reactors') that
 * simulation has finished
 */
template <typename t_Glue>
void cmxSim<t_Glue>::observers_finish_all() {
    // Finish observers
    const size_t num_obs = m_observers.size();
    for (size_t i = 0; i < num_obs; ++i) {
        cmxAssert(static_cast<bool>(m_observers[i]));
        m_observers[i]->finish(*this);
    }
    // Finish reactors
    const size_t num_react = m_reactors.size();
    for (size_t i = 0; i < num_react; ++i) {
        cmxAssert(static_cast<bool>(m_reactors[i]));
        m_reactors[i]->finish(*this);
    }
}
