//****************************************************************************
// cmxObsMixHistCons.h
//****************************************************************************

/**
 * @brief Console histogram observer for model mixture weights
 */

#ifndef cmxObsMixHistCons_h
#define cmxObsMixHistCons_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxCaptMixPmfTypes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObsMixPmfSnap.h"
#include "cmxOpts.h"

#include <string>
#include <vector>

//****************************************************************************
// cmxObsMixHistCons
//****************************************************************************

/**
 * Mixture weights histogram console observer - reports mixture weights to
 *  stdout as a histogram at a regular, user-defined interval. Depends on
 *  snapshot observer
 */
template <typename t_Glue>
class cmxObsMixHistCons : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Snapshot observer type
     */
    typedef cmxObsMixPmfSnap<t_Glue> obs_mix_pmf_snap_t;

    /**
     * Default console period
     */
    enum { default_period = 1 };

    /**
     * Number of '+' to represent histogram, currently not configurable
     */
    enum { max_crosses = 50 };

    /**
     * Default constructor
     */
    cmxObsMixHistCons() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_mix_hist_cons_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Extract snapshot observer - must occur before console observer
        if (!obs_t::find_raw(&mp_snap, sim, index)) {
            cmxLogf(
                "Error: mixture histogram console observer unable to find "
                "handle to snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_period = default_period;
        mp_snap = NULL;
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, reports to stdout
     * if iteration matches period
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if ((iter % m_period) != CMX_TO_UINT(0)) {
            // Early out if not yet at reporting interval
            return;
        }
        // Iterate over model mixture PMFs
        cmxAssert(mp_snap);
        const std::vector<cmxMixPmfPqueue_t>& mix_pmfs =
            mp_snap->get_mix_pmfs();
        const size_t NUM_MODELS = mix_pmfs.size();
        cmxAssertPosEq(NUM_MODELS, sim.get_num_models());
        for (size_t i_model = 0; i_model < NUM_MODELS; ++i_model) {
            cmxMixPmfPqueue_t pqueue = mix_pmfs[i_model];
            // Print PMF to stdout
            cmxLogf("(ITER %d, MODEL %d) MIX PMF =\n", (int)iter, (int)i_model);
            cmxAssert(!pqueue.empty());
            while (!pqueue.empty()) {
                const cmxMixPmfInfo_t nfo = pqueue.top();
                const cmxReal p = nfo.m_weight;
                const cmxReal n_crossesf = CMX_TO_REAL(max_crosses) * p;
                const size_t n_crosses = static_cast<size_t>(n_crossesf);
                if (n_crosses < 1) {
                    // Avoid printing empty mixture components
                    break;
                }
                const std::string crosses(n_crosses, '+');
                cmxLogf("\t%4d: %s %.4f\n",
                        ((int)nfo.m_index),
                        crosses.c_str(),
                        ((double)p));
                pqueue.pop();
            }
        }
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_mix_hist_cons)

private:
    /**
     * Reports to stdout on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Handle to snapshot observer - raw pointer is used as lifetime
     * of pointer does not exceed that of parent simulation's shared_ptr
     */
    const obs_mix_pmf_snap_t* mp_snap;
};

#endif  // cmxObsClustHistCons_h
