//****************************************************************************
// cmxBnetMpopSharcoRo
//****************************************************************************

/**
 * @brief Read-only shared data for multi-population Bayesian networks
 */

#ifndef cmxBnetMpopSharcoRo_h
#define cmxBnetMpopSharcoRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetMpopSharcoRo
//****************************************************************************

/**
 * Stores read-only shared data for multi-population Bayesian networks
 */
class cmxBnetMpopSharcoRo {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     */
    void init(const cmxSpConstConfig_t config) {
        this->clear();
        // Configure block size
        m_bnet_mpop_block_size =
            cmxConfigUtils::get_Bnet_mpop_block_size(config);
        cmxAssertBoundsInc(m_bnet_mpop_block_size,
                           CMX_TO_UINT(1),
                           cmxConfigUtils::get_mix_size(config));
        // Configure block proposal blend
        config->read_into(m_bnet_mpop_block_weighted_blend,
                          cmxOpt_Bnet_mpop_block_weighted_blend);
        // Clip blend to [0,1]
        m_bnet_mpop_block_weighted_blend =
            CMX_CLIP(m_bnet_mpop_block_weighted_blend,
                     CMX_TO_REAL(0.0),
                     CMX_TO_REAL(1.0));
    }

    /**
     * Reset read-only state
     */
    void clear() {
        m_bnet_mpop_block_size = CMX_DEFAULT_BNET_MPOP_BLOCK_SIZE;
        m_bnet_mpop_block_weighted_blend =
            CMX_DEFAULT_BNET_MPOP_BLOCK_WEIGHTED_BLEND;
    }

    // Accessors

    /**
     * @return The fixed size of the random subpopulation of each MCMC proposal
     */
    inline cmxUInt get_bnet_mpop_block_size() const {
        cmxAssert(m_bnet_mpop_block_size > CMX_TO_UINT(0));
        return m_bnet_mpop_block_size;
    }

    /**
     * @return Linear blend factor between uniform and weighted selection for
     *  MCMC proposals
     */
    inline cmxReal get_bnet_mpop_block_weighted_blend() const {
        cmxAssertRealBoundsInc(m_bnet_mpop_block_weighted_blend,
                               CMX_TO_REAL(0.0),
                               CMX_TO_REAL(1.0));
        return m_bnet_mpop_block_weighted_blend;
    }

private:
    /**
     * The fixed size of the random subpopulation of each MCMC proposal
     */
    cmxUInt m_bnet_mpop_block_size;

    /**
     * Percentage of multi-population Bayesian network MCMC sub-population
     *  proposal that corresponds to a weighted selection. Real-value in [0,1]
     *  where 0 -> uniform selection, 1 -> weighted selection, and any value
     *  in-between blends between the two approaches. The weight of each
     *  network corresponds to the associated mixture weight
     */
    cmxReal m_bnet_mpop_block_weighted_blend;
};

#endif  // cmxBnetMpopSharcoRo_h
