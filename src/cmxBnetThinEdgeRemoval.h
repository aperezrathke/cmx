//****************************************************************************
// cmxBnetThinEdgeRemoval.h
//****************************************************************************

/**
 * @brief Miscellaneous constructs need to facilitate O(1) edge removal
 */

#ifndef cmxBnetThinEdgeRemoval_h
#define cmxBnetThinEdgeRemoval_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxTypes.h"

//****************************************************************************
// Defines
//****************************************************************************

/**
 * Reserved value indicating an edge is missing (not available)
 */
#define CMX_BNET_EDGE_NA CMX_UINT_MAX

//****************************************************************************
// cmxBnetThinEdgeRemovalInfo
//****************************************************************************

/**
 * Additional data needed to remove an edge in O(1) time
 */
typedef struct {
    // An edge is defined as a tuple (v_src, v_dst) where v_src is a source
    // vertex and v_dst is a destination vertex

    /**
     * Location within v_src's destination vertex list
     */
    cmxUInt v_dst_ix;

    /**
     * Location within v_dst's source vertex list
     */
    cmxUInt v_src_ix;

} cmxBnetThinEdgeRemovalInfo_t;

//****************************************************************************
// cmxBnetThinEdgePostRemovalInfo
//****************************************************************************

/**
 * Additional data needed to update state after an edge removal. For instance,
 * when an edge is removed, the tail elements of the destination and parent
 * vertex lists are swapped into the corresponding removed edge's locations.
 * Therefore, this info allows propagation of these state changes to maintain
 * O(1) edge removal.
 */
typedef struct {
    // An edge is defined as a tuple (v_src, v_dst) where v_src is a source
    // vertex and v_dst is a destination vertex

    /**
     * This value will now reside in v_src's destination vertex list in place
     * of the removed v_dst. If CMX_BNET_EDGE_NA, then destination vertex was
     * last child added.
     */
    cmxUInt v_dst_swap;

    /**
     * This value will now reside in v_dst's source vertex list in place of
     * the removed parent v_src. If CMX_BNET_EDGE_NA, then source vertex was
     * last parent added.
     */
    cmxUInt v_src_swap;

} cmxBnetThinEdgePostRemovalInfo_t;

#endif  // cmxBnetThinEdgeRemoval_h
