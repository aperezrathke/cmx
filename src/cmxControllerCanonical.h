//****************************************************************************
// cmxControllerCanonical.h
//****************************************************************************

/**
 * @brief Canonical simulation controller, relies on user to specify thinning
 *  steps, burn-in iterations, and post burn-in iterations
 */

#ifndef cmxControllerCanonical_h
#define cmxControllerCanonical_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxController.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxMcmcGibbsRev.h"
#include "cmxOpts.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxController
//****************************************************************************

/**
 * Controller manages the detailed running of all models
 */
template <typename t_Glue>
class cmxControllerCanonical : public cmxController<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Default constructor
     */
    cmxControllerCanonical() { clear(); }

    /**
     * Initialize from user configuration, assumed called from main thread
     * @param config - user configuration
     * @param sim - simulation object to manage, observers assumed to be
     *  initialized, this method may add and initialize additional observers
     */
    virtual void init(cmxSpConstConfig_t config, sim_t& sim) {
        clear();
        config->read_into(m_thin_steps, cmxOpt_controller_thin_steps);
        config->read_into(m_burn_iters, cmxOpt_controller_burn_iters);
        config->read_into(m_post_burn_iters, cmxOpt_controller_post_burn_iters);
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_thin_steps = m_burn_iters = m_post_burn_iters = CMX_TO_UINT(0);
    }

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Run simulation for all models until completion
     * @param sim - simulation object to run until completion
     * @return cmxTRUE if simulation successful, cmxFALSE o/w
     */
    virtual cmxBool run(sim_t& sim) {
        // Compute number of steps defining an iteration
        const cmxUInt num_steps_per_iter = m_thin_steps + 1;

        // Handle to set of models
        std::vector<model_t>& models = sim.get_models();

        // Phase: burn-in, idea is to "burn-in" models such that the Markov
        // chain has converged to the target distribution. Observers are not
        // called during burn-in and generally this data is discarded.
        cmxLogf("Running simulation burn-in.\n");
        model_batch_runner_t::run(m_burn_iters * num_steps_per_iter,
                                  models,
                                  sim CMX_MAIN_THREAD_ID_ARG);

        // Phase: post burn-in, models are assumed to be 'converged' to target
        // distribution, data may be collected during this phase
        cmxLogf("Running simulation post burn.\n");
        sim.observers_mark_stale();
        for (cmxUInt i = 0; i < m_post_burn_iters; ++i) {
            // Update models
            model_batch_runner_t::run(
                num_steps_per_iter, models, sim CMX_MAIN_THREAD_ID_ARG);
            // Update active observers
            sim.observers_update(i);
        }

        // Simulation is over, finalize observers and reactors
        sim.observers_finish_all();

        // Signal successful completion
        return cmxTRUE;
    }

private:
    /**
     * Number of intermediate Gibbs steps between iterations
     */
    cmxUInt m_thin_steps;

    /**
     * Number of burn-in iterations
     */
    cmxUInt m_burn_iters;

    /**
     * Number of post burn-in iterations
     */
    cmxUInt m_post_burn_iters;
};

#endif  // cmxControllerCanonical_h
