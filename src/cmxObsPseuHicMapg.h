//****************************************************************************
// cmxObsPseuHicMapg.h
//****************************************************************************

/**
 * @brief Records global maximum a posteriori (MAP) estimate for latent
 *  Pseudo-population interaction frequencies
 */

#ifndef cmxObsPseuHicMapg_h
#define cmxObsPseuHicMapg_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxExportCore.h"
#include "cmxFilesystem.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxModelTypes.h"
#include "cmxObs.h"
#include "cmxObsLogscoreMapg.h"
#include "cmxSmartPtr.h"

#include <fstream>
#include <string>

//****************************************************************************
// cmxObsPseuHicMapg
//****************************************************************************

/**
 * Global MAP observer - captures global (across all MCMC chains) maximum a
 *  posteriori estimate of observed parameter
 */
template <typename t_Glue>
class cmxObsPseuHicMapg : public cmxObsLogscoreMapg<t_Glue>::map_reactor_t {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Default constructor
     */
    cmxObsPseuHicMapg() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within reactors array
     */
    virtual void init_react(cmxSpConstConfig_t config,
                            sim_t& sim,
                            const size_t index) {
        // Reset
        this->clear();
        // Determine optional export path
        m_fpath = cmxExport::get_fpath(cmxOpt_obs_pseu_hic_mapg_export,
                                       config,
                                       "",             /*subdir*/
                                       "map.pseu.hic", /*file_id*/
                                       "csv");         /*ext*/
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_map.clear();
        m_fpath.clear();
    }

    /**
     * Reactor update managed by logscore MAP observer, when a new MAP
     * estimate is encountered, this method will be called
     * @param iter - Typically post burn-in iteration number (0-based)
     * @param sim - Outer simulation
     * @param i_model - Index of MAP model
     * @param logscore - new MAP logscore associated to MAP model
     */
    virtual void update_react(const cmxUInt iter,
                              sim_t& sim,
                              const size_t i_model,
                              const cmxReal logscore) {
        // Update MAP estimate
        cmxAssertBounds(i_model, 0, sim.get_num_models());
        m_map = sim.get_models()[i_model].get_Pseudopop().get_pseudopop_freqs();
    }

    /**
     * Callback for when simulation has finished running
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        if (m_fpath.empty() || m_map.is_empty()) {
            // Early out if user did not specify file path or no data present
            return;
        }
        cmxLogf("Exporting pseudo Hi-C global MAP estimate: %s\n",
                m_fpath.c_str());
        // Make sure output directory exists
        cmxFs_create_parent_dirs(m_fpath);
        // Export MAP estimate to disk
        std::ofstream ofs(m_fpath.c_str());
        if (ofs.good()) {
            for (cmxMatSz_t i = 0; i < m_map.n_elem; ++i) {
                ofs << m_map.at(i) << "\n";
            }
        } else {
            // Inform user that export path is invalid
            cmxLogf(
                "Warning: Pseudo Hi-C global MAP observer unable to export to "
                "path: %s\n",
                m_fpath.c_str());
        }
        ofs.close();
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_pseu_hic_mapg)

private:
    /**
     * MAP (maximum a posteriori) estimate
     */
    cmxContactFreqs_t m_map;

    /**
     * Export path
     */
    std::string m_fpath;
};

#endif  // cmxObsPseuHicMapg_h
