//****************************************************************************
// cmxCommandletHooks.inl
//****************************************************************************

/**
 * General work flow for adding a commandlet:
 * 1) Create a new commandlet header file cmxCmdlet<name>.h
 * 2) Within commandlet header file, follow header guard and build flags
 *      policy as in other commandlets:
 *      - throw error if header included multiple times
 *      - wrap code in #ifdef CMX_BUILD_ENABLE_COMMANDLETS
 * 3) Within commandlet header file, wrap utility methods and class in
 *      anonymous namespace
 * 4) Within commandlet header file, define cmxCmdlet<name>Main(opts) method
 *      (outside of anonymous namespace)
 * 5) In cmxOpts.h, within the commandlet options demarcated by build flag, add
 *      cmxOpt_cmdlet_<name> to the set of enums
 * 6) In cmxOpts.cpp, in the section of CMX_OPT_INFOS demarcated by build flag,
 *      add line DECLARE_CMD_UNARY_OPT("cmdlet_<name>", "<description>").
 *      - make sure this is in same order as enum structure in cmxOpts.h
 * 7.) In cmxCommandletHooks.inl, include commandlet header file
 * 8.) In cmxCommandletHooks.inl, add to the GAppInfo structure the commandlet
 *      details
 */

// Not meant to be included more than once
#ifdef cmxCommandletHooks_inl
#   error "Commandlet Hooks included multiple times!"
#endif // cmxCommandletHooks_inl
#define cmxCommandletHooks_inl

// Implementation of commandlet hooks extracted to separate file to avoid
// cluttering cmxMain.cpp

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_COMMANDLETS

#include "cmxAppHooks.h"
#include "cmxExitCodes.h"

// Include additional commandlets here

// Include printf
#include <stdio.h>

// Placeholder main that dumps information about project
// Usage:
// -cmdlet_info
static int cmxCmdletInfoMain(const cmxCmdOptsMap& cmd_opts) {
    printf(
        "Chromatin mixture modeling through Dirichlet processes.\nApplication "
        "framework built with SKELeton C++ utilities.\n");
    return cmxExitCode_normal;
}

// Register commandlets here
static struct cmxAppInfo GCommandlets[] = {
      { cmxOpt_get_cmd_switch(cmxOpt_cmdlet_info), &cmxCmdletInfoMain }
};

// Commandlets are enabled, route to the proper commandlet hook:
// Parse command line and run any commandlets specified
#define CMX_CONDITIONAL_RUN_COMMANDLETS(cmd_opts) \
    CMX_CONDITIONAL_RUN_APPS(cmd_opts, GCommandlets)

#else

// Commandlets are not enabled, strip away call to commandlet hook
#define CMX_CONDITIONAL_RUN_COMMANDLETS(cmd_opts) ((void)0)

#endif // CMX_BUILD_ENABLE_COMMANDLETS
