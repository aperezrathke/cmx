//****************************************************************************
// cmxSmartPtr.h
//****************************************************************************

/**
 * @brief Selects between C++11 and boost smart pointer implementations
 */

#ifndef cmxSmartPtr_h
#define cmxSmartPtr_h

#include "cmxBuild.h"

// Check if C++11 is enabled
#ifdef CMX_BUILD_CXX_11
#   include <memory>
#   define cmxSmartPtr std
#else
// Else, defer to boost implementation
#   include <boost/enable_shared_from_this.hpp>
#   include <boost/make_shared.hpp>
#   include <boost/shared_ptr.hpp>
#   include <boost/weak_ptr.hpp>
#   define cmxSmartPtr boost
#endif

#endif  // cmxSmartPtr_h
