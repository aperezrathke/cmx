//****************************************************************************
// cmxPearsonCorr.h
//****************************************************************************

/**
 * @brief C++ port of GSL's gsl_stats_correlation(...)
 */

/**
 * Below is original license and source comments from GSL library, see also
 * https://www.gnu.org/software/gsl/
 */

/**
 * Copyright(C) 1996, 1997, 1998, 1999, 2000, 2007 Jim Davies, Brian Gough
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110 - 1301,
USA.
 * /

/*
 gsl_stats_correlation()
 Calculate Pearson correlation = cov(X, Y) / (sigma_X * sigma_Y)
 This routine efficiently computes the correlation in one pass of the
 data and makes use of the algorithm described in:

 B. P. Welford, "Note on a Method for Calculating Corrected Sums of
 Squares and Products", Technometrics, Vol 4, No 3, 1962.

 This paper derives a numerically stable recurrence to compute a sum
 of products

 S = sum_{i=1..N} [ (x_i - mu_x) * (y_i - mu_y) ]

 with the relation

 S_n = S_{n-1} + ((n-1)/n) * (x_n - mu_x_{n-1}) * (y_n - mu_y_{n-1})
*/

#ifndef cmxPearsonCorr_h
#define cmxPearsonCorr_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxTypes.h"

#include <math.h>

//****************************************************************************
// cmxPearsonCorr
//****************************************************************************

/**
 * Pearson correlation, effectively a namespace
 */
class cmxPearsonCorr {
public:
    /**
     * Computes Pearson correlation between strided elements of input arrays
     * @param data1 - input vector, must be length 'n'
     * @param stride1 - only elements which are multiples of stride are used,
     *  e.g. this can be used to compute correlation of two rows in a
     *  column-major matrix (set stride to number of rows) or two columns in a
     *  row-major major matrix (set stride to number of columns). If
     *  correlation of all elements is desired, set stride to 1
     * @param data2 - input vector, must be length 'n'
     * @param stride2 - see 'stride1' description
     * @param n - length of each array 'data1' and 'data2'
     * @return Pearson correlation between data1 and data2
     */
    inline static cmxReal get(const cmxReal* const data1,
                              const size_t stride1,
                              const cmxReal* const data2,
                              const size_t stride2,
                              const size_t n) {
        size_t i;
        cmxReal sum_xsq = 0.0;
        cmxReal sum_ysq = 0.0;
        cmxReal sum_cross = 0.0;
        cmxReal ratio;
        cmxReal delta_x, delta_y;
        cmxReal mean_x, mean_y;
        cmxReal r;

        /*
         * Compute:
         * sum_xsq = Sum [ (x_i - mu_x)^2 ],
         * sum_ysq = Sum [ (y_i - mu_y)^2 ] and
         * sum_cross = Sum [ (x_i - mu_x) * (y_i - mu_y) ]
         * using the above relation from Welford's paper
         */

        mean_x = data1[0 * stride1];
        mean_y = data2[0 * stride2];

        for (i = 1; i < n; ++i) {
            ratio = i / (i + 1.0);
            delta_x = data1[i * stride1] - mean_x;
            delta_y = data2[i * stride2] - mean_y;
            sum_xsq += delta_x * delta_x * ratio;
            sum_ysq += delta_y * delta_y * ratio;
            sum_cross += delta_x * delta_y * ratio;
            mean_x += delta_x / (i + 1.0);
            mean_y += delta_y / (i + 1.0);
        }

        r = sum_cross / (sqrt(sum_xsq) * sqrt(sum_ysq));

        return r;
    }

    /**
     * Compute Pearson correlation between all elements of input arrays
     * @param data1 - input vector, must be length 'n'
     * @param data2 - input vector, must be length 'n'
     * @param n - length of each array 'data1' and 'data2'
     * @return Pearson correlation between all elements of data1 and data2
     */
    inline static cmxReal get(const cmxReal* const data1,
                              const cmxReal* const data2,
                              const size_t n) {
        // https://stackoverflow.com/questions/8980092/how-to-use-the-gsl-implementation-of-the-pearson-correlation-coefficient
        return get(data1, 1, data2, 1, n);
    }
};

#endif  // cmxPearsonCorr_h
