//****************************************************************************
// cmxNullSharcoRo.h
//****************************************************************************

/**
 * @brief Minimal null interface for a read-only shared resource component
 *
 * Sharco == SHARed COmponent
 */

#ifndef cmxNullSharcoRo_h
#define cmxNullSharcoRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"

//****************************************************************************
// cmxNullSharcoRo
//****************************************************************************

/**
 * Null shared read-only data policy, resides at simulation level.
 *
 * The minimal interface expected for a shared read-only policy is:
 *  void <shared_type>::init(cmxSpConstConfig_t config) { ... }
 *
 * Note, many of the read-only shared data policies are conditional on the
 *  the associated model-level component being deterministic. If instead the
 *  component is random (i.e. Gibbs sampled), then the shared policy will
 *  likely need to utilize a "null" version of the shared data to keep the
 *  interfaces consistent, hence the existence of this type
 */
class cmxNullSharcoRo {
public:
    /**
     * Null interface for initializing read-only resource
     * @param config - [ignored] user options
     */
    void init(cmxSpConstConfig_t config) {}

    /**
     * Null interface for clearing read-only resource
     */
    void clear() {}
};

#endif  // cmxNullSharcoRo
