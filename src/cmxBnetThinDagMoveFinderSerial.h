//****************************************************************************
// cmxBnetThinDagMoveFinderSerial.h
//****************************************************************************

/**
 * @brief Determines the legal set of moves that can be applied to a thin DAG.
 *  This is a serial (non-parallel) implementation.
 */

#ifndef cmxBnetThinDagMoveFinderSerial_h
#define cmxBnetThinDagMoveFinderSerial_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxBnetThinDag.h"
#include "cmxBnetThinEdgeStatesMu.h"
#include "cmxBnetThinEdgeStatesRo.h"
#include "cmxStats.h"
#include "cmxThread.h"

//****************************************************************************
// cmxBnetThinDagMoveFinderSerial
//****************************************************************************

/**
 * Serial (i.e. non-parallel) thin DAG move finder
 */
class cmxBnetThinDagMoveFinderSerial {
public:
    /**
     * Determine legal set of moves which can be applied to thin DAG
     * @param dag - Directed Acyclic Graph
     * @param mu - mutable edge state within parameter DAG
     * @param ro - read-only edge state for parameter DAG
     * @param out_moves - output vector of edge indices which may have their
     *  states toggled (should reserve to number of thin edges)
     * @param in_deg_buf - scratch buffer for storing in-degrees at each
     *  source vertex (reserve to size N where N is number of vertices)
     * @param stack_buf - scratch buffer used during cycle checking (reserve
     *  to size N where N is number of vertices)
     */
    static inline void get_moves(const cmxBnetThinDag& dag,
                                 const cmxBnetThinEdgeStatesMu& mu,
                                 const cmxBnetThinEdgeStatesRo& ro,
                                 std::vector<cmxUInt>& out_moves,
                                 std::vector<cmxUInt>& in_deg_buf,
                                 std::vector<cmxUInt>& stack_buf) {
        CMX_SCOPED_STAT_TIMER(cmxSTAT_BnetThinDagMoveFinderTime);
        out_moves.clear();

        const cmxUInt num_edges = ro.get_e_num();
        for (cmxUInt i = 0; i < num_edges; ++i) {
            if (mu.has_edge(i)) {
                // If edge exists, then removing edge will not create a cycle.
                // Therefore, the toggle operation of removing an existing
                // edge will always be legal, so add it to set of legal moves.
                out_moves.push_back(i);
            } else {
                // Edge does not exist and can only be added if it does not
                // induce a cycle.
                if (dag.can_add_edge(ro.get_v_src(i),
                                     ro.get_v_dst(i),
                                     in_deg_buf,
                                     stack_buf)) {
                    out_moves.push_back(i);
                }
            }
        }

        // If no moves, then we are stuck, should not happen!
        cmxAssert(!out_moves.empty());
    }

private:
    // Disallow instantiation, copy, and assignment for now
    cmxBnetThinDagMoveFinderSerial();
    cmxBnetThinDagMoveFinderSerial(const cmxBnetThinDagMoveFinderSerial&);
    cmxBnetThinDagMoveFinderSerial& operator=(
        const cmxBnetThinDagMoveFinderSerial&);
};

#endif  // cmxBnetThinDagMoveFinderSerial_h
