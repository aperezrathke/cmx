//****************************************************************************
// cmxDpStickSpopMoco.h
//****************************************************************************

/**
 * Nominal interface to 'mixture components' for single-population (THERE
 *  IS ONLY 1 COMPONENT IN THIS CASE!)
 */

#ifndef cmxDpStickSpopMoco_h
#define cmxDpStickSpopMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxDpStickSpopMoco
//****************************************************************************

/**
 * Expected Dp_stick model component interface
 *  get_mix_weight(mix_index) - must return the associated mixture weight for
 *      the parameter mixture identifier
 *  get_mix_ent() - must return entropy of mixture PMF
 */
template <typename t_Glue>
class cmxDpStickSpopMoco : public cmxMoco<t_Glue> {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    // This component is always deterministic!
    enum { is_random = cmxFALSE };

    // Accessor(s)

    /**
     * Interface for querying mixture component weight (probability mass)
     * @return Probability mass assigned to 'mix_index' component
     */
    inline cmxReal get_mix_weight(const cmxUInt mix_index) const {
        cmxAssert(mix_index == CMX_TO_UINT(0));
        return CMX_TO_REAL(1.0);
    }

    /**
     * Interface for querying mixture entropy
     * @return Entropy of mixture PMF
     */
    inline cmxReal get_mix_ent() const { return CMX_TO_REAL(0.0); }
};

#endif  // cmxDpStickSpopMoco_h
