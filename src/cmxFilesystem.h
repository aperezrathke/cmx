//****************************************************************************
// cmxFilesystem.h
//****************************************************************************

/**
 * @brief Wraps boost or std filesystem library
 */

#ifndef cmxFilesystem_h
#define cmxFilesystem_h

#include "cmxBuild.h"

#include <string>

/**
 * Defer to boost filesystem, this may later be swapped to std namespace
 */
#include <boost/filesystem.hpp>
#define cmxFs boost::filesystem

/**
 * Creates any directories needed for parameter path
 * @param dir_path - path to directory
 * @return result of cmxFs::create_directories(...) for dir_path
 */
extern bool cmxFs_create_dirs(const cmxFs::path& dir_path);

/**
 * Creates any parent directories needed for parameter file path
 * @param file_path - path to file
 * @return result of cmxFs::create_directories(...) for parent directories
 */
extern bool cmxFs_create_parent_dirs(const cmxFs::path& file_path);

/**
 * Creates lexically normal (removes extra path separators and dots) directory
 * path without trailing path separator slash(es)
 * @param dir_path - directory path to normalize
 */
extern void cmxFs_normalize_dir_path(cmxFs::path& dir_path);
extern void cmxFs_normalize_dir_path(std::string& dir_path);

#endif  // cmxFilesystem_h
