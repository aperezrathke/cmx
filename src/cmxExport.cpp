//****************************************************************************
// cmxExport.cpp
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxExportBnet.h"
#include "cmxExportCore.h"
#include "cmxFilesystem.h"
#include "cmxLogf.h"
#include "cmxOpts.h"
#include "cmxStringUtils.h"
#include "cmxTypes.h"

#include <string>

// Anonymous utility namespace
namespace {

/**
 * Utility for incrementally constructing a file path.
 * @param fpath - Output file path. If param 'term' is empty, then fpath
 *  is not modified. Else, 'term' is appended according to:
 *      if fpath is empty: fpath = term
 *      else: fpath = fpath / term where '/' is directory separator
 * @param term - The term to append to 'fpath' parameter
 */
void extend_path(std::string& fpath, const std::string& term) {
    if (!term.empty()) {
        if (!fpath.empty()) {
            fpath = (cmxFs::path(fpath) / cmxFs::path(term)).string();
        } else {
            fpath = term;
        }
    }
}

}  // end of anonymous namespace

namespace cmxExport {

//****************************************************************************
// Core
//****************************************************************************

/**
 * Obtain base directory for writing exported data
 * @param output_dir - string to store output directory
 * @param config - user configuration
 */
void get_output_dir(std::string& output_dir, cmxSpConstConfig_t config) {
    output_dir = "";
    config->resolve_path(output_dir, cmxOpt_output_dir);
}

/**
 * Obtain job prefix to avoid overwriting data from previous runs
 * @param output_dir - string to store job identifier prefix
 * @param config - user configuration
 */
void get_job_id(std::string& job_id, cmxSpConstConfig_t config) {
    job_id = "";
    config->read_into(job_id, cmxOpt_job_id);
}

/**
 * Utility to obtain full export path for a single export file
 * @param config - The configuration object
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return The export path for a single export file
 */
std::string get_fpath(cmxSpConstConfig_t config,
                      const std::string& subdir,
                      const std::string& file_id,
                      const std::string& ext) {
    // Final output path
    std::string fpath("");

    // Get user configured output directory
    std::string output_dir;
    get_output_dir(output_dir, config);
    if (!output_dir.empty()) {
        fpath = output_dir;
    }

    // Append user configured job identifier
    std::string job_id;
    get_job_id(job_id, config);
    extend_path(fpath, job_id);

    // Append subdirectory
    extend_path(fpath, subdir);

    // Determine file name
    std::string fname(file_id);
    // Only append extension if non-empty
    if (!ext.empty()) {
        fname += "." + ext;
    }

    // Append file name
    extend_path(fpath, fname);

    // Return final path
    return fpath;
}

/**
 * Utility to obtain full export path for a single export file
 * @param config - The configuration object
 * @param index - Model index, will result in parent folder to subdir
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return The export path for a single export file
 */
std::string get_fpath(cmxSpConstConfig_t config,
                      const size_t index,
                      const std::string& subdir,
                      const std::string& file_id,
                      const std::string& ext) {
    // Create path: <index>/<subdir>
    std::string subd = cmx2Str(index);
    extend_path(subd, subdir);
    return get_fpath(config, subd, file_id, ext);
}

/**
 * Obtain full export path for a single file if enabled by user.
 * @param check - User option to check if user configured to TRUE
 * @param config - The configuration object
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return If check option evaluates to TRUE, then file path is evaluated
 *  according to arguments and returned. Else, empty string is returned.
 */
std::string get_fpath(const enum cmxOptE check,
                      cmxSpConstConfig_t config,
                      const std::string& subdir,
                      const std::string& file_id,
                      const std::string& ext) {
    // Determine optional export path
    cmxBool b_export = CMX_TO_UINT(0);
    config->read_into(b_export, check);
    if (b_export) {
        return cmxExport::get_fpath(config, subdir, file_id, ext);
    }
    return std::string("");
}

/**
 * Obtain full export path for a single file if enabled by user.
 * @param check - User option to check if user configured to TRUE
 * @param config - The configuration object
 * @param index - Model index, will result in parent folder to subdir
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return If check option evaluates to TRUE, then file path is evaluated
 *  according to arguments and returned. Else, empty string is returned.
 */
std::string get_fpath(const enum cmxOptE check,
                      cmxSpConstConfig_t config,
                      const size_t index,
                      const std::string& subdir,
                      const std::string& file_id,
                      const std::string& ext) {
    // Create path: <index>/<subdir>
    std::string subd = cmx2Str(index);
    extend_path(subd, subdir);
    return get_fpath(check, config, subd, file_id, ext);
}

//****************************************************************************
// Bnet
//****************************************************************************

/**
 * Write Bayesian network DAG adjacency mapping in CSV format
 * @param adjmap - DAG adjacency map to export
 * @param fpath - output file path
 * @return TRUE if write successful, FALSE o/w
 */
cmxBool write_bnet_dag_adjmap_csv(const cmxBnetDagAdjMap_t& adjmap,
                                  const std::string& fpath) {
    if (fpath.empty() || adjmap.empty()) {
        // Early out if user did not specify file path or no data present
        return cmxFALSE;
    }
    // Make sure output directory exists
    cmxFs_create_parent_dirs(fpath);
    std::ofstream ofs(fpath.c_str());
    if (ofs.good()) {
        // Write CSV format with first column being node identifier and each
        // row element (past 1st column) the set of nodes which share an edge
        const size_t num_v = adjmap.size();
        for (size_t i = 0; i < num_v; ++i) {
            // First element is node identifier
            ofs << i;
            cmxAssertBounds(i, 0, adjmap.size());
            const cmxBnetDagVxList_t& vlist = adjmap[i];
            const size_t num_e = vlist.size();
            for (size_t j = 0; j < num_e; ++j) {
                cmxAssertBounds(j, 0, vlist.size());
                // Remaining elements are nodes which share an edge
                ofs << "," << vlist[j];
            }
            ofs << "\n";
        }
    } else {
        // Inform user that export path is invalid
        cmxLogf(
            "Warning: unable to create file for Bayesian network DAG adjacency "
            "mapping:\n\t%s\n",
            fpath.c_str());
        return cmxFALSE;
    }
    ofs.close();
    return cmxTRUE;
}

/**
 * Write Bayesian network DAG adjacency mapping in Graphviz DOT format
 * @param adjmap - DAG adjacency map to export
 * @param fpath - output file path
 * @return TRUE if write successful, FALSE o/w
 */
cmxBool write_bnet_dag_adjmap_gv(const cmxBnetDagAdjMap_t& adjmap,
                                 const std::string& fpath) {
    if (fpath.empty() || adjmap.empty()) {
        // Early out if user did not specify file path or no data present
        return cmxFALSE;
    }
    // Make sure output directory exists
    cmxFs_create_parent_dirs(fpath);
    std::ofstream ofs(fpath.c_str());
    if (ofs.good()) {
        ofs << "digraph {\n";
        const size_t num_v = adjmap.size();
        for (size_t i = 0; i < num_v; ++i) {
            // First element is node identifier
            ofs << "  " << i;
            cmxAssertBounds(i, 0, adjmap.size());
            const cmxBnetDagVxList_t& vlist = adjmap[i];
            const size_t num_e = vlist.size();
            if (num_e > 0) {
                ofs << " ->";
                if (num_e > 1) {
                    ofs << " {";
                }
                for (size_t j = 0; j < num_e; ++j) {
                    cmxAssertBounds(j, 0, vlist.size());
                    // Remaining elements are nodes which share an edge
                    ofs << " " << vlist[j];
                }
                if (num_e > 1) {
                    ofs << " }";
                }
            }
            ofs << "\n";
        }
        ofs << "}\n";
    } else {
        // Inform user that export path is invalid
        cmxLogf(
            "Warning: unable to create file for Bayesian network DAG adjacency "
            "mapping:\n\t%s\n",
            fpath.c_str());
        return cmxFALSE;
    }
    ofs.close();
    return cmxTRUE;
}

/**
 * Write Bayesian network topological order in CSV format
 * @param order - topological ordering of Bayesian network nodes to export
 * @param fpath - output file path
 * @return TRUE if write successful, FALSE o/w
 */
cmxBool write_bnet_order_csv(const std::vector<cmxUInt>& order,
                             const std::string& fpath) {
    if (fpath.empty() || order.empty()) {
        // Early out if user did not specify file path or no data present
        return cmxFALSE;
    }
    // Make sure output directory exists
    cmxFs_create_parent_dirs(fpath);
    std::ofstream ofs(fpath.c_str());
    if (ofs.good()) {
        const size_t num_v = order.size();
        for (size_t i = 0; i < num_v; ++i) {
            ofs << order[i] << "\n";
        }
    } else {
        // Inform user that export path is invalid
        cmxLogf(
            "Warning: unable to create file for Bayesian network topological "
            "ordering:\n\t%s\n",
            fpath.c_str());
        return cmxFALSE;
    }
    ofs.close();
    return cmxTRUE;
}

/**
 * Write Bayesian network conditional probability tables for Boolean state space
 * @param cpts - set of Boolean conditional probability tables
 * @param fpath - output file path
 * @return TRUE if write succesful, FALSE o/w
 */
cmxBool write_bnet_bool_cpt_csv(const std::vector<cmxBnetBoolCt>& cpts,
                                const std::string& fpath) {
    if (fpath.empty() || cpts.empty()) {
        // Early out if user did not specify file path or no data present
        return cmxFALSE;
    }
    // Make sure output directory exists
    cmxFs_create_parent_dirs(fpath);
    std::ofstream ofs(fpath.c_str());
    if (ofs.good()) {
        const size_t num_v = cpts.size();
        for (size_t i = 0; i < num_v; ++i) {
            const cmxBnetBoolCt& cpt = cpts[i];
            const cmxMatrix& tab = cpt.table();
            if (!tab.save(ofs, cmxMatrixUtils::csv_ascii)) {
                cmxLogf(
                    "Failed to write Bayesian network CPT at node %d for file: "
                    "%s\n",
                    (int)i,
                    fpath.c_str());
                ofs.close();
                return cmxFALSE;
            }
        }
    } else {
        // Inform user that export path is invalid
        cmxLogf(
            "Warning: unable to create file for Bayesian network CPTs:\n\t%s\n",
            fpath.c_str());
        return cmxFALSE;
    }
    ofs.close();
    return cmxTRUE;
}

}  // namespace cmxExport
