//****************************************************************************
// cmxPseudopopBfwdImpl.h
//****************************************************************************

/**
 * @brief Pseudo-population implementation uses forward sampling from the
 *  associated parent Bayesian network(s) as the the proposal distribution.
 */

/**
 * Pseudo-population Impl must define:
 *
 *  ::mcmc_mixin_t - The MCMC mixin type (Metropolis vs Metropolis-Hastings)
 *  ::Mh_prime_cand(...) - Primes current and candidate states for MCMC
 *  ::randomize(...) - Randomizes parameter MCMC state
 */

#ifndef cmxPseudopopBfwdImpl_h
#define cmxPseudopopBfwdImpl_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxGlobals.h"
#include "cmxGlue.h"
#include "cmxMcmcMh4gMixin.h"
#include "cmxModelTypes.h"
#include "cmxPseudopopCore.h"
#include "cmxRand.h"
#include "cmxShufflerFisherYates.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxPseudopopBfwdImpl
//****************************************************************************

/**
 * Metropolis-within-Gibbs sampled pseudo-population. The log-likelihood of
 * the observed Hi-C given a pseudo-population is provided by the model's
 * Contact frequency model component which should provide interface:
 *  model_t::Contact_freqs_t::logscore_using(Pseudopop)
 *
 * The pseudo-population log posterior probability is given by:
 *
 *      (1) logpost = loglike(Hi-C|Pseudopop) + log(P(Pseudopop|Bnet)) + logZ
 *
 *  where P(Pseudopop|Bnet) is the probability of forward-sampling the
 *  pseudo-population according to each individual pseudo-sample's assigned
 *  Bayesian network (and logZ is the log of the normalizing constant which
 *  isn't computed).
 *
 * For Metropolis-Hastings, the proposal distribution of any state is always
 * log(P(Pseudopop|Bnet)) (i.e. the probability of forward sampling according
 * to the underlying assigned Bayesian network(s)). Hence, the log Metropolis
 * acceptance criterion is given by:
 *
 *      (2) logp_accept =
 *              logp_tar_cand + logp_pro_curr - logp_tar_curr - logp_pro_cand
 *
 *  where logp_tar_cand = logpost(candidate),
 *        logp_pro_curr = log(P(current|Bnet))
 *        logp_tar_curr = logpost(current),
 *        logp_pro_cand = log(P(candidate|Bnet))
 *
 *  and 'current' is the current pseudo-population state and 'candidate' is
 *  the proposed candidate pseudo-population state.
 *
 * Combining equations (1) and (2), we see that the Metropolis-Hastings
 * acceptance criterion simplifies to a simple Metropolis criterion:
 *
 *      (3) logp_accept = loglike(Hi-C|candidate) - loglike(Hi-C|current)
 *
 * Therefore, we do not have to keep track of the proposal probability (though
 * we will need to compute it when calling logscore())
 */
template <typename t_Glue>
class cmxPseudopopBfwdImpl {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * MCMC state type
     */
    typedef cmxPseudopopMocoStateMetro_t state_t;

    /**
     * Impl interface - MCMC mixin type
     */
    typedef cmxMcmcMh4gMetroMixin<glue_t, state_t> mcmc_mixin_t;

    /**
     * Pseudo-population core utilities
     */
    typedef cmxPseudopopCore<t_Glue> core_t;

    /**
     * Typedefs for utilized model component(s)
     */
    typedef typename glue_t::Contact_freqs_t contact_freqs_moco_t;

    /**
     * Metropolis-Hastings prime (i.e. propose) candidate sample
     */
    static void Mh_prime_cand(state_t& cand,
                              state_t& curr,
                              const cmxUIVecCol& indices,
                              const model_t& model,
                              sim_t& sim CMX_THREAD_ID_PARAM) {
        // Pre-conditions
        cmxAssertPosEq(cand.frame.n_cols, curr.frame.n_cols);
        cmxAssertPosEq(cand.frame.n_rows, curr.frame.n_rows);
        cmxAssertPosEq(cand.freqs.n_elem, curr.freqs.n_elem);
        cmxAssert(core_t::check_block_indices(cand.frame, indices));

        // Sample new candidate state
        sample_and_refresh(
            cand.frame, cand.freqs, indices, model, sim CMX_THREAD_ID_ARG);

        // Score candidate state
        const contact_freqs_moco_t& contact_freqs_moco =
            model.get_Contact_freqs();
        cand.logp_tar = contact_freqs_moco.logscore_using(
            cand.frame, cand.freqs, model, sim);

        // Check if current state should also be re-scored
        if (!core_t::use_cached_hic_loglike) {
            // Re-compute target score
            curr.logp_tar = contact_freqs_moco.logscore_using(
                curr.frame, curr.freqs, model, sim);
        }
        // Verify correct value is cached
        cmxAssert(curr.logp_tar == contact_freqs_moco.logscore_using(
                                       curr.frame, curr.freqs, model, sim));
    }

    /**
     * Randomize state - sample from prior
     * @param s - Metropolis-Hastings state to randomize from prior
     * @param config - user configuration
     * @param model - parent model
     * @param sim - outer simulation
     */
    static void randomize(state_t& s,
                          cmxSpConstConfig_t config,
                          const model_t& model,
                          sim_t& sim CMX_THREAD_ID_PARAM) {
        // Get index vector 0:PSEUDOPOP_SIZE-1
        cmxUIVecCol indices;
        cmxShufflerFisherYatesCore::get_sorted_indices(indices, s.frame.n_cols);
        sample_and_refresh(
            s.frame, s.freqs, indices, model, sim CMX_THREAD_ID_ARG);
        // Compute target score
        s.logp_tar = model.get_Contact_freqs().logscore_using(
            s.frame, s.freqs, model, sim);
    }

private:
    /**
     * Samples new pseudo-population frame and refreshes associated contact
     *  frequencies
     * @param frame - Pseudo-population frame
     * @param freqs - Associated pseudo-population contact frequencies
     * @param indices - Block (column) indices of sub-population
     * @param model - Parent model
     * @param sim - Outer simulation
     */
    static void sample_and_refresh(cmxPseudopopFrame_t& frame,
                                   cmxContactFreqs_t& freqs,
                                   const cmxUIVecCol& indices,
                                   const model_t& model,
                                   sim_t& sim CMX_THREAD_ID_PARAM) {
        // Handle to random number generator
        cmxRand& rng = cmxRng;
        // Generate bit matrix
        core_t::sample_bfwd_trunc(frame, indices, model, sim, rng);
        // Refresh contact frequencies
        core_t::refresh(freqs, frame, sim CMX_THREAD_ID_ARG);
    }
};

#endif  // cmxPseudopopBfwdImpl_h
