//****************************************************************************
// cmxObsLogscoreMapc.h
//****************************************************************************

/**
 * @brief Captures maximum observed log-score at each MCMC chain. MAP is the
 *  maximum a posteriori log-score, can also filter by Hi-C correlation
 */

#ifndef cmxObsLogscoreMapc_h
#define cmxObsLogscoreMapc_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxExportCore.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxMapReactor.h"
#include "cmxObs.h"
#include "cmxObsCorrHicSnap.h"
#include "cmxObsLogscoreSnap.h"
#include "cmxSmartPtr.h"

#include <limits>
#include <queue>
#include <vector>

//****************************************************************************
// Defines
//****************************************************************************

#ifdef CMX_OBS_LOGSCORE_MAPC_INVALID_ITER
#error "Duplicate macro name encountered!"
#endif  // CMX_OBS_LOGSCORE_MAPC_INVALID_ITER

/**
 * Reserved iteration value to mark non-captured MAP estimates
 */
#define CMX_OBS_LOGSCORE_MAPC_INVALID_ITER CMX_UINT_MAX

//****************************************************************************
// cmxObsLogscoreMapc
//****************************************************************************

/**
 * Log-score chain MAP observer - depends on log-score and Hi-C correlation
 *  snapshot observers
 */
template <typename t_Glue>
class cmxObsLogscoreMapc : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Log-score snapshot observer type
     */
    typedef cmxObsLogscoreSnap<t_Glue> obs_logscore_snap_t;

    /**
     * Hi-C correlation snapshot observer type
     */
    typedef cmxObsCorrHicSnap<t_Glue> obs_corr_hic_snap_t;

    /**
     * Base map reactor type
     */
    typedef cmxMapReactor<t_Glue, cmxObsLogscoreMapc<t_Glue> > map_reactor_t;

    /**
     * Defaults
     */
    enum { default_start_iter = 0 };

    /**
     * Default constructor
     */
    cmxObsLogscoreMapc() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read start iteration
        config->read_into(m_start_iter, cmxOpt_obs_logscore_mapc_start_iter);
        // Read minimal Hi-C correlation
        config->read_into(m_min_corr_hic,
                          cmxOpt_obs_logscore_mapc_min_corr_hic);
        // Extract logscore snapshot observer - must occur before MAP observer
        if (!obs_t::find_raw(&mp_snap_logscore, sim, index)) {
            cmxLogf(
                "Error: log-score chain MAP observer unable to find handle to "
                "log-score snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap_logscore);
        // Extract optional Hi-C correlation snapshot observer
        if (!obs_t::find_raw(&mp_snap_corr_hic, sim, index)) {
            cmxLogf(
                "Warning: log-score chain MAP observer unable to find handle "
                "to Hi-C correlation snapshot observer.\n");
            mp_snap_corr_hic = NULL;
            m_min_corr_hic = -CMX_REAL_MAX;
        }
        // Initialize MAP estimates at each model chain
        cmxAssert(sim.get_num_models() > 0);
        m_maps.resize(sim.get_num_models(), -CMX_REAL_MAX);
        m_iters.resize(sim.get_num_models(),
                       CMX_OBS_LOGSCORE_MAPC_INVALID_ITER);
    }

    /**
     * Reset to default state
     */
    void clear() {
        mp_snap_logscore = NULL;
        mp_snap_corr_hic = NULL;
        m_reactors.clear();
        m_start_iter = default_start_iter;
        m_min_corr_hic = -CMX_REAL_MAX;
        m_maps.clear();
        m_iters.clear();
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        // Use same period as configured logscore snapshot observer
        cmxAssert(mp_snap_logscore);
        cmxAssert(mp_snap_corr_hic);
        cmxAssert(iter != CMX_OBS_LOGSCORE_MAPC_INVALID_ITER);
        if ((iter < m_start_iter) || (!mp_snap_logscore->should_update(iter))) {
            // Early out if not yet at sampling interval
            return;
        }
        // Determine if any new MAP scores encountered
        const std::vector<cmxReal>& logscores =
            mp_snap_logscore->get_logscores();
        const size_t num_logscores = logscores.size();
        cmxAssertPosEq(num_logscores, m_maps.size());
        std::vector<size_t> chain_indices;
        for (size_t i = 0; i < num_logscores; ++i) {
            const cmxReal chain_map = m_maps[i];
            if (logscores[i] > chain_map) {
                chain_indices.push_back(i);
            }
        }

        // Make sure Hi-C correlation snapshot is up-to-date
        // Allow soft fail if Hi-C correlation observer not configured
        if (!chain_indices.empty() && mp_snap_corr_hic) {
            if (mp_snap_corr_hic->is_stale(iter)) {
                // Hi-C correlation is stale, capture new snapshot
                mp_snap_corr_hic->force_update(iter, sim);
            }
        }

        // Determine if any MAP scores meet Hi-C correlation
        const size_t num_chains = chain_indices.size();
        for (size_t k = 0; k < num_chains; ++k) {
            const size_t max_i = chain_indices[k];
            bool should_capture = true;
            if (mp_snap_corr_hic) {
                const std::vector<cmxReal>& corrs =
                    mp_snap_corr_hic->get_corrs();
                cmxAssertBounds(max_i, 0, corrs.size());
                const cmxReal max_corr_hic = corrs[max_i];
                // Only record MAP if min Hi-C correlation threshold is met
                should_capture = (max_corr_hic >= m_min_corr_hic);
            }
            if (should_capture) {
                // New MAP estimate encountered that meets Hi-C correlation
                cmxAssertBounds(max_i, 0, logscores.size());
                const cmxReal max_logscore = logscores[max_i];
                cmxAssertBounds(max_i, 0, m_maps.size());
                m_maps[max_i] = max_logscore;
                cmxAssertBounds(max_i, 0, m_iters.size());
                m_iters[max_i] = iter;
                // Update reactors
                const size_t num_reactors = m_reactors.size();
                for (size_t i = 0; i < num_reactors; ++i) {
                    m_reactors[i]->update_react(iter, sim, max_i, max_logscore);
                }
            }
        }
    }

    /**
     * Callback for when simulation has finished running
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        // Export MAP log-scores at each MCMC model chain
        this->export_csv(m_maps,
                         "map.chain.logscore", /*file_id*/
                         true,                 /*b_warn*/
                         sim);
        // Export iteration at which MAP estimates were captured at each chain
        this->export_csv(m_iters,
                         "map.chain.iter", /*file_id*/
                         false,            /*b_warn*/
                         sim);
    }

    /**
     * Register reactor for updating when new MAP estimates are found
     */
    void add_reactor(map_reactor_t& reactor) { m_reactors.push_back(&reactor); }

    /**
     * Utility method to determine if MAP estimate was captured at chain index
     * @param chain_index - 0-based MCMC model chain index
     * @return true if MAP estimate was captured for MCMC chain, false o/w
     */
    inline bool has_capture(const size_t chain_index) const {
        cmxAssertBounds(chain_index, 0, m_iters.size());
        cmxAssertPosEq(m_iters.size(), m_maps.size());
        const cmxUInt iter = m_iters[chain_index];
        // Test if capture iteration is not reserved value
        cmxAssert(iter >= m_start_iter);
        return iter != CMX_OBS_LOGSCORE_MAPC_INVALID_ITER;
    }

    /**
     * Utility method to export a MAP observer/reactor vector to CSV format
     * @param v - vector of captured MAP associated values
     * @param file_id - base file name w/o file extension suffix
     * @param b_warn - if true, will warn user when no capture exists
     * @param sim - parent simulation
     */
    template <typename t_value>
    void export_csv(const std::vector<t_value>& v,
                    const std::string& file_id,
                    const bool b_warn,
                    const sim_t& sim) const {
        // Determine export path
        cmxSpConstConfig_t config(sim.get_config());
        const std::string fpath = cmxExport::get_fpath(config,
                                                       "", /*subdir*/
                                                       file_id,
                                                       "csv"); /*ext*/
        // Make sure output directory exists
        cmxFs_create_parent_dirs(fpath);
        // Export MAP estimates to disk
        std::ofstream ofs(fpath.c_str());
        if (ofs.good()) {
            ofs << "CHAIN,MAP\n";
            const size_t num_maps = v.size();
            for (size_t i = 0; i < num_maps; ++i) {
                if (this->has_capture(i)) {
                    const t_value chain_map = v[i];
                    ofs << i << "," << chain_map << "\n";
                } else if (b_warn) {
                    cmxLogf(
                        "Warning: missing MAP capture for model chain %i.\n",
                        (int)i);
                }
            }
        } else {
            // Inform user that export path is invalid
            cmxLogf(
                "Warning: Chain MAP observer/reactor unable to export to path: "
                "%s\n",
                fpath.c_str());
        }
        // Close file handle
        ofs.close();
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_logscore_mapc)

private:
    /**
     * Handle to log-score snapshot observer - raw pointer is used as
     * lifetime of pointer does not exceed that of parent simulation's
     * shared_ptr
     */
    const obs_logscore_snap_t* mp_snap_logscore;

    /**
     * Handle to Hi-C correlation snapshot observer
     */
    obs_corr_hic_snap_t* mp_snap_corr_hic;

    /**
     * Set of registered reactors
     */
    std::vector<map_reactor_t*> m_reactors;

    /**
     * Iteration at which to begin MAP checking
     */
    cmxUInt m_start_iter;

    /**
     * MAP estimates are only recorded conditional on minimal Hi-C correlation
     *  threshold
     */
    cmxReal m_min_corr_hic;

    /**
     * Maximum observed value of tracked parameter at each MCMC chain
     */
    std::vector<cmxReal> m_maps;

    /**
     * Iteration at which MAP estimate was captured at each MCMC chain
     */
    std::vector<cmxUInt> m_iters;
};

/**
 * Remove pre-processor define (make it "private")
 */
#undef CMX_OBS_LOGSCORE_MAPC_INVALID_ITER

#endif  // cmxObsLogscoreMapc_h
