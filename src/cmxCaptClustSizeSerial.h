//****************************************************************************
// cmxCaptClustSizeSerial.h
//****************************************************************************

/**
 * @brief Serial (non-parallel) batch capture of model cluster sizes
 */

#ifndef cmxCaptClustSizeSerial_h
#define cmxCaptClustSizeSerial_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxCaptClustSizeTypes.h"
#include "cmxGlue.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxCaptClustSizeSerial
//****************************************************************************

/**
 * Performs batched capture of model cluster sizes (serial version)
 */
template <typename t_Glue>
class cmxCaptClustSizeSerial {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Capture sorted cluster sizes at each simulated model
     * @param out - vector of output sorted cluster sizes, will correspond to
     *  each simulated model
     * @param sim - Parent simulation
     */
    static void capture(std::vector<cmxClustSizePqueue_t>& out,
                        const sim_t& sim CMX_THREAD_ID_PARAM) {
        const std::vector<model_t>& models = sim.get_models();
        const size_t NUM_MODELS = models.size();
        out.resize(NUM_MODELS);
        cmxAssertPosEq(models.size(), out.size());
        // Iterate over models
        for (size_t i_model = 0; i_model < NUM_MODELS; ++i_model) {
            const model_t& model = models[i_model];
            cmxClustSizePqueue_t& pqueue = out[i_model];
            cmx_clust_size_fill(pqueue, model, sim);
        }  // End iteration over models
    }
};

#endif  // cmxCaptClustSizeSerial_h
