//****************************************************************************
// cmxTestGlobalInit.h
//****************************************************************************

/**
 * Commandlet for testing initialization of global data. This serves as the
 * basic reference template for writing a minimal application.
 *
 * Usage:
 * -test_global_init |optional configuration params|
 */

#ifdef cmxTestGlobalInit_h
#   error "Test Global Init included multiple times!"
#endif  // cmxTestGlobalInit_h
#define cmxTestGlobalInit_h

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_TESTS

#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxGlobals.h"
#include "cmxStats.h"

/**
 * Entry point for test
 */
int cmxTestGlobalInitMain(const cmxCmdOptsMap& cmd_opts) {
    // Create configuration
    cmxSpConfig_t config = cmxSmartPtr::make_shared<cmxConfig>();
    // Parse command line and any INI configuration (override defaults)
    config->init(cmd_opts);

    // Output configuration
    cmxLogf("Testing global initialization: configuration set to:\n");
    config->print();

    // Initialize globals
    cmxLogf("Testing global initialization: calling init...\n");
    cmxG::init(config);

    CMX_LOG_NUM_WORKER_THREADS;

    /**
     * Do application work here!
     */

    CMX_STATS_REPORT;

    // Destroy globals
    cmxLogf("Testing global initialization: calling teardown...\n");
    cmxG::teardown();

    // We consider it a success if we make it here without crashing!
    cmxLogf("Testing global initialization: test finished.\n");
    return cmxExitCode_normal;
}

#endif  // CMX_BUILD_ENABLE_TESTS
