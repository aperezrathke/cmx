//****************************************************************************
// cmxNullSharcoMu.h
//****************************************************************************

/**
 * @brief Minimal null interface for a mutable shared resource component
 *
 * Sharco == SHARed COmponent
 */

#ifndef cmxNullSharcoMu_h
#define cmxNullSharcoMu_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"

//****************************************************************************
// cmxNullSharcoMu
//****************************************************************************

/**
 * Null shared mutable data policy, resides at simulation level.
 *
 * The minimal interface expected for a shared mutable policy is:
 *
 * template <typename sim_t>
 * void init(cmxSpConstConfig_t config, const sim_t& sim) {...}
 *
 * void clear() {...}
 */
class cmxNullSharcoMu {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     * @param sim - outer simulation
     */
    template <typename sim_t>
    void init(cmxSpConstConfig_t config, const sim_t& sim) {}

    /**
     * Clear shared buffer(s)
     */
    void clear() {}
};

#endif  // cmxNullSharcoMu
