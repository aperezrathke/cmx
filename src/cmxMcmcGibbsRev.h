//****************************************************************************
// cmxMcmcGibbsRev.h
//****************************************************************************

/**
 * @brief Gibbs sampler that is reversible
 */

#ifndef cmxMcmcGibbsRev_h
#define cmxMcmcGibbsRev_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxGlobals.h"
#include "cmxRand.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxMcmcGibbs
//****************************************************************************

/**
 * Gibbs sampler satisfying detailed balance (chain is reversible)! Methods
 * assume the templated model provides the following interface:
 *
 *  t_model::Gibbs_get_num_random() - returns number of random variables that
 *      can be sampled
 *  t_model::Gibbs_update(cmxUInt v) - performs the Gibbs posterior update for
 *      the random variable with identifier 'v'
 *
 *  t_model::Gibbs_args_t - Any model-specific arguments to pass to the
 *      t_model::Gibbs*() interface (passed by value!)
 */
class cmxMcmcGibbsRev {
public:
    /**
     * Run sampler for a single step.
     * @param model - the model to be sampled
     * @param gibbs_args - model-specific Gibbs sampler arguments
     */
    template <typename t_model>
    inline static void step(t_model& model,
                            const typename t_model::Gibbs_args_t gibbs_args
                                CMX_THREAD_ID_PARAM) {
        /**
         * Since we have equal chance of selecting each random variable at
         * every time step, the chain is reversible. This is in contrast to
         * traditional Gibbs sampling which block iterates over each variable.
         * Some authors claim to have better mixing with this technique. See:
         *
         *  Sudderth, Erik Blaine. "Graphical models for visual object
         *  recognition and tracking." PhD diss., Massachusetts Institute of
         *  Technology, 2006.

         *  which on pg. 86 states:
         *
         *      "Randomly permuting the order in which variables are
         *       resampled, rather than repeating a single fixed order, often
         *       improves the rate of convergence [246]."
         *
         * and reference [246] is the following:
         *
         *  Roberts, Gareth O., and Sujit K. Sahu. "Updating schemes,
         *  correlation structure, blocking and parameterization for the Gibbs
         *  sampler." Journal of the Royal Statistical Society: Series B
         *  (Statistical Methodology) 59, no. 2 (1997): 291-317.
         *
         * See also this article showing how detailed balance is satisfied:
         * https://en.wikipedia.org/wiki/Gibbs_sampling#Mathematical_background
         *
         * This reversibility is required to formulate Gibbs sampling as a
         * special case of Metropolis-Hastings, see:
         *
         *  Geyer, Charles J. "Markov chain Monte Carlo lecture notes."
         *  Course notes, Spring Quarter (1998).
         */
        cmxAssert(model.Gibbs_get_num_random() > CMX_TO_UINT(0));
        cmxRand& rng = cmxRng;
        const cmxUInt v = rng.unif_int(
            CMX_TO_UINT(0), CMX_TO_UINT(model.Gibbs_get_num_random() - 1));
        cmxAssertBounds(v, 0, model.Gibbs_get_num_random());
        model.Gibbs_update(v, gibbs_args CMX_THREAD_ID_ARG);
    }

    /**
     * Run model for a fixed number of steps (no thinning)
     * @param n_steps - number of steps to run Gibbs sampler
     * @param model - the model to be sampled
     * @param gibbs_args - model-specific Gibbs sampler arguments
     * @param rng - random number generator
     */
    template <typename t_model>
    inline static void run(const cmxUInt n_steps,
                           t_model& model,
                           const typename t_model::Gibbs_args_t gibbs_args
                               CMX_THREAD_ID_PARAM) {
        for (cmxUInt i = 0; i < n_steps; ++i) {
            cmxMcmcGibbsRev::step(model, gibbs_args CMX_THREAD_ID_ARG);
        }
    }

private:
    // Disallow instantiation, copy, and assignment for now
    cmxMcmcGibbsRev();
    cmxMcmcGibbsRev(const cmxMcmcGibbsRev&);
    cmxMcmcGibbsRev& operator=(const cmxMcmcGibbsRev&);
};

#endif  // cmxMcmcGibbsRev_h
