//****************************************************************************
// cmxContactFreqsSharcoRo.h
//****************************************************************************

/**
 * @brief Read-only shared data for contact frequencies (e.g. observed Hi-C)
 */

#ifndef cmxContactFreqsSharcoRo_h
#define cmxContactFreqsSharcoRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxLogf.h"
#include "cmxModelTypes.h"
#include "cmxOpts.h"
#include "cmxTypes.h"

#include <string>

//****************************************************************************
// cmxContactFreqsSharcoRo
//****************************************************************************

/**
 * Contact frequencies read-only shared resources
 */
class cmxContactFreqsSharcoRo {
public:
    /**
     * Utility for loading Hi-C frequencies, will exit program if step fails
     * @param config - user configuration
     * @param out_hic - output Hi-C frequencies
     */
    static void load_hic_exit_on_fail(const cmxSpConstConfig_t config,
                                      cmxContactFreqs_t& out_hic) {
        out_hic.clear();
        // Read Hi-C data path
        std::string hic_path;
        if (!config->resolve_path(hic_path, cmxOpt_hic_path)) {
            cmxLogf(
                "Error: path to Hi-C frequencies not specified. See %s for "
                "usage. Exiting\n",
                cmxOpt_get_cmd_switch(cmxOpt_help));
            exit(cmxExitCode_invalid_hic);
        }
        // Load plain-text Hi-C frequencies
        const bool b_load = out_hic.load(hic_path, cmxMatrixUtils::raw_ascii);
        if (!b_load || (out_hic.n_elem < 2)) {
            cmxLogf(
                "Error: unable to load Hi-C frequencies from file %s or not "
                "enough elements. Exiting\n",
                hic_path.c_str());
            exit(cmxExitCode_invalid_hic);
        }
        // Validate Hi-C frequencies in [0,1]
        for (cmxMatSz_t i = 0; i < out_hic.n_elem; ++i) {
            if ((out_hic.at(i) < CMX_TO_REAL(0.0)) ||
                (out_hic.at(i) > CMX_TO_REAL(1.0))) {
                cmxLogf("Error: Hi-C frequency %f not in [0,1]. Exiting.\n",
                        ((double)out_hic.at(i)));
                exit(cmxExitCode_invalid_hic);
            }
        }
    }

    /**
     * Initialize from user configuration
     * @param config - user options
     */
    void init(const cmxSpConstConfig_t config) {
        load_hic_exit_on_fail(config, m_hic);
    }

    /**
     * Reset read-only state
     */
    void clear() { m_hic.clear(); }

    /**
     * @return Observed Hi-C contact frequencies
     */
    inline const cmxContactFreqs_t& get_hic() const { return m_hic; }

private:
    /**
     * Observed Hi-C frequencies
     */
    cmxContactFreqs_t m_hic;
};

#endif  // cmxContactFreqsSharcoRo_h
