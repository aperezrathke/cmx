//****************************************************************************
// cmxObsBnetSpopRing.h
//****************************************************************************

/**
 * @brief Ring-buffered trace observer for single population Bayesian network
 *  modeling. The ring buffer will overwrite old data if capacity is exceeded.
 */

#ifndef cmxObsBnetSpopRing_h
#define cmxObsBnetSpopRing_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetDagTypes.h"
#include "cmxExitCodes.h"
#include "cmxExportBnet.h"
#include "cmxExportCore.h"
#include "cmxFilesystem.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObs.h"
#include "cmxObsBnetSpopSnap.h"

#include <boost/circular_buffer.hpp>
#include <fstream>
#include <string>

//****************************************************************************
// cmxObsBnetSpopRing
//****************************************************************************

/**
 * Ring observer - captures history at a regular, user-defined interval, will
 *  overwrite old data (and possibly flush buffer to disk) when capacity
 *  exceeded. Depends on snapshot observer
 */
template <typename t_Glue>
class cmxObsBnetSpopRing : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Snapshot observer type
     */
    typedef cmxObsBnetSpopSnap<t_Glue> obs_bnet_spop_snap_t;

    /**
     * Defaults
     */
    enum { default_period = 1, default_start_iter = 1, default_capacity = 500 };

    /**
     * Default constructor
     */
    cmxObsBnetSpopRing() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        cmxAssert(sim.get_shared_ro().get_Clust().get_mix_size() ==
                  CMX_TO_UINT(1));
        // Reset
        this->clear();

        // Extract snapshot observer - must occur before tracer observer
        if (!obs_t::find_raw(&mp_snap, sim, index)) {
            cmxLogf(
                "Error: Bayesian network (single-population) ring observer "
                "unable to find handle to snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);

        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_bnet_ring_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Read start iteration
        config->read_into(m_start_iter, cmxOpt_obs_bnet_ring_start_iter);
        // Read ring buffer capacities (max # of elements per buffer)
        config->read_into(m_capacity, cmxOpt_obs_bnet_ring_capacity);
        // Ensure capacity is positive
        m_capacity = std::max(CMX_TO_UINT(1), m_capacity);

        // Read export flags
        read_flag(
            mb_export_src2dst, config, cmxOpt_obs_bnet_ring_export_src2dst);
        read_flag(
            mb_export_dst2src, config, cmxOpt_obs_bnet_ring_export_dst2src);
        read_flag(mb_export_order, config, cmxOpt_obs_bnet_ring_export_order);
        read_flag(mb_export_cpt, config, cmxOpt_obs_bnet_ring_export_cpt);

        // Read trace flags
        read_flag(mb_trac_src2dst,
                  config,
                  cmxOpt_obs_bnet_ring_trac_src2dst,
                  mb_export_src2dst);
        read_flag(mb_trac_dst2src,
                  config,
                  cmxOpt_obs_bnet_ring_trac_dst2src,
                  mb_export_dst2src);
        read_flag(mb_trac_order,
                  config,
                  cmxOpt_obs_bnet_ring_trac_order,
                  mb_export_order);
        read_flag(
            mb_trac_cpt, config, cmxOpt_obs_bnet_ring_trac_cpt, mb_export_cpt);

        // Allocate history buffers
        const size_t num_bnets = mp_snap->get_num_bnets();
        cmxAssertPosEq(num_bnets, sim.get_num_models());
        cmxAssert(m_capacity > CMX_TO_UINT(0));
        alloc_hist(m_src2dsts, num_bnets, m_capacity);
        alloc_hist(m_dst2srcs, num_bnets, m_capacity);
        alloc_hist(m_orders, num_bnets, m_capacity);
        alloc_hist(m_cpts, num_bnets, m_capacity);
        m_iters.set_capacity(m_capacity);
    }

    /**
     * Reset to default state
     */
    void clear() {
        mp_snap = NULL;
        m_period = default_period;
        m_start_iter = default_start_iter;
        m_capacity = default_capacity;
        m_head = CMX_TO_UINT(0);

        mb_trac_src2dst = cmxTRUE;
        mb_trac_dst2src = cmxTRUE;
        mb_trac_order = cmxFALSE;
        mb_trac_cpt = cmxFALSE;

        mb_export_src2dst = mb_trac_src2dst;
        mb_export_dst2src = mb_trac_dst2src;
        mb_export_order = mb_trac_order;
        mb_export_cpt = mb_trac_cpt;

        m_src2dsts.clear();
        m_dst2srcs.clear();
        m_orders.clear();
        m_cpts.clear();
        m_iters.clear();
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, records history of
     * current model state for tracked parameter
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        cmxAssertBoundsInc(m_head, CMX_TO_UINT(0), m_capacity);
        if ((iter < m_start_iter) || ((iter % m_period) != CMX_TO_UINT(0))) {
            // Early out if not yet at sampling interval
            return;
        }

        // If we've reached capacity, check if we need to export some history
        // before it starts to get overwritten
        if (m_head == m_capacity) {
            this->export_hist_all(sim);
            // Reset head
            m_head = CMX_TO_UINT(0);
        }

        // Obtain handle to latest snapshot
        cmxAssert(mp_snap);
        const std::vector<cmxBnetBool>& bnets = mp_snap->get_bnets();
        const size_t num_bnets = bnets.size();

        // Store iteration
        m_iters.push_back(iter);

        // Store src2dst
        if (mb_trac_src2dst) {
            cmxAssertPosEq(num_bnets, m_src2dsts.size());
            for (size_t i = 0; i < num_bnets; ++i) {
                m_src2dsts[i].push_back(bnets[i].get_dag().get_src2dst_map());
            }
        }

        // Store dst2src
        if (mb_trac_dst2src) {
            cmxAssertPosEq(num_bnets, m_dst2srcs.size());
            for (size_t i = 0; i < num_bnets; ++i) {
                m_dst2srcs[i].push_back(bnets[i].get_dag().get_dst2src_map());
            }
        }

        // Store order
        if (mb_trac_order) {
            cmxAssertPosEq(num_bnets, m_orders.size());
            for (size_t i = 0; i < num_bnets; ++i) {
                m_orders[i].push_back(bnets[i].get_order());
            }
        }

        // Store cpt
        if (mb_trac_cpt) {
            cmxAssertPosEq(num_bnets, m_cpts.size());
            for (size_t i = 0; i < num_bnets; ++i) {
                m_cpts[i].push_back(bnets[i].get_cpts());
            }
        }

        // Update head
        ++m_head;
    }

    /**
     * Callback for when simulation has finished running, writes trace history
     * to disk
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        cmxAssertBoundsInc(m_head, CMX_TO_UINT(0), m_capacity);
        if (m_iters.empty() || !(mb_export_src2dst || mb_export_dst2src ||
                                 mb_export_order || mb_export_cpt)) {
            // Early out if user did not specify file path or no export flags
            // enabled
            return;
        }

        this->export_hist_all(sim, cmxTRUE /*should_log*/);
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_bnet_ring)

private:
    /**
     * Initialize flag from user configuration
     */
    static inline void read_flag(cmxBool& flag,
                                 cmxSpConstConfig_t config,
                                 const enum cmxOptE eopt,
                                 const cmxBool depend = cmxFALSE) {
        config->read_into(flag, eopt);
        flag = (cmxFALSE != flag) || depend;
    }

    /**
     * Utility for allocating vector of ring buffers
     */
    template <typename t_vec>
    static inline void alloc_hist(t_vec& hist,
                                  const size_t outer,
                                  const size_t inner) {
        cmxAssert(outer > 0);
        cmxAssert(inner > 0);
        hist.resize(outer);
        for (size_t i = 0; i < outer; ++i) {
            hist[i].set_capacity(inner);
        }
    }

    /**
     * Export ring history buffers to disk
     */
    void export_hist_all(const sim_t& sim,
                         const cmxBool should_log = cmxFALSE) const {
        cmxAssertBoundsInc(m_head, CMX_TO_UINT(0), m_capacity);
        cmxSpConstConfig_t config = sim.get_config();
        if (m_head > CMX_TO_UINT(0)) {
            export_src2dst(config, should_log);
            export_dst2src(config, should_log);
            export_order(config, should_log);
            export_cpt(config, should_log);
        }
    }

    /**
     * Export ring history for Bayesian network source-to-destination edge
     *  mappings
     */
    void export_src2dst(cmxSpConstConfig_t config,
                        const cmxBool should_log) const {
        if (!mb_export_src2dst) {
            return;
        }
        cmxAssert(mb_trac_src2dst);
        if (should_log) {
            cmxLogf("Exporting Bayesian network ring history: src2dst\n");
        }
        const std::string subdir_csv("src2dst.csv");
        const std::string subdir_gv("src2dst.gv");
        const std::string file_id("bn.s2d");
        export_hist(m_src2dsts,
                    config,
                    subdir_csv,
                    file_id,
                    cmxExport::bnet_dag_adjmap_csv_t());
        export_hist(m_src2dsts,
                    config,
                    subdir_gv,
                    file_id,
                    cmxExport::bnet_dag_adjmap_gv_t());
    }

    /**
     * Export ring history for Bayesian network destination-to-source edge
     *  mappings
     */
    void export_dst2src(cmxSpConstConfig_t config,
                        const cmxBool should_log) const {
        if (!mb_export_dst2src) {
            return;
        }
        cmxAssert(mb_trac_dst2src);
        if (should_log) {
            cmxLogf("Exporting Bayesian network ring history: dst2src\n");
        }
        const std::string subdir("dst2src");
        const std::string file_id("bn.d2s");
        export_hist(m_dst2srcs,
                    config,
                    subdir,
                    file_id,
                    cmxExport::bnet_dag_adjmap_csv_t());
    }

    /**
     * Export ring history for Bayesian network topological sort orderings
     */
    void export_order(cmxSpConstConfig_t config,
                      const cmxBool should_log) const {
        if (!mb_export_order) {
            return;
        }
        cmxAssert(mb_trac_order);
        if (should_log) {
            cmxLogf("Exporting Bayesian network ring history: order\n");
        }
        const std::string subdir("order");
        const std::string file_id("bn.ord");
        export_hist(
            m_orders, config, subdir, file_id, cmxExport::bnet_order_csv_t());
    }

    /**
     * Export ring history for Bayesian network CPTs
     */
    void export_cpt(cmxSpConstConfig_t config, const cmxBool should_log) const {
        if (!mb_export_cpt) {
            return;
        }
        cmxAssert(mb_trac_cpt);
        if (should_log) {
            cmxLogf("Exporting Bayesian network ring history: cpt");
        }
        const std::string subdir("cpt");
        const std::string file_id("bn.cpt");
        export_hist(
            m_cpts, config, subdir, file_id, cmxExport::bnet_cpt_csv_t());
    }

    /**
     * Workhorse utility for exporting a ring-buffered history
     */
    template <typename t_vec, typename t_exporter>
    void export_hist(const t_vec& hist,
                     cmxSpConstConfig_t config,
                     const std::string& subdir,
                     const std::string& file_id,
                     const t_exporter expcb) const {
        cmxAssert(m_head > CMX_TO_UINT(0));
        cmxAssert(m_head <= m_iters.size());
        const size_t num_models = hist.size();
        const std::string ext(expcb.get_ext());
        for (size_t i = 0; i < num_models; ++i) {
            const std::string fprefix =
                cmxExport::get_fpath(config, i, subdir, file_id, "" /*ext*/);
            cmxAssert(m_head <= hist[i].size());
            std::string fpath;
            for (cmxUInt j = 0; j < m_head; ++j) {
                const cmxUInt iter = m_iters[j];
                fpath = fprefix + "." + cmx2Str(iter) + "." + ext;
                expcb(hist[i][j], fpath);
            }
        }
    }

    /**
     * Handle to snapshot observer - raw pointer is used as lifetime of
     * pointer does not exceed that of parent simulation's shared_ptr
     */
    const obs_bnet_spop_snap_t* mp_snap;

    /**
     * History is recorded on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Iteration at which to begin recording history
     */
    cmxUInt m_start_iter;

    /**
     * Maximum size of circular (i.e. ring) buffers
     */
    cmxUInt m_capacity;

    /**
     * Location of next write, used for keeping track of when to flush buffers
     *  to disk and for keeping track of which data has already been flushed
     */
    cmxUInt m_head;

    /**
     * Boolean fields for toggling whether a Bayesian network component is
     *  tracked or not
     */
    cmxBool mb_trac_src2dst;
    cmxBool mb_trac_dst2src;
    cmxBool mb_trac_order;
    cmxBool mb_trac_cpt;

    /**
     * Boolean fields for toggling whether a Bayesian network component is
     *  exported or not
     */
    cmxBool mb_export_src2dst;
    cmxBool mb_export_dst2src;
    cmxBool mb_export_order;
    cmxBool mb_export_cpt;

    /**
     * Circular (i.e. ring) history buffers for each simulation model
     */
    std::vector<boost::circular_buffer<cmxBnetDagAdjMap_t> > m_src2dsts;
    std::vector<boost::circular_buffer<cmxBnetDagAdjMap_t> > m_dst2srcs;
    std::vector<boost::circular_buffer<std::vector<cmxUInt> > > m_orders;
    std::vector<boost::circular_buffer<std::vector<cmxBnetBoolCt> > > m_cpts;

    /**
     * Track which iterations were recorded, all circular history buffers
     * should be parallel to this buffer
     */
    boost::circular_buffer<cmxUInt> m_iters;
};

#endif  // cmxObsCorrHicTrac_h
