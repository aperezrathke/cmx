//****************************************************************************
// cmxObsPaccBnetCons.h
//****************************************************************************

/**
 * @brief Console observer for acceptance probability of Bayesian network
 *  model components
 */

#ifndef cmxObsPaccBnetCons_h
#define cmxObsPaccBnetCons_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObs.h"
#include "cmxOpts.h"

//****************************************************************************
// cmxObsPaccBnetCons
//****************************************************************************

/**
 * Bayesian network acceptance probability console observer - reports to
 * stdout at a regular, user-defined interval.
 */
template <typename t_Glue>
class cmxObsPaccBnetCons : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Default console period
     */
    enum { default_period = 1 };

    /**
     * Default constructor
     */
    cmxObsPaccBnetCons() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_pacc_bnet_cons_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
    }

    /**
     * Reset to default state
     */
    void clear() { m_period = default_period; }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, reports to stdout
     * if iteration matches period
     *
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if ((iter % m_period) != CMX_TO_UINT(0)) {
            // Early out if not yet at sampling interval
            return;
        }
        // Handle to simulated models
        const std::vector<model_t>& models = sim.get_models();
        const size_t NUM_MODELS = models.size();
        // Report acceptance probability at largest cluster for each model
        for (size_t i_model = 0; i_model < NUM_MODELS; ++i_model) {
            const model_t& model = models[i_model];
            const cmxRunStat_t& stats = model.get_Bnet().get_stats();
            cmxLogf("(ITER %d, MODEL %d) BNET PACC = %f\n",
                    (int)iter,
                    (int)i_model,
                    (double)stats.mean());
        }
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_pacc_bnet_cons)

private:
    /**
     * Reports to stdout on multiples of this period
     */
    cmxUInt m_period;
};

#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

#endif  // cmxObsPaccBnetCons_h
