//****************************************************************************
// cmxPseudopopSharcoRo.h
//****************************************************************************

/**
 * @brief Read-only shared data for pseudo-population.
 */

#ifndef cmxPseudopopSharcoRo_h
#define cmxPseudopopSharcoRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxExitCodes.h"
#include "cmxLogf.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxPseudopopSharcoRo
//****************************************************************************

/**
 * Pseudo-population read-only shared resources
 */
class cmxPseudopopSharcoRo {
public:
    /**
     * Default constructor
     */
    cmxPseudopopSharcoRo() { this->clear(); }

    /**
     * Initialize from user configuration
     * @param config - user options
     */
    void init(const cmxSpConstConfig_t config) {
        // Reset to default state
        this->clear();
        // Configure pseudo-population size
        m_pseudopop_size = cmxConfigUtils::get_pseudopop_size(config);
        cmxAssert(m_pseudopop_size > CMX_TO_UINT(0));
        // Configure pseudo-population block size
        m_pseudopop_block_size =
            cmxConfigUtils::get_pseudopop_block_size(config);
        cmxAssertBoundsInc(
            m_pseudopop_block_size, CMX_TO_UINT(1), m_pseudopop_size);
        // Configure pseudo-population Polya urn sampler
        config->read_into(m_pseudopop_polya_alpha,
                          cmxOpt_pseudopop_polya_alpha);
        config->read_into(m_pseudopop_polya_delta,
                          cmxOpt_pseudopop_polya_delta);
        // Configure pseudo-population Bpoly sampler
        config->read_into(m_pseudopop_bpoly_corr_thresh,
                          cmxOpt_pseudopop_bpoly_corr_thresh);
        if (!this->check_polya()) {
            cmxLogf(
                "Error: Pseudo-population Polya urn must have positive alpha "
                "and non-negative ball count on final draw! Exiting.\n");
            exit(cmxExitCode_invalid_pseudopop_polya);
        }
    }

    /**
     * Reset read-only state
     */
    void clear() {
        m_pseudopop_size = CMX_DEFAULT_PSEUDOPOP_SIZE;
        m_pseudopop_block_size = CMX_DEFAULT_PSEUDOPOP_BLOCK_SIZE;
        m_pseudopop_polya_alpha = CMX_DEFAULT_PSEUDOPOP_POLYA_ALPHA;
        m_pseudopop_polya_delta = CMX_DEFAULT_PSEUDOPOP_POLYA_DELTA;
        m_pseudopop_bpoly_corr_thresh = CMX_DEFAULT_PSEUDOPOP_BPOLY_CORR_THRESH;
    }

    /**
     * @return Number of pseudo-samples within pseudo-population
     */
    inline cmxUInt get_pseudopop_size() const {
        cmxAssert(m_pseudopop_size > CMX_TO_UINT(0));
        return m_pseudopop_size;
    }

    /**
     * @return The fixed size of the random subpopulation of each MCMC proposal
     */
    inline cmxUInt get_pseudopop_block_size() const {
        cmxAssertBoundsInc(
            m_pseudopop_block_size, CMX_TO_UINT(1), this->get_pseudopop_size());
        return m_pseudopop_block_size;
    }

    /**
     * @return Initial number of balls in pseudo-population Polya urn sampler
     */
    inline cmxReal get_pseudopop_polya_alpha() const {
        cmxAssert(this->check_polya());
        return m_pseudopop_polya_alpha;
    }

    /**
     * @return Incremental number of balls added in pseudo-population Polya
     *  urn sampler
     */
    inline cmxReal get_pseudopop_polya_delta() const {
        cmxAssert(this->check_polya());
        return m_pseudopop_polya_delta;
    }

    /**
     * @return When below this correlation, Polya urn proposal is used
     */
    inline cmxReal get_pseudopop_bpoly_corr_thresh() const {
        return m_pseudopop_bpoly_corr_thresh;
    }

private:
    /**
     * @return FALSE if Polya urn is guaranteed to have a non-positive ball
     *  count on final draw, TRUE o/w
     */
    inline bool check_polya() const {
        bool is_valid = false;
        cmxVerify(is_valid = (m_pseudopop_polya_alpha > CMX_TO_REAL(0.0)));
        if (is_valid) {
            // Number of draws from urn
            const cmxReal num_draws = CMX_TO_REAL(this->get_pseudopop_size());
            // Number of balls added to urn by final draw
            const cmxReal tot_delta =
                ((num_draws - CMX_TO_REAL(1.0)) * m_pseudopop_polya_delta);
            cmxAssert(m_pseudopop_polya_alpha > CMX_TO_REAL(0.0));
            // Number of balls in urn on final draw
            const cmxReal fin_count = m_pseudopop_polya_alpha + tot_delta;
            cmxVerify(is_valid = fin_count > CMX_TO_REAL(0.0));
        }
        return is_valid;
    }

    /**
     * Size of pseudo-population
     */
    cmxUInt m_pseudopop_size;

    /**
     * The fixed size of the random subpopulation of each MCMC proposal
     */
    cmxUInt m_pseudopop_block_size;

    /**
     * When sampling pseudo-population bit matrix via Polya urn, this defines
     * the initial number of balls (positive real) in the urn
     */
    cmxReal m_pseudopop_polya_alpha;

    /**
     * When sampling pseudo-population bit matrix via Polya urn, this defines
     * the incremental number of balls added (any real value) to the urn of
     * the selected color after each draw. Note the following:
     *      delta == -1 -> hypergeometric distribution (sample w/o replace)
     *      delta == 0 -> binomial or bernoulli (sample w/ replace)
     *      delta == 1 -> beta-bernoulli
     */
    cmxReal m_pseudopop_polya_delta;

    /**
     * Cutoff correlation threshold when using a Bpoly proposal. If current
     *  pseudo-population correlation is below this threshold, then the
     *  proposal is via a Polya urn, else it is forward sampled from the
     *  parent Bayesian network(s)
     */
    cmxReal m_pseudopop_bpoly_corr_thresh;
};

#endif  // cmxPseudopopSharcoRo_h
