//****************************************************************************
// cmxBnetBoolCptSharcoMu.h
//****************************************************************************

/**
 * @brief Mutable shared resources used by boolean CPTs. Assumes each thread
 *  will be allocated a separate instance of this class!
 */

/**
 * Note the following design framework:
 *  - Shared mutable (i.e. writable) data buffers should be replicated across
 *      threads (thread-local storage - TLS) to avoid race conditions
 *  - Shared read-only (i.e. non-writable|non-mutable) data should be stored
 *      as single instance at simulation level to avoid unnecessary
 *      duplication across threads and therefore conserve memory
 */

#ifndef cmxBnetBoolCptSharcoMu_h
#define cmxBnetBoolCptSharcoMu_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxBnetBoolCt.h"
#include "cmxConfig.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetBoolCptSharcoMu
//****************************************************************************

/**
 * Shared mutable data resources for boolean conditional probability tables,
 * client must restrict access to only a single thread
 */
class cmxBnetBoolCptSharcoMu {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     * @param sim - outer simulation
     */
    template <typename sim_t>
    void init(cmxSpConstConfig_t config, const sim_t& sim) {}

    /**
     * Clear shared buffer(s)
     */
    void clear() { m_cpt_counts_buf.clear(); }

    // Accessors

    /**
     * @return mutable vector of conditional count tables
     */
    inline std::vector<cmxBnetBoolCt>& get_cpt_counts_buf() const {
        return m_cpt_counts_buf;
    }

private:
    /**
     * Scratch buffer used for tabulating pseudo-population counts
     */
    mutable std::vector<cmxBnetBoolCt> m_cpt_counts_buf;
};

#endif  // cmxBnetBoolCptSharedMu_h
