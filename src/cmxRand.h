//****************************************************************************
// cmxRand.h
//****************************************************************************

/**
 * Utility for generating random numbers
 */

#ifndef cmxRand_h
#define cmxRand_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxLogf.h"
#include "cmxTypes.h"

// Make sure to initialize and expose random API from matrix library
#include "cmxArmadillo.h"

// If C++11 is not supported, default to boost implementation
#ifdef CMX_BUILD_CXX_11
#   include <random>
#   define cmxRandom std
#else
#   include <boost/random.hpp>
#   include <boost/random/random_device.hpp>
#   define cmxRandom boost::random
#endif  // CMX_BUILD_CXX_11

//****************************************************************************
// Interface
//****************************************************************************

/**
 * Interface for uniform random number generation
 * @TODO - add functions for generating random vectors and matrices as needed
 * @TODO - investigate SIMD implementation of Mersenne twister:
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/index.html
 *  (SIMD-oriented Fast Mersenne Twister (SFMT))
 */
class cmxRand {
public:
    /**
     * Default constructor - initializes RNG engine
     */
    cmxRand() : m_eng(get_seed()) {
        // Note: thread_local must be supported in order to have Mersenne
        // twister support within arma
        cmxMatrixUtils::arma_rng::set_seed(get_seed());
    }

    /**
     * @return Uniform random number in [0.0, 1.0)
     */
    inline cmxReal unif_real() { return m_unif_real_dis(m_eng); }

    /**
     * @return Uniform random number in [lower_, upper_)
     */
    inline cmxReal unif_real(const cmxReal lower_, const cmxReal upper_) {
        cmxAssert(lower_ <= upper_);
        cmxRandom::uniform_real_distribution<cmxReal> dis(lower_, upper_);
        return dis(m_eng);
    }

    /**
     * @return Integer in [lower_, upper_] inclusive
     */
    template <typename t_int>
    inline t_int unif_int(const t_int lower_, const t_int upper_) {
        cmxAssert(lower_ <= upper_);
        cmxRandom::uniform_int_distribution<t_int> dis(lower_, upper_);
        return dis(m_eng);
    }

    /**
     * @param dis - distribution object
     * @return sampled value from arbitrary distribution
     */
    template <typename t_dis>
    inline typename t_dis::result_type sample(t_dis& dis) {
        return dis(m_eng);
    }

    /**
     * @HACK - Force Intel compilers to seed RNG - certain versions of the
     *  Intel compiler do not appear to seed properly via constructor
     *  initializer list => expose reseed method to force compiler to register
     *  seed call!
     */
    void reseed() { m_eng.seed(get_seed()); }

private:
    // According to:
    // https://channel9.msdn.com/Events/GoingNative/2013/rand-Considered-Harmful
    // Copying the random number engine is expensive as Mersenne twister has a
    // large internal state. It's "okay" to copy the distribution object, but
    // definitely avoid copying the engine object.

    // It would be nice to forbid (make private) the copy constructor and
    // assignment operator; however, our thread local storage implementation
    // requires them. (It's recommended to have 1 instance per thread). So,
    // for now, we are not forbidding implicit copy and assignment operations.
    typedef cmxRandom::random_device::result_type seed_type;

    /**
     * @return Seed to initialize RNG
     */
    static seed_type get_seed() {
#ifdef CMX_BUILD_SEED_RAND
        static cmxRandom::random_device rd;
        const seed_type seed = rd();
#else
        const seed_type seed = 7331;
#endif  // CMX_BUILD_SEED_RAND
        cmxLogf("Random seed: %u\n", seed);
        return seed;
    }

    /**
     * Typedef the random number engine
     */
    typedef cmxRandom::mt19937 reng_t;

    /**
     * Engine which generates the random numbers
     */
    reng_t m_eng;

    /**
     * Uniform distribution on [0,1) interval
     */
    cmxRandom::uniform_real_distribution<cmxReal> m_unif_real_dis;
};

#endif  // cmxRand_h
