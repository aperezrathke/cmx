//****************************************************************************
// cmxObsCorrHicCons.h
//****************************************************************************

/**
 * @brief Console observer for model empirical Hi-C correlation. Console
 *  observers report values to stdout and typically rely on Snap ('Snapshot')
 *  observers.
 */

#ifndef cmxObsCorrHicCons_h
#define cmxObsCorrHicCons_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObsCorrHicSnap.h"
#include "cmxSmartPtr.h"

//****************************************************************************
// cmxObsCorrHicCons
//****************************************************************************

/**
 * Hi-C correlation console observer - reports correlation to stdout at a
 * regular, user-defined interval. Depends on snapshot observer
 */
template <typename t_Glue>
class cmxObsCorrHicCons : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Snapshot observer type
     */
    typedef cmxObsCorrHicSnap<t_Glue> obs_corr_hic_snap_t;

    /**
     * Default console period
     */
    enum { default_period = 1 };

    /**
     * Default constructor
     */
    cmxObsCorrHicCons() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_corr_hic_cons_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Extract snapshot observer - must occur before console observer
        if (!obs_t::find_raw(&mp_snap, sim, index)) {
            cmxLogf(
                "Error: Hi-C correlation console observer unable to find "
                "handle to snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_period = default_period;
        mp_snap = NULL;
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, reports to stdout
     * for all models if iteration matches period
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if ((iter % m_period) != CMX_TO_UINT(0)) {
            // Early out if not yet at sampling interval
            return;
        }
        // Report latest snapshot to stdout
        cmxAssert(mp_snap);
        const std::vector<cmxReal>& corrs = mp_snap->get_corrs();
        const size_t num_corrs = corrs.size();
        for (size_t i = 0; i < num_corrs; ++i) {
            cmxLogf("(ITER %d, MODEL %d) HI-C CORR = %f\n",
                    (int)iter,
                    (int)i,
                    (double)(corrs[i]));
        }
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_corr_hic_cons)

private:
    /**
     * Reports to stdout on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Handle to snapshot observer - raw pointer is used as lifetime
     * of pointer does not exceed that of parent simulation's shared_ptr
     */
    const obs_corr_hic_snap_t* mp_snap;
};

#endif  // cmxObsCorrHicCons_h
