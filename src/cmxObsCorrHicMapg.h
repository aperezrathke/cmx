//****************************************************************************
// cmxObsCorrHicMapg.h
//****************************************************************************

/**
 * @brief Records global maximum a posteriori (MAP) estimate for empirical
 *  Hi-C correlation
 */

#ifndef cmxObsCorrHicMapg_h
#define cmxObsCorrHicMapg_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObs.h"
#include "cmxObsCorrHicSnap.h"
#include "cmxObsLogscoreMapg.h"
#include "cmxSmartPtr.h"

//****************************************************************************
// cmxObsCorrHicMapg
//****************************************************************************

/**
 * Global MAP observer - captures global (across all MCMC chains) maximum a
 *  posteriori estimate of observed parameter
 */
template <typename t_Glue>
class cmxObsCorrHicMapg : public cmxObsLogscoreMapg<t_Glue>::map_reactor_t {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Hi-C correlation snapshot observer type
     */
    typedef cmxObsCorrHicSnap<t_Glue> obs_corr_hic_snap_t;

    /**
     * Default constructor
     */
    cmxObsCorrHicMapg() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within reactors array
     */
    virtual void init_react(cmxSpConstConfig_t config,
                            sim_t& sim,
                            const size_t index) {
        // Reset
        this->clear();
        // Extract snapshot observer
        if (!obs_t::find_raw(&mp_snap, sim)) {
            cmxLogf(
                "Error: Hi-C correlation global MAP observer unable to find "
                "handle to snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
    }

    /**
     * Reset to default state
     */
    void clear() {
        mp_snap = NULL;
        m_map = CMX_TO_REAL(0.0);
    }

    /**
     * Reactor update managed by logscore MAP observer, when a new MAP
     * estimate is encountered, this method will be called
     * @param iter - Typically post burn-in iteration number (0-based)
     * @param sim - Outer simulation
     * @param i_model - Index of MAP model
     * @param logscore - new MAP logscore associated to MAP model
     */
    virtual void update_react(const cmxUInt iter,
                              sim_t& sim,
                              const size_t i_model,
                              const cmxReal logscore) {
        cmxAssert(mp_snap);
        // Note, snapshot for Hi-C correlation is assumed to never be stale as
        // the logscore MAP observer should always update the Hi-C correlation
        // when testing for a new MAP estimate; therefore we no longer need to
        // call mp_snap->force_update(...)
        // Update MAP estimate
        cmxAssertBounds(i_model, 0, mp_snap->get_num_corrs());
        m_map = mp_snap->get_corrs()[i_model];
    }

    /**
     * Callback for when simulation has finished running
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        cmxLogf("MAP (global): Hi-C correlation: %f\n", (double)m_map);
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_corr_hic_mapg)

private:
    /**
     * Handle to snapshot observer - raw pointer is used as lifetime of
     * pointer does not exceed that of parent simulation's shared_ptr
     */
    obs_corr_hic_snap_t* mp_snap;

    /**
     * MAP (maximum a posteriori) estimate
     */
    cmxReal m_map;
};

#endif  // cmxObsCorrHicMapg_h
