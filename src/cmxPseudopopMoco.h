//****************************************************************************
// cmxPseudopopMoco.h
//****************************************************************************

/**
 * @brief Model component representing individual samples defining a chromatin
 *  contact matrix. This is a latent (unobserved) variable and therefore we
 *  call it a "pseudo-population" after the "pseudo-counts" represented by the
 *  alpha parameters in Dirichlet priors.
 */

#ifndef cmxPseudopopMoco_h
#define cmxPseudopopMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxModelTypes.h"
#include "cmxPseudopopCore.h"
#include "cmxPseudopopPolyaImpl.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxPseudopopMoco
//****************************************************************************

/**
 * Template arguments:
 *
 *  t_Glue: See cmxGlue.h
 *  t_Impl: Implementation hooks used by member methods Mh_prime_cand() and
 *      randomize_state(). Must define:
 *      ::mcmc_mixin_t - The MCMC mixin type (Metropolis vs
 *                          Metropolis-Hastings)
 *      ::Mh_prime_cand(...) - Primes current and candidate states for MCMC
 *      ::randomize(...) - Randomizes parameter MCMC state
 */
template <typename t_Glue, typename t_Impl = cmxPseudopopPolyaImpl<t_Glue> >
class cmxPseudopopMoco : public cmxMoco<t_Glue>, public t_Impl::mcmc_mixin_t {
private:
    /**
     * Implementation type
     */
    typedef t_Impl impl_t;

    /**
     * MCMC mixin type
     */
    typedef typename impl_t::mcmc_mixin_t mcmc_mixin_t;

    /**
     * MCMC state type
     */
    typedef typename mcmc_mixin_t::state_t state_t;

    /**
     * Pseudo-population core utilities
     */
    typedef cmxPseudopopCore<t_Glue> core_t;

public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * MCMC arguments
     */
    typedef typename core_t::Mh_args_t Mh_args_t;

    /**
     * Component is random and requires sampling
     */
    enum { is_random = cmxTRUE };

    /**
     * Initialization from user configuration. Model component should seek to
     * initialize to single population, independent null model where possible.
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void init(cmxSpConstConfig_t config,
                      model_t& model,
                      sim_t& sim CMX_THREAD_ID_PARAM) {
        this->init_states(*this, config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Initialize to a random state, should be called after init()
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void randomize(cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {
        this->randomize_states(*this, config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Gibbs posterior update, should only be called if model component is
     * random! Generates a sample from the posterior distribution P(Xi | X-i)
     * where Xi is i-th random variable and X-i is set of of all random
     * variables except for Xi in the model.
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void Gibbs_posterior(model_t& model,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {
        // To satisfy detailed balance, perform single Metropolis step
        Mh_args_t mh_args = {&model, &sim};
        mcmc_mixin_t::Mh_step(*this, mh_args CMX_THREAD_ID_ARG);
    }

    /**
     * Intended to assess Gibbs sampler convergence ideally showing that chain
     * is exploring high probability regions. This may be an expensive call to
     * make and should be used sparingly at regular intervals.
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    virtual cmxReal logscore(model_t& model, sim_t& sim) {
        // Handle to current pseudo-population
        const cmxPseudopopFrame_t& frame = this->get_pseudopop_frame();
        const cmxUInt n_pseu = CMX_TO_UINT(frame.n_cols);
        // Compute log-likelihood of pseudo-population
        cmxReal score = CMX_TO_REAL(0.0);
        for (cmxUInt i_pseu = CMX_TO_UINT(0); i_pseu < n_pseu; ++i_pseu) {
            score += core_t::logscore_bfwd(frame, i_pseu, model, sim);
        }
        return score;
    }

    // Metropolis-Hastings MCMC interface

    /**
     * Metropolis-Hastings interface
     * Prime (i.e. propose) candidate sample
     */
    void Mh_prime_cand(const Mh_args_t mh_args CMX_THREAD_ID_PARAM) {
        // Recover parent model and outer simulation handles
        model_t& model = *mh_args.p_model;
        sim_t& sim = *mh_args.p_sim;
        const cmxUIVecCol& indices =
            core_t::sample_block_indices(sim CMX_THREAD_ID_ARG);
        impl_t::Mh_prime_cand(this->get_cand(),
                              this->get_curr(),
                              indices,
                              model,
                              sim CMX_THREAD_ID_ARG);
    }

    /**
     * Callback for when current state is swapped by MCMC
     */
    inline void Mh_on_accept_cand(const Mh_args_t mh_args) {
        // Forward call to parent
        this->mcmc_mixin_t::Mh_on_accept_cand();
    }

    /**
     * Callback for when current state is retained by MCMC
     */
    inline void Mh_on_reject_cand(const Mh_args_t mh_args) {
        // Forward call to parent
        this->mcmc_mixin_t::Mh_on_reject_cand();
    }

    // Accessors

    /**
     * @return pseudo-population samples
     */
    inline const cmxPseudopopFrame_t& get_pseudopop_frame() const {
        cmxAssert(!this->get_curr().frame.is_empty());
        cmxAssertPosEq(this->get_curr().freqs.n_elem,
                       this->get_curr().frame.n_rows);
        return this->get_curr().frame;
    }

    /**
     * @return pseudo-population aggregated contact frequencies
     */
    inline const cmxContactFreqs_t& get_pseudopop_freqs() const {
        cmxAssert(!this->get_curr().freqs.is_empty());
        cmxAssertPosEq(this->get_curr().freqs.n_elem,
                       this->get_curr().frame.n_rows);
        return this->get_curr().freqs;
    }

    /**
     * Initialize state from user configuration, initializes to independent
     * (no edge) null model
     */
    static void init_state(state_t& s,
                           cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {
        core_t::init(s, config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Randomize state - sample from prior
     * @param s - Metropolis-Hastings state to randomize from prior
     * @param config - user configuration
     * @param model - parent model
     * @param sim - outer simulation
     */
    static void randomize_state(state_t& s,
                                cmxSpConstConfig_t config,
                                model_t& model,
                                sim_t& sim CMX_THREAD_ID_PARAM) {
        impl_t::randomize(s, config, model, sim CMX_THREAD_ID_ARG);
    }
};

#endif  // cmxPseudopopMoco_h
