//****************************************************************************
// cmxSimSharedMu.h
//****************************************************************************

/**
 * @brief Mutable shared object encapsulates all policies for how model data
 *  is shared in a multi-threaded environment.
 */

#ifndef cmxSimSharedMu_h
#define cmxSimSharedMu_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxMoco.h"
#include "cmxThread.h"

//****************************************************************************
// cmxSharedMu
//****************************************************************************

/**
 * Shared data policy, resides at simulation level. The component glue shared
 * policies determine the multi-thread behavior of the shared resources. The
 * template argument t_GlueMu is not expected to be necessarily same Glue type
 * as defined by "cmxGlue". Instead, t_GlueMu is a mutable shared glue type
 * with typedefs for mutable shared resources for:
 *
 *  - Bnet_dag_t: writable scratch buffers for topology sorting, etc
 *  - Bnet_cpt_t: writable scratch buffer for tabulating counts
 *  - Bnet_mpop_t: writable scratch buffers for multi-population network
 *  - Clust_t: writable scratch buffers for cluster association
 *  - Pseudopop_t: model-level shared data for pseudo-population
 */
template <typename t_GlueMu>
class cmxSharedMu {
    /**
     * Shared resources policy type for Bayesian network DAG
     *  (e.g. cmxBnetThinDagSharcoMu)
     */
    CMX_SHARCO_DECLARE(t_GlueMu, Bnet_dag);

    /**
     * Shared resources policy type for Bayesian network CPTs
     *  (e.g. cmxBnetBoolCptSharcoMu)
     */
    CMX_SHARCO_DECLARE(t_GlueMu, Bnet_cpt);

    /**
     * Shared resources policy type for multi-population Bayesian network
     *  (e.g. cmxBnetMpopSharcoMu)
     */
    CMX_SHARCO_DECLARE(t_GlueMu, Bnet_mpop);

    /**
     * Shared resources policy type for cluster association
     *  (e.g. cmxClustMpopSharcoMu)
     */
    CMX_SHARCO_DECLARE(t_GlueMu, Clust);

    /**
     * Shared resources policy type for pseudo-population
     *  (e.g cmxPseudopopSharcoMu)
     */
    CMX_SHARCO_DECLARE(t_GlueMu, Pseudopop);

public:
    /**
     * Initializes registered shared resources
     * @param config - user options
     * @param sim - parent simulation
     */
    template <typename sim_t>
    void init(cmxSpConstConfig_t config, const sim_t& sim) {
        this->get_Bnet_dag().init(config, sim);
        this->get_Bnet_cpt().init(config, sim);
        this->get_Bnet_mpop().init(config, sim);
        this->get_Clust().init(config, sim);
        this->get_Pseudopop().init(config, sim);
    }

    /**
     * Clear (reset) shared data
     */
    void clear() {
        this->get_Bnet_dag().clear();
        this->get_Bnet_cpt().clear();
        this->get_Bnet_mpop().clear();
        this->get_Clust().clear();
        this->get_Pseudopop().clear();
    }
};

#endif  // cmxSharedMu_h
