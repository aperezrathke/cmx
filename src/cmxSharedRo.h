//****************************************************************************
// cmxSharedRo.h
//****************************************************************************

/**
 * @brief Shared object for read-only data common to all Gibbs chains
 */

#ifndef cmxSharedRo_h
#define cmxSharedRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxMoco.h"

//****************************************************************************
// cmxSharedRo
//****************************************************************************

/**
 * Shared read-only data policy, resides at simulation level. The template
 * argument t_GlueRo is not expected to be necessarily same Glue type as
 * defined by "cmxGlue" and used by model_t or shared_mu_t. Instead, t_Glue
 * is a shared glue type with typedefs for read-only shared resources for:
 *
 *  - Dp_stick_alpha_t: Read-only data utilized by Dirichlet process
 *      concentration parameter
 *  - Bnet_dag_kappa_t: Read-only data utilized by Bayesian network
 *      structure prior
 *  - Bnet_cpt_alpha_t: Read-only data utilized by Bayesian network
 *      prior on conditional probability table (CPT) parameters
 *  - Bnet_infeas_lambda_shared_t: Read-only data used by hyper-parameter on
 *      infeasible samples
 *  - Bnet_infeas_t: Read-only infeasible samples
 *  - Bnet_dag_t: Read-only shared data utilized by Bayesian network
 *      DAG component
 *  - Bnet_mpop_t: Read-only shared data specific to multi-population Bayesian
 *      network
 *  - Clust_t: Read-only shared data for cluster assignment component
 *  - Pseudopop_t: Read-only shared data for pseudo-population
 *  - Contact_freqs_alpha_t: Read-only shared data for contact
 *      frequencies (empirical Hi-C) alpha hyper-parameter
 *  - Contact_freqs_t: Read-only shared data for contact frequencies
 *
 * The minimal interface expected for a shared read-only policy is:
 *  void <shared_type>::init(cmxSpConstConfig_t config) { ... }
 *
 * Note, many of the read-only shared data policies are conditional on the
 *  the associated model-level component being deterministic. If instead the
 *  component is random (i.e. Gibbs sampled), then the shared policy will
 *  likely need to utilize a "null" version of the shared data to keep the
 *  interfaces consistent. See "cmxNullSharcoRo.h"
 */
template <typename t_GlueRo>
class cmxSharedRo {

    /**
     * Read-only shared resources for Dirichlet process alpha concentration
     * parameter
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Dp_stick_alpha);

    /**
     * Read-only shared resources for *prior* on Bayesian network DAG
     *  (e.g. cmxBnetDagKappaDetrSharedRo)
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Bnet_dag_kappa);

    /**
     * Read-only shared resources for *prior* on Bayesian network CPTs
     *  (e.g. cmxBnetCptAlphaDetrSharedRo)
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Bnet_cpt_alpha);

    /**
     * Read-only shared resources for hyper-parameter on infeasible samples
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Bnet_infeas_lambda);

    /**
     * Read-only shared resources for prior on infeasible samples
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Bnet_infeas);

    /**
     * Read-only shared resources for Bayesian network DAG
     *  (e.g. cmxBnetThinDagSharedRo)
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Bnet_dag);

    /**
     * Read-only shared resources specific to multi-population Bayesian networks
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Bnet_mpop);

    /**
     * Read-only shared resources for cluster assignment
     *  (e.g. cmxClustSpopSharedRo)
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Clust);

    /**
     * Read-only shared resources for pseudo-population
     *  (e.g cmxPseudopopSharedRo)
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Pseudopop);

    /**
     * Read-only shared resource for Hi-C alpha
     *  (e.g. cmxContactFreqsAlphaDetrSharedRo)
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Contact_freqs_alpha);

    /**
     * Read-only shared resource for contact frequencies (Hi-C)
     *  (e.g. cmxContactFreqsSharedRo)
     */
    CMX_SHARCO_DECLARE(t_GlueRo, Contact_freqs);

public:
    /**
     * Initializes registered shared read-only resources
     * @param config - user options
     */
    void init(cmxSpConstConfig_t config) {
        this->get_Dp_stick_alpha().init(config);
        this->get_Bnet_dag_kappa().init(config);
        this->get_Bnet_cpt_alpha().init(config);
        this->get_Bnet_infeas_lambda().init(config);
        this->get_Bnet_infeas().init(config);
        this->get_Bnet_dag().init(config);
        this->get_Bnet_mpop().init(config);
        this->get_Clust().init(config);
        this->get_Pseudopop().init(config);
        this->get_Contact_freqs_alpha().init(config);
        this->get_Contact_freqs().init(config);
    }

    /**
     * Clear (reset) shared data
     */
    void clear() {
        this->get_Dp_stick_alpha().clear();
        this->get_Bnet_dag_kappa().clear();
        this->get_Bnet_cpt_alpha().clear();
        this->get_Bnet_infeas_lambda().clear();
        this->get_Bnet_infeas().clear();
        this->get_Bnet_dag().clear();
        this->get_Bnet_mpop().clear();
        this->get_Clust().clear();
        this->get_Pseudopop().clear();
        this->get_Contact_freqs_alpha().clear();
        this->get_Contact_freqs().clear();
    }
};

#endif  // cmxSharedRo_h
