//****************************************************************************
// cmxScalarDetrMoco.h
//****************************************************************************

/**
 * @brief Real-valued scalar hyper-parameter that can be configured by user.
 *  Intended primarily for use as the alpha parameter of a symmetric
 *  "Dirichlet-style" distribution. The nomenclature:
 *      Root -> In Bayesian network, this is a root node without parents
 *      Scalar -> 1-D real-valued
 *      Detr -> 'Deterministic', this component is non-random
 */

#ifndef cmxScalarDetrMoco_h
#define cmxScalarDetrMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxScalarDetrMoco
//****************************************************************************

/**
 * Root-level deterministic scalar node, template arguments:
 *  - t_Glue: Glue object as defined by cmxGlue
 *  - t_SharedQuery: Stateless interface for querying scalar value, provides
 *      static method ::get_scalar(const sim) which returns a cmxReal
 */
template <typename t_Glue, typename t_SharedQuery>
class cmxScalarDetrMoco : public cmxMoco<t_Glue> {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Component is deterministic and requires no sampling
     */
    enum { is_random = cmxFALSE };

    /**
     * @param sim - outer simulation
     * @return real-valued scalar hyper-parameter
     */
    inline cmxReal get_scalar(const sim_t& sim) const {
        return t_SharedQuery::get_scalar(sim);
    }
};

#endif  // cmxScalarDetrMoco_h
