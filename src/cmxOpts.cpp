//****************************************************************************
// cmxOpts.cpp
//
// Program options
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxOpts.h"
#include "cmxLogf.h"

#include <stdio.h>

//****************************************************************************
// Implementation
//****************************************************************************

// Macros to declare option elements
#define UNARY_OPT_PREFIX "-"
#define BINARY_OPT_PREFIX "--"
#define DECLARE_CMD_UNARY_OPT(key, desc) \
    { "", UNARY_OPT_PREFIX key, desc }
#define DECLARE_CMD_BINARY_OPT(key, desc) \
    { "", BINARY_OPT_PREFIX key, desc }
#define DECLARE_INI_OPT(key, desc) \
    { key, BINARY_OPT_PREFIX key, desc }

/**
 * Array of option info structures. Array is indexed according to cmxOpt enum.
 */
const struct cmxOptInfo CMX_OPT_INFOS[cmxOpt_NUM_OPTS] = {
      DECLARE_CMD_UNARY_OPT("help", "Shows command line options and how to use program.")
    , DECLARE_CMD_UNARY_OPT("app_help", "Shows command line options and how to use program.")
    , DECLARE_CMD_UNARY_OPT("app_spop_canon", "Runs canonical, single-population simulation. Usage -app_spop_canon")
    , DECLARE_CMD_UNARY_OPT("app_mpop_canon", "Runs canonical, multi-population simulation. Usage -app_mpop_canon")
    , DECLARE_CMD_BINARY_OPT("conf", "Specifies path to INI configuration file. Usage: --conf <filename>")
    , DECLARE_INI_OPT("conf_child", "Specifies optional path to a child configuration. Can be specified on command line only for root parent, else must be specified via ini. Usage --conf_child <filename>")
    , DECLARE_INI_OPT("arch", "[Optional] Specifies path to archetype INI configuration. Options not found in main INI will be searched for in archetype - useful for setting common shared options and/or defaults. Usage --arch <filename>")
    , DECLARE_INI_OPT("output_dir", "Specifies base output directory to store generated data. Usage --output_dir <dirpath>")
    , DECLARE_INI_OPT("job_id", "Prefix appended to all output data, intended to avoid overwriting data from previous runs. Usage: --job_id <job_id>")
    , DECLARE_INI_OPT("num_models", "Number of models (independent Gibbs MCMC chains) to simulate. Usage --num_models <+integer>")

    // Primer parameters

    , DECLARE_INI_OPT("primer_steps", "Steps alloted for priming a model (pre-burn). Usage --primer_steps <non-neg integer>")
    , DECLARE_INI_OPT("primer_attempts", "Maximum number of attempts at priming a model. Usage --primer_attempts <non-neg integer>")
    , DECLARE_INI_OPT("primer_corr_hic_thresh", "Target empirical Hi-C correlation for primed models. Usage --primer_corr_hic_thresh <real in [-1,1)>")

    // Controller parameters

    , DECLARE_INI_OPT("controller_type", "Specifies type of controller (e.g. canonical). Usage: --controller_type <name>")
    , DECLARE_INI_OPT("controller_thin_steps", "Number of intermediate Gibbs steps ignored between model iterations, may be ignored by controller. Usage --controller_thin_steps <non-neg integer>")
    , DECLARE_INI_OPT("controller_burn_iters", "Number of iterations used for Gibbs model burn-in, may be ignored by controller. Usage --controller_burn_iters <non-neg integer>")
    , DECLARE_INI_OPT("controller_post_burn_iters", "Number of iterations to perform after burn-in, may be ignored by controller. Usage --controller_post_burn_iters <non-neg integer>")

    // Model parameters

    , DECLARE_INI_OPT("Dp_stick_alpha_gamma_shape", "Positive shape argument for Gamma(shape, scale) prior on Dirichlet process alpha. Usage --Dp_stick_alpha_gamma_shape <real>")
    , DECLARE_INI_OPT("Dp_stick_alpha_gamma_scale", "Positive scale argument for Gamma(shape, scale) prior on Dirichlet process alpha. Usage --Dp_stick_alpha_gamma_scale <real>")
    , DECLARE_INI_OPT("Dp_stick_alpha", "Fixed positive alpha concentration parameter for Dirichlet stick-breaking process (monotonic to expected number of clusters). Usage --Dp_stick_alpha <real>")
    , DECLARE_INI_OPT("Bnet_dag_kappa", "Fixed kappa parameter for Bayesian network directed acyclic graph prior. Usage --Bnet_dag_kappa <real>")
    , DECLARE_INI_OPT("Bnet_dag_edges_path", "File path to Bayesian network DAG allowed edges matrix (column major). Usage --Bnet_dag_edges_path <file_path>")
    , DECLARE_INI_OPT("Bnet_cpt_alpha", "Fixed alpha parameter for Bayesian network conditional probability table (CPT) prior. Usage --Bnet_cpt_alpha <real>")
    , DECLARE_INI_OPT("Bnet_infeas_scale", "Fixed scale parameter for infeasible (negative) corpus prior, should be *positive* to serve as penalty. Usage --Bnet_infeas_scale <non-neg real>")
    , DECLARE_INI_OPT("Bnet_infeas_ceil", "Fixed ceiling parameter for infeasible (negative) corpus prior, should be *positive*, limits maximum reward for assigning low probability to infeasible set. Usage --Bnet_infeas_ceil <non-neg real>")
    , DECLARE_INI_OPT("Bnet_infeas_path", "File Path to infeasible (negative) corpus row-major bit matrix. Usage --Bnet_infeas_path <file_path>")
    , DECLARE_INI_OPT("Bnet_randomize_steps", "[Optional] Number of steps to randomize Bayesian networks by sampling from prior. Usage --Bnet_randomize_steps <+integer>")
    , DECLARE_INI_OPT("Bnet_mpop_block_size", "Size of multi-population Bayesian network sub-population selected during MCMC proposals. Usage --Bnet_mpop_block_size <+integer>")
    , DECLARE_INI_OPT("Bnet_mpop_block_weighted_blend", "Percentage of multi-population Bayesian network MCMC sub-population that is proposed according to a weighted selection. Real-value in [0,1] where 0 is uniform selection, 1 is weighted selection, and anything in-between blends the two approaches. Usage --Bnet_mpop_block_weighted_blend <real in [0,1]>")
    , DECLARE_INI_OPT("mix_size", "Integer number of mixture components in [1, inf). See Ishwaran, Biometrika 87.2 (2000): 371-390. Usage --mix_size <+integer>")
    , DECLARE_INI_OPT("pseudopop_size", "Size of pseudo-population. Usage --pseudopop_size <+integer>")
    , DECLARE_INI_OPT("pseudopop_block_size", "Size of pseudo-population sub-population selected during MCMC proposals. Usage --pseudopop_block_size <+integer>")
    , DECLARE_INI_OPT("pseudopop_polya_alpha", "When sampling pseudo-population via Polya urn, defines initial number of balls in the urn. Usage --pseudopop_polya_alpha <non-neg real>")
    , DECLARE_INI_OPT("pseudopop_polya_delta", "When sampling pseudo-population via Polya urn, defines incremental number of balls of selected color added to urn after each draw. Usage --pseudopop_polya_delta <real>")
    , DECLARE_INI_OPT("pseudopop_bpoly_corr_thresh", "When sampling pseudo-population bit matrix via Bpoly proposal, defines cutoff correlation at which below a Polya urn is used, else the parent Bayesian network(s) are used (default=0.6). Usage --pseudopop_bpoly_corr_thresh <real in [-1,1]>")
    , DECLARE_INI_OPT("hic_alpha", "Fixed alpha parameter for empirical Hi-C likelihood. Usage --hic_alpha <real>")
    , DECLARE_INI_OPT("hic_path", "File path to observed Hi-C data, assumes normalization to frequencies in [0,1]. Usage --hic_path <file_path>")

    // Observer parameters

    , DECLARE_INI_OPT("obs_enable", "Vector argument to enable observer types. Usage obs_enable_<0-based_integer:index> = <string:observer_name> where 'observer_name' is one of logscore_cons|logscore_snap|logscore_trac|corr_hic_cons|corr_hic_snap|corr_hic_trac - this list may not be exhaustive.")
    , DECLARE_INI_OPT("obs_logscore_cons_period", "[Optional] log-score stdout report interval (default=1). Usage --obs_logscore_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_logscore_mapc_min_corr_hic", "[Optional] Minimum Hi-C correlation needed in [-1,1] for a chain MAP estimate to be captured (default=-inf). Usage --obs_logscore_mapc_min_corr_hic <real in [-1,1]>")
    , DECLARE_INI_OPT("obs_logscore_mapg_min_corr_hic", "[Optional] Minimum Hi-C correlation needed in [-1,1] for a global MAP estimate to be captured (default=-inf). Usage --obs_logscore_mapg_min_corr_hic <real in [-1,1]>")
    , DECLARE_INI_OPT("obs_logscore_mapc_start_iter", "[Optional] log-score chain MAP observing will begin on this (post burn-in) iteration (default=0). Usage --obs_logscore_mapc_start_iter <non-neg integer>")
    , DECLARE_INI_OPT("obs_logscore_mapg_start_iter", "[Optional] log-score global MAP observing will begin on this (post burn-in) iteration (default=0). Usage --obs_logscore_mapg_start_iter <non-neg integer>")
    , DECLARE_INI_OPT("obs_logscore_snap_period", "[Optional] log-score snapshot is taken on iterations which are multiples of this period (default=1). Usage --obs_logscore_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_logscore_trac_export", "[Optional] Set to 1 to enable CSV export of log-score trace plot (default=0). Usage --obs_logscore_trac_export <0|1>")
    , DECLARE_INI_OPT("obs_logscore_trac_period", "[Optional] log-score trace plots record history on multiples of period (default=1). Usage --obs_logscore_trac_period <+integer>")
    , DECLARE_INI_OPT("obs_logscore_trac_start_iter", "[Optional] log-score trace plots start iteration (default=0). Usgae --obs_logscore_trac_start_iter <non-neg integer>")
    , DECLARE_INI_OPT("obs_corr_hic_cons_period", "[Optional] Hi-C correlation stdout report interval (default=1). Usage --obs_corr_hic_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_corr_hic_snap_period", "[Optional] Hi-C correlation snapshot is taken on iterations which are multiples of this period (default=1). Usage --obs_corr_hic_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_corr_hic_trac_export", "[Optional] Set to 1 to enable CSV export of Hi-C correlation trace plot. Usage --obs_corr_hic_trac_export <0|1>")
    , DECLARE_INI_OPT("obs_corr_hic_trac_period", "[Optional] Hi-C correlation trace plots record history on multiples of period (default=1). Usage --obs_corr_hic_trac_period <+integer>")
    , DECLARE_INI_OPT("obs_corr_hic_trac_start_iter", "[Optional] Hi-C correlation trace plots start iteration (default=0). Usage --obs_corr_hic_trac_start_iter <non-neg integer>")
    , DECLARE_INI_OPT("obs_bnet_ring_capacity", "[Optional] History capacity (before old values overwritten and possibly flushed to disk) for Bayesian network ring observer (default=500). Usage --obs_bnet_ring_capacity <+integer>")
    , DECLARE_INI_OPT("obs_bnet_ring_export_cpt", "[Optional] Set to 1 to enable export of Bayesian network CPT history (default=0). Usage --obs_bnet_ring_export_cpt <0|1>")
    , DECLARE_INI_OPT("obs_bnet_ring_export_dst2src", "[Optional] Set to 1 to enable export of Bayesian network DAG destination-to-source edge mapping history (default=1). Usage --obs_bnet_ring_export_dst2src <0|1>")
    , DECLARE_INI_OPT("obs_bnet_ring_export_order", "[Optional] Set to 1 to enable export of Bayesian network topological sort order history (default=0). Usage --obs_bnet_ring_export_order <0|1>")
    , DECLARE_INI_OPT("obs_bnet_ring_export_src2dst", "[Optional] Set to 1 to enable export of Bayesian network DAG source-to-destination edge mapping history (default=1). Usage --obs_bnet_ring_export_src2dst <0|1>")
    , DECLARE_INI_OPT("obs_bnet_ring_period", "[Optional] Bayesian network ring history performs collection on multiples of this period (default=1). Usage --obs_bnet_ring_period <+integer>")
    , DECLARE_INI_OPT("obs_bnet_ring_start_iter", "[Optional] Bayesian network ring history start iteration (default=1). Usage --obs_bnet_ring_start_iter <non-neg integer>")
    , DECLARE_INI_OPT("obs_bnet_ring_trac_cpt", "[Optional] Set to 1 to enable ring history tracking for Bayesian network CPTs, automatically enabled if obs_bnet_ring_export_cpt=1 (default=0). Usage --obs_bnet_ring_trac_cpt <0|1>")
    , DECLARE_INI_OPT("obs_bnet_ring_trac_dst2src", "[Optional] Set to 1 to enable ring history tracking for Bayesian network DAG destination-to-source edge mapping, automatically enabled if obs_bnet_ring_export_dst2src=1 (default=1). Usage --obs_bnet_ring_trac_dst2src <0|1>")
    , DECLARE_INI_OPT("obs_bnet_ring_trac_order", "[Optional] Set to 1 to enable ring history tracking for Bayesian network topological sort order, automatically enabled if obs_bnet_ring_export_order=1 (default=0). Usage --obs_bnet_ring_trac_order <0|1>")
    , DECLARE_INI_OPT("obs_bnet_ring_trac_src2dst", "[Optional] Set to 1 to enable ring history tracking for Bayesian network DAG source-to-destination edge mapping, automatically enabled if obs_bnet_ring_export_src2dst=1 (default=1). Usage --obs_bnet_ring_trac_src2dst <0|1>")
    , DECLARE_INI_OPT("obs_bnet_snap_period", "[Optional] Bayesian network snapshot period (default=1). Usage --obs_bnet_snap_period <+integer>")
    , DECLARE_INI_OPT("obs_clust_hist_cons_period", "[Optional] Cluster histogram report to stdout period (default=1). Usage --obs_clust_hist_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_clust_size_snap_period", "[Optional] Snapshot interval for sorted cluster size observer (default=1). Usage --obs_clust_size_snap_period <+integer>")
    , DECLARE_INI_OPT("obs_mix_ent_cons_period", "[Optional] Mixture PMF entropy report to stdout period (default=1). Usage --obs_mix_ent_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_mix_ent_snap_period", "[Optional] Snapshot interval for mixture PMF entropy observer (default=1). Usage --obs_mix_ent_snap_period <+integer>")
    , DECLARE_INI_OPT("obs_mix_ent_trac_export", "[Optional] Set to 1 to enable CSV export of mixture entropy traces (default=0). Usage --obs_mix_ent_trac_export <0|1>")
    , DECLARE_INI_OPT("obs_mix_ent_trac_period", "[Optional] Mixture PMF trace collections occur on multiples of this period (default=1). Usage --obs_mix_ent_trac_period <+integer>")
    , DECLARE_INI_OPT("obs_mix_ent_trac_start_iter", "[Optional] Mixture PMF trace start iteration (default=0). Usage --obs_mix_ent_trac_start_iter <+integer>")
    , DECLARE_INI_OPT("obs_mix_hist_cons_period", "[Optional] Mixture weights histogram report to stdout period (default=1). Usage --obs_mix_hist_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_mix_pmf_snap_period", "[Optional] Snapshot interval for mixture weights observer (default=1). Usage --obs_mix_pmf_snap_period <+integer>")
    , DECLARE_INI_OPT("obs_pacc_bnet_cons_period", "[Optional] Bayesian network acceptance probability stdout report interval (default=1). Usage --obs_pacc_bnet_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_pacc_pseu_cons_period", "[Optional] Pseudo-population acceptance probability stdout report interval (default=1). Usage --obs_pacc_pseu_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_pseu_frame_cons_period", "[Optional] Pseudo-population frame stdout report interval (default=1). Usage --obs_pseu_frame_cons_period <+integer>")
    , DECLARE_INI_OPT("obs_pseu_frame_mapc_export", "[Optional] Set to 1 to enable CSV export of pseudo-population frame chain MAP estimates (default=0). Usage --obs_pseu_frame_mapc_export 1")
    , DECLARE_INI_OPT("obs_pseu_frame_mapg_export", "[Optional] Set to 1 to enable CSV export of pseudo-population frame global MAP estimate (default=0). Usage --obs_pseu_frame_mapg_export 1")
    , DECLARE_INI_OPT("obs_pseu_frame_snap_period", "[Optional] Pseudo-population frame snapshot is taken on iterations which are multiples of period (default=1). Usage --obs_pseu_frame_snap_period <+integer>")
    , DECLARE_INI_OPT("obs_pseu_hic_mapc_export", "[Optional] Set to 1 to enable CSV export of pseudo-population Hi-C chain MAP estimates (default=0). Usage --obs_pseu_hic_mapc_export 1")
    , DECLARE_INI_OPT("obs_pseu_hic_mapg_export", "[Optional] Set to 1 to enable CSV export of pseudo-population Hi-C global MAP estimate (default=0). Usage --obs_pseu_hic_mapg_export 1")

    // Thread parameters

#ifdef CMX_BUILD_ENABLE_THREADS
    , DECLARE_INI_OPT("num_worker_threads", "Specifies the number of worker threads. Usage --num_worker_threads <positive integer>")
#endif // CMX_BUILD_ENABLE_THREADS

    // Commandlet parameters

#ifdef CMX_BUILD_ENABLE_COMMANDLETS
    , DECLARE_CMD_UNARY_OPT("cmdlet_info", "Prints basic information about program.")
#endif // CMX_BUILD_ENABLE_COMMANDLETS

    // Test parameters

#ifdef CMX_BUILD_ENABLE_TESTS
    , DECLARE_CMD_UNARY_OPT("test_bnet_thin_dag", "Tests thin Bnet DAG structure operations")
    , DECLARE_CMD_UNARY_OPT("test_dir_distrib", "Tests Dirichlet distribution utilities")
    , DECLARE_CMD_UNARY_OPT("test_dp_stick", "Tests various Dirichlet stick-breaking process implementations")
    , DECLARE_CMD_UNARY_OPT("test_gibbs_mpop", "Tests multi-population Gibbs sampler")
    , DECLARE_CMD_UNARY_OPT("test_gibbs_spop", "Tests single-population Gibbs sampler")
    , DECLARE_CMD_UNARY_OPT("test_global_init", "Tests global initialization")
    , DECLARE_CMD_UNARY_OPT("test_hash", "Tests basic hash operations")
    , DECLARE_CMD_UNARY_OPT("test_rng", "Tests random number generator")
#ifdef CMX_BUILD_ENABLE_THREADS
    , DECLARE_CMD_UNARY_OPT("test_parallel_mapper", "Tests simple use case of parallel mapper")
#endif // CMX_BUILD_ENABLE_THREADS
#endif // CMX_BUILD_ENABLE_TESTS
};

/**
 * Callback to print usage for a single switch
 */
void cmxOpt_print_usage(const cmxOptE ix) {
    const cmxOptInfo& o = CMX_OPT_INFOS[ix];
    cmxLogf("---------------------------------\n");
    cmxLogf("INI key: %s\n", o.ini_key);
    cmxLogf("command line switch: %s\n", o.cmd_switch);
    cmxLogf("%s\n", o.desc);
}

/**
 * Callback for --help switch
 */
void cmxOpt_print_usage_all() {
    cmxLogf(
        "#################################\nOptions\n##########################"
        "#######\n");
    cmxLogf(
        "Most program options can be specified via command line or via a INI "
        "file.\n");
    cmxLogf(
        "If an option has no INI key, then it can only be specified by "
        "command line.\n");
    cmxLogf("Options:\n");
    // @TODO - would be nice to format into a table
    for (int i = 0; i < cmxOpt_NUM_OPTS; ++i) {
        cmxOpt_print_usage(static_cast<cmxOptE>(i));
    }
}
