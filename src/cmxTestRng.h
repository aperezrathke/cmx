//****************************************************************************
// cmxTestRng.h
//****************************************************************************

/**
 * Commandlet for testing random number generation
 *
 * Usage:
 * -test_rng
 */

#ifdef cmxTestRng_h
#   error "Test Rng included multiple times!"
#endif  // cmxTestRng_h
#define cmxTestRng_h

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_TESTS

#include "cmxCmdOptsMap.h"
#include "cmxExitCodes.h"
#include "cmxGlobals.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <iostream>

namespace {
namespace cmxTestRng {
/**
 * Utility to report the generated number and the interval it should fall within
 */
template <typename t_>
void report_interval(t_ rn, t_ lower_, t_ upper_) {
    std::cout << "Generated " << rn << " in [" << lower_ << ", " << upper_
              << ")\n";
}

/**
 * For interval in [0,1)
 */
void generate_and_report_interval(CMX_THREAD_ID_0_PARAM) {
    report_interval(cmxRng.unif_real(), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
}

/**
 * For interval in [lower_, upper_] in real numbers
 */
void generate_and_report_interval(cmxReal lower_,
                                  cmxReal upper_ CMX_THREAD_ID_PARAM) {
    report_interval(cmxRng.unif_real(lower_, upper_), lower_, upper_);
}

/**
 * For interval in [lower_, upper_] in integer numbers
 */
void generate_and_report_interval(int lower_, int upper_ CMX_THREAD_ID_PARAM) {
    report_interval(cmxRng.unif_int(lower_, upper_), lower_, upper_);
}
}  // namespace cmxTestRng
}  // Anonymous namespace

/**
 * Entry point for test
 */
int cmxTestRngMain(const cmxCmdOptsMap& cmd_opts) {
    cmxG::default_init();
    using namespace cmxTestRng;
    generate_and_report_interval(CMX_MAIN_THREAD_ID_0_ARG);
    generate_and_report_interval(CMX_TO_REAL(-1.0),
                                 CMX_TO_REAL(1.0) CMX_MAIN_THREAD_ID_ARG);
    generate_and_report_interval(CMX_TO_REAL(-5.0),
                                 CMX_TO_REAL(-1.0) CMX_MAIN_THREAD_ID_ARG);
    generate_and_report_interval(-100, 100 CMX_MAIN_THREAD_ID_ARG);
    generate_and_report_interval(-100, 0 CMX_MAIN_THREAD_ID_ARG);
    generate_and_report_interval(0, 100 CMX_MAIN_THREAD_ID_ARG);
    cmxG::teardown();
    return cmxExitCode_normal;
}

#endif  // CMX_BUILD_ENABLE_TESTS
