//****************************************************************************
// cmxGlobals.h
//****************************************************************************

/**
 * Houses global data that is configurable at run time
 */

#ifndef cmxGlobals_h
#define cmxGlobals_h

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxRand.h"
#include "cmxThread.h"

/**
 * Namespace to house global data
 */
namespace cmxG {
/**
 * If threads enabled, this value will hold the number of worker threads
 * available (the main thread is not included in this count). To access:
 * cmxG::NUM_WORKER_THREADS_VAR_NAME (will cause build errors if threads not
 * enabled).
 */
CMX_EXTERN_NUM_WORKER_THREADS

/**
 * If threads enabled, this declares an extern for a parallel mapper. To
 * access: cmxG:PARALLEL_MAPPER_VAR_NAME (will cause build errors if threads
 * not enabled).
 */
CMX_EXTERN_PARALLEL_MAPPER

/**
 * Random number generator (each thread has its own instance)
 */
extern CMX_DECLARE_TLS_DATA(cmxRand, rng);

/**
 * Initialize global data from runtime configuration
 */
extern void init(cmxSpConstConfig_t cfg);

/**
 * Initializes global data to default values
 */
extern void default_init();

/**
 * If global data is initialized, teardown() must be called before main()
 * exits!
 * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
 * This seems to be a deadlock bug caused by Microsoft's implementation of
 * std::thread
 */
extern void teardown();

}  // namespace cmxG

/**
 * Macro for accessing the global random number generator
 */
#define cmxRng CMX_ACCESS_TLS_DATA(cmxG::rng)

#endif  // cmxGlobals_h
