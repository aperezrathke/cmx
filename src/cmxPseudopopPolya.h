//****************************************************************************
// cmxPseudopopPolya.h
//****************************************************************************

/**
 * @brief Core Polya urn routines
 */

#ifndef cmxPseudopopPolya_h
#define cmxPseudopopPolya_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxModelTypes.h"
#include "cmxRand.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxPseudopopPolya
//****************************************************************************

template <typename t_Glue>
class cmxPseudopopPolya {
private:
    /**
     * Bernoulli distribution for flipping a coin
     */
    // @HACK - BOOST random appears to require template arguments
#ifdef CMX_BUILD_CXX_11
    typedef cmxRandom::bernoulli_distribution bernoulli_dist_t;
#else
    typedef cmxRandom::bernoulli_distribution<> bernoulli_dist_t;
#endif // CMX_BUILD_CXX_11

    /**
     * Urn colors: ko = knock-out, ki = knock-in
     */
    enum { color_ko = 0, color_ki = 1, num_colors = 2 };

    /**
     * Visitor for computing log-probability of pseudo-population
     */
    class logprob_visitor {
    public:
        /**
         * Constructor
         */
        logprob_visitor() : m_logp(CMX_TO_REAL(0.0)) {}

        /**
         * Visitor callback - updates log-probability
         * @param frame - Current pseudo-population (unused)
         * @param i_row - Row index into frame
         * @param i_col - Column index into frame
         * @param p_ki - Probability of selecting knock-in color
         * @return color at (i_row, i_col)
         */
        inline cmxBool operator()(const cmxPseudopopFrame_t& frame,
                                  const cmxMatSz_t i_row,
                                  const cmxMatSz_t i_col,
                                  const cmxReal p_ki) {
            cmxAssertBoundsInc(p_ki, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            // Compute numerically stable probabilities for each color
            // (NOTE - DO NOT SWITCH COMPUTATION ORDER!)
            m_probs[color_ki] = std::max(p_ki, CMX_REAL_EPS);
            m_probs[color_ko] = std::max(CMX_TO_REAL(1.0) - p_ki, CMX_REAL_EPS);
            // Determine sampled color
            cmxAssertBounds(i_row, CMX_TO_MAT_SZ_T(0), frame.n_rows);
            cmxAssertBounds(i_col, CMX_TO_MAT_SZ_T(0), frame.n_cols);
            const cmxBool color = frame.at(i_row, i_col);
            cmxAssert((color == cmxTRUE) || (color == cmxFALSE));
            cmxAssertBounds(color, CMX_TO_UINT(0), CMX_TO_UINT(num_colors));
            // Increment log-score
            m_logp += log(m_probs[color]);
            return color;
        }

        /**
         * @return log-probability from Polya urn traversal
         */
        inline cmxReal get_logp() const { return m_logp; }

    private:
        cmxReal m_probs[num_colors];
        cmxReal m_logp;
    };

    /**
     * Visitor for generating a random pseudo-population
     */
    class sample_visitor {
    public:
        /**
         * Constructor
         * @param rng - random number generator
         * @param frame - output pseudo-population frame, pre-sized
         */
        sample_visitor(cmxRand& rng, cmxPseudopopFrame_t& frame)
            : m_rng(rng), m_frame(frame) {}

        /**
         * Visitor callback - generates random boolean at (i_row, i_col)
         * @param frame - Current pseudo-population (unused)
         * @param i_row - Row index into frame
         * @param i_col - Column index into frame
         * @param p_ki - Probability of selecting knock-in color
         * @return color at (i_row, i_col)
         */
        inline cmxBool operator()(const cmxPseudopopFrame_t& frame,
                                  const cmxMatSz_t i_row,
                                  const cmxMatSz_t i_col,
                                  const cmxReal p_ki) {
            // Assume frame aliasing
            cmxAssert(&frame == &m_frame);
            cmxAssertBoundsInc(p_ki, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            // Sample from Bernoulli distribution
            bernoulli_dist_t dis(p_ki);
            const cmxBool color = CMX_TO_UINT(m_rng.sample(dis));
            cmxAssert((color == cmxTRUE) || (color == cmxFALSE));
            // Update pseudo-population entry
            cmxAssertBounds(i_row, CMX_TO_MAT_SZ_T(0), m_frame.n_rows);
            cmxAssertBounds(i_col, CMX_TO_MAT_SZ_T(0), m_frame.n_cols);
            m_frame.at(i_row, i_col) = color;
            return color;
        }

    private:
        cmxRand& m_rng;
        cmxPseudopopFrame_t& m_frame;
    };

    /**
     * Visitor for generating a random pseudo-population and simultaneously
     *  computing log-probability
     */
    class sample_and_logscore_visitor {
    public:
        /**
         * Constructor
         * @param rng - random number generator
         * @param frame - output pseudo-population frame, pre-sized
         */
        sample_and_logscore_visitor(cmxRand& rng, cmxPseudopopFrame_t& frame)
            : m_rng(rng), m_frame(frame), m_logp(CMX_TO_REAL(0.0)) {}

        /**
         * Visitor callback - generates random boolean at (i_row, i_col) and
         *  updates log-score
         * @param frame - Current pseudo-population (unused)
         * @param i_row - Row index into frame
         * @param i_col - Column index into frame
         * @param p_ki - Probability of selecting knock-in color
         * @return color at (i_row, i_col)
         */
        inline cmxBool operator()(const cmxPseudopopFrame_t& frame,
                                  const cmxMatSz_t i_row,
                                  const cmxMatSz_t i_col,
                                  const cmxReal p_ki) {
            // Assume frame aliasing
            cmxAssert(&frame == &m_frame);
            cmxAssertBoundsInc(p_ki, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            // Compute numerically stable probabilities for each color
            // (NOTE - DO NOT SWITCH COMPUTATION ORDER!)
            m_probs[color_ki] = std::max(p_ki, CMX_REAL_EPS);
            m_probs[color_ko] = std::max(CMX_TO_REAL(1.0) - p_ki, CMX_REAL_EPS);
            // Sample from Bernoulli distribution
            bernoulli_dist_t dis(p_ki);
            const cmxBool color = CMX_TO_UINT(m_rng.sample(dis));
            cmxAssert((color == cmxTRUE) || (color == cmxFALSE));
            // Update pseudo-population entry
            cmxAssertBounds(i_row, CMX_TO_MAT_SZ_T(0), m_frame.n_rows);
            cmxAssertBounds(i_col, CMX_TO_MAT_SZ_T(0), m_frame.n_cols);
            m_frame.at(i_row, i_col) = color;
            // Update log-probability
            cmxAssertBounds(color, CMX_TO_UINT(0), CMX_TO_UINT(num_colors));
            m_logp += log(m_probs[color]);
            return color;
        }

        /**
         * @return log-probability from Polya urn traversal
         */
        inline cmxReal get_logp() const { return m_logp; }

    private:
        cmxRand& m_rng;
        cmxPseudopopFrame_t& m_frame;
        cmxReal m_probs[num_colors];
        cmxReal m_logp;
    };

public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Computes log-probability of pseudo-population frame assuming it was
     *  sampled from a Polya Urn model
     * @param frame - Pseudo-population matrix to score
     * @param indices - Block (column) indices of sub-population
     * @param model - owning model
     * @param sim - outer simulation
     * @return Polya urn log-probability
     */
    static cmxReal logscore(const cmxPseudopopFrame_t& frame,
                            const cmxUIVecCol& indices,
                            const model_t& model,
                            const sim_t& sim) {
        logprob_visitor v;
        traverse(v, frame, indices, model, sim);
        return v.get_logp();
    }

    /**
     * Utility samples pseudo-population according to Polya urn sampler
     *  centered at corresponding Hi-C frequencies
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to forward sample
     * @param indices - Block (column) indices of sub-population
     * @param model - Parent model
     * @param sim - Outer simulation
     * @param rng - Random number generator
     */
    static void sample(cmxPseudopopFrame_t& frame,
                       const cmxUIVecCol& indices,
                       const model_t& model,
                       const sim_t& sim,
                       cmxRand& rng) {
        sample_visitor v(rng, frame);
        traverse(v, frame, indices, model, sim);
    }

    /**
     * Utility samples and scores a pseudo-population according to Polya urn
     *  sampler centered at corresponding Hi-C frequencies
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to forward sample
     * @param indices - Block (column) indices of sub-population
     * @param model - Parent model
     * @param sim - Outer simulation
     * @param rng - Random number generator
     * @return Polya urn log-probability
     */
    static cmxReal sample_and_logscore(cmxPseudopopFrame_t& frame,
                                       const cmxUIVecCol& indices,
                                       const model_t& model,
                                       const sim_t& sim,
                                       cmxRand& rng) {
        sample_and_logscore_visitor v(rng, frame);
        traverse(v, frame, indices, model, sim);
        return v.get_logp();
    }

private:
    /**
     * Visits each (i_row, i_col) cell in pseudo-population frame according to
     *  Polya urn model. Each row of pseudo-population bit matrix is
     *  independently modeled as a Polya urn with color proportion centered at
     *  the corresponding empirical Hi-C frequency
     * @param visitor - Visitor callback for each visited cell
     * @param frame - Pseudo-population bit matrix to traverse
     * @param indices - Block (column) indices of sub-population
     * @param model - Parent model
     * @param sim - Outer simulation
     */
    template <typename t_visitor>
    static void traverse(t_visitor& visitor,
                         const cmxPseudopopFrame_t& frame,
                         const cmxUIVecCol& indices,
                         const model_t& model,
                         const sim_t& sim) {
        // Assumptions needed for proper color tabulation
        cmxAssert(CMX_TO_UINT(color_ko) == CMX_TO_UINT(false));
        cmxAssert(CMX_TO_UINT(color_ki) == CMX_TO_UINT(true));
        // Get handle to Hi-C data
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();
        cmxAssertPosEq(hic.n_elem, frame.n_rows);
        // Get initial ball count
        const cmxReal alpha =
            sim.get_shared_ro().get_Pseudopop().get_pseudopop_polya_alpha();
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        // Get number of balls to add of selected color
        const cmxReal delta =
            sim.get_shared_ro().get_Pseudopop().get_pseudopop_polya_delta();
        // Sample according to Polya urn model
        const cmxMatSz_t n_rows = frame.n_rows;
        const cmxMatSz_t n_cols = frame.n_cols;
        const cmxMatSz_t n_ixs = indices.n_elem;
        cmxReal balls[num_colors];
        for (cmxMatSz_t i_row = CMX_TO_MAT_SZ_T(0); i_row < n_rows; ++i_row) {
            cmxAssertBounds(i_row, CMX_TO_MAT_SZ_T(0), hic.n_elem);
            cmxAssertRealBoundsInc(
                hic.at(i_row), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            // For numerical stability, ensure ball counts are always positive
            balls[color_ki] = std::max(alpha * hic.at(i_row), CMX_REAL_EPS);
            balls[color_ko] = std::max(alpha - balls[color_ki], CMX_REAL_EPS);
            for (cmxMatSz_t i_ix = CMX_TO_MAT_SZ_T(0); i_ix < n_ixs; ++i_ix) {
                const cmxMatSz_t i_col = CMX_TO_MAT_SZ_T(indices.at(i_ix));
                cmxAssertBounds(i_col, CMX_TO_MAT_SZ_T(0), n_cols);
                // Compute probability of knock-in success based on urn state
                cmxAssert((balls[color_ko] + balls[color_ki]) >
                          CMX_TO_REAL(0.0));
                const cmxReal p_ki_raw =
                    balls[color_ki] / (balls[color_ko] + balls[color_ki]);
                // Ensure probability is in [0,1]
                const cmxReal p_ki = std::min(
                    CMX_TO_REAL(1.0), std::max(CMX_TO_REAL(0.0), p_ki_raw));
                cmxAssertRealBoundsInc(
                    p_ki, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
                // Defer to visitor for color at (i_row, i_col)
                const cmxBool color = visitor(frame, i_row, i_col, p_ki);
                // Update urn
                cmxAssertBounds(color, CMX_TO_UINT(0), CMX_TO_UINT(num_colors));
                balls[color] += delta;
                // For numerical stability, ensure positive color counts
                balls[color] = std::max(balls[color], CMX_REAL_EPS);
            }
        }
    }

    // Disallow instantiation, copy, and assignment for now
    cmxPseudopopPolya();
    cmxPseudopopPolya(const cmxPseudopopPolya&);
    cmxPseudopopPolya& operator=(const cmxPseudopopPolya&);
};

#endif  // cmxPseudopopPolya_h
