//****************************************************************************
// cmxExportBnet.h
//****************************************************************************

#ifndef cmxExportBnet_h
#define cmxExportBnet_h

#include "cmxBuild.h"
#include "cmxBnetBoolCt.h"
#include "cmxBnetDagTypes.h"
#include "cmxTypes.h"

#include <string>
#include <vector>

namespace cmxExport {

/**
 * Write Bayesian network DAG adjacency mapping in CSV format
 * @param adjmap - DAG adjacency map to export
 * @param fpath - output file path
 * @return TRUE if write successful, FALSE o/w
 */
extern cmxBool write_bnet_dag_adjmap_csv(const cmxBnetDagAdjMap_t& adjmap,
                                         const std::string& fpath);

/**
 * Write Bayesian network DAG adjacency mapping in Graphviz DOT format
 * @param adjmap - DAG adjacency map to export
 * @param fpath - output file path
 * @return TRUE if write successful, FALSE o/w
 */
extern cmxBool write_bnet_dag_adjmap_gv(const cmxBnetDagAdjMap_t& adjmap,
                                        const std::string& fpath);

/**
 * Write Bayesian network topological order in CSV format
 * @param order - topological ordering of Bayesian network nodes to export
 * @param fpath - output file path
 * @return TRUE if write successful, FALSE o/w
 */
extern cmxBool write_bnet_order_csv(const std::vector<cmxUInt>& order,
                                    const std::string& fpath);

/**
 * Write Bayesian network conditional probability tables for Boolean state space
 * @param cpts - set of Boolean conditional probability tables
 * @param fpath - output file path
 * @return TRUE if write succesful, FALSE o/w
 */
extern cmxBool write_bnet_bool_cpt_csv(const std::vector<cmxBnetBoolCt>& cpts,
                                       const std::string& fpath);

/**
 * Functor for Bayesian network DAG adjacency map CSV export
 */
typedef struct {
    inline cmxBool operator()(const cmxBnetDagAdjMap_t& adjmap,
                              const std::string fpath) const {
        return write_bnet_dag_adjmap_csv(adjmap, fpath);
    }
    inline std::string get_ext() const { return "csv"; }
} bnet_dag_adjmap_csv_t;

/**
 * Functor for Bayesian network DAG adjacency map Graphviz export
 */
typedef struct {
    inline cmxBool operator()(const cmxBnetDagAdjMap_t& adjmap,
                              const std::string fpath) const {
        return write_bnet_dag_adjmap_gv(adjmap, fpath);
    }
    inline std::string get_ext() const { return "gv"; }
} bnet_dag_adjmap_gv_t;

/**
 * Functor for Bayesian network topological sort order CSV export
 */
typedef struct {
    inline cmxBool operator()(const std::vector<cmxUInt>& order,
                              const std::string fpath) const {
        return write_bnet_order_csv(order, fpath);
    }
    inline std::string get_ext() const { return "csv"; }
} bnet_order_csv_t;

/**
 * Functor for Bayesian network CPT tables CSV export
 */
typedef struct {
    inline cmxBool operator()(const std::vector<cmxBnetBoolCt>& cpts,
                              const std::string& fpath) const {
        return write_bnet_bool_cpt_csv(cpts, fpath);
    }
    inline std::string get_ext() const { return "csv"; }
} bnet_cpt_csv_t;

}  // namespace cmxExport

#endif  // cmxExportBnet_h
