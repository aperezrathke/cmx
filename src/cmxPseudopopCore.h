//****************************************************************************
// cmxPseudopopCore.h
//****************************************************************************

/**
 * @brief Common utilities used by various Pseudo-population classes
 */

#ifndef cmxPseudopopCore_h
#define cmxPseudopopCore_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetBool.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxGlobals.h"
#include "cmxGlue.h"
#include "cmxModelTypes.h"
#include "cmxPseudopopPolya.h"
#include "cmxRand.h"
#include "cmxShufflerFisherYates.h"
#include "cmxStats.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// MCMC utility structures
//****************************************************************************

/**
 * Metropolis state for Pseudo-population model component
 */
typedef struct {
    /**
     * The pseudo-population data
     */
    cmxPseudopopFrame_t frame;

    /**
     * Aggregated contact frequencies (i.e. Pseudo-Hi-C)
     */
    cmxContactFreqs_t freqs;

    /**
     * The cached log target probability
     */
    cmxReal logp_tar;
} cmxPseudopopMocoStateMetro_t;

/**
 * Metropolis-Hastings state for Pseudo-population model component
 */
typedef struct {
    /**
     * The pseudo-population data
     */
    cmxPseudopopFrame_t frame;

    /**
     * Aggregated contact frequencies (i.e. Pseudo-Hi-C)
     */
    cmxContactFreqs_t freqs;

    /**
     * The log probability of proposing alternate state from this state
     */
    cmxReal logp_pro;

    /**
     * The cached log target probability
     */
    cmxReal logp_tar;

    /**
     * Cached Hi-C target probability - should only be used if
     *  cmxPseudopopCore::use_cached_hic_loglike is TRUE
     */
    cmxReal logp_tar_hic;
} cmxPseudopopMocoStateMhast_t;

/**
 * Metropolis-Hastings state for Pseudo-population model component with
 *  cached Hi-C correlation
 */
typedef struct {
    /**
     * The pseudo-population data
     */
    cmxPseudopopFrame_t frame;

    /**
     * Aggregated contact frequencies (i.e. Pseudo-Hi-C)
     */
    cmxContactFreqs_t freqs;

    /**
     * The log probability of proposing alternate state from this state
     */
    cmxReal logp_pro;

    /**
     * The cached log target probability
     */
    cmxReal logp_tar;

    /**
     * Cached Hi-C target probability - should only be used if
     *  cmxPseudopopCore::use_cached_hic_loglike is TRUE
     */
    cmxReal logp_tar_hic;

    /**
     * Cached correlation of freqs with Hi-C
     */
    cmxReal corr;
} cmxPseudopopMocoStateMhast2_t;

//****************************************************************************
// cmxPseudopopCore
//****************************************************************************

template <typename t_Glue>
class cmxPseudopopCore {
public:
    //**************************************************************
    // Types, typedefs, enums
    //**************************************************************

    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Typedefs for utilized model components
     */
    typedef typename glue_t::Bnet_t bnet_moco_t;
    typedef typename glue_t::Clust_t clust_moco_t;
    typedef typename glue_t::Contact_freqs_alpha_t contact_freqs_alpha_moco_t;
    typedef typename glue_t::Contact_freqs_t contact_freqs_moco_t;

    /**
     * MCMC argument
     */
    typedef struct {
        model_t* p_model;
        sim_t* p_sim;
    } Mh_args_t;

    /**
     * Polya urn utility type
     */
    typedef cmxPseudopopPolya<glue_t> polya_t;

    /**
     * Likelihood of observed Hi-C given a fixed pseudo-population will only
     * change if additional likelihood parameters are also random variables.
     * In this case, the likelihood must be re-computed every time, else we
     * can save a few cycles by using a cached log-likelihood.
     */
    enum {
        use_cached_hic_loglike = !(contact_freqs_alpha_moco_t::is_random ||
                                   contact_freqs_moco_t::is_random)
    };

    //**************************************************************
    // Score
    //**************************************************************

    /**
     * Computes log-probability of pseudo-population frame assuming it was
     *  forward-sampled from a corresponding set of Bayesian networks
     * @param frame - Pseudo-population matrix to score
     * @param indices - Block (column) indices of sub-population
     * @param model - owning model
     * @param sim - outer simulation
     * @return Bayesian network log probability
     */
    static cmxReal logscore_bfwd(const cmxPseudopopFrame_t& frame,
                                 const cmxUIVecCol& indices,
                                 const model_t& model,
                                 const sim_t& sim) {
        cmxAssert(check_block_indices(frame, indices));
        const cmxMatSz_t n_ixs = indices.n_elem;
        cmxReal score = CMX_TO_REAL(0.0);
        for (cmxMatSz_t i = 0; i < n_ixs; ++i) {
            const cmxUInt i_pseu = indices.at(i);
            cmxAssertBounds(i_pseu, CMX_TO_UINT(0), CMX_TO_UINT(frame.n_cols));
            // Update score
            score += logscore_bfwd(frame, i_pseu, model, sim);
        }
        return score;
    }

    /**
     * Computes log-probability of pseudo-population column assuming it was
     *  forward-sampled from a corresponding Bayesian network
     * @param frame - Pseudo-population matrix
     * @param i_pseu - Column index to score
     * @param model - owning model
     * @param sim - outer simulation
     * @return Bayesian network log probability
     */
    static cmxReal logscore_bfwd(const cmxPseudopopFrame_t& frame,
                                 const cmxUInt& i_pseu,
                                 const model_t& model,
                                 const sim_t& sim) {
        // Simply compute probability of forward sampling pseudo-population
        // given cluster assignments and the corresponding associated Bayesian
        // network distributions
        cmxAssertBounds(i_pseu, CMX_TO_UINT(0), CMX_TO_UINT(frame.n_cols));
        // Handle to cluster model component
        const clust_moco_t& clust_moco = model.get_Clust();
        // Handle to Bayesian network model component
        const bnet_moco_t& bnet_moco = model.get_Bnet();
        // Determine assigned cluster for this pseudo-sample
        const cmxUInt i_mix = clust_moco.get_mix_index(CMX_TO_UINT(i_pseu));
        cmxAssertBounds(
            i_mix, 0, sim.get_shared_ro().get_Clust().get_mix_size());
        // Handle to associated Bayesian network
        const cmxBnetBool& bnet = bnet_moco.get_bnet(i_mix);
        cmxAssertPosEq(CMX_TO_MAT_SZ_T(bnet.get_cpts().size()), frame.n_rows);
        // Underlying contact bit vector
        const cmxBool* const bits = frame.colptr(CMX_TO_MAT_SZ_T(i_pseu));
        // Score sample
        return bnet.logprob(bits);
    }

    /**
     * Computes log-probability of pseudo-population frame assuming it was
     *  sampled from a Polya Urn model
     * @param frame - Pseudo-population matrix to score
     * @param indices - Block (column) indices of sub-population
     * @param model - owning model
     * @param sim - outer simulation
     * @return Polya urn log probability
     */
    static cmxReal logscore_polya(const cmxPseudopopFrame_t& frame,
                                  const cmxUIVecCol& indices,
                                  const model_t& model,
                                  const sim_t& sim) {
        cmxAssert(check_block_indices(frame, indices));
        return polya_t::logscore(frame, indices, model, sim);
    }

    //**************************************************************
    // Initialization
    //**************************************************************

    /**
     * Initialize Metropolis state to independent, null (no edge) model
     * @param s - Metropolis MCMC state
     * @param config - User configuration
     * @param model - Parent model
     * @param sim - Outer simulation
     */
    static void init(cmxPseudopopMocoStateMetro_t& s,
                     cmxSpConstConfig_t config,
                     model_t& model,
                     sim_t& sim CMX_THREAD_ID_PARAM) {
        // Initialize frequencies and frame
        init(s.freqs, s.frame, config, model, sim CMX_THREAD_ID_ARG);
        // Reset target score
        s.logp_tar = CMX_TO_REAL(0.0);
    }

    /**
     * Initialize Metropolis-Hastings state to independent, null (no edge)
     *  model
     * @param s - Metropolis-Hastings MCMC state
     * @param config - User configuration
     * @param model - Parent model
     * @param sim - Outer simulation
     */
    static void init(cmxPseudopopMocoStateMhast_t& s,
                     cmxSpConstConfig_t config,
                     model_t& model,
                     sim_t& sim CMX_THREAD_ID_PARAM) {
        // Initialize frequencies and frame
        init(s.freqs, s.frame, config, model, sim CMX_THREAD_ID_ARG);
        // Reset proposal scores
        s.logp_pro = CMX_TO_REAL(0.0);
        // Reset target score
        s.logp_tar = CMX_TO_REAL(0.0);
        s.logp_tar_hic = CMX_TO_REAL(0.0);
    }

    /**
     * Initialize Metropolis-Hastings state to independent, null (no edge)
     *  model
     * @param s - Metropolis-Hastings MCMC state
     * @param config - User configuration
     * @param model - Parent model
     * @param sim - Outer simulation
     */
    static void init(cmxPseudopopMocoStateMhast2_t& s,
                     cmxSpConstConfig_t config,
                     model_t& model,
                     sim_t& sim CMX_THREAD_ID_PARAM) {
        // Initialize frequencies and frame
        init(s.freqs, s.frame, config, model, sim CMX_THREAD_ID_ARG);
        // Reset proposal score
        s.logp_pro = CMX_TO_REAL(0.0);
        // Reset target score
        s.logp_tar = CMX_TO_REAL(0.0);
        s.logp_tar_hic = CMX_TO_REAL(0.0);
        // Reset correlation (does not match!)
        s.corr = CMX_TO_REAL(0.0);
    }

    /**
     * Initialize frequencies and frame from user configuration, initializes
     *  to independent (no edge) null model
     * @param freqs - Output pseudo-population contact frequencies
     * @param frame - Output pseudo-population frame (bit matrix)
     * @param config - User configuration
     * @param model - Parent model
     * @param sim - Outer simulation
     */
    static void init(cmxContactFreqs_t& freqs,
                     cmxPseudopopFrame_t& frame,
                     cmxSpConstConfig_t config,
                     model_t& model,
                     sim_t& sim CMX_THREAD_ID_PARAM) {
        ///////////////////////////////////////////////////
        // Allocate pseudo-population frame

        // Determine number of frame columns
        const cmxUInt pseudopop_size =
            cmxConfigUtils::get_pseudopop_size(config);
        cmxAssert(pseudopop_size > 0);

        // Determine number of frame rows
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();
        const cmxUInt hic_size = CMX_TO_UINT(hic.n_elem);
        cmxAssert(hic_size > 0);

        // Allocate pseudo-population frame
        frame.set_size(hic_size /*rows*/, pseudopop_size /*cols*/);
        cmxAssertPosEq(frame.n_rows, hic_size);
        cmxAssertPosEq(frame.n_cols, pseudopop_size);

        ///////////////////////////////////////////////////
        // Initialize to empirical Hi-C frequencies

        // Handle to random number generator
        cmxRand& rng = cmxRng;
        // Generate pseudo-population matrix
        cmxUIVecCol indices;
        cmxShufflerFisherYatesCore::get_sorted_indices(indices, pseudopop_size);
        sample_fishy_trunc(frame, indices, sim, rng);
        // Refresh contact frequencies
        refresh(freqs, frame, sim CMX_THREAD_ID_ARG);
    }

    //**************************************************************
    // Feasibility
    //**************************************************************

    /**
     * Determines if pseudo-population overlaps infeasible corpus
     * @param index_index - Start index within block 'indices' argument for
     *  feasibility testing - will be modified to store block index of first
     *  encountered infeasible pseudo-population column or indices.n_elem if
     *  none encountered
     * @param frame - Pseudo-population bit matrix
     * @param indices - Block (column) indices of sub-population
     * @param sim - Outer simulation
     * @return TRUE if all members of simulation's infeasible population are
     *      not present within parameter pseudo-population frame, FALSE o/w
     */
    static cmxBool is_feasible(cmxMatSz_t& index_index,
                               const cmxPseudopopFrame_t& frame,
                               const cmxUIVecCol& indices,
                               const sim_t& sim) {
        // Handle to infeasible set
        const cmxPseudopopFrame_t& infeas =
            sim.get_shared_ro().get_Bnet_infeas().get_infeas_pop();
        return is_feasible(index_index, frame, indices, infeas);
    }

    /**
     * Determines if pseudo-population overlaps infeasible corpus
     * @param index_index - Start index within block 'indices' argument for
     *  feasibility testing - will be modified to store block index of first
     *  encountered infeasible pseudo-population column or indices.n_elem if
     *  none encountered. Assumes elements before index_index have already
     *  been verified as feasible!
     * @param frame - Pseudo-population bit matrix
     * @param indices - Block (column) indices of sub-population
     * @param infeas - Infeasible population bit matrix
     * @return TRUE if any member of infeasible population is not present
     *      within parameter pseudo-population frame, FALSE o/w
     */
    static cmxBool is_feasible(cmxMatSz_t& index_index,
                               const cmxPseudopopFrame_t& frame,
                               const cmxUIVecCol& indices,
                               const cmxPseudopopFrame_t& infeas) {
        cmxAssert(check_block_indices(frame, indices));
        cmxAssertBoundsInc(index_index, CMX_TO_MAT_SZ_T(0), indices.n_elem);
        if (infeas.empty()) {
            // Early out if no infeasible population present
            index_index = indices.n_elem;
            return cmxTRUE;
        }
        cmxAssertPosEq(frame.n_rows, infeas.n_rows);
        const size_t NUM_BYTES =
            sizeof(cmxPseudopopFrame_t::elem_type) * frame.n_rows;
        for (cmxMatSz_t i_ix = index_index; i_ix < indices.n_elem; ++i_ix) {
            const cmxMatSz_t i_pseu = CMX_TO_MAT_SZ_T(indices.at(i_ix));
            cmxAssertBounds(i_pseu, CMX_TO_UINT(0), frame.n_cols);
            const cmxPseudopopFrame_t::elem_type* const p_pseudo =
                frame.colptr(i_pseu);
            for (cmxMatSz_t i_infeas = CMX_TO_MAT_SZ_T(0);
                 i_infeas < infeas.n_cols;
                 ++i_infeas) {
                const cmxPseudopopFrame_t::elem_type* const p_infeas =
                    infeas.colptr(i_infeas);
                if (0 == memcmp((void*)p_infeas, (void*)p_pseudo, NUM_BYTES)) {
                    // Infeasible column found within pseudo-population!
                    index_index = i_ix;
                    return cmxFALSE;
                }
            }  // end iteration over infeasible set
        }      // end iteration over pseudo-population
        // No infeasible column found within pseudo-population frame
        index_index = indices.n_elem;
        return cmxTRUE;
    }

    //**************************************************************
    // Refresh
    //**************************************************************

    /**
     * Must be called anytime pseudo-population frame is modified. Refreshes
     * the pseudo-population contact frequencies for associated frame
     * @param freqs - Contact frequencies to refresh
     * @param frame - Pseudo-population frame with stale frequencies
     * @param sim - Outer simulation
     */
    static void refresh(cmxContactFreqs_t& freqs,
                        const cmxPseudopopFrame_t& frame,
                        sim_t& sim CMX_THREAD_ID_PARAM) {
        // Update contact frequencies (pseudo Hi-C)
        // BEGIN scoping for timer stat
        {
            CMX_SCOPED_STAT_TIMER(cmxSTAT_PseudpopRefreshFreqsTime);
            cmxUIVecCol& rowsums = sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                                       .get_Pseudopop()
                                       .get_rowsums_buf();
            rowsums = cmxMatrixUtils::sum(frame, 1 /*sum each row*/);
            freqs = cmxMatrixUtils::conv_to<cmxContactFreqs_t>::from(rowsums);
            const cmxReal inv_pseudopop_size =
                CMX_TO_REAL(1.0) / CMX_TO_REAL(frame.n_cols);
            freqs *= inv_pseudopop_size;
        }
        // END scoping for timer stat
#ifdef CMX_BUILD_ENABLE_ASSERT
        // Verify all frequencies are in [0,1]
        for (cmxMatSz_t i = 0; i < freqs.n_elem; ++i) {
            cmxAssertBoundsInc(freqs.at(i), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        }
#endif  // CMX_BUILD_ENABLE_ASSERT
    }

    //**************************************************************
    // Samplers
    //**************************************************************

    /**
     * Generate a random selection of pseudo-population frame columns, will
     *  return a random selection every time this method is called
     * @param sim - Outer simulation
     * @return Handle to view of randomly selected indices of frame columns
     */
    static const cmxUIVecCol& sample_block_indices(
        const sim_t& sim CMX_THREAD_ID_PARAM) {
        const cmxUInt block_size =
            sim.get_shared_ro().get_Pseudopop().get_pseudopop_block_size();
        cmxAssert(block_size > CMX_TO_UINT(0));
        cmxShufflerFisherYates<cmxUIVecCol>& fishy =
            sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                .get_Pseudopop()
                .get_block_shuffler();
        return fishy.shuffle(block_size, cmxRng);
    }

    /**
     * Utility uses Bayesian network forward sampling to generate a new
     *  pseudo-population which does not overlap infeasible corpus
     *  (truncated distribution)
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to forward sample
     * @param indices - Block (column) indices of sub-population
     * @param model - Parent model
     * @param sim - Outer simulation
     * @param rng - Random number generator
     */
    static void sample_bfwd_trunc(cmxPseudopopFrame_t& frame,
                                  const cmxUIVecCol& indices,
                                  const model_t& model,
                                  const sim_t& sim,
                                  cmxRand& rng) {
        CMX_SCOPED_STAT_TIMER(cmxSTAT_PseudpopResampleTime);
        cmxAssert(check_block_indices(frame, indices));
        // Sample new pseudo-population by forward sampling Bayesian networks
        sample_bfwd(frame, indices, model, sim, rng);

        // Repeat while infeasible samples encountered
        // @TODO - How to handle infinite loops!?
        cmxMatSz_t index_index = CMX_TO_MAT_SZ_T(0);
        while (!is_feasible(index_index, frame, indices, sim)) {
            cmxAssertBounds(index_index, CMX_TO_MAT_SZ_T(0), indices.n_elem);
            const cmxMatSz_t i_pseu = CMX_TO_MAT_SZ_T(indices.at(index_index));
            cmxAssertBounds(i_pseu, CMX_TO_MAT_SZ_T(0), frame.n_cols);
            // Resample the failed column
            sample_bfwd(frame, i_pseu, model, sim, rng);
        }
        cmxAssert(index_index == indices.n_elem);
    }

    /**
     * Utility uses Bayesian network forward sampling to generate a new
     *  pseudo-population
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to forward sample
     * @param indices - Block (column) indices of sub-population
     * @param model - Parent model
     * @param sim - Outer simulation
     * @param rng - Random number generator
     */
    static void sample_bfwd(cmxPseudopopFrame_t& frame,
                            const cmxUIVecCol& indices,
                            const model_t& model,
                            const sim_t& sim,
                            cmxRand& rng) {
        cmxAssert(check_block_indices(frame, indices));
        // Forward sample from associated Bayesian network(s)
        for (cmxMatSz_t i_ix = CMX_TO_MAT_SZ_T(0); i_ix < indices.n_elem;
             ++i_ix) {
            const cmxMatSz_t i_pseu = CMX_TO_MAT_SZ_T(indices.at(i_ix));
            cmxAssertBounds(i_pseu, CMX_TO_UINT(0), frame.n_cols);
            sample_bfwd(frame, i_pseu, model, sim, rng);
        }
    }

    /**
     * Utility uses Bayesian network forward sampling to generate a new
     *  sample at parameter column
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to forward sample a single column
     * @param i_pseu - Index of column to sample within frame
     * @param model - Parent model
     * @param sim - Outer simulation
     * @param rng - Random number generator

     */
    static void sample_bfwd(cmxPseudopopFrame_t& frame,
                            const cmxMatSz_t i_pseu,
                            const model_t& model,
                            const sim_t& sim,
                            cmxRand& rng) {
        // Bounds check
        cmxAssert(!frame.empty());
        cmxAssertBounds(i_pseu, CMX_TO_MAT_SZ_T(0), frame.n_cols);
        // Handle to cluster model component
        const clust_moco_t& clust_moco = model.get_Clust();
        // Handle to Bayesian network model component
        const bnet_moco_t& bnet_moco = model.get_Bnet();
        // Determine assigned cluster for this pseudo-sample
        const cmxUInt i_mix = clust_moco.get_mix_index(CMX_TO_UINT(i_pseu));
        cmxAssertBounds(i_mix,
                        CMX_TO_UINT(0),
                        sim.get_shared_ro().get_Clust().get_mix_size());
        // Handle to associated Bayesian network
        const cmxBnetBool& bnet = bnet_moco.get_bnet(i_mix);
        cmxAssertPosEq(CMX_TO_MAT_SZ_T(bnet.get_cpts().size()), frame.n_rows);
        // Forward sample new contact bit vector
        cmxPseudopopFrame_t::elem_type* const bits = frame.colptr(i_pseu);
        bnet.sample(rng, bits);
    }

    /**
     * Utility uses Fisher-Yates shuffle to initialize a random bit matrix
     *  which matches empirical Hi-C frequencies and does not overlap
     *  infeasible corpus (truncated distribution)
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to initialize from Fisher-Yates
     * @param indices - Block (column) indices of sub-population
     * @param hic - Outer simulation
     * @param rng - Random number generator
     */
    static void sample_fishy_trunc(cmxPseudopopFrame_t& frame,
                                   const cmxUIVecCol& indices,
                                   const sim_t& sim,
                                   cmxRand& rng) {
        cmxAssert(check_block_indices(frame, indices));
        // Get handle to Hi-C data
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();

        // Generate bit matrix
        sample_fishy(frame, indices, hic, rng);

        // Repeat sampling if pseudo-population overlaps infeasible corpus
        cmxMatSz_t index_index = CMX_TO_MAT_SZ_T(0);
        while (!is_feasible(index_index, frame, indices, sim)) {
            sample_fishy(frame, indices, hic, rng);
            index_index = CMX_TO_MAT_SZ_T(0);
        }
        cmxAssert(index_index == indices.n_elem);
    }

    /**
     * Utility uses Fisher-Yates shuffle to initialize a random bit matrix
     *  which matches empirical Hi-C frequencies
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to initialize from Fisher-Yates
     * @param indices - Block (column) indices of sub-population
     * @param hic - Empirical Hi-C contact frequencies
     * @param rng - Random number generator
     */
    static void sample_fishy(cmxPseudopopFrame_t& frame,
                             const cmxUIVecCol& indices,
                             const cmxContactFreqs_t& hic,
                             cmxRand& rng) {
        cmxAssert(check_block_indices(frame, indices));
        const cmxMatSz_t pseudosubpop_size = indices.n_elem;
        const cmxReal pseudosubpop_sizef = CMX_TO_REAL(pseudosubpop_size);
        const cmxMatSz_t hic_size = hic.n_elem;
        cmxAssertPosEq(hic.n_elem, frame.n_rows);
        // Reset subpopulation columns
        const size_t NUM_BYTES =
            sizeof(cmxPseudopopFrame_t::elem_type) * frame.n_rows;
        for (cmxMatSz_t i_ix = CMX_TO_MAT_SZ_T(0); i_ix < pseudosubpop_size;
             ++i_ix) {
            const cmxMatSz_t i_col = indices.at(i_ix);
            cmxAssertBounds(i_col, CMX_TO_MAT_SZ_T(0), frame.n_cols);
            cmxPseudopopFrame_t::elem_type* const ptr_col = frame.colptr(i_col);
            memset((void*)ptr_col, cmxFALSE, NUM_BYTES);
        }
        // Match empirical Hi-C frequencies within block's subpopulation
        cmxShufflerFisherYates<cmxUIVecCol> fishy(pseudosubpop_size);
        for (cmxMatSz_t i_hic = 0; i_hic < hic_size; ++i_hic) {
            cmxAssertBounds(i_hic, CMX_TO_MAT_SZ_T(0), frame.n_rows);
            // Empirical Hi-C frequency in [0,1]
            const cmxReal p = hic.at(i_hic);
            // Round to nearest integer count in [0, pseudosubpop_size]
            const cmxReal mf = (p * pseudosubpop_sizef) + CMX_TO_REAL(0.5);
            const cmxMatSz_t m =
                std::min(pseudosubpop_size, CMX_TO_MAT_SZ_T(mf));
            if (m < CMX_TO_MAT_SZ_T(1)) {
                continue;
            }
            // Randomly select 'm' pseudo-samples to have the contact
            const cmxUIVecCol& selected = fishy.shuffle(CMX_TO_UINT(m), rng);
            cmxAssert(selected.n_elem == m);
            cmxAssert(indices.n_elem >= m);
            for (cmxMatSz_t i_sel = CMX_TO_MAT_SZ_T(0); i_sel < m; ++i_sel) {
                const cmxUInt i_ix = selected.at(i_sel);
                cmxAssertBounds(
                    i_ix, CMX_TO_UINT(0), CMX_TO_UINT(indices.n_elem));
                const cmxUInt i_col = indices.at(CMX_TO_MAT_SZ_T(i_ix));
                cmxAssertBounds(
                    i_col, CMX_TO_UINT(0), CMX_TO_UINT(frame.n_cols));
                cmxAssert(frame.at(i_hic, i_col) == cmxFALSE);
                frame.at(i_hic, i_col) = cmxTRUE;
            }
        }
    }

    /**
     * Utility samples pseudo-population according to Polya urn sampler
     *  centered at corresponding Hi-C frequencies and which does not
     *  overlap with infeasible corpus (truncated distribution)
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to forward sample
     * @param indices - Block (column) indices of sub-population
     * @param model - Parent model
     * @param sim - Outer simulation
     * @param rng - Random number generator
     */
    static void sample_polya_trunc(cmxPseudopopFrame_t& frame,
                                   const cmxUIVecCol& indices,
                                   const model_t& model,
                                   const sim_t& sim,
                                   cmxRand& rng) {
        cmxAssert(check_block_indices(frame, indices));
        // Generate bit matrix
        sample_polya(frame, indices, model, sim, rng);

        // Repeat sampling if pseudo-population overlaps infeasible corpus
        cmxMatSz_t index_index = CMX_TO_MAT_SZ_T(0);
        while (!is_feasible(index_index, frame, indices, sim)) {
            sample_polya(frame, indices, model, sim, rng);
            index_index = CMX_TO_MAT_SZ_T(0);
        }
        cmxAssert(index_index == indices.n_elem);
    }

    /**
     * Utility samples pseudo-population according to Polya urn sampler
     *  centered at corresponding Hi-C frequencies
     * @WARNING - DON'T FORGET TO CALL refresh(...) ON ASSOCIATED FRAME'S
     *      ASSOCIATED CONTACT FREQUENCIES!
     * @param frame - Pseudo-population to forward sample
     * @param indices - Block (column) indices of sub-population
     * @param model - Parent model
     * @param sim - Outer simulation
     * @param rng - Random number generator
     */
    static void sample_polya(cmxPseudopopFrame_t& frame,
                             const cmxUIVecCol& indices,
                             const model_t& model,
                             const sim_t& sim,
                             cmxRand& rng) {
        polya_t::sample(frame, indices, model, sim, rng);
    }

#ifdef CMX_BUILD_ENABLE_ASSERT
    /**
     * @WARNING - DOES NOT BOUNDS CHECK INDEX ELEMENTS!
     * @param frame - Parent pseudo-population frame
     * @param indices - Block indices into frame
     * @return TRUE if block indices are unique and non-empty, FALSE o/w
     */
    static cmxBool check_block_indices(const cmxPseudopopFrame_t& frame,
                                       const cmxUIVecCol& indices) {
        cmxAssertBoundsInc(indices.n_elem, CMX_TO_MAT_SZ_T(1), frame.n_cols);
        cmxUIVecCol tmp = indices;
        std::sort(tmp.begin(), tmp.end());
        cmxAssert(tmp.cend() == std::adjacent_find(tmp.cbegin(), tmp.cend()));
        return cmxTRUE;
    }
#endif  // CMX_BUILD_ENABLE_ASSERT

private:
    // Disallow instantiation, copy, and assignment for now
    cmxPseudopopCore();
    cmxPseudopopCore(const cmxPseudopopCore&);
    cmxPseudopopCore& operator=(const cmxPseudopopCore&);
};

#endif  // cmxPseudopopCore_h
