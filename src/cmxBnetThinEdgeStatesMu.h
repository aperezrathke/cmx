//****************************************************************************
// cmxBnetThinEdgeStatesMu.h
//****************************************************************************

/**
 * @brief Class defines the mutable state for legal edges within a thin
 *  DAG representation. Essentially, every instance of a thin DAG must also
 *  have an instance of the mutable edge states. This is in contrast to the
 *  read-only (i.e. immutable) legal edge states which may be shared across
 *  all DAG instances.
 */

#ifndef cmxBnetThinEdgeStatesMu_h
#define cmxBnetThinEdgeStatesMu_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetThinEdgeRemoval.h"
#include "cmxBnetThinEdgeStatesRo.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetThinEdgeStatesMu
//****************************************************************************

/**
 * Stores the mutable state of the allowable edges that may exist within a
 * thin DAG.
 */
class cmxBnetThinEdgeStatesMu {
private:
    /**
     * Enum used for addressing legal edge mutable properties
     */
    enum EdgePropertyMu {
        // Location within source vertex's outgoing list
        cmxBnetThinEdge_mu_dst_ix = 0,
        // Location within destination vertex's incoming list
        cmxBnetThinEdge_mu_src_ix,
        // Number of mutable edge properties
        cmxBnetThinEdge_mu_num
    };

public:
    /**
     * Initialize from read-only thin edge states
     */
    void init(const cmxBnetThinEdgeStatesRo& ro) {
        cmxAssert(ro.get_v_num() > 1);
        cmxAssert(ro.get_e_num() > 0);
        // All edges set to missing
        m_props.set_size(cmxBnetThinEdge_mu_num, /* in_rows */
                         ro.get_e_num()          /* in_cols */
        );
        m_props.fill(CMX_BNET_EDGE_NA);
        cmxAssert(ro.get_e_num() == m_props.n_cols);
    }

    /**
     * @param key - unique legal edge identifier
     * @return cmxTRUE if edge is represented in DAG, cmxFALSE o/w
     */
    inline cmxBool has_edge(const cmxUInt key) const {
#ifdef CMX_BUILD_ENABLE_ASSERT
        // Check if NA is set in one of the fields, then it should be set for
        // all fields (dst_ix and src_ix).
        const cmxUInt dst_ix = this->get_prop(key, cmxBnetThinEdge_mu_dst_ix);
        const cmxUInt src_ix = this->get_prop(key, cmxBnetThinEdge_mu_src_ix);
        const cmxBool has_na =
            (dst_ix == CMX_BNET_EDGE_NA) || (src_ix == CMX_BNET_EDGE_NA);
        cmxAssert((!has_na) || (src_ix == dst_ix));
#endif  // CMX_BUILD_ENABLE_ASSERT
        return CMX_BNET_EDGE_NA !=
               this->get_prop(key, cmxBnetThinEdge_mu_dst_ix);
    }

    /**
     * Callback for when an edge is added to associated DAG. MUST be called
     * immediately after edge is added in order to maintain list of existing
     * edges.
     * @param key - unique legal edge identifier of added edge
     * @param eri - additional data needed for future edge removal in O(1)
     */
    inline void on_add_edge(const cmxUInt key,
                            const cmxBnetThinEdgeRemovalInfo_t eri) {
        cmxAssert(!this->has_edge(key));
        cmxAssert(eri.v_dst_ix != CMX_BNET_EDGE_NA);
        cmxAssert(eri.v_src_ix != CMX_BNET_EDGE_NA);
        this->set_prop(key, cmxBnetThinEdge_mu_dst_ix, eri.v_dst_ix);
        this->set_prop(key, cmxBnetThinEdge_mu_src_ix, eri.v_src_ix);
    }

    /**
     * Callback for when an edge is removed from associated DAG. MUST be
     * called immediately after edge is removed in order to maintain list of
     * existing edges. Essentially, when an edge is removed from our thin DAG
     * representation, O(1) values may be swapped into the vacated entries
     * w/in the adjacency lists; we need to update the status of the swapped
     * edges in order to facilitate future O(1) removal.
     * @param key - unique legal edge identifier of removed edge
     * @param epri - post-removal info needed to update removal state
     * @param ro - read-only properties
     */
    inline void on_remove_edge(const cmxUInt key,
                               const cmxBnetThinEdgePostRemovalInfo_t epri,
                               const cmxBnetThinEdgeStatesRo& ro) {
        cmxAssert(this->has_edge(key));
        cmxAssert(ro.get_v_num() > 1);
        // Propagate any induced swaps from edge removal
        this->internal_on_remove_edge(epri.v_dst_swap,           // check
                                      key,                       // key
                                      ro,                        // ro
                                      ro.get_v_src(key),         // v_src
                                      epri.v_dst_swap,           // v_dst
                                      cmxBnetThinEdge_mu_dst_ix  // prop
        );
        this->internal_on_remove_edge(epri.v_src_swap,           // check
                                      key,                       // key
                                      ro,                        // ro
                                      epri.v_src_swap,           // v_src
                                      ro.get_v_dst(key),         // v_dst
                                      cmxBnetThinEdge_mu_src_ix  // prop
        );
        // Mark edge as removed
        this->set_prop(key, cmxBnetThinEdge_mu_dst_ix, CMX_BNET_EDGE_NA);
        this->set_prop(key, cmxBnetThinEdge_mu_src_ix, CMX_BNET_EDGE_NA);
        cmxAssert(!this->has_edge(key));
    }

    /**
     * Populates edge removal structure for target edge to be removed
     * @WARNING - ASSUMES EDGE EXISTS!
     * @param key - unique legal edge identifier for edge to be removed
     * @param eri - output edge removal structure
     */
    inline void get_eri(const cmxUInt key,
                        cmxBnetThinEdgeRemovalInfo_t& out_eri) const {
        cmxAssert(this->has_edge(key));
        out_eri.v_dst_ix = this->get_prop(key, cmxBnetThinEdge_mu_dst_ix);
        out_eri.v_src_ix = this->get_prop(key, cmxBnetThinEdge_mu_src_ix);
    }

private:
    /**
     * Accessor for querying legal edge properties
     * @param key - unique legal edge identifier
     * @param prop - the property category identifier
     * @return property for legal edge identified by key
     */
    inline cmxUInt get_prop(const cmxUInt key,
                            const enum EdgePropertyMu prop) const {
        cmxAssertBounds(key, 0, m_props.n_cols);
        cmxAssertBounds(CMX_TO_MAT_SZ_T(prop), 0, m_props.n_rows);
        cmxAssert(m_props.n_rows == cmxBnetThinEdge_mu_num);
        return m_props.at(prop, key);
    }

    /**
     * Mutator for modifying legal edge properties
     * @param key - unique legal edge identifier
     * @param prop - the property category identifier
     * @param val - the new value for this property
     * @return property for legal edge identified by key
     */
    inline void set_prop(const cmxUInt key,
                         const enum EdgePropertyMu prop,
                         const cmxUInt val) {
        cmxAssertBounds(key, 0, m_props.n_cols);
        cmxAssertBounds(CMX_TO_MAT_SZ_T(prop), 0, m_props.n_rows);
        cmxAssert(m_props.n_rows == cmxBnetThinEdge_mu_num);
        m_props.at(prop, key) = val;
    }

    /**
     * Handles propagating swap info when an edge is removed
     */
    inline void internal_on_remove_edge(const cmxUInt check,
                                        const cmxUInt key,
                                        const cmxBnetThinEdgeStatesRo& ro,
                                        const cmxUInt v_src,
                                        const cmxUInt v_dst,
                                        const enum EdgePropertyMu prop) {
        if (CMX_BNET_EDGE_NA != check) {
            const cmxUInt key_swap = ro.get_key(v_src, v_dst);
            cmxAssert(this->has_edge(key_swap));
            this->set_prop(key_swap, prop, this->get_prop(key, prop));
            cmxAssert(this->has_edge(key_swap));
        }
    }

    /**
     * A cmxBnetThinEdge_mu_num x N matrix where N is number of legal edges.
     * Each column stores the mutable edge properties for a single edge.
     * SAME ORDER AS CORRESPONDING LEGAL EDGE READ-ONLY DATA!
     */
    cmxUIMatrix m_props;
};

#endif  // cmxBnetThinEdgeStatesMu_h
