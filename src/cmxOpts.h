//****************************************************************************
// cmxOpts.h
//****************************************************************************

/**
 * Defines the program options
 */

#ifndef cmxOpts_h
#define cmxOpts_h

#include "cmxBuild.h"
#include "cmxAssert.h"

/**
 * Enumerated user options
 */
enum cmxOptE {
    /**
     * Prints command line options and how to use program
     * Note: option can only be specified via command line
     */
      cmxOpt_help = 0

    /**
     * Same as cmxOpt_help
     */
    , cmxOpt_app_help

    /**
     * Runs canonical, single-population simulation
     */
    , cmxOpt_app_spop_canon

    /**
     * Run canonical, multi-population simulation
     */
    , cmxOpt_app_mpop_canon

    /**
     * Specifies path to configuration INI file
     * Note: option can only be specified via command line
     */
    , cmxOpt_conf

    /**
     * Specifies optional path to a child configuration
     * Can be specified on command line only for root parent, else must be
     * specified via ini
     */
    , cmxOpt_conf_child

    /**
     * Archetype configuration used by INI file, option values not specified
     * by main INI will be searched for in the archetype - meant to specify
     * defaults and/or common option settings
     */
    , cmxOpt_arch

    /**
     * Specifies base output directory to store generated data
     */
    , cmxOpt_output_dir

    /**
     * Prefix appended to all output data, intended to avoid overwriting data
     * from previous runs
     */
    , cmxOpt_job_id

    /**
     * Number of independent models (Gibbs chains) to simulate
     */
    , cmxOpt_num_models

    // Primer parameters

    /**
     * Steps alloted for priming a model
     */
    , cmxOpt_primer_steps

    /**
     * Maximum number of attempts at priming a model
     */
    , cmxOpt_primer_attempts

    /**
     * Target empirical Hi-C correlation for primed models
     */
    , cmxOpt_primer_corr_hic_thresh

    // Controller parameters

    /**
     * Specifies type of controller (e.g. 'canonical')
     */
    , cmxOpt_controller_type

    /**
     * Number of intermediate Gibbs steps ignored between model iterations,
     * the controller type is free to determine how to interpret and/or
     * may ignore this argument.
     */
    , cmxOpt_controller_thin_steps

    /**
     * Number of simulation iterations required for Gibbs model burn-in, the
     * controller type is free to determine how to interpret and/or may ignore
     * this argument
     */
    , cmxOpt_controller_burn_iters

    /**
     * Number of simulation iterations to perform after burn-in, the
     * controller type is free to determine how to interpret and/or may ignore
     * this argument
     */
    , cmxOpt_controller_post_burn_iters

    // Model parameters

    /**
     * Positive shape argument for Gamma(shape, scale) prior on Dirichlet
     *  process alpha concentration
     */
    , cmxOpt_Dp_stick_alpha_gamma_shape

    /**
     * Positive scale argument for Gamma(shape, scale) prior on Dirichlet
     *  process alpha concentration
     */
    , cmxOpt_Dp_stick_alpha_gamma_scale

    /**
     * Fixed positive alpha concentration parameter for Dirichlet stick-
     * breaking process (monotonic to expected number of clusters)
     */
    , cmxOpt_Dp_stick_alpha

    /**
     * Fixed kappa parameter for Bayesian network directed acyclic graph prior
     */
    , cmxOpt_Bnet_dag_kappa

    /**
     * Path to CSV-formatted, column major allowed edges matrix 
     */
    , cmxOpt_Bnet_dag_edges_path

    /**
     * Fixed alpha parameter for Bayesian network conditional probability
     * table (CPT) prior
     */
    , cmxOpt_Bnet_cpt_alpha

    /**
     * Fixed scale parameter for infeasible (negative) corpus prior, defines
     * how much to penalize probability mass assigned to the infeasible
     * corpus, can be zero but should be *positive* to serve as penalty
     */
    , cmxOpt_Bnet_infeas_scale

    /**
     * Fixed ceiling parameter for infeasible (negative) corpus prior, defines
     * maximum reward attainable for assigning low probability mass to the
     * infeasible corpus, must be non-negative
     */
    , cmxOpt_Bnet_infeas_ceil

    /**
     * File path to infeasible (negative) corpus row-major bit matrix
     */
    , cmxOpt_Bnet_infeas_path

    /**
     * Number of steps to randomize Bayesian networks by sampling from prior
     */
    , cmxOpt_Bnet_randomize_steps

    /**
     * Size of multi-population Bayesian network sub-population selected
     *  during MCMC proposals.
     */
    , cmxOpt_Bnet_mpop_block_size

    /**
     * Percentage of multi-population Bayesian network MCMC sub-population
     *  proposal that corresponds to a weighted selection. Real-value in
     *  [0,1] where 0 -> uniform selection, 1 -> weighted selection, and any
     *  value in-between blends between the two approaches. The weight of
     *  each network corresponds to the associated mixture weight
     */
    , cmxOpt_Bnet_mpop_block_weighted_blend

    /**
     * Integer number of mixture components in [1, inf). For truncated
     * Dirichlet processes, an adequate truncation level may be selected
     * according to methods documented in the following:
     *
     *  (1) Ishwaran, Hemant, and Mahmoud Zarepour. "Markov chain Monte Carlo
     *      in approximate Dirichlet and beta two-parameter process
     *      hierarchical models." Biometrika 87, no. 2 (2000): 371-390.
     *
     * and
     *
     *  (2) Ishwaran, Hemant, and Lancelot F. James. "Gibbs sampling methods
     *      for stick-breaking priors." Journal of the American Statistical
     *      Association 96, no. 453 (2001): 161-173.
     *
     * Note, for most Dirichlet processes, alpha is in (0,10] and hence a size
     * of 50 will have negligible truncation error according to reference (1).
     */
    , cmxOpt_mix_size

    /**
     * Size of pseudo-population
     */
    , cmxOpt_pseudopop_size

    /**
     * Size of pseudo-population sub-population selected during MCMC proposals
     */
    , cmxOpt_pseudopop_block_size

    /**
     * When sampling pseudo-population bit matrix via Polya urn, this defines
     * the initial number of balls (positive real) in the urn
     */
    , cmxOpt_pseudopop_polya_alpha

    /**
     * When sampling pseudo-population bit matrix via Polya urn, this defines
     * the incremental number of balls added (any real value) to the urn of
     * the selected color after each draw. Note the following:
     *      delta == -1 -> hypergeometric distribution (sample w/o replace)
     *      delta == 0 -> binomial or bernoulli (sample w/ replace)
     *      delta == 1 -> beta-bernoulli
     */
    , cmxOpt_pseudopop_polya_delta

    /**
     * When sampling pseudo-population bit matrix via Bpoly proposal, defines
     *  cutoff correlation at which below a Polya urn is used, else the parent
     *  Bayesian network(s) are used
     */
    , cmxOpt_pseudopop_bpoly_corr_thresh

    /**
     * Fixed alpha parameter for empirical Hi-C likelihood
     */
    , cmxOpt_hic_alpha

    /**
     * File path to observed Hi-C data, assumes frequencies in [0,1]
     */
    , cmxOpt_hic_path

    // Observer parameters

    /**
     * Vector argument to enable observer types
     *
     * Format:
     * obs_enable_<0-based_integer:index> = <string:observer_name>
     *
     * where <index> is a unique, sequential 0-based index identifying the
     * the next observer type as specified by <observer_name>. See
     * cmxObs.h for valid types: e.g. logscore_cons, logscore_map,
     * logscore_snap, logscore_trac, corr_hic_cons, corr_hic_map,
     * corr_hic_snap, corr_hic_trac, pseu_hic_map
     *
     * E.g.:
     *
     *  obs_enable_0 = logscore_cons
     *  obs_enable_1 = logscore_trac
     *
     * will create log-score console and tracer observers respectively.
     */
    , cmxOpt_obs_enable

    /**
     * Log-score stdout report interval
     */
    , cmxOpt_obs_logscore_cons_period

    /**
     * The minimal Hi-C correlation needed for a chain MAP estimate to be captured
     */
    , cmxOpt_obs_logscore_mapc_min_corr_hic

    /**
     * The minimal Hi-C correlation needed for a global MAP estimate to be captured
     */
    , cmxOpt_obs_logscore_mapg_min_corr_hic

    /**
     * Start iteration for log-score chain MAP observer
     */
    , cmxOpt_obs_logscore_mapc_start_iter

    /**
     * Start iteration for log-score global MAP observer
     */
    , cmxOpt_obs_logscore_mapg_start_iter

    /**
     * Log-score snapshot is taken on iterations which are multiples of period
     */
    , cmxOpt_obs_logscore_snap_period

    /**
     * [Optional] Set to 1 to enable CSV export of log-score trace plot
     */
    , cmxOpt_obs_logscore_trac_export

    /**
     * Log-score trace plots record history on multiples of period
     */
    , cmxOpt_obs_logscore_trac_period

    /**
     * Log-score trace plots start iteration (default = 0)
     */
    , cmxOpt_obs_logscore_trac_start_iter

    /**
     * Hi-C correlation stdout report interval
     */
    , cmxOpt_obs_corr_hic_cons_period

    /**
     * Hi-C correlation snapshot is taken on iterations which are multiples of
     * period
     */
    , cmxOpt_obs_corr_hic_snap_period

    /**
     * [Optional] Set to 1 to enable CSV export of Hi-C correlation trace plot
     */
    , cmxOpt_obs_corr_hic_trac_export

    /**
     * Hi-C correlation trace plots record history on multiples of period
     */
    , cmxOpt_obs_corr_hic_trac_period

    /**
     * Hi-C correlation trace plots start iteration (default=0)
     */
    , cmxOpt_obs_corr_hic_trac_start_iter

    /**
     * [Optional] History capacity (before old values overwritten and possibly
     *  flushed to disk) for Bayesian network ring observer (default=500).
     *
     *  Usage --obs_bnet_ring_capacity <+integer>
     */
    , cmxOpt_obs_bnet_ring_capacity

    /**
     * [Optional] Set to 1 to enable export of Bayesian network CPT history
     *  (default=0). Usage --obs_bnet_ring_export_cpt <0|1>
     */
    , cmxOpt_obs_bnet_ring_export_cpt
    
    /**
     * [Optional] Set to 1 to enable export of Bayesian network DAG
     *  destination-to-source edge mapping history (default=1).
     *
     *  Usage --obs_bnet_ring_export_dst2src <0|1>
     */
    , cmxOpt_obs_bnet_ring_export_dst2src

    /**
     * [Optional] Set to 1 to enable export of Bayesian network CPT history
     *  (default=0). Usage --obs_bnet_ring_export_cpt <0|1>
     */
    , cmxOpt_obs_bnet_ring_export_order

    /**
     * [Optional] Set to 1 to enable export of Bayesian network DAG
     *  source-to-destination edge mapping history (default=1).
     *
     *  Usage --obs_bnet_ring_export_srct2dst <0|1>
     */
    , cmxOpt_obs_bnet_ring_export_src2dst

    /**
     * [Optional] Bayesian network ring history performs collection on
     *  multiples of this period (default=1).
     *
     *  Usage --obs_bnet_ring_period <+integer>
     */
    , cmxOpt_obs_bnet_ring_period

    /**
     * [Optional] Bayesian network ring history start iteration (default=1).
     *
     *  Usage --obs_bnet_ring_start_iter <non-neg integer>
     */
    , cmxOpt_obs_bnet_ring_start_iter

    /**
     * [Optional] Set to 1 to enable ring history tracking for Bayesian
     *  network CPTs, automatically enabled if:
     *      obs_bnet_ring_export_cpt=1 (default=0).
     *
     *  Usage --obs_bnet_ring_trac_cpt <0|1>
     */
    , cmxOpt_obs_bnet_ring_trac_cpt

    /**
     * [Optional] Set to 1 to enable ring history tracking for Bayesian
     *  network DAG destination-to-source edge mapping, automatically enabled
     *  if:
     *      obs_bnet_ring_export_dst2src=1 (default=1).
     *
     *  Usage --obs_bnet_ring_trac_dst2src <0|1>
     */
    , cmxOpt_obs_bnet_ring_trac_dst2src

    /**
     * [Optional] Set to 1 to enable ring history tracking for Bayesian
     *  network topological sort order, automatically enabled if:
     *      obs_bnet_ring_export_order=1 (default=0).
     *
     *  Usage --obs_bnet_ring_trac_order <0|1>
     */
    , cmxOpt_obs_bnet_ring_trac_order

    /**
     * [Optional] Set to 1 to enable ring history tracking for Bayesian
     *  network DAG source-to-destination edge mapping, automatically enabled
     *  if obs_bnet_ring_export_src2dst=1 (default=1).
     *
     *  Usage --obs_bnet_ring_trac_src2dst <0|1>
     */
    , cmxOpt_obs_bnet_ring_trac_src2dst

    /**
     * [Optional] Bayesian network snapshot period
     */
    , cmxOpt_obs_bnet_snap_period

    /**
     * [Optional] Cluster histogram report to stdout period
     */
    , cmxOpt_obs_clust_hist_cons_period

    /**
     * [Optional] Snapshot interval for sorted cluster size observer
     */
    , cmxOpt_obs_clust_size_snap_period

    /**
     * [Optional] Mixture PMF entropy report to stdout period
     */
    , cmxOpt_obs_mix_ent_cons_period

    /**
     * [Optional] Snapshot interval for mixture PMF entropies
     */
    , cmxOpt_obs_mix_ent_snap_period

    /**
     * [Optional] Set to 1 to enable CSV export of mixture entropy traces
     */
    , cmxOpt_obs_mix_ent_trac_export

    /**
     * [Optional] Mixture entropy trace observer performs collections on
     *  multiples of this period
     */
    , cmxOpt_obs_mix_ent_trac_period

    /**
     * [Optional] Mixture entropy trace observer start iteration
     */
    , cmxOpt_obs_mix_ent_trac_start_iter

    /**
     * [Optional] Mixture weights histogram report to stdout period
     */
    , cmxOpt_obs_mix_hist_cons_period

    /**
     * [Optional Snapshot interval for sorted mixture weights observer
     */
    , cmxOpt_obs_mix_pmf_snap_period

    /**
     * [Optional] Bayesian network acceptance probability stdout report
     *  interval
     */
    , cmxOpt_obs_pacc_bnet_cons_period

    /**
     * [Optional] Pseudo-population acceptance probability stdout report
     *  interval
     */
    , cmxOpt_obs_pacc_pseu_cons_period
 
    /**
     * [Optional] Pseudo-population frame stdout report interval
     */
    , cmxOpt_obs_pseu_frame_cons_period

    /**
     * [Optional] Set to 1 to enable CSV export of pseudo-population
     *  frame chain MAP estimates
     */
    , cmxOpt_obs_pseu_frame_mapc_export

    /**
     * [Optional] Set to 1 to enable CSV export of pseudo-population
     *  frame global MAP estimate
     */
    , cmxOpt_obs_pseu_frame_mapg_export

    /**
     * [Optional] Pseudo-population frame snapshot is taken on iterations
     *  which are multiples of period
     */
    , cmxOpt_obs_pseu_frame_snap_period

    /**
     * [Optional] Set to 1 to enable CSV export of pseudo-population Hi-C
     *  chain MAP estimates
     */
    , cmxOpt_obs_pseu_hic_mapc_export

    /**
     * [Optional] Set to 1 to enable CSV export of pseudo-population Hi-C
     *  global MAP estimate
     */
    , cmxOpt_obs_pseu_hic_mapg_export

    // Thread parameters

#ifdef CMX_BUILD_ENABLE_THREADS
    /**
     * Specifies the number of worker threads
     */
    , cmxOpt_num_worker_threads
#endif // CMX_BUILD_ENABLE_THREADS

    // Commandlet parameters

#ifdef CMX_BUILD_ENABLE_COMMANDLETS
    /**
     * Prints basic information about program.
     */
    , cmxOpt_cmdlet_info
#endif // CMX_BUILD_ENABLE_COMMANDLETS

    // Test parameters

#ifdef CMX_BUILD_ENABLE_TESTS

    /**
     * Tests Bnet DAG utilities like cycle checking
     */
    , cmxOpt_test_bnet_thin_dag

    /**
     * Tests Dirichlet distribution utility
     */
    , cmxOpt_test_dir_distrib

    /**
     * Tests various Dirichlet stick-breaking process implementations
     */
    , cmxOpt_test_dp_stick

    /**
     * Tests multi-population Gibbs sampler
     */
    , cmxOpt_test_gibbs_mpop

    /**
     * Tests single-population Gibbs sampler
     */
    , cmxOpt_test_gibbs_spop

    /**
     * Tests global initialization
     */
    , cmxOpt_test_global_init
    
    /**
     * Tests basic hash operations
     */
    , cmxOpt_test_hash

    /**
     * Tests random number generator
     */
    , cmxOpt_test_rng

#ifdef CMX_BUILD_ENABLE_THREADS
    /**
     * Tests simple use case for parallel mapper
     */
    , cmxOpt_test_parallel_mapper
#endif // CMX_BUILD_ENABLE_THREADS

#endif // CMX_BUILD_ENABLE_TESTS

    /**
     * Total number of options, not an actual option
     */
    , cmxOpt_NUM_OPTS
};

/**
 * Info containing the option key, switch, and help description
 */
struct cmxOptInfo {
    /**
     * The key string for use in INI
     */
    const char* ini_key;

    /**
     * The key string for use on the command line
     */
    const char* cmd_switch;

    /**
     * The description output when user types '--help'
     */
    const char* desc;
};

/**
 * Array of option info structures. Array is indexed according to cmxOpt enum.
 */
extern const struct cmxOptInfo CMX_OPT_INFOS[cmxOpt_NUM_OPTS];

/**
 * Callback to print usage for a single switch
 */
extern void cmxOpt_print_usage(const cmxOptE ix);

/**
 * Callback for --help switch
 */
extern void cmxOpt_print_usage_all();

/**
 * Accessor for returning the ini key associated with option
 *
 * @param ix - enumerated index of option
 * @return the ini key string associated with option
 */
inline const char* cmxOpt_get_ini_key(const enum cmxOptE ix) {
    cmxAssertBounds(ix, 0, cmxOpt_NUM_OPTS);
    return CMX_OPT_INFOS[ix].ini_key;
}

/**
 * Accessor for returning the command line switch associated with option
 *
 * @param ix - enumerated index of option
 * @return the command line switch string associated with option
 */
inline const char* cmxOpt_get_cmd_switch(const enum cmxOptE ix) {
    cmxAssertBounds(ix, 0, cmxOpt_NUM_OPTS);
    return CMX_OPT_INFOS[ix].cmd_switch;
}

/**
 * Function pointer to accessor function
 * Makes it easier for client functions to remain agnostic of INI vs command
 * line keys
 */
typedef const char* (*cmxOpt_get_key_func)(const enum cmxOptE);

#endif  // cmxOpts_h
