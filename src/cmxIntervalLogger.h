//****************************************************************************
// cmxIntervalLogger.h
//****************************************************************************

/**
 * @brief Simple utility for reporting time taken by a code region for each
 *   execution of that code.
 */

#ifndef cmxIntervalLogger_h
#define cmxIntervalLogger_h

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_INTERVAL_LOGGING

#include "cmxAutoTimer.h"
#include "cmxLogf.h"

/**
 * Enumerated unique identifiers for all interval logger stats
 */
enum cmxAllIntervalLoggerStats {
    // Total execution time
    cmxSTAT_TotalExecInterval = 0,
    // ADD PROJECT SPECIFIC STATS HERE
    cmxSTAT_IntervalLoggerMax
};

#define CMX_SCOPED_INTERVAL_LOGGER(stat_id) \
    cmxStats::ScopedStatIntervalLogger ScopedLogger_##stat_id(stat_id)

namespace cmxStats {
/**
 * The static descriptions used for interval logging
 */
extern const char* GCounterIntervalLoggerDescs[cmxSTAT_IntervalLoggerMax];

/**
 * A synchronous timer for a single interval. Interval time span is
 * reported on destruction
 */
class ScopedStatIntervalLogger {
public:
    explicit ScopedStatIntervalLogger(const cmxAllIntervalLoggerStats stat_id)
        : m_stat_id(stat_id) {}

    ~ScopedStatIntervalLogger() {
        cmxLogf("%s\n", GCounterIntervalLoggerDescs[m_stat_id]);
    }

private:
    const cmxAllIntervalLoggerStats m_stat_id;
    cmxAutoTimer m_timer;
};
}  // namespace cmxStats

#else

#define CMX_SCOPED_INTERVAL_LOGGER(stat_id) ((void)0)

#endif  // CMX_BUILD_ENABLE_INTERVAL_LOGGING

#endif  // cmxIntervalLogger_h
