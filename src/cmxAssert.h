//****************************************************************************
// cmxAssert.h
//****************************************************************************

/**
 * @brief Assertion hooks
 *
 * cmxAssert(expr_) - If assertions are enabled, expr_ is evaluated and
 *  execution is halted if expr_ == false. If assertions are disabled, then
 *  this code is a noop and has no performance penalty.
 *
 * cmxVerify(expr_) - If assertions are enabled, then will behave exactly as
 *  cmxAssert. If assertions are disabled, then will still evaluate expr_ but
 *  program will no longer halt if expr_ evaluates to false.
 */

#ifndef cmxAssert_h
#define cmxAssert_h

#include "cmxBuild.h"
#include "cmxTypes.h"

/**
 * Make sure that NDEBUG is defined if asserts are not enabled. C++ standard
 * specifies to define NDEBUG if assertions are to be turned off.
 */
#ifndef CMX_BUILD_ENABLE_ASSERT
#   ifndef NDEBUG
#       define NDEBUG
#   endif // NDEBUG
#endif // CMX_BUILD_ENABLE_ASSERT

/**
 * Add compiler messages on MSVC and GCC compilers to let us know that
 * assertions are enabled.
 */
#if !defined(NDEBUG) || defined(CMX_BUILD_ENABLE_ASSERT)
#   ifdef CMX_BUILD_COMPILER_MSVC
#       pragma message("Assertions enabled.")
#   elif (defined(CMX_BUILD_COMPILER_GCC) || defined(CMX_BUILD_COMPILER_ICC))
#       pragma message "Assertions enabled."
#   endif // compiler check
#endif // !defined(NDEBUG) || defined(CMX_BUILD_ENABLE_ASSERT)

#include <assert.h>

//****************************************************************************
// Override
//****************************************************************************

#ifdef CMX_BUILD_COMPILER_MSVC
#   ifdef _WIN64
#       include <intrin.h>
#   endif // _WIN64
#endif // CMX_BUILD_COMPILER_MSVC

/**
 * This part is ugly, but, at the end of the day, to toggle assertions,
 * just define CMX_BUILD_ENABLE_ASSERT within cmxBuild.h or through command line
 * and this part will override (if need be) to give the desired behavior.
 *
 * We want to route to the std assert implementation in the following
 * conditions:
 * - Asserts are disabled via undefined CMX_BUILD_ENABLE_ASSERT directive
 * - or standard NDEBUG preprocessor directive is not defined and asserts are
 *   enabled
 */
#if (!defined(CMX_BUILD_ENABLE_ASSERT) || (defined(CMX_BUILD_ENABLE_ASSERT) && !defined(NDEBUG)))
    // Defer to std assert implementation
#   define cmxAssert assert
#else
    /**
     * Else, in CMake build system, it's hard to modify the NDEBUG flag which
     * is always defined in release builds. So if we really want asserts in
     * release mode, we need to 'override' by routing to our own version.
     */
    inline void cmxAssert(const bool expr) {
        if (!expr) {
            /**
             * Call compiler/platform specific break routine. Note: asm int 3
             * is inline assembly to call interrupt 3 which usually
             * programmatically adds a breakpoint (or traps to the debugger)
             */
    #ifdef CMX_BUILD_COMPILER_MSVC
            /**
             * https://msdn.microsoft.com/en-us/library/45yd4tzz.aspx
             * see also __debugbreak() intrinsic:
             * https://msdn.microsoft.com/en-us/library/f408b4et.aspx
             */
        #ifdef _WIN64
            // inline assembly is not supported for 64-bit architectures
            __debugbreak();
        #else
            __asm int 3;
        #endif // _WIN64
    #elif defined(CMX_BUILD_COMPILER_GCC)
            // http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
            __asm__("int $3");
    #elif defined(CMX_BUILD_COMPILER_ICC)
            /**
             * Intel compiler supports both microsoft and gcc style asm tags
             * but recommends gcc/gnu-style
             * https://software.intel.com/en-us/node/513428
             */
            __asm__("int $3");
    #else
    #       error Unrecognized platform
    # endif // compiler check
        }
    }
#endif // override assert

//****************************************************************************
// Macros
//****************************************************************************

// Additional includes for assertion utilities
#ifdef CMX_BUILD_ENABLE_ASSERT
#   include <cmath>
#   include <limits>
#endif // CMX_BUILD_ENABLE_ASSERT

/**
 * Asserts that a == b and a > 0 (is positive)
 */
#define cmxAssertPosEq(a, b) cmxAssert(((a) > 0) && ((a) == (b)))

/**
 * Bounds checking that element i is within [l, u)
 */
#define cmxAssertBounds(i_, l_, u_) cmxAssert(((i_) >= (l_)) && ((i_) < (u_)))

/**
 * Bounds checking inclusive that element i is within [l, u]
 */
#define cmxAssertBoundsInc(i_, l_, u_) \
    cmxAssert(((i_) >= (l_)) && ((i_) <= (u_)))

//****************************************************************************
// Macros for floating-point numbers
//****************************************************************************

/**
 * Asserts on floating point equality within a threshold
 */
#define cmxAssertRealEq(a, b) cmxAssert(CMX_REAL_CMP_EQ(a, b))

/**
 * Asserts on floating point a <= b within a threshold
 */
#define cmxAssertRealLte(a, b) cmxAssert(CMX_REAL_CMP_LTE((a), (b)))

/**
 * Asserts on floating point a >= b within a threshold
 */
#define cmxAssertRealGte(a, b) cmxAssert(CMX_REAL_CMP_GTE((a), (b)))

/**
 * Asserts that a == b and a > 0 (is positive) for real values
 */
#define cmxAssertRealPosEq(a, b)           \
    do {                                   \
        cmxAssert((a) > CMX_TO_REAL(0.0)); \
        cmxAssertRealEq(a, b);             \
    } while (0)

/**
 * Bounds checking that real element i is within [l, u)
 */
#define cmxAssertRealBounds(i_, l_, u_) \
    do {                                \
        cmxAssertRealGte((i_), (l_));   \
        cmxAssert((i_) < (u_));         \
    } while (0)

/**
 * Bounds checking inclusive that real element i is within [l, u]
 */
#define cmxAssertRealBoundsInc(i_, l_, u_) \
    do {                                   \
        cmxAssertRealGte((i_), (l_));      \
        cmxAssertRealLte((i_), (u_));      \
    } while (0)

//****************************************************************************
// Verify
//****************************************************************************

/**
 * Code within a verify(<code>) block will always be executed even if asserts
 * are disabled. However, if asserts are enabled, it will additionally trigger
 * an assert if code evaluates to a false conditional.
 */
#ifdef CMX_BUILD_ENABLE_ASSERT
#   define cmxVerify cmxAssert
#else
#   define cmxVerify(expr_) expr_
#endif  // CMX_BUILD_ENABLE_ASSERT

#endif  // cmxAssert_h
