//****************************************************************************
// cmxBnetInfeasLambdaDetrMoco.h
//****************************************************************************

/**
 * @brief Real-valued (scale, ceil) tuple hyper-parameter for infeasible
 *  (negative) corpus penalty prior
 */

#ifndef cmxBnetInfeasLambdaDetrMoco_h
#define cmxBnetInfeasLambdaDetrMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetInfeasLambdaDetrMoco
//****************************************************************************

/**
 * Simulation-level read-only shared data, provides user configured lambda
 * which consists of a (scale, ceil) tuple of non-negative scalars that define
 * the infeasible corpus penalty.
 */
class cmxBnetInfeasLambdaDetrSharcoRo {
public:
    /**
     * Default constructor
     */
    cmxBnetInfeasLambdaDetrSharcoRo()
        : m_scale(CMX_DEFAULT_BNET_INFEAS_SCALE),
          m_ceil(CMX_DEFAULT_BNET_INFEAS_CEIL) {}

    /**
     * Initializes shared resource(s) - captures user-supplied scalars
     * @param config - user options
     */
    void init(cmxSpConstConfig_t config) {
        // Reset state
        this->clear();
        cmxAssert(m_scale == CMX_DEFAULT_BNET_INFEAS_SCALE);
        cmxAssert(m_ceil == CMX_DEFAULT_BNET_INFEAS_CEIL);
        // Read user configuration
        config->read_into(m_scale, cmxOpt_Bnet_infeas_scale);
        config->read_into(m_ceil, cmxOpt_Bnet_infeas_ceil);
        // Error if negative scale encountered
        if (m_scale < CMX_TO_REAL(0.0)) {
            cmxLogf(
                "Error: negative Bnet infeas scale detected, this will reward "
                "putative infeasible configurations! Exiting.\n");
            exit(cmxExitCode_invalid_infeas);
        }
        // Error if negative ceil encountered
        if (m_ceil < CMX_TO_REAL(0.0)) {
            cmxLogf("Error: negative Bnet infeas ceil detected! Exiting.\n");
            exit(cmxExitCode_invalid_infeas);
        }
    }

    /**
     * Reset read-only state
     */
    void clear() {
        m_scale = CMX_DEFAULT_BNET_INFEAS_SCALE;
        m_ceil = CMX_DEFAULT_BNET_INFEAS_CEIL;
    }

    /**
     * @return user configured scale term
     */
    inline cmxReal get_scale() const { return m_scale; }

    /**
     * @return user configured ceil term
     */
    inline cmxReal get_ceil() const { return m_ceil; }

private:
    /**
     * Scale factor for infeasible corpus penalty prior
     */
    cmxReal m_scale;

    /**
     * Ceiling factor for infeasible corpus penalty prior
     */
    cmxReal m_ceil;
};

/**
 * Root-level deterministic scalar node representing hyper-parameter tuple for
 *  penalty prior on infeasible (negative) corpus configurations
 */
template <typename t_Glue>
class cmxBnetInfeasLambdaDetrMoco : public cmxMoco<t_Glue> {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Component is deterministic and requires no sampling
     */
    enum { is_random = cmxFALSE };

    /**
     * @return user configured scale term
     */
    inline cmxReal get_scale(const sim_t& sim) const {
        const cmxReal lambda_scale =
            sim.get_shared_ro().get_Bnet_infeas_lambda().get_scale();
        cmxAssert(lambda_scale >= CMX_TO_REAL(0.0));
        return lambda_scale;
    }

    /**
     * @return user configured ceil term
     */
    inline cmxReal get_ceil(const sim_t& sim) const {
        const cmxReal lambda_ceil =
            sim.get_shared_ro().get_Bnet_infeas_lambda().get_ceil();
        cmxAssert(lambda_ceil >= CMX_TO_REAL(0.0));
        return lambda_ceil;
    }
};

#endif  // cmxBnetInfeasLambdaDetrMoco_h
