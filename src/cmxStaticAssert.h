//****************************************************************************
// cmxStaticAssert.h
//****************************************************************************

/**
 * @brief Compile time assertion support
 */

#ifndef cmxStaticAssert_h
#define cmxStaticAssert_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

//****************************************************************************
// CMX_STATIC_ASSERT
//****************************************************************************

#ifdef CMX_BUILD_CXX_11
#   include <type_traits>
    /**
     * Compile time assert for expression x. Note, C++11 does not support
     * static_assert w/o a msg. To overcome this, use trick that boost uses
     * and convert input expression to a string (C++17 does support w/o msg).
     */
#   define CMX_STATIC_ASSERT(x) static_assert( x, #x )
    /**
     * Compile time assert for expression x with error message.
     */
#   define CMX_STATIC_ASSERT_MSG(x, msg) static_assert(x, msg)
#else
    /**
     * C++11 not supported, defer to boost implementations
     */
#   include <boost/static_assert.hpp>
#   define CMX_STATIC_ASSERT(x) BOOST_STATIC_ASSERT(x)
#   define CMX_STATIC_ASSERT_MSG(x, msg) BOOST_STATIC_ASSERT_MSG(x, msg)
#endif // CMX_BUILD_CXX_11

#endif  // cmxStaticAssert_h
