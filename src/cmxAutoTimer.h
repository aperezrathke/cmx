//****************************************************************************
// cmxAutoTimer.h
//****************************************************************************

/**
 * @brief Portable utility meant to mimic boost::timer::auto_cpu_timer
 */

#ifndef cmxAutoTimer_h
#define cmxAutoTimer_h

#include "cmxBuild.h"
#include "cmxLogf.h"
#include "cmxTime.h"

/**
 * Simple timer which reports elapsed duration between creation and destruction
 */
class cmxAutoTimer {
public:
    /**
     * Default constructor, initialize creation time
     */
    cmxAutoTimer()
        : m_start_secs_wall(cmx_time_secs_wall()),
          m_start_secs_cpu(cmx_time_secs_cpu()) {}

    /**
     * Destructor reports lifetime of timer in seconds
     */
    ~cmxAutoTimer() {
        cmxLogf("%fs wall, %fs CPU\n",
                cmx_time_secs_wall() - m_start_secs_wall,
                cmx_time_secs_cpu() - m_start_secs_cpu);
    }

private:
    /**
     * Creation times
     */
    double m_start_secs_wall;
    double m_start_secs_cpu;
};

#endif  // cmxAutoTimer_h
