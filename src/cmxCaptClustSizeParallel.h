//****************************************************************************
// cmxCaptClustSizeParallel.h
//****************************************************************************

/**
 * @brief Parallel batch capture of model cluster sizes
 */

#ifndef cmxCaptClustSizeParallel_h
#define cmxCaptClustSizeParallel_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_THREADS

#include "cmxAssert.h"
#include "cmxCaptClustSizeTypes.h"
#include "cmxGlue.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxCaptClustSizeParallel
//****************************************************************************

/**
 * Performs batched capture of model cluster sizes (parallel version)
 */
template <typename t_Glue>
class cmxCaptClustSizeParallel {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

private:
    /**
     * Defines the chunk size - the amount of work each thread grabs and
     * must complete before grabbing another chunk of work
     */
    enum { inc_by = 1 };

    /**
     * Work object to process set of models
     */
    class Work : public cmxMappableWork {
    public:
        /**
         * Constructor
         */
        Work(std::vector<cmxClustSizePqueue_t>& out,
             const std::vector<model_t>& models,
             const sim_t& sim)
            : m_num_models(CMX_TO_UINT(models.size())),
              m_out(out),
              m_models(models),
              m_sim(sim) {
            const size_t NUM_MODELS = models.size();
            m_out.resize(NUM_MODELS);
            cmxAssertPosEq(m_out.size(), m_models.size());
            cmxAssertPosEq((size_t)m_num_models, m_out.size());
        }

        /**
         * Defines what work is to be performed for the parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         * @return TRUE if work still to be done, FALSE if finished
         */
        virtual cmxBool do_range(const cmxUInt ix_start,
                                 const cmxUInt count,
                                 const cmxUInt thread_id) {
            // Perform work chunk
            const cmxUInt end = std::min(ix_start + count, m_num_models);
            // Iterate over range
            for (cmxUInt i = ix_start; i < end; ++i) {
                cmxAssertBounds(((size_t)i), 0, m_models.size());
                cmxAssertBounds(((size_t)i), 0, m_out.size());
                const model_t& model = m_models[i];
                cmxClustSizePqueue_t& pqueue = m_out[i];
                cmx_clust_size_fill(pqueue, model, m_sim);
            }  // End iteration over models
            return end != m_num_models;
        }

    private:
        // Cached number of models
        const cmxUInt m_num_models;
        // Handle to outputs
        std::vector<cmxClustSizePqueue_t>& m_out;
        // Handle to models
        const std::vector<model_t>& m_models;
        // Handle to outer simulation
        const sim_t& m_sim;
    };

public:
    /**
     * Capture sorted cluster sizes at each simulated model
     * @param out - vector of output sorted cluster sizes, will correspond to
     *  each simulated model
     * @param sim - Parent simulation
     */
    static void capture(std::vector<cmxClustSizePqueue_t>& out,
                        const sim_t& sim CMX_THREAD_ID_PARAM) {
        // @TODO - if calling from non - main thread, then default
        // to serial method. Only main thread should be using worker queue.

        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        cmxAssert(CMX_THREAD_ID_0_ARG == CMX_MAIN_THREAD_ID_0_ARG);

        // Create work object
        const std::vector<model_t>& models = sim.get_models();
        Work w(out, models, sim);

        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will likely
        // deadlock as we only have a single, global parallel mapper.
        cmxParMap.wait_until_work_finished(w, inc_by);
    }
};

#else  // !defined (CMX_BUILD_ENABLE_THREADS)

// Default to serial method
#ifdef CMX_BUILD_COMPILER_MSVC
#pragma message( \
    "Warning: parallel cluster size batch capture disabled. Defaulting to serial.")
#elif defined(CMX_BUILD_COMPILER_GCC)
#pragma message \
    "Warning: parallel cluster size batch capture disabled. Defaulting to serial."
#endif  // compiler check

#include "cmxCaptClustSizeSerial.h"

// "Typedef" parallel runner to serial version
#define cmxCaptClustSizeParallel cmxCaptClustSizeSerial

#endif  // CMX_BUILD_ENABLE_THREADS

#endif  // cmxCaptClustSizeParallel_h
