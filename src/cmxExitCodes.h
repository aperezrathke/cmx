//****************************************************************************
// cmxExitCodes.h
//****************************************************************************

/**
 * Defines the program exit codes
 */

#ifndef cmxExitCodes_h
#define cmxExitCodes_h

#include "cmxBuild.h"

/**
 * Exit codes
 */
enum cmxExitCode {
    /**
     * Program terminated normally
     */
    cmxExitCode_normal = 0,
    /**
     * Program terminated with error
     */
    cmxExitCode_error,
    /**
     * Error in Dirichlet process stick-breaking alpha
     */
    cmxExitCode_invalid_dp_stick_alpha,
    /**
     * Error with loading allowed Bayesian network edges
     */
    cmxExitCode_invalid_bnet_dag_edges,
    /**
     * Multi-population Bayesian network must have positive block size
     */
    cmxExitCode_invalid_bnet_mpop_block_size,
    /**
     * Error with loading Hi-C data
     */
    cmxExitCode_invalid_hic,
    /**
     * Error in loaded infeasible (negative) corpus or configuration
     */
    cmxExitCode_invalid_infeas,
    /**
     * Number of models must be positive integer
     */
    cmxExitCode_invalid_num_models,
    /**
     * Mixture size must be positive integer
     */
    cmxExitCode_invalid_mix_size,
    /**
     * Pseudo-population size must be positive integer
     */
    cmxExitCode_invalid_pseudopop_size,
    /**
     * Pseudo-population block size must be positive integer
     */
    cmxExitCode_invalid_pseudopop_block_size,
    /**
     * Pseudo-population Polya urn must maintain positive ball count
     */
    cmxExitCode_invalid_pseudopop_polya
};

#endif  // cmxExitCodes_h
