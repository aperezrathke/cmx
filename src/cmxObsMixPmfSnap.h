//****************************************************************************
// cmxObsMixPmfSnap.h
//****************************************************************************

/**
 * @brief Snapshot observer for mixture probability mass functions (PMF)
 *  sorted by mass size; pmf == mixture weights!
 */

#ifndef cmxObsMixPmfSnap_h
#define cmxObsMixPmfSnap_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxCaptMixPmfParallel.h"
#include "cmxCaptMixPmfTypes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObs.h"
#include "cmxOpts.h"

#include <queue>
#include <vector>

//****************************************************************************
// cmxObsMixPmfSnap
//****************************************************************************

/**
 * Captures and sorts mixture weights (PMF) at regular, user-defined interval
 */
template <typename t_Glue>
class cmxObsMixPmfSnap : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Default console period
     */
    enum { default_period = 1 };

    /**
     * Default constructor
     */
    cmxObsMixPmfSnap() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_mix_pmf_snap_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_period = default_period;
        m_last_snap_iter = CMX_UINT_MAX;
        m_mix_pmfs.clear();
    }

    /**
     * @return true if snapshot does not match iteration
     */
    inline cmxBool is_stale(const cmxUInt iter) const {
        return iter != m_last_snap_iter;
    }

    /**
     * Mark current capture as stale without clearing
     */
    virtual void mark_stale() { m_last_snap_iter = CMX_UINT_MAX; }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, captures sorted
     *  mixture weights at each model chain
     * @param iter - 0-based iteration that has just completed
     * @param sim - outer simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if (should_update(iter)) {
            this->force_update(iter, sim);
        }
    }

    /**
     * Updates snapshot regardless of iteration
     * @param iter - 0-based iteration that has just completed
     * @param sim - outer simulation
     */
    void force_update(const cmxUInt iter, const sim_t& sim) {
        cmxAssert(m_last_snap_iter != iter);
        m_last_snap_iter = iter;
        capt_mix_pmf(m_mix_pmfs, sim);
    }

    /**
     * Utility to capture mixture weights
     * @param out - output priority queues on mixture weights at each model
     * @param - outer simulation
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD
     */
    template <typename t_Capt = cmxCaptMixPmfParallel<glue_t> >
    static void capt_mix_pmf(std::vector<cmxMixPmfPqueue_t>& out,
                             const sim_t& sim) {
        // Defer to batch capture utility
        typedef t_Capt capt_t;
        capt_t::capture(out, sim CMX_MAIN_THREAD_ID_ARG);
    }

    /**
     * Const handle to mixture weights at each model
     */
    inline const std::vector<cmxMixPmfPqueue_t>& get_mix_pmfs() const {
        return m_mix_pmfs;
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_mix_pmf_snap)

private:
    /**
     * @return TRUE if update is scheduled for parameter iteration, FALSE o/w
     */
    inline cmxBool should_update(const cmxUInt iter) const {
        return ((iter % m_period) == CMX_TO_UINT(0)) && is_stale(iter);
    }

    /**
     * Snapshots captured on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Iteration at which last snapshot was captured
     */
    cmxUInt m_last_snap_iter;

    /**
     * Snapshot mixture weights at each model
     */
    std::vector<cmxMixPmfPqueue_t> m_mix_pmfs;
};

#endif  // cmxObsMixPmfSnap_h
