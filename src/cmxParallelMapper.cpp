//****************************************************************************
// cmxParallelMapper.cpp
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_THREADS

#include "cmxAssert.h"
#include "cmxLogf.h"
#include "cmxParallelMapper.h"

//****************************************************************************
// cmxMapperThread
//****************************************************************************

/**
 * Hook to route a thread to its member main
 */
static void thread_entry(void* p_thread) {
    cmxAssert(p_thread);
    cmxMapperThread* const p_mapper_thread = ((cmxMapperThread*)p_thread);
    p_mapper_thread->thread_main();
}

/**
 * Constructs mapper thread
 * @param mapper - owning mapper pool
 * @param thread_id - a unique identifier for this thread
 */
cmxMapperThread::cmxMapperThread(cmxParallelMapper& mapper,
                                 const cmxUInt thread_id)
    : m_mapper(mapper),
      m_id(thread_id),
      mp_sys_thread(new cmxThread_t(thread_entry, (void*)this)) {
    cmxAssert(NULL != mp_sys_thread);
}

/**
 * Destructor
 */
cmxMapperThread::~cmxMapperThread() {
    cmxAssert(NULL != mp_sys_thread);
    // Owning pool is responsible for waking up thread and making sure it
    // exits
    cmxAssert(should_exit());
    // Risky - we are expecting that pool will have woken us up
    if (mp_sys_thread->joinable()) {
        mp_sys_thread->join();
    }
    delete mp_sys_thread;
}

/**
 * Thread entry point - analogous to process main
 */
void cmxMapperThread::thread_main() {
    while (!this->should_exit()) {
        // Wait until notified that work is available
        this->wait_until_work_ready();

        // Kick out of loop if we're exiting
        if (this->should_exit()) {
            break;
        }

        // Do work until we run out of it
        this->do_available_work();
    }
}

/**
 * @return TRUE if thread should exit, FALSE o/w
 */
cmxBool cmxMapperThread::should_exit() const { return m_mapper.exiting(); }

/**
 * Waits until signaled to start
 * Also notifies mapper thread when all workers have become idle
 */
void cmxMapperThread::wait_until_work_ready() const {
    cmxLock_t lk(m_mapper.m_mutex);
    // Note - mapper is responsible for setting initial active worker count
    // upon receiving work (or initializing)
    if (m_mapper.m_num_active_workers.fetch_sub(
            1, cmxTh::memory_order_relaxed) == 1) {
        // Tell mapper we need more work - this is a "pessimistic" notify as the
        // the mapper thread will immediately block (until mutex is released by
        // worker thread wating on cv_work_begin), but we need this to remove
        // race conditions and it should be more efficient than having the
        // mapper thread loop using sleep(0).
        m_mapper.m_cv_all_workers_idle.notify_one();
    }
    // Use loop to handle spurious wake ups
    do {
        m_mapper.m_cv_work_begin.wait(lk);
    } while ((m_mapper.mp_work == NULL) && !m_mapper.exiting());
}

/**
 * Keeps grabbing chunks of work until none is available
 */
void cmxMapperThread::do_available_work() const {
    // Verify that work is valid
    cmxAssert(NULL != m_mapper.mp_work);
    // If we are not incrementing by a positive amount then we could spin
    // forever!
    cmxAssert(m_mapper.m_inc_by > 0);

    const cmxUInt inc_by = m_mapper.m_inc_by.load(cmxTh::memory_order_relaxed);

    // Grab a chunk of work to do until we run out of it
    while (m_mapper.mp_work->do_range(
        m_mapper.m_iter.fetch_add(inc_by, cmxTh::memory_order_relaxed),
        m_mapper.m_inc_by,
        m_id)) {
    }
}

//****************************************************************************
// cmxParallelMapper
//****************************************************************************

/**
 * Default constructor
 */
cmxParallelMapper::cmxParallelMapper()
    : mp_work(NULL),
      m_inc_by(0),
      m_iter(0),
      m_num_active_workers(0),
      m_should_exit(cmxFALSE) {
    cmxAssert(m_inc_by.is_lock_free());
    cmxAssert(m_iter.is_lock_free());
    cmxAssert(m_num_active_workers.is_lock_free());
    cmxAssert(m_should_exit.is_lock_free());
}

/**
 * Destructor
 */
cmxParallelMapper::~cmxParallelMapper() { teardown(); }

/**
 * Can only be called once, initializes thread pool
 */
void cmxParallelMapper::init(const cmxUInt num_threads) {
    cmxAssert(num_threads > 0);
    cmxAssert(m_num_active_workers == 0);
    cmxAssert(m_pool.size() == 0);
    cmxAssert(NULL == mp_work);

    m_should_exit.store(cmxFALSE, cmxTh::memory_order_seq_cst);
    m_pool.resize(num_threads, NULL);
    m_num_active_workers.store(CMX_TO_UINT(m_pool.size()),
                               cmxTh::memory_order_seq_cst);
    // Allocate threads
    cmxLock_t lk(m_mutex);
    m_pool.resize(num_threads, NULL);
    for (size_t i = 0; i < m_pool.size(); ++i) {
        m_pool[i] = new cmxMapperThread(*this, CMX_TO_UINT(i));
        cmxAssert(NULL != m_pool[i]);
    }
    // Block until all worker threads are waiting to be assigned new work
    // (note: waiting on condition variable releases lock)
    wait_until_all_workers_idle(lk);
}

/**
 * Barrier method - waits until workers threads
 * have blocked on condition variable used for work
 * available notification. This helps avoid race
 * conditions that could otherwise occur if work
 * becomes available before worker threads block
 * (which would cause cond. var::notify_all() to be
 * called before worker thread is waiting on it, which
 * when worker thread does finally call wait, will cause
 * the thread to block instead, which leads to deadlock
 * as main thread is counting on workers to decrement
 * m_num_active_workers to 0.
 */
void cmxParallelMapper::wait_until_all_workers_idle(cmxLock_t& lk) {
    // Loop to handle any spurious wake up calls
    do {
        // Wait call releases lock
        m_cv_all_workers_idle.wait(lk);
    } while (m_num_active_workers > 0);
}

/**
 * Processes work object and blocks calling thread until finished.
 * @param work - the work to process in parallel
 * @param inc_by - the chunk size of each work unit processed by a thread
 */
void cmxParallelMapper::wait_until_work_finished(cmxMappableWork& work,
                                                 const cmxUInt inc_by) {
    cmxAssert(NULL == mp_work);
    cmxAssert(inc_by > 0);
    cmxAssert(0 == m_num_active_workers);
    cmxAssert(cmxFALSE == m_should_exit);
    cmxAssert(m_pool.size() > 0);

    mp_work = &work;
    m_iter.store(0, cmxTh::memory_order_relaxed);
    m_inc_by.store(inc_by, cmxTh::memory_order_relaxed);
    m_num_active_workers.store(CMX_TO_UINT(m_pool.size()),
                               cmxTh::memory_order_seq_cst);

    // Note - this is a "pessimistic" lock as any workers that wake up
    // immediately from the notify call will block on the shared mutex.
    // However, we need this to avoid race conditions as we need to
    // guarantee parent mapper is actually waiting on the all workers idle
    // condition variable (cv) before any of the workers can notify on it.
    // (If we didn't guard this section, then it's theoretically possible
    // for all workers to finish, then signal the mapper, before the
    // mapper is actually waiting on the all workers idle cv; in which case,
    // we would deadlock)
    cmxLock_t lk(m_mutex);
    // Wake up worker threads
    m_cv_work_begin.notify_all();
    // Block until all threads finish - releases lock
    wait_until_all_workers_idle(lk);
    cmxAssert(0 == m_num_active_workers);

    // Clear our work pointer
    // (client is responsible for managing its lifetime)
    mp_work = NULL;
}

/**
 * @return TRUE if threads should exit, FALSE o/w
 */
cmxBool cmxParallelMapper::exiting() const {
    return (cmxTRUE == m_should_exit.load(cmxTh::memory_order_relaxed));
}

/**
 * Outputs mapper status
 */
void cmxParallelMapper::log_status() const {
    cmxLogf("-----------------------\n");
    cmxLogf("ParallelMapper status:\n");
    cmxLogf("-mp_work is null: %s\n", mp_work == NULL ? "true" : "false");
    cmxLogf("-m_inc_by: %u\n", (unsigned int)m_inc_by.load());
    cmxLogf("-m_iter: %u\n", (unsigned int)m_iter.load());
    cmxLogf("-m_num_active_workers: %u\n",
            (unsigned int)m_num_active_workers.load());
    cmxLogf("-m_should_exit: %u\n", (unsigned int)m_should_exit.load());
    cmxLogf("-m_pool size: %u\n", (unsigned int)m_pool.size());
    cmxLogf("-----------------------\n");
}

/**
 * Address the following issue:
 * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
 * Basically on windows, if threads are still "alive" before main thread
 * exits, then the program will hang. Therefore, we need to destroy our
 * threads prior to main() exiting.
 */
void cmxParallelMapper::teardown() {
    // Nothing should still be working
    cmxAssert(0 == m_num_active_workers);
    // We shouldn't have any assigned work
    cmxAssert(NULL == mp_work);

    // Signal that we are exiting
    m_should_exit.store(cmxTRUE, cmxTh::memory_order_seq_cst);
    m_cv_work_begin.notify_all();

    // Call thread destructors
    for (size_t i = 0; i < m_pool.size(); ++i) {
        cmxAssert(NULL != m_pool[i]);
        delete m_pool[i];
    }

    m_pool.clear();
}

#endif  // CMX_BUILD_ENABLE_THREADS
