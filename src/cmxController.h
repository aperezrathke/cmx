//****************************************************************************
// cmxController.h
//****************************************************************************

/**
 * @brief Simulation controller interface for managing administration tasks
 *  such as burn-in, convergence, and stop criterion
 */

#ifndef cmxController_h
#define cmxController_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxObs.h"
#include "cmxSmartPtr.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxController
//****************************************************************************

/**
 * Controller manages the detailed running of all models
 */
template <typename t_Glue>
class cmxController {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Virtual destructor
     */
    virtual ~cmxController() {}

    /**
     * Initialize from user configuration, assumed called from main thread
     * @param config - user configuration
     * @param sim - simulation object to manage, observers assumed to be
     *  initialized, this method may add and initialize additional observers
     */
    virtual void init(cmxSpConstConfig_t config, sim_t& sim) {}

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Run simulation for all models until completion
     * @param sim - simulation object to run until completion
     * @return cmxTRUE if simulation successful, cmxFALSE o/w
     */
    virtual cmxBool run(sim_t& sim) { return cmxFALSE; }
};

#endif  // cmxController_h
