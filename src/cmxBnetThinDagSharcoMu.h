//****************************************************************************
// cmxBnetThinDagSharcoMu.h
//****************************************************************************

/**
 * @brief Mutable shared resources used by thin DAGs. Assumes each thread will
 *  be allocated a separate instance of this class!
 */

/**
 * Note the following design framework:
 *  - Shared mutable (i.e. writable) data buffers should be replicated across
 *      threads (thread-local storage - TLS) to avoid race conditions
 *  - Shared read-only (i.e. non-writable|non-mutable) data should be stored
 *      as single instance at simulation level to avoid unnecessary
 *      duplication across threads and therefore conserve memory
 */

#ifndef cmxBnetThinDagSharcoMu_h
#define cmxBnetThinDagSharcoMu_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetThinDagSharcoMu
//****************************************************************************

/**
 * Shared mutable data resources for thin DAGs, client must restrict access
 * to only a single thread
 */
class cmxBnetThinDagSharcoMu {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     * @param sim - outer simulation
     */
    template <typename sim_t>
    void init(cmxSpConstConfig_t config, const sim_t& sim) {}

    /**
     * Clear shared buffers
     */
    void clear() {
        m_in_deg_buf.clear();
        m_stack_buf.clear();
    }

    // Accessors

    /**
     * @return mutable in-degrees scratch buffer, used for cycle checking
     * and topology sorting
     */
    inline std::vector<cmxUInt>& get_in_deg_buf() const { return m_in_deg_buf; }

    /**
     * @return mutable buffer for storing transient zero in-degree vertices,
     *  used for cycle checking and topology sorting
     */
    inline std::vector<cmxUInt>& get_stack_buf() const { return m_stack_buf; }

private:
    /**
     * Scratch buffer needed for cycle checking and topology sorting, stores
     * transient in-degrees at each DAG vertex
     */
    mutable std::vector<cmxUInt> m_in_deg_buf;

    /**
     * Scratch buffer needed for cycle checking and topology sorting, stores
     * the current set of root vertices after incoming edge elimination
     */
    mutable std::vector<cmxUInt> m_stack_buf;
};

#endif  // cmxBnetThinDagSharcoMu_h
