//****************************************************************************
// cmxControllerFactory.h
//****************************************************************************

/**
 * @brief Factory pattern for allocating simulation controller
 */

#ifndef cmxControllerFactory_h
#define cmxControllerFactory_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxSmartPtr.h"

//****************************************************************************
// cmxControllerFactory
//****************************************************************************

/**
 * Factory pattern for dynamically allocating simulation controller. The
 * template t_Glue has same format as defined in "cmxGlue".
 *
 * For more on the Factory pattern, see also:
 * https://en.wikipedia.org/wiki/Factory_(object-oriented_programming)
 * https://stackoverflow.com/questions/757743/what-is-the-difference-between-builder-design-pattern-and-factory-design-pattern
 */
template <typename t_Glue>
class cmxControllerFactory {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Allocates simulation controller based on user configuration
     * @param config - user configuration
     * @return shared pointer to allocated simulation controller
     */
    static sp_controller_t create(cmxSpConstConfig_t config);
};

// Include implementation as we are a template class
#include "cmxControllerFactory.inl"

#endif  // cmxControllerFactory_h
