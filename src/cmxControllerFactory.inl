//****************************************************************************
// cmxControllerFactory.inl
//****************************************************************************

// Not meant to be included more than once
#ifdef cmxControllerFactory_inl
#error "Cmx Controller Factory implementation included multiple times!"
#endif  // cmxControllerFactory_inl
#define cmxControllerFactory_inl

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxAssert.h"
#include "cmxControllerCanonical.h"

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Allocates simulation controller based on user configuration
 * @param config - user configuration
 * @return shared pointer to allocated simulation controller
 */
template <typename t_Glue>
typename cmxControllerFactory<t_Glue>::sp_controller_t
    cmxControllerFactory<t_Glue>::create(cmxSpConstConfig_t config) {
    // @TODO - add support for other controllers based on switching
    // cmxOpt_controller_type. See:
    // https://stackoverflow.com/questions/25084918/factory-method-creating-shared-ptr-object

    // Default to canonical controller (should we warn user?)
    return cmxSmartPtr::make_shared<cmxControllerCanonical<t_Glue> >();
}
