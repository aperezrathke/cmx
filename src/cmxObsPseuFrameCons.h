//****************************************************************************
// cmxObsPseuFrameCons.h
//****************************************************************************

/**
 * @brief Console observer for model pseudo-population frame. Console
 *  observers report values to stdout and typically rely on Snap ('Snapshot')
 *  observers.
 */

#ifndef cmxObsPseuFrameCons_h
#define cmxObsPseuFrameCons_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxOpts.h"
#include "cmxSmartPtr.h"

//****************************************************************************
// cmxObsPseuFrameCons
//****************************************************************************

/**
 * Pseudo-population frame console observer - reports frame to stdout at a
 * regular, user-defined interval.
 *
 * CURRENTLY, ONLY FIRST MODEL'S FRAME IS REPORTED TO STDOUT
 */
template <typename t_Glue>
class cmxObsPseuFrameCons : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Default console period
     */
    enum { default_period = 1 };

    /**
     * Default constructor
     */
    cmxObsPseuFrameCons() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_pseu_frame_cons_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
    }

    /**
     * Reset to default state
     */
    void clear() { m_period = default_period; }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, reports to stdout
     * if iteration matches period
     *
     * CURRENTLY, ONLY FIRST MODEL'S FRAME IS REPORTED TO STDOUT
     *
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if ((iter % m_period) != CMX_TO_UINT(0)) {
            // Early out if not yet at sampling interval
            return;
        }

        const std::vector<model_t>& models = sim.get_models();
        // Report only first model's frame state to limit console spew
        cmxAssert(!models.empty());
        const size_t i = 0;
        const model_t& model = models[i];
        const cmxPseudopopFrame_t& pseu_frame =
            model.get_Pseudopop().get_pseudopop_frame();
        cmxLogf("(ITER %d, MODEL %d) PSEUDO-POPULATION FRAME =\n",
                (int)iter,
                (int)i);
        pseu_frame.print();
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_pseu_frame_cons)

private:
    /**
     * Reports to stdout on multiples of this period
     */
    cmxUInt m_period;
};

#endif  // cmxObsPseuFrameCons_h
