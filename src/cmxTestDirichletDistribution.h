//****************************************************************************
// cmxTestDirichletDistribution.h
//****************************************************************************

/**
 * Commandlet for testing Dirichlet distribution methods
 *
 * Usage:
 * -test_dir_distrib
 */

#ifdef cmxTestDirichletDistribution_h
#   error "Dirichlet Distribution Test included multiple times!"
#endif  // cmxTestDirichletDistribution_h
#define cmxTestDirichletDistribution_h

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_TESTS

#include "cmxAssert.h"
#include "cmxCmdOptsMap.h"
#include "cmxDirichletDistribution.h"
#include "cmxExitCodes.h"
#include "cmxLogf.h"
#include "cmxRand.h"
#include "cmxTypes.h"

#include <vector>

/**
 * Entry point for test
 */
int cmxTestDirichletDistributionMain(const cmxCmdOptsMap& cmd_opts) {
    cmxLogf("Running Test Dirichlet Distribution.\n");

    // Alpha range
    const cmxReal alpha_min = CMX_TO_REAL(0.5);
    const cmxReal alpha_max = CMX_TO_REAL(10.0);
    cmxAssert(alpha_min <= alpha_max);
    cmxAssert(alpha_min > CMX_TO_REAL(0.0));

    // Pseudo-count size
    const cmxUInt K = 10;
    cmxAssert(K > 1);

    // Random number generator
    cmxRand rng;

    // Create pseudo-counts vector
    std::vector<cmxReal> ALPHA(K);
    for (size_t i = 0; i < K; ++i) {
        ALPHA[i] = rng.unif_real(alpha_min, alpha_max);
        cmxAssertBoundsInc(ALPHA[i], alpha_min, alpha_max);
        cmxLogf("ALPHA[%d]: %f\n", ((int)i), ((double)ALPHA[i]));
    }

    // Test generated samples form a pmf
    std::vector<cmxReal> THETA(K, CMX_TO_REAL(0.0));
    cmxDirichletDistribution::sample(rng, K, &(ALPHA[0]), &(THETA[0]));
    cmxReal sum = CMX_TO_REAL(0.0);
    for (size_t i = 0; i < K; ++i) {
        const cmxReal theta = THETA[i];
        cmxAssertBoundsInc(theta, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        cmxLogf("THETA[%d]: %f\n", ((int)i), ((double)theta));
        sum += theta;
    }
    cmxAssertRealEq(sum, CMX_TO_REAL(1.0));
    cmxLogf("SUM(THETA) = %f\n", ((double)sum));

    // Test density calls
    const cmxReal p =
        cmxDirichletDistribution::pdf(K, &(ALPHA[0]), &(THETA[0]));
    const cmxReal lnp =
        cmxDirichletDistribution::lnpdf(K, &(ALPHA[0]), &(THETA[0]));
    cmxAssert(p == exp(lnp));
    cmxLogf("Probability density of sample: %f\n", ((double)p));
    cmxLogf("Log(probability density) of sample: %f\n", ((double)lnp));

    return cmxExitCode_normal;
}

#endif  // CMX_BUILD_ENABLE_TESTS
