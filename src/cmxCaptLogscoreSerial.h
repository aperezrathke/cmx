//****************************************************************************
// cmxCaptLogscoreSerial.h
//****************************************************************************

/**
 * @brief Serial (non-parallel) batch capture of model log-scores
 */

#ifndef cmxCaptLogscoreSerial_h
#define cmxCaptLogscoreSerial_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxCaptLogscoreSerial
//****************************************************************************

/**
 * Performs batched capture of model log-scores (serial version)
 */
template <typename t_Glue>
class cmxCaptLogscoreSerial {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Model component base type
     */
    typedef typename model_t::moco_t moco_t;

    /**
     * Capture log-scores of all models belonging to simulation
     * @param out_logscores_presized - pre-sized vector of output log-scores,
     *  MUST BE SAME SIZE AS NUMBER OF MODELS!
     * @param sim - Outer simulation containing models
     */
    static void capture(std::vector<cmxReal>& out_logscores_presized,
                        sim_t& sim CMX_THREAD_ID_PARAM) {
        std::vector<model_t>& models = sim.get_models();
        cmxAssertPosEq(models.size(), out_logscores_presized.size());
        const size_t num_models = models.size();
        // Iterate over models
        for (size_t i_model = 0; i_model < num_models; ++i_model) {
            model_t& model = models[i_model];
            std::vector<moco_t*>& mocos = model.get_moco_all();
            const size_t num_mocos = mocos.size();
            cmxReal logscore = CMX_TO_REAL(0.0);
            // Iterate over model components
            for (size_t i_moco = 0; i_moco < num_mocos; ++i_moco) {
                moco_t* const p_moco = mocos[i_moco];
                cmxAssert(p_moco);
                logscore += p_moco->logscore(model, sim);
            }  // End iteration over random variables
            cmxAssertBounds(i_model, 0, out_logscores_presized.size());
            // Update model's log-score
            out_logscores_presized[i_model] = logscore;
        }  // End iteration over models
    }
};

#endif  // cmxCaptLogscoreSerial_h
