//****************************************************************************
// cmxPseudopopBpolyImpl.h
//****************************************************************************

/**
 * @brief Pseudo-population implementation uses a combination of forward
 *  sampling from the associated parent Bayesian network(s) or a Polya urn
 *  as the the proposal distribution depending on the current correlation
 */

/**
 * Bpoly - *B*ayesian network and *Poly*a urn
 */

/**
 * Pseudo-population Impl must define:
 *
 *  ::mcmc_mixin_t - The MCMC mixin type (Metropolis vs Metropolis-Hastings)
 *  ::Mh_prime_cand(...) - Primes current and candidate states for MCMC
 *  ::randomize(...) - Randomizes parameter MCMC state
 */

#ifndef cmxPseudopopBpolyImpl_h
#define cmxPseudopopBpolyImpl_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxGlobals.h"
#include "cmxGlue.h"
#include "cmxMcmcMh4gMixin.h"
#include "cmxModelTypes.h"
#include "cmxPearsonCorr.h"
#include "cmxPseudopopCore.h"
#include "cmxRand.h"
#include "cmxShufflerFisherYates.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxPseudopopBpolyImpl
//****************************************************************************

/**
 * Uses Polya urn as proposal distribution for candidate pseudo-population
 * when the Hi-C correlation is poor, otherwise the pseudo-population is
 * forward sampled from the parent Bayesian network(s)
 */
template <typename t_Glue>
class cmxPseudopopBpolyImpl {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * MCMC state type
     */
    typedef cmxPseudopopMocoStateMhast2_t state_t;

    /**
     * Impl interface - MCMC mixin type
     */
    typedef cmxMcmcMh4gMhastMixin<glue_t, state_t> mcmc_mixin_t;

    /**
     * Pseudo-population core utilities
     */
    typedef cmxPseudopopCore<t_Glue> core_t;

    /**
     * Typedefs for utilized model component(s)
     */
    typedef typename glue_t::Contact_freqs_t contact_freqs_moco_t;

    /**
     * Metropolis-Hastings prime (i.e. propose) candidate sample
     */
    static void Mh_prime_cand(state_t& cand,
                              state_t& curr,
                              const cmxUIVecCol& indices,
                              const model_t& model,
                              sim_t& sim CMX_THREAD_ID_PARAM) {
        // Pre-conditions
        cmxAssertPosEq(cand.frame.n_cols, curr.frame.n_cols);
        cmxAssertPosEq(cand.frame.n_rows, curr.frame.n_rows);
        cmxAssertPosEq(cand.freqs.n_elem, curr.freqs.n_elem);
        cmxAssert(core_t::check_block_indices(cand.frame, indices));

        ////////////////////////////////////////////////////////////
        // Candidate: sample new state

        // Handle to random number generator
        cmxRand& rng = cmxRng;
        // Generate candidate bit matrix
        if (bpoly_is_polya(curr, sim)) {
            core_t::sample_polya_trunc(cand.frame, indices, model, sim, rng);
        } else {
            core_t::sample_bfwd_trunc(cand.frame, indices, model, sim, rng);
        }
        // Refresh candidate contact frequencies
        core_t::refresh(cand.freqs, cand.frame, sim CMX_THREAD_ID_ARG);
        // Refresh candidate correlation
        bpoly_refresh_corr(cand, sim);

        ////////////////////////////////////////////////////////////
        // Candidate: determine probability of proposing current

        // This methods computes logp_pro of cand and assigns Bfwd logscore to
        //  logp_tar of curr
        bpoly_refresh_logp_incomplete(cand, curr, indices, model, sim);

        ////////////////////////////////////////////////////////////
        // Current: determine probability of proposing candidate

        // This methods computes logp_pro of curr and assigns Bfwd logscore to
        //  logp_tar of cand
        bpoly_refresh_logp_incomplete(curr, cand, indices, model, sim);

        ////////////////////////////////////////////////////////////
        // Candidate: determine target probability

        // Score candidate state according to Hi-C
        const contact_freqs_moco_t& contact_freqs_moco =
            model.get_Contact_freqs();
        cand.logp_tar_hic = contact_freqs_moco.logscore_using(
            cand.frame, cand.freqs, cand.corr, model, sim);
        // Modulate target likelihood by Hi-C score
        cmxAssert(cand.logp_tar ==
                  core_t::logscore_bfwd(cand.frame, indices, model, sim));
        cand.logp_tar += cand.logp_tar_hic;

        ////////////////////////////////////////////////////////////
        // Current: determine target probability

        if (!core_t::use_cached_hic_loglike) {
            // Re-score current state according to Hi-C
            curr.logp_tar_hic = contact_freqs_moco.logscore_using(
                curr.frame, curr.freqs, curr.corr, model, sim);
        }
        // Verify correct values are cached
        cmxAssert(curr.logp_tar_hic ==
                  contact_freqs_moco.logscore_using(
                      curr.frame, curr.freqs, curr.corr, model, sim));
        cmxAssert(curr.logp_tar ==
                  core_t::logscore_bfwd(curr.frame, indices, model, sim));
        // Modulate target likelihood by Hi-C score
        curr.logp_tar += curr.logp_tar_hic;
    }

    /**
     * Randomize state - sample from prior
     * @param s - Metropolis-Hastings state to randomize from prior
     * @param config - user configuration
     * @param model - parent model
     * @param sim - outer simulation
     */
    static void randomize(state_t& s,
                          cmxSpConstConfig_t config,
                          const model_t& model,
                          sim_t& sim CMX_THREAD_ID_PARAM) {

        ////////////////////////////////////////////////////////////
        // Sample new state

        // Get index vector 0:PSEUDOPOP_SIZE-1
        cmxUIVecCol indices;
        cmxShufflerFisherYatesCore::get_sorted_indices(indices, s.frame.n_cols);
        // Handle to random number generator
        cmxRand& rng = cmxRng;
        // Sample from parent Bayesian network(s)
        core_t::sample_bfwd_trunc(s.frame, indices, model, sim, rng);
        // Refresh contact frequencies
        core_t::refresh(s.freqs, s.frame, sim CMX_THREAD_ID_ARG);
        // Refresh correlation
        bpoly_refresh_corr(s, sim);

        ////////////////////////////////////////////////////////////
        // Determine probability of proposing

        // Note: logp_pro is probability of proposing alternate state,
        //  just set to self proposal bias for now
        s.logp_pro = bpoly_is_polya(s, sim)
                         ? core_t::logscore_polya(s.frame, indices, model, sim)
                         : core_t::logscore_bfwd(s.frame, indices, model, sim);

        ////////////////////////////////////////////////////////////
        // Determine target probability

        // Score according to Hi-C
        const contact_freqs_moco_t& contact_freqs_moco =
            model.get_Contact_freqs();
        s.logp_tar = s.logp_tar_hic = contact_freqs_moco.logscore_using(
            s.frame, s.freqs, s.corr, model, sim);
        // Modulate by likelihood according to parent Bayesian network(s)
        s.logp_tar += core_t::logscore_bfwd(s.frame, indices, model, sim);
    }

private:
    /**
     * @return TRUE if alt state would be sampled from Polya urn, FALSE o/w
     */
    inline static cmxBool bpoly_is_polya(const state_t& s, const sim_t& sim) {
        return s.corr < bpoly_get_corr_thresh(sim);
    }

    /**
     * @return Read-only Bpoly correlation threshold
     */
    inline static cmxReal bpoly_get_corr_thresh(const sim_t& sim) {
        return sim.get_shared_ro()
            .get_Pseudopop()
            .get_pseudopop_bpoly_corr_thresh();
    }

    /**
     * Utility computes correlation with Hi-C
     */
    static void bpoly_refresh_corr(state_t& s, const sim_t& sim) {
        // Handle to empirical Hi-C
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();
        cmxAssertPosEq(hic.n_elem, s.freqs.n_elem);
        // Compute correlation between empirical and pseudo Hi-C
        s.corr =
            cmxPearsonCorr::get(hic.memptr(), s.freqs.memptr(), hic.n_elem);
        cmxAssertRealBoundsInc(s.corr, CMX_TO_REAL(-1.0), CMX_TO_REAL(1.0));
    }

    /**
     * Performs following:
     *  - 'from_' : refresh .logp_pro
     *  - 'to_' : assigns Bfwd contribution to .logp_tar
     */
    static void bpoly_refresh_logp_incomplete(state_t& from_,
                                              state_t& to_,
                                              const cmxUIVecCol& indices,
                                              const model_t& model,
                                              const sim_t& sim) {
        to_.logp_tar = core_t::logscore_bfwd(to_.frame, indices, model, sim);
        if (bpoly_is_polya(from_, sim)) {
            from_.logp_pro =
                core_t::logscore_polya(to_.frame, indices, model, sim);
        } else {
            // Assume we have cached Bfwd probability in .logp_tar
            cmxAssert(to_.logp_tar ==
                      core_t::logscore_bfwd(to_.frame, indices, model, sim));
            from_.logp_pro = to_.logp_tar;
        }
    }
};

#endif  // cmxPseudopopBpolyImpl_h
