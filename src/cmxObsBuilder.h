//****************************************************************************
// cmxObsBuilder.h
//****************************************************************************

/**
 * @brief Builder pattern for allocating and initializing model observers
 */

#ifndef cmxObsBuilder_h
#define cmxObsBuilder_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxController.h"
#include "cmxGlue.h"
#include "cmxObs.h"
#include "cmxSmartPtr.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxObsBuilder
//****************************************************************************

/**
 * Builder pattern for constructing a complex composite object. In this case,
 * the object is an array of dynamically-bound observers which may have
 * acyclic dependencies on each other. The template t_Glue has same format as
 * defined in "cmxGlue".
 *
 * For more on Builder pattern, see also:
 * https://en.wikipedia.org/wiki/Builder_pattern
 * https://stackoverflow.com/questions/757743/what-is-the-difference-between-builder-design-pattern-and-factory-design-pattern
 */
template <typename t_Glue>
class cmxObsBuilder {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Allocates and initializes observers from user configuration
     * @param config - user configuration
     * @param sim - parent simulation with initialized models
     */
    static void init(cmxSpConstConfig_t config, sim_t& sim);
};

// Include implementation as we are a template class
#include "cmxObsBuilder.inl"

#endif  // cmxObsBuilder_h
