//****************************************************************************
// cmxParallelMapper.h
//****************************************************************************

/**
 * @brief Utility calls the same operation on each element of an array but in
 * parallel among a parameter number of threads. It's like Map-Reduce but
 * without the reduce.
 */

#ifndef cmxParallelMapper_h
#define cmxParallelMapper_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_THREADS

#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxMappableWork
//****************************************************************************

/**
 * Interface for an array to be processed in parallel
 */
class cmxMappableWork {
public:
    /**
     * Virtual destructor
     */
    virtual ~cmxMappableWork() {}

    /**
     * Defines what work is to be performed for the parameter range
     * @param ix_start - the starting index to perform work at
     * @param count - the number of work units to perform beginning at
     *  ix_start
     * @param thread_id - a unique calling thread identifier
     * @return TRUE if work still to be done, FALSE if finished
     */
    virtual cmxBool do_range(const cmxUInt ix_start,
                             const cmxUInt count,
                             const cmxUInt thread_id) = 0;
};

//****************************************************************************
// cmxMapperThread
//****************************************************************************

class cmxParallelMapper;

/**
 * Wrapper around system thread.
 */
class cmxMapperThread {
public:
    /**
     * Constructs mapper thread
     * @param mapper - owning mapper pool
     * @param thread_id - a unique identifier for this thread
     */
    cmxMapperThread(cmxParallelMapper& mapper, const cmxUInt thread_id);

    /**
     * Destructor
     */
    ~cmxMapperThread();

    /**
     * Thread entry point - analogous to process main
     */
    void thread_main();

    /**
     * Our thread identifier
     */
    const cmxUInt m_id;

private:
    // Prohibit copy and assignment
    cmxMapperThread(const cmxMapperThread&);
    cmxMapperThread& operator=(const cmxMapperThread&);

    /**
     * @return TRUE if thread should exit, FALSE o/w
     */
    cmxBool should_exit() const;

    /**
     * Waits until signaled to start
     * Also notifies mapper thread when all workers have become idle
     */
    void wait_until_work_ready() const;

    /**
     * Keeps grabbing chunks of work until none is available
     */
    void do_available_work() const;

    /**
     * Parent mapper owning this thread
     */
    cmxParallelMapper& m_mapper;

    /**
     * The actual thread object
     */
    cmxThread_t* mp_sys_thread;
};

//****************************************************************************
// cmxParallelMapper
//****************************************************************************

class cmxParallelMapper {

public:
    /**
     * Default constructor
     */
    cmxParallelMapper();

    /**
     * Destructor
     */
    ~cmxParallelMapper();

    /**
     * Can only be called once, initializes thread pool
     */
    void init(const cmxUInt num_threads);

    /**
     * Address the following issue:
     * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
     * Basically on windows, if threads are still "alive" before main thread
     * exits, then the program will hang. Therefore, we need to destroy our
     * threads prior to main() exiting.
     */
    void teardown();

    /**
     * Processes work object and blocks calling thread until finished.
     * @param work - the work to process in parallel
     * @param inc_by - the chunk size of each work unit processed by a thread
     */
    void wait_until_work_finished(cmxMappableWork& work, const cmxUInt inc_by);

    /**
     * Outputs mapper status
     */
    void log_status() const;

private:
    // Prohibit copy and assignment
    cmxParallelMapper(const cmxParallelMapper&);
    cmxParallelMapper& operator=(const cmxParallelMapper&);

    /**
     * @return TRUE if threads should exit, FALSE o/w
     */
    cmxBool exiting() const;

    /**
     * Barrier method - waits until worker threads have
     * blocked on condition variable used for work
     * available notification. This helps avoid race
     * conditions that could otherwise occur if work
     * becomes available before worker threads block
     * (which would cause cond. var::notify_all() to be
     * called before worker thread is waiting on it, which
     * when worker thread does finally call wait, will cause
     * the thread to block instead, which leads to deadlock
     * as main thread is counting on workers to decrement
     * m_num_active_workers to 0).
     */
    void wait_until_all_workers_idle(cmxLock_t& lk);

    /**
     * The work currently being undertaken - client is responsible for
     * managing this memory - the mapper does not delete this work!
     */
    cmxMappableWork* mp_work;

    /**
     * The amount to increment the iteration counter by. Essentially defines
     * the size of a work package a thread performs before attempting to get
     * the next work package.
     */
    cmxAtomicUInt m_inc_by;

    /**
     * An atomic counter - allows atomic operations such as incrementing and
     * decrementing the counter value. This counter is used for iterating over
     * an array of work.
     */
    cmxAtomicUInt m_iter;

    /**
     * Atomic counter used for keeping track of which threads have outstanding
     * work. It is the responsibility of the threads to update this
     * counter when they complete and then notify parent mapper of this event.
     */
    cmxAtomicUInt m_num_active_workers;

    /**
     * Counter to signal that threads should be exiting
     */
    cmxAtomicUInt m_should_exit;

    /**
     * Mutex used for synchronizing access to condition variables
     */
    cmxMutex_t m_mutex;

    /**
     * Used for notifying threads that work is ready to be processed
     */
    cmxConditionVariable_t m_cv_work_begin;

    /**
     * Used for notifying the main thread that all worker threads have finished
     */
    cmxConditionVariable_t m_cv_all_workers_idle;

    /**
     * Our pool of worker threads
     */
    std::vector<cmxMapperThread*> m_pool;

    /**
     * Give threads direct access
     */
    friend class cmxMapperThread;
};

#endif  // CMX_BUILD_ENABLE_THREADS

#endif  // cmxParallelMapper_h
