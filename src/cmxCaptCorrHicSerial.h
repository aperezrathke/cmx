//****************************************************************************
// cmxCaptCorrHicSerial.h
//****************************************************************************

/**
 * @brief Serial (non-parallel) batch capture of model Hi-C correlations
 */

#ifndef cmxCaptCorrHicSerial_h
#define cmxCaptCorrHicSerial_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxModelTypes.h"
#include "cmxPearsonCorr.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxCaptCorrHicSerial
//****************************************************************************

/**
 * Performs batched capture of model Hi-C correlations (serial version)
 */
template <typename t_Glue>
class cmxCaptCorrHicSerial {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Capture Hi-C correlations of all models belonging to simulation
     * @param out_corrs_presized - pre-sized vector of output correlations,
     *  MUST BE SAME SIZE AS models!
     * @param models - set of models, must be same size as out_corrs_presized
     * @param hic - empirical (observed) Hi-C frequencies
     */
    static void capture(std::vector<cmxReal>& out_corrs_presized,
                        const std::vector<model_t>& models,
                        const cmxContactFreqs_t& hic CMX_THREAD_ID_PARAM) {
        cmxAssertPosEq(models.size(), out_corrs_presized.size());
        const size_t num_models = models.size();
        // Iterate over models
        for (size_t i_model = 0; i_model < num_models; ++i_model) {
            const model_t& model = models[i_model];
            const cmxContactFreqs_t& pseu_hic =
                model.get_Pseudopop().get_pseudopop_freqs();
            cmxAssertPosEq(hic.n_elem, pseu_hic.n_elem);
            // Compute correlation between empirical and pseudo Hi-C
            const cmxReal corr = cmxPearsonCorr::get(
                hic.memptr(), pseu_hic.memptr(), hic.n_elem);
            cmxAssertBounds(i_model, 0, out_corrs_presized.size());
            out_corrs_presized[i_model] = corr;
        }  // End iteration over models
    }
};

#endif  // cmxCaptCorrHicSerial_h
