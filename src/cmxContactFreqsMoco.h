//****************************************************************************
// cmxContactFreqsMoco.h
//****************************************************************************

/**
 * @brief Model component for empirical (observed) contact frequencies (e.g.
 *  Hi-C data).
 */

#ifndef cmxContactFreqsMoco_h
#define cmxContactFreqsMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxHicScorer.h"
#include "cmxMoco.h"
#include "cmxModelTypes.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxContactFreqsMoco
//****************************************************************************

/**
 * Expected contact freqs model component interface:
 *  ::logscore_using(frame, freqs, model, sim) - return the log-likelihood of
 *      observed frequencies using parameter pseudo-population state
 *  ::logscore_using(frame, freqs, corr, model, sim) - similarly returns the
 *      log-likelihood of observed frequencies but may leverage the parameter
 *      correlation (corr) to avoid redundant computations
 * The logscore computation is actually deferred to template Hi-C scorer which
 * has similar stateless interface:
 *  t_HicScorer::logscore_using(frame, freqs, model, sim)
 *  t_HicScorer::logscore_using(frame, freqs, corr, model, sim)
 */
template <typename t_Glue, typename t_HicScorer = cmxHicScorerPearson<t_Glue> >
class cmxContactFreqsMoco : public cmxMoco<t_Glue> {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Hi-C scoring type
     */
    typedef t_HicScorer hic_scorer_t;

    // Model component interface

    // This component is always deterministic!
    enum { is_random = cmxFALSE };

    /**
     * Intended to assess Gibbs sampler convergence ideally showing that chain
     * is exploring high probability regions. This may be an expensive call to
     * make and should be used sparingly at regular intervals.
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    virtual cmxReal logscore(model_t& model, sim_t& sim) {
        typedef typename model_t::Pseudopop_t pseudopop_moco_t;
        const pseudopop_moco_t& pseudopop_moco = model.get_Pseudopop();
        return logscore_using(pseudopop_moco.get_pseudopop_frame(),
                              pseudopop_moco.get_pseudopop_freqs(),
                              model,
                              sim);
    }

    /**
     * Compute log-likelihood of empirical Hi-C data given parameter
     *  pseudo-population state
     * @param frame - pseudo-population contact bit matrix
     * @param freqs - pseudo-population aggregated contact frequencies
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    static inline cmxReal logscore_using(const cmxPseudopopFrame_t& frame,
                                         const cmxContactFreqs_t& freqs,
                                         const model_t& model,
                                         const sim_t& sim) {
        return hic_scorer_t::logscore_using(frame, freqs, model, sim);
    }

    /**
     * Overload to use pre-computed correlation - compute log-likelihood of
     *  empirical Hi-C data given parameter pseudo-population state
     * @param frame - pseudo-population contact bit matrix
     * @param freqs - pseudo-population aggregated contact frequencies
     * @param corr - pre-computed correlation, may help avoid redundant
     *  correlation computations
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    static inline cmxReal logscore_using(const cmxPseudopopFrame_t& frame,
                                         const cmxContactFreqs_t& freqs,
                                         const cmxReal corr,
                                         const model_t& model,
                                         const sim_t& sim) {
        return hic_scorer_t::logscore_using(frame, freqs, corr, model, sim);
    }
};

#endif  // cmxContactFreqsMoco_h
