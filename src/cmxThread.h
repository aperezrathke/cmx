//****************************************************************************
// cmxThread.h
//****************************************************************************

/**
 * Our threading specification
 */

#ifndef cmxThread_h
#define cmxThread_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxLogf.h"

#include <time.h>

#ifdef CMX_BUILD_ENABLE_THREADS

#include "cmxAssert.h"

#include <vector>

#ifdef CMX_BUILD_CXX_11
#include <atomic>
#include <condition_variable>
#include <thread>
#define cmxTh std
#else
#include <boost/atomic.hpp>
#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>
#define cmxTh boost
#endif  // CMX_BUILD_CXX_11

//****************************************************************************
// Defines
//****************************************************************************

// The number of threads to initialize by default
#define CMX_DEFAULT_NUM_THREADS \
    (std::max<cmxUInt>(cmxTh::thread::hardware_concurrency(), 1))

//****************************************************************************
// Typedefs
//****************************************************************************

typedef cmxTh::thread cmxThread_t;
typedef cmxTh::mutex cmxMutex_t;
typedef cmxTh::unique_lock<cmxMutex_t> cmxLock_t;
typedef cmxTh::lock_guard<cmxMutex_t> cmxScopedLock_t;
typedef cmxTh::condition_variable cmxConditionVariable_t;
typedef cmxTh::atomic<unsigned int> cmxAtomicUInt;
typedef cmxTh::atomic<time_t> cmxAtomicTime_t;
typedef cmxTh::atomic<double> cmxAtomicDouble_t;

/**
 * Handle atomic add for double type
 * @param atomic_a - atomic double to increment by b
 * @param double_b - amount to increment a by
 * https://stackoverflow.com/questions/23116279/how-to-perform-basic-operations-with-stdatomic-when-the-type-is-not-integral
 */
#define CMX_ATOMIC_ADD_DOUBLE(atomic_a, double_b)                              \
    do {                                                                       \
        double a_loaded = atomic_a.load();                                     \
        while (!atomic_a.compare_exchange_weak(a_loaded, a_loaded + double_b)) \
            ;                                                                  \
    } while (0)

//****************************************************************************
// Parallel mapper
//****************************************************************************

#include "cmxParallelMapper.h"

//****************************************************************************
// TLS - Our home-brewed thread local storage system
//****************************************************************************

/**
 * Am rolling own TLS system because I don't trust boost TLS or
 * std::thread::id to not trap to the kernel.
 */

#define CMX_MAIN_THREAD_ID_0_ARG 0

#define CMX_MAIN_THREAD_ID_ARG , CMX_MAIN_THREAD_ID_0_ARG

#define CMX_THREAD_ID_VAR_NAME tId__

#define CMX_THREAD_ID_0_ARG CMX_THREAD_ID_VAR_NAME

#define CMX_THREAD_ID_ARG , CMX_THREAD_ID_VAR_NAME

#define CMX_THREAD_ID_0_PARAM const cmxUInt CMX_THREAD_ID_VAR_NAME

#define CMX_THREAD_ID_PARAM , CMX_THREAD_ID_0_PARAM

#define CMX_NUM_WORKER_THREADS_VAR_NAME num_worker_threads

#define CMX_EXTERN_NUM_WORKER_THREADS \
    extern cmxUInt CMX_NUM_WORKER_THREADS_VAR_NAME;

#define CMX_DECLARE_NUM_WORKER_THREADS cmxUInt CMX_NUM_WORKER_THREADS_VAR_NAME;

#define CMX_PARALLEL_MAPPER_VAR_NAME par_map

#define CMX_EXTERN_PARALLEL_MAPPER \
    extern cmxParallelMapper CMX_PARALLEL_MAPPER_VAR_NAME;

#define CMX_DECLARE_PARALLEL_MAPPER \
    cmxParallelMapper CMX_PARALLEL_MAPPER_VAR_NAME;

#define cmxParMap cmxG::CMX_PARALLEL_MAPPER_VAR_NAME

#define CMX_SET_NUM_WORKER_THREADS(config, num) \
    config->set_option(cmxOpt_num_worker_threads, num)

#define CMX_LOG_NUM_WORKER_THREADS            \
    cmxLogf("Available worker threads: %u\n", \
            cmxG::CMX_NUM_WORKER_THREADS_VAR_NAME)

#else  // !defined(CMX_BUILD_ENABLED_THREADS)

typedef unsigned int cmxAtomicUInt;

typedef time_t cmxAtomicTime_t;

typedef double cmxAtomicDouble_t;

#define CMX_ATOMIC_ADD_DOUBLE(atomic_a, double_b) \
    do {                                          \
        atomic_a += double_b;                     \
    } while (0)

#define CMX_MAIN_THREAD_ID_0_ARG

#define CMX_MAIN_THREAD_ID_ARG

#define CMX_THREAD_ID_0_ARG

#define CMX_THREAD_ID_ARG

#define CMX_THREAD_ID_0_PARAM

#define CMX_THREAD_ID_PARAM

#define CMX_EXTERN_NUM_WORKER_THREADS

#define CMX_DECLARE_NUM_WORKER_THREADS

#define CMX_EXTERN_PARALLEL_MAPPER

#define CMX_DECLARE_PARALLEL_MAPPER

#define CMX_SET_NUM_WORKER_THREADS(config, num) ((void)0)

#define CMX_LOG_NUM_WORKER_THREADS ((void)0)

#endif  // CMX_BUILD_ENABLE_THREADS

// Include default thread local storage accessors
#include "cmxThreadTLS.inl"

#endif  // cmxThread_h
