//****************************************************************************
// cmxScalarUnifMoco.h
//****************************************************************************

/**
 * @brief Real-valued scalar hyper-parameter following continuous uniform
 *  distribution.
 */

#ifndef cmxScalarUnifMoco_h
#define cmxScalarUnifMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxGlobals.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxMcmcMh4gMixin.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxRand.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <limits>

//****************************************************************************
// cmxScalarUnifMocoStateMh_t
//****************************************************************************

/**
 * Metropolis-within-Gibbs POD state for uniform scalar Gibbs updating.
 * Assumes a Metropolis symmetric proposal (uniform) and therefore does not
 * store any form of the proposal probability.
 */
typedef struct {
    /**
     * The scalar value associated to this state (proposed or current)
     */
    cmxReal scalar;
    /**
     * The log probability of this state under the target distribution
     */
    cmxReal logp_tar;
} cmxScalarUnifMocoStateMh_t;

//****************************************************************************
// cmxScalarUnifMoco
//****************************************************************************

/**
 * Template arguments:
 *  t_Glue - Glue object as defined in cmxGlue
 *  t_LoHiQuery - stateless policy for querying [lo, hi) uniform distribution
 *      range (likely stored in shared data), must provide static interface:
 *          ::get_lo(model, sim) - return lower bound of uniform range
 *          ::get_hi(model, sim) - return upper bound of uniform range
 *  t_ChildQuery - stateless policy for querying child node properties, must
 *      provide static interface:
 *          ::logscore_using(scalar, model, sim) - returns the log probability
 *              of child node(s) given scalar argument
 *
 * @WARNING - block index is ignored for Gibbs sampling
 */
template <typename t_Glue, typename t_LoHiQuery, typename t_ChildQuery>
class cmxScalarUnifMoco
    : public cmxMoco<t_Glue>,
      public cmxMcmcMh4gMetroMixin<t_Glue, cmxScalarUnifMocoStateMh_t> {
private:
    /**
     * Metropolis[-Hastings] state type
     */
    typedef cmxScalarUnifMocoStateMh_t state_t;

public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Query types
     */
    typedef t_LoHiQuery lo_hi_query_t;
    typedef t_ChildQuery child_query_t;

    /**
     * Metropolis argument
     */
    typedef struct {
        model_t* p_model;
        sim_t* p_sim;
    } Mh_args_t;

    /**
     * Component is random and requires sampling
     */
    enum { is_random = cmxTRUE };

    /**
     * Initialization from user configuration. Model component should seek to
     * initialize to single population, independent null model where possible.
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void init(cmxSpConstConfig_t config,
                      model_t& model,
                      sim_t& sim CMX_THREAD_ID_PARAM) {
        this->init_states(*this, config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Initialize to a random state, should be called after init()
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void randomize(cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {
        this->randomize_states(*this, config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Gibbs posterior update - generates a sample from the conditional
     * posterior distribution as defined by child query policy. Performs
     * Metropolis-within-Gibbs updating to generate new sample
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void Gibbs_posterior(model_t& model,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {
        // To satisfy detailed balance, perform single Metropolis step
        Mh_args_t mh_args = {&model, &sim};
        cmxMcmcMh4gMetroMixin<glue_t, state_t>::Mh_step(
            *this, mh_args CMX_THREAD_ID_ARG);
    }

    /**
     * Metropolis-Hastings interface
     * Proposes new candidate state and updates target probabilities of
     * current and candidate states given the current state of parent model
     */
    void Mh_prime_cand(const Mh_args_t mh_args CMX_THREAD_ID_PARAM) {
        // Recover parent model and outer simulation handles
        model_t& model = *mh_args.p_model;
        sim_t& sim = *mh_args.p_sim;

        // Determine uniform sampling range
        const cmxReal lo = lo_hi_query_t::get_lo(model, sim);
        const cmxReal hi = lo_hi_query_t::get_hi(model, sim);
        cmxAssert(lo <= hi);

        // Update target probability of current Metropolis state given the
        // current state of the model
        state_t& curr = this->get_curr();
        cmxAssertBoundsInc(curr.scalar, lo, hi);
        curr.logp_tar = child_query_t::logscore_using(curr.scalar, model, sim);

        // Propose new candidate Metropolis state
        state_t& cand = this->get_cand();
        // Sample uniformly from range [lo, hi)
        cmxRand& rng = cmxRng;
        cand.scalar = rng.unif_real(lo, hi);

        // Update target probability of candidate Metropolis state
        cand.logp_tar = child_query_t::logscore_using(cand.scalar, model, sim);
    }

    /**
     * @param sim - outer simulation, unused
     * @return real-valued scalar hyper-parameter
     */
    inline cmxReal get_scalar(const sim_t& sim) const {
        return this->get_curr().scalar;
    }

private:
    /**
     * Initialize state from user configuration
     */
    void init_state(state_t& s,
                    cmxSpConstConfig_t config,
                    model_t& model,
                    sim_t& sim CMX_THREAD_ID_PARAM) {
        const cmxReal lo = lo_hi_query_t::get_lo(model, sim);
        const cmxReal hi = lo_hi_query_t::get_hi(model, sim);
        cmxAssert(lo <= hi);
        // Set to mean of interval [lo,hi]
        s.scalar = CMX_TO_REAL(0.5) * (lo + hi);
        s.logp_tar = CMX_TO_REAL(0.0);
    }

    /**
     * Randomize state - sample from uniform prior
     */
    void randomize_state(state_t& s,
                         cmxSpConstConfig_t config,
                         model_t& model,
                         sim_t& sim CMX_THREAD_ID_PARAM) {
        const cmxReal lo = lo_hi_query_t::get_lo(model, sim);
        const cmxReal hi = lo_hi_query_t::get_hi(model, sim);
        cmxAssert(lo <= hi);
        // Sample uniformly from range [lo, hi)
        cmxRand& rng = cmxRng;
        s.scalar = rng.unif_real(lo, hi);
        // Note, range is inclusive only if lo == hi
        cmxAssertBoundsInc(s.scalar, lo, hi);
    }

    /**
     * Allow mixin access to private init_state(...) and randomize_state(..)
     * members
     */
    friend cmxMcmcMh4gMetroMixin<t_Glue, state_t>;
};

#endif  // cmxScalarUnifMoco_h
