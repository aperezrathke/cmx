//****************************************************************************
// cmxMoco.h
//****************************************************************************

/**
 * @brief Moco - [Mo]del [Co]mponents define random or deterministic variables
 *  within a Bayesian hierarchical model
 */

#ifndef cmxMoco_h
#define cmxMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxMoco
//****************************************************************************

/**
 * Model component base class for both random and deterministic variables. The
 * virtual methods that can be overloaded include:
 *
 *  ~cmxMoco - destructor
 *  init() - initialization after parent model handle has been set, should
 *      seek to initialize to a 'semi-deterministic' independent null state
 *  randomize() - initialize to random state
 *  Gibbs_posterior() - generates sample from the Gibbs posterior distribution
 *  logscore() - return [improper] log score for this component according to
 *      current state of model
 *
 * In addition, child classes will need to define:
 *  enum { is_random = <cmxTRUE|cmxFALSE> }
 * in order for parent model to determine if component can be sampled.
 *
 * The template parameter t_Glue is assumed to be same format as cmxModel
 */
template <typename t_Glue>
class cmxMoco {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * NOTE: CHILD CLASSES MUST PROVIDE THE FOLLOWING:
     *
     *  enum { is_random = <cmxTRUE|cmxFALSE> }
     *
     * IN ORDER FOR PARENT MODEL TO CONDITIONALLY SAMPLE COMPONENT!
     *
     * This enum is purposefully not defined in base class so that compiler is
     * more likely to give error when child class forgets to define.
     */

    /**
     * Virtual destructor
     */
    virtual ~cmxMoco() {}

    /**
     * Initialization from user configuration. Model component should seek to
     * initialize to single population, independent null model where possible.
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void init(cmxSpConstConfig_t config,
                      model_t& model,
                      sim_t& sim CMX_THREAD_ID_PARAM) {}

    /**
     * Initialize to a random state, should be called after init()
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void randomize(cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {}

    /**
     * Virtual Gibbs posterior update, should only be called if model
     * component is random! Generates a sample from the posterior distribution
     * P(Xi | X-i) where Xi is i-th random variable and X-i is set of of all
     * random variables except for Xi in the model.
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void Gibbs_posterior(model_t& model,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {}

    /**
     * Intended to assess Gibbs sampler convergence ideally showing that chain
     * is exploring high probability regions. This may be an expensive call to
     * make and should be used sparingly at regular intervals.
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    virtual cmxReal logscore(model_t& model, sim_t& sim) {
        return CMX_TO_REAL(0.0);
    }
};

//****************************************************************************
// Defines
//****************************************************************************

/**
 * Macro for declaring a model component within a parent model. This is to
 * facilitate easier construction of hard-coded models in which the components
 * are known at compile-time. This allows component memory and lifetimes to
 * be easily managed by the parent model without any messy runtime accounting.
 * Macro provides the following:
 *  - public typedef of model component
 *  - public accessors of model component
 *  - private declaration of model component member variable
 * @WARNING - SCOPE IS SWITCHED TO PRIVATE
 * @param t_Glue - name of glue template object
 * @param C_ - core name of component
 */
#define CMX_MOCO_DECLARE(t_Glue, C_)                         \
public:                                                      \
    typedef typename t_Glue::C_##_t C_##_t;                  \
    inline C_##_t& get_##C_() { return m_##C_; }             \
    inline const C_##_t& get_##C_() const { return m_##C_; } \
                                                             \
private:                                                     \
    C_##_t m_##C_

/**
 * Macro for declaring associated shared resources policy of a model
 * component, meant to be for use within the mutable shared policy. Parameters
 * are same as for CMX_DECLARE_MOCO(...) macro.
 */
#define CMX_SHARCO_DECLARE(t_Glue, C_) CMX_MOCO_DECLARE(t_Glue, C_)

/**
 * Macro for registering a component - ideally within model's constructor.
 * Assumes model has a vector for storing all components as well as a vector
 * for just the random components. Assumes components were declared using
 * CMX_DECLARE_MOCO macro. This macro will append the corresponding member
 * component to each of these vectors conditional on value of C_t::is_random.
 * @param C_ - core name of component
 * @param vec_all - vector for storing all model component pointers
 * @param vec_ran - vector for storing only random model components pointers
 */
#define CMX_MOCO_REGISTER(C_, vec_all, vec_ran) \
    do {                                        \
        vec_all.push_back(&m_##C_);             \
        if (C_##_t::is_random) {                \
            vec_ran.push_back(&m_##C_);         \
        }                                       \
    } while (0)

#endif  // cmxMoco_h
