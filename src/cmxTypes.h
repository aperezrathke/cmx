//****************************************************************************
// cmxTypes.h
//****************************************************************************

/**
 * @brief Defines core primitive types
 */

#ifndef cmxTypes_h
#define cmxTypes_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#include <limits>

//****************************************************************************
// Types
//****************************************************************************

/**
 * Our default floating point type
 */
typedef double cmxReal;

/**
 * Macro for casting to cmxReal
 */
#define CMX_TO_REAL(x) (static_cast<cmxReal>((x)))

/**
 * Maximum positive real value
 * Note: Maximum negative real value is -CMX_REAL_MAX!
 */
#define CMX_REAL_MAX (std::numeric_limits<cmxReal>::max())

/**
 * The difference between 1.0 and the next representable value of the given
 * floating-point type. Can be used for checking floating-point "equality".
 * Explicitly Using float type because thats the threshold level of precision
 * we need (and want) for most comparison operations.
 */
#define CMX_REAL_EPS CMX_TO_REAL(std::numeric_limits<float>::epsilon())

/**
 * Macro for comparing real valued equality
 */
#define CMX_REAL_CMP_EQ(a, b) \
    (std::abs((a) - (b)) < (CMX_TO_REAL(2.0) * CMX_REAL_EPS))

/**
 * Macro for comparing real valued a <= b
 */
#define CMX_REAL_CMP_LTE(a, b) (CMX_REAL_CMP_EQ((a), (b)) || ((a) < (b)))

/**
 * Macro for comparing real valued a >= b
 */
#define CMX_REAL_CMP_GTE(a, b) CMX_REAL_CMP_LTE((b), (a))

/**
 * Boolean type
 */
typedef unsigned int cmxBool;

#ifdef cmxTRUE
#   error cmxTRUE already defined!
#endif  // cmxTRUE

#define cmxTRUE 1

#ifdef cmxFALSE
#   error cmxFALSE already defined!
#endif  // cmxFALSE

#define cmxFALSE 0

/**
 * Unsigned integer type
 */
typedef unsigned int cmxUInt;

/**
 * Macro for casting to cmxUInt
 */
#define CMX_TO_UINT(x) (static_cast<cmxUInt>((x)))

/**
 * The maximum cmxUInt value
 */
#define CMX_UINT_MAX (std::numeric_limits<cmxUInt>::max())

/**
 * Macro which is 1 if unsigned integer argument is power of 2, 0 o/w
 */
#define CMX_UINT_IS_POW_2(ui) ((ui) && !((ui) & ((ui)-1)))

/**
 * Macro to clip a scalar 'x' to be in range [lo, hi]
 */
#define CMX_CLIP(x_, lo_, hi_) std::max(std::min((x_), (hi_)), (lo_))

/**
 * Matrix type
 */
#include "cmxArmadillo.h"

#endif  // cmxTypes_h
