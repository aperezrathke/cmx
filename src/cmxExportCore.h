//****************************************************************************
// cmxExportCore.h
//****************************************************************************

#ifndef cmxExportCore_h
#define cmxExportCore_h

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxOpts.h"

#include <string>

namespace cmxExport {

/**
 * Obtain base directory for writing exported data
 * @param output_dir - string to store output directory
 * @param config - user configuration
 */
extern void get_output_dir(std::string& output_dir, cmxSpConstConfig_t config);

/**
 * Obtain job prefix to avoid overwriting data from previous runs
 * @param output_dir - string to store job identifier prefix
 * @param config - user configuration
 */
extern void get_job_id(std::string& job_id, cmxSpConstConfig_t config);

/**
 * Utility to obtain full export path for a single export file
 * @param config - The configuration object
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return The export path for a single export file
 */
extern std::string get_fpath(cmxSpConstConfig_t config,
                             const std::string& subdir,
                             const std::string& file_id,
                             const std::string& ext);

/**
 * Utility to obtain full export path for a single export file
 * @param config - The configuration object
 * @param index - Model index, will result in parent folder to subdir
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return The export path for a single export file
 */
extern std::string get_fpath(cmxSpConstConfig_t config,
                             const size_t index,
                             const std::string& subdir,
                             const std::string& file_id,
                             const std::string& ext);

/**
 * Obtain full export path for a single file if enabled by user.
 * @param check - User option to check if user configured to TRUE
 * @param config - The configuration object
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return If check option evaluates to TRUE, then file path is evaluated
 *  according to arguments and returned. Else, empty string is returned.
 */
extern std::string get_fpath(const enum cmxOptE check,
                             cmxSpConstConfig_t config,
                             const std::string& subdir,
                             const std::string& file_id,
                             const std::string& ext);

/**
 * Obtain full export path for a single file if enabled by user.
 * @param check - User option to check if user configured to TRUE
 * @param config - The configuration object
 * @param index - Model index, will result in parent folder to subdir
 * @param subdir - The subdirectory to write data to
 * @param file_id - The unique file identifier
 * @param ext - The file extension with *no* dot included (eg: pdb)
 * @return If check option evaluates to TRUE, then file path is evaluated
 *  according to arguments and returned. Else, empty string is returned.
 */
extern std::string get_fpath(const enum cmxOptE check,
                             cmxSpConstConfig_t config,
                             const size_t index,
                             const std::string& subdir,
                             const std::string& file_id,
                             const std::string& ext);

}  // namespace cmxExport

#endif  // cmxExportCore_h
