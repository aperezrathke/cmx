//****************************************************************************
// cmxClustSpopSharcoRo.h
//****************************************************************************

/**
 * @brief Read-only shared data for single-population cluster association. If
 *  model is for only a single population, then we only need to initialize and
 *  store the pseudo-population indices once.
 */

#ifndef cmxClustSpopSharcoRo_h
#define cmxClustSpopSharcoRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxModelTypes.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxClustSpopSharcoRo
//****************************************************************************

/**
 * Stores read-only shared pseudo-population indices 1 to N where N is pseudo-
 * population size. This is a trivial class just meant to allow single
 * population model to be special case of multi-population model (and to work
 * within same API).
 */
class cmxClustSpopSharcoRo {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     */
    void init(const cmxSpConstConfig_t config) {
        // Determine pseudo-population size
        const cmxUInt pseudopop_size =
            cmxConfigUtils::get_pseudopop_size(config);
        cmxAssert(pseudopop_size > 0);
        // Verify we are configured for single population model
        cmxAssert(cmxConfigUtils::get_mix_size(config) == 1);
        m_pseudopop_indices.clear();
        m_pseudopop_indices.reserve(pseudopop_size);
        // Single-population => all samples belong to 1 cluster
        for (cmxUInt i = 0; i < pseudopop_size; ++i) {
            m_pseudopop_indices.push_back(i);
        }
    }

    /**
     * Reset read-only state
     */
    void clear() { m_pseudopop_indices.clear(); }

    // Accessors

    /**
     * Trivial method - obtain mixture size (1 for single population)
     * @return 1 (hard-coded for single population model)
     */
    inline cmxUInt get_mix_size() const { return CMX_TO_UINT(1); }

    /**
     * Trivial method - just returns all pseudo-population indices as we are
     * assuming only a single population => single mixture (cluster) component
     * @return vector of all pseudo-population sample identifiers
     */
    inline const cmxPseudopopIndices_t& get_pseudopop_indices_spop() const {
        cmxAssert(!m_pseudopop_indices.empty());
        return m_pseudopop_indices;
    }

private:
    /**
     * Vector containing indices 1 to N where N is pseudo-population size.
     */
    cmxPseudopopIndices_t m_pseudopop_indices;
};

#endif  // cmxClustSpopSharcoRo
