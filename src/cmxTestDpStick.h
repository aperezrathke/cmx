//****************************************************************************
// cmxTestDpStick.h
//****************************************************************************

/**
 * Tests and profiles various Dirichlet process stick-breaking implementations
 *
 * Usage:
 * -test_dp_stick [--mix_size <+int>] [--Dp_stick_alpha <+real>]
 */

/**
 * Profiling results on linux CentOS 7, Intel Xeon CPU E5-2650 v3 @ 2.30GHz
 * demonstrated GSL Beta sampling as ~10X faster than BOOST Beta sampling
 */

#ifdef cmxTestDpStick_h
#error "Dirichlet Stick-breaking Process Test included multiple times!"
#endif  // cmxTestDpStick_h
#define cmxTestDpStick_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_TESTS

#include "cmxAssert.h"
#include "cmxAutoTimer.h"
#include "cmxCmdOptsMap.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxDirichletDistribution.h"
#include "cmxExitCodes.h"
#include "cmxLogf.h"
#include "cmxRand.h"
#include "cmxTypes.h"

#include <boost/math/distributions/beta.hpp>
#include <string>
#include <vector>

//****************************************************************************
// Utilities
//****************************************************************************

namespace cmxTestDpStick {

/**
 * Uses GSL based sampling method
 */
class gsl_sampler {
public:
    /**
     * Samples N-1 betas (and assigns last element to 1) to model truncated
     * Dirichlet process from GSL-based implementation of Beta distribution
     * @param rng - Random number generator
     * @param out_betas - Pre-sized vector of size N, output is N-1 samples
     *      from Beta(1, alpha) distribution
     * @param alpha - Dirichlet stick-breaking process concentration parameter
     */
    static void sample_betas(cmxRand& rng,
                             std::vector<cmxReal>& out_betas,
                             const cmxReal alpha) {
        cmxAssert(!out_betas.empty());
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        const size_t N = out_betas.size();
        const cmxUInt K = CMX_TO_UINT(2);
        const cmxReal V_ALPHA[K] = {CMX_TO_REAL(1.0), alpha};
        cmxReal v_theta[K];
        for (size_t i = 0; i < (N - 1); ++i) {
            cmxDirichletDistribution::sample(rng, K, V_ALPHA, v_theta);
            out_betas[i] = v_theta[0];
            cmxAssertBoundsInc(
                out_betas[i], CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        }
        out_betas[N - 1] = CMX_TO_REAL(1.0);
    }
};

/**
 * Uses BOOST library based sampling method
 */
class boost_sampler {
public:
    /**
     * Samples N-1 betas (and assigns last element to 1) to model truncated
     * Dirichlet process from boost-based implementation of Beta distribution
     * @param rng - Random number generator
     * @param out_betas - Pre-sized vector of size N, output is N-1 samples
     *      from Beta(1, alpha) distribution
     * @param alpha - Dirichlet stick-breaking process concentration parameter
     */
    static void sample_betas(cmxRand& rng,
                             std::vector<cmxReal>& out_betas,
                             const cmxReal alpha) {
        cmxAssert(!out_betas.empty());
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        const size_t N = out_betas.size();
        boost::math::beta_distribution<cmxReal> beta_dist(CMX_TO_REAL(1.0),
                                                          alpha);
        for (size_t i = 0; i < (N - 1); ++i) {
            // Uniformly sample quantile
            const cmxReal q = rng.unif_real();
            cmxAssertRealBoundsInc(q, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            // Use inverse cdf to generate beta sample
            out_betas[i] = boost::math::quantile(beta_dist, q);
            cmxAssertBoundsInc(
                out_betas[i], CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        }
        out_betas[N - 1] = CMX_TO_REAL(1.0);
    }
};

/**
 * @return TRUE if vector of probability masses is proper, FALSE o/w
 */
bool is_valid_pmf(const std::vector<cmxReal>& pmf) {
    cmxAssert(!pmf.empty());
    cmxReal cmf = CMX_TO_REAL(0.0);
    const size_t N = pmf.size();
    for (size_t i = 0; i < N; ++i) {
        cmxAssertRealBoundsInc(pmf[i], CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        cmf += pmf[i];
    }
    cmxAssert(CMX_REAL_CMP_EQ(cmf, CMX_TO_REAL(1.0)));
    return CMX_REAL_CMP_EQ(cmf, CMX_TO_REAL(1.0));
}

/**
 * Computes mixture probability masses given samples from Beta distribution
 * @param out_primed - pre-sized vector containing input samples from Beta
 *  distribution with last element == 1, will store output pmf
 * @param out_tail_lnprob - out log of the tail probability
 */
void get_mix_pmf_from_betas(std::vector<cmxReal>& out_primed,
                            cmxReal& out_tail_lnprob) {
    cmxAssert(!out_primed.empty());
    cmxAssertRealBoundsInc(out_primed[0], CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
    const size_t N = out_primed.size();
    cmxReal cmf = CMX_TO_REAL(0.0);
    for (size_t i = 0; i < (N - 1); ++i) {
        cmxAssertRealBoundsInc(
            out_primed[i], CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        out_primed[i] = (CMX_TO_REAL(1.0) - cmf) * out_primed[i];
        cmxAssertRealBoundsInc(
            out_primed[i], CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        cmf += out_primed[i];
    }
    cmxAssertRealBoundsInc(cmf, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
    out_primed[N - 1] = (CMX_TO_REAL(1.0) - cmf);
    cmxAssert(is_valid_pmf(out_primed));
    cmxAssertRealBoundsInc(
        out_primed[N - 1], CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
    out_tail_lnprob = log(out_primed[N - 1]);
}

/**
 * Generates mixture PMF using stick-breaking process
 * @param rng - Random number generator
 * @param out_presized - pre-sized vector for output mixture PMF
 * @param alpha - Dirichlet process concentration parameter
 * @param out_tail_lnprob - output scalar for log of tail probability
 */
template <typename t_sampler = gsl_sampler>
void get_mix_pmf(cmxRand& rng,
                 std::vector<cmxReal>& out_presized,
                 const cmxReal alpha,
                 cmxReal& out_tail_lnprob) {
    t_sampler::sample_betas(rng, out_presized, alpha);
    get_mix_pmf_from_betas(out_presized, out_tail_lnprob);
}

/**
 * Profiles a beta sampler
 * @param rng - Random number generator
 * @param out_presized - pre-sized vector for storing N-1 sampled Betas
 * @param alpha - Dirichlet process concentration parameter
 * @param reps - Number of replicate attempts
 */
template <typename t_sampler = gsl_sampler>
void prof_betas(cmxRand& rng,
                std::vector<cmxReal>& out_presized,
                const cmxReal alpha,
                const size_t reps = 1000) {
    cmxAutoTimer profiler;
    // Hopefully this token prevents call from being optimized away
    cmxReal token = CMX_TO_REAL(0.0);
    for (size_t i = 0; i < reps; ++i) {
        t_sampler::sample_betas(rng, out_presized, alpha);
        token += out_presized[0];
    }
    cmxLogf("TOKEN: %f\n", (double)token);
}

/**
 * Ascii visualization of probability mass
 */
void print_pmf(std::vector<cmxReal>& pmf) {
    const size_t N = pmf.size();
    const size_t MAX_STAR = 100;
    for (size_t i = 0; i < N; ++i) {
        const size_t n_star = MAX_STAR * pmf[i];
        const std::string star(n_star, '*');
        cmxLogf("%4d: %s %.2f\n", (int)i, star.c_str(), pmf[i]);
    }
}

}  // namespace cmxTestDpStick

//****************************************************************************
// Main
//****************************************************************************

/**
 * Entry point for test
 */
int cmxTestDpStickMain(const cmxCmdOptsMap& cmd_opts) {
    using namespace cmxTestDpStick;

    //////////////////////////////////////////////////////////////////////////
    // Setup

    cmxLogf("Running Test Dirichlet Stick-breaking Process.\n");

    // Create configuration
    cmxSpConfig_t config = cmxSmartPtr::make_shared<cmxConfig>();
    // Parse command line and any INI configuration (override defaults)
    config->init(cmd_opts);
    // Output configuration
    config->print();

    // Initialize stick-breaking alpha
    cmxReal Dp_stick_alpha = CMX_DEFAULT_DP_STICK_ALPHA;
    config->read_into(Dp_stick_alpha, cmxOpt_Dp_stick_alpha);
    if (Dp_stick_alpha <= CMX_TO_REAL(0.0)) {
        cmxLogf("Error: Dirichlet process alpha <= 0.0. Exiting.\n");
        exit(cmxExitCode_invalid_dp_stick_alpha);
    }
    cmxAssert(Dp_stick_alpha > CMX_TO_REAL(0.0));
    cmxLogf("Initialized Dp_stick_alpha: %f\n", (double)Dp_stick_alpha);

    // Initialize mixture size
    const cmxUInt mix_size = cmxConfigUtils::get_mix_size(config);
    cmxAssert(mix_size >= CMX_TO_UINT(1));
    cmxLogf("Initialized mix_size: %d\n", (int)mix_size);

    // Random number generator
    cmxRand rng;
    rng.reseed();

    // PMF buffer
    std::vector<cmxReal> pmf(mix_size, CMX_TO_REAL(0.0));

    //////////////////////////////////////////////////////////////////////////
    // Profile Beta samplers

    const size_t N_PROF_PASS = 2;
    for (size_t i_prof = 0; i_prof < N_PROF_PASS; ++i_prof) {
        cmxLogf("Profile GSL sampler pass %d...\n", (int)(i_prof + 1));
        prof_betas<gsl_sampler>(rng, pmf, Dp_stick_alpha);
        cmxLogf("Profile BOOST sampler pass %d...\n", (int)(i_prof + 1));
        prof_betas<boost_sampler>(rng, pmf, Dp_stick_alpha);
    }

    //////////////////////////////////////////////////////////////////////////
    // Visualize PMFs

    const size_t N_PMF_PASS = 4;
    cmxReal tail_lnprob = CMX_TO_REAL(0.0);
    for (size_t i_pmf = 0; i_pmf < N_PMF_PASS; ++i_pmf) {
        cmxLogf("Visualize GSL PMF pass %d...\n", (int)(i_pmf + 1));
        get_mix_pmf<gsl_sampler>(rng, pmf, Dp_stick_alpha, tail_lnprob);
        print_pmf(pmf);
        cmxLogf("TAIL: %f\n", tail_lnprob);
        cmxLogf("Visualize BOOST PMF pass %d...\n", (int)(i_pmf + 1));
        get_mix_pmf<boost_sampler>(rng, pmf, Dp_stick_alpha, tail_lnprob);
        print_pmf(pmf);
        cmxLogf("TAIL: %f\n", tail_lnprob);
    }

    // Test finished
    return cmxExitCode_normal;
}

#endif  // CMX_BUILD_ENABLE_TESTS
