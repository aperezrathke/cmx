//****************************************************************************
// cmxBnetInfeasSharcoRo
//****************************************************************************

/**
 * @brief Read-only shared data for infeasible (negative) corpus configuration
 *  bit vectors.
 */

#ifndef cmxBnetInfeasSharcoRo_h
#define cmxBnetInfeasSharcoRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxLogf.h"
#include "cmxModelTypes.h"
#include "cmxTypes.h"

// Needed for checking infeasible corpus has same dimensionality as Hi-C
#include "cmxContactFreqsSharcoRo.h"

//****************************************************************************
// cmxBnetInfeasSharcoRo
//****************************************************************************

/**
 * Stores read-only shared infeasible (negative) corpus. The infeasible corpus
 * is a collection of bit vectors, where each column is a bit vector that
 * was non-satisfiable from our physical simulations. In other words, each
 * column is a bit vector of knock-in/knock-out constraints that are
 * putatively non-embeddable according to our folding model.
 *
 * NOTE: Infeasible corpus is assumed ROW-MAJOR on disk but is stored
 *  COLUMN-MAJOR in RAM!
 */
class cmxBnetInfeasSharcoRo {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     */
    void init(const cmxSpConstConfig_t config) {
        this->clear();
        // Read path to infeasible (negative) corpus
        std::string infeas_path;
        const cmxBool b_resolved =
            config->resolve_path(infeas_path, cmxOpt_Bnet_infeas_path);
        if ((!b_resolved) || infeas_path.empty()) {
            // Early out, path not found
            cmxLogf(
                "Warning: no infeasible (negative) corpus path specified.\n");
            return;
        }
        // Load plain-text negative corpus. Assumes row-major format where
        // each row is a bit-vector representing an infeasible configuration
        const bool b_load =
            m_infeas_pop.load(infeas_path, cmxMatrixUtils::csv_ascii);
        if (!b_load) {
            cmxLogf(
                "Warning: unable to load infeasible corpus from path:\n\t%s\n",
                infeas_path.c_str());
            m_infeas_pop.clear();
            return;
        }
        // Note: m_infeas_pop is loaded row-major, so we have not transposed yet
        if (m_infeas_pop.n_rows < CMX_TO_MAT_SZ_T(1)) {
            cmxLogf("Warning: 0 infeasible corpus elements loaded!\n");
            return;
        }
        // Validate all elements in {0,1}
        for (cmxMatSz_t j = CMX_TO_MAT_SZ_T(0); j < m_infeas_pop.n_cols; ++j) {
            for (cmxMatSz_t i = CMX_TO_MAT_SZ_T(0); i < m_infeas_pop.n_rows;
                 ++i) {
                const cmxUInt val = m_infeas_pop.at(i, j);
                if ((val != CMX_TO_UINT(0)) && (val != CMX_TO_UINT(1))) {
                    cmxLogf(
                        "Error: infeasible corpus element (%d, %d) not in "
                        "{0,1}. Exiting.\n",
                        (int)i,
                        (int)j);
                    exit(cmxExitCode_invalid_infeas);
                }
            }  // end iteration over rows
        }      // end iteration over columns
        // Convert to column-major as expected by program
        cmxMatrixUtils::inplace_trans(m_infeas_pop);
        // Verify consistent with Hi-C dimensions
        if (!check_infeas_pop(config)) {
            cmxLogf(
                "Error: infeasible corpus column number does not match Hi-C "
                "size. Exiting.\n");
            exit(cmxExitCode_invalid_infeas);
        }
        cmxLogf("Loaded infeasible corpus of size %d from:\n\t%s\n",
                (int)m_infeas_pop.n_cols,
                infeas_path.c_str());
    }

    /**
     * Reset read-only state
     */
    void clear() { m_infeas_pop.clear(); }

    // Accessors

    /**
     * @return Read-only view for infeasible population matrix
     */
    inline const cmxPseudopopFrame_t& get_infeas_pop() const {
        return m_infeas_pop;
    }

private:
    /**
     * @WARNING - ASSUMES COLUMN-MAJOR FORMAT!
     * @param config - user configuration
     * @return TRUE if infeasible corpus is okay, FALSE o/w
     */
    cmxBool check_infeas_pop(const cmxSpConstConfig_t config) const {
        if (m_infeas_pop.is_empty()) {
            // Early out if no corpus loaded
            return cmxTRUE;
        }
        // Verify expected number of rows - should match Hi-C data
        cmxContactFreqs_t hic;
        cmxContactFreqsSharcoRo::load_hic_exit_on_fail(config, hic);
        return (hic.n_elem == m_infeas_pop.n_rows);
    }

    /**
     * Bit matrix for infeasible configurations
     */
    cmxPseudopopFrame_t m_infeas_pop;
};

#endif  // cmxBnetInfeasSharcoRo_h
