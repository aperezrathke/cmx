//****************************************************************************
// cmxBnetMpopSharcoMu.h
//****************************************************************************

/**
 * @brief Mutable shared resources used by multi-population Bayesian network
 *  model component. Assumes each thread will be allocated a separate instance
 *  of this class!
 */

/**
 * Note the following design framework:
 *  - Shared mutable (i.e. writable) data buffers should be replicated across
 *      threads (thread-local storage - TLS) to avoid race conditions
 *  - Shared read-only (i.e. non-writable|non-mutable) data should be stored
 *      as single instance at simulation level to avoid unnecessary
 *      duplication across threads and therefore conserve memory
 */

#ifndef cmxBnetMpopSharcoMu_h
#define cmxBnetMpopSharcoMu_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetMpopSharcoMu
//****************************************************************************

/**
 * Shared mutable data resources for multi-population Bayesian network model
 *  component, client must restrict access to only a single thread
 */
class cmxBnetMpopSharcoMu {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     * @param sim - outer simulation
     */
    template <typename sim_t>
    void init(cmxSpConstConfig_t config, const sim_t& sim) {
        // Pre-allocate
        m_mpop_block_indices_buf.set_size(
            cmxConfigUtils::get_Bnet_mpop_block_size(config));
        m_mpop_block_weights_buf.set_size(cmxConfigUtils::get_mix_size(config));
    }

    /**
     * Clear shared buffer(s)
     */
    void clear() {
        m_mpop_block_indices_buf.clear();
        m_mpop_block_weights_buf.clear();
    }

    // Accessors

    /**
     * @return mutable vector for multi-pop block indices
     */
    inline cmxUIVecCol& get_mpop_block_indices_buf() const {
        return m_mpop_block_indices_buf;
    }

    /**
     * @return mutable vector for multi-pop block weights
     */
    inline cmxVecCol& get_mpop_block_weights_buf() const {
        return m_mpop_block_weights_buf;
    }

private:
    /**
     * Buffer for storing randomly selected block indices
     */
    mutable cmxUIVecCol m_mpop_block_indices_buf;

    /**
     * Buffer for storing block selection weights
     */
    mutable cmxVecCol m_mpop_block_weights_buf;
};

#endif  // cmxBnetMpopSharcoMu_h
