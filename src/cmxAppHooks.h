//****************************************************************************
// cmxAppHooks.h
//****************************************************************************

/**
 * @brief Generic system for registering and launching applications that can
 * be executed.
 */

#ifndef cmxAppHooks_h
#define cmxAppHooks_h

#include "cmxBuild.h"
#include "cmxCmdOptsMap.h"
#include "cmxTypes.h"

/**
 * Application info structure
 */
struct cmxAppInfo {
    /**
     * The name of the application
     */
    const char* name;
    /**
     * The callback to run the application
     */
    int (*fp_main)(const cmxCmdOptsMap&);
};

/**
 * @return true if command line says we should run parameter commandlet, false
 * otherwise
 */
inline cmxBool cmxShouldRunApp(const cmxCmdOptsMap& cmd_opts,
                               const char* app_name) {
    return cmd_opts.key_exists(app_name);
}

/**
 * Parse command line and run any utilities specified
 * Note: do { ... } while(0) allows multi-line macro to behave like single-line
 */
#define CMX_CONDITIONAL_RUN_APPS(cmd_opts, apps_array)                \
    do {                                                              \
        const int NUM_APPS = sizeof(apps_array) / sizeof(cmxAppInfo); \
        for (int ix_app = 0; ix_app < NUM_APPS; ++ix_app) {           \
            if (cmxShouldRunApp(cmd_opts, apps_array[ix_app].name)) { \
                return (*(apps_array[ix_app].fp_main))(cmd_opts);     \
            }                                                         \
        }                                                             \
    } while (0)

#endif  // cmxAppHooks_h
