//****************************************************************************
// cmxIsFinite.h
//****************************************************************************

/**
 * @brief Relatively portable isfinite() implementation
 */

/**
 * Based on discussion:
 * https://stackoverflow.com/questions/23092615/a-portable-check-for-infinity-and-not-a-number
 */

#ifndef cmxIsFinite_h
#define cmxIsFinite_h

#include "cmxBuild.h"

#ifdef CMX_BUILD_CXX_11
#   include <cmath>
#else
#   include <limits>
#endif  // CMX_BUILD_CXX_11

/**
 * Default implementation always returns true
 */
template <typename t_real>
inline bool cmxIsFinite(const t_real r) {
    return true;
}

/**
 * Specialization for floats
 * @return true if 'r' is finite (not nan and not inf)
 */
template <>
inline bool cmxIsFinite(const float r) {
#ifdef CMX_BUILD_CXX_11
    return std::isfinite(r);
#else
    typedef float real_t__;
    const real_t__ r_max = std::numeric_limits<real_t__>::max();
    const volatile real_t__ r_ = r;
    return (r_ == r_) && (r >= -r_max) && (r <= r_max);
#endif  // CMX_BUILD_CXX_11
}

/**
 * Specialization for doubles
 * @return true if x is finite (not nan and not inf)
 */
template <>
inline bool cmxIsFinite(const double r) {
#ifdef CMX_BUILD_CXX_11
    return std::isfinite(r);
#else
    typedef double real_t__;
    const real_t__ r_max = std::numeric_limits<real_t__>::max();
    const volatile real_t__ r_ = r;
    return (r_ == r_) && (r >= -r_max) && (r <= r_max);
#endif  // CMX_BUILD_CXX_11
}

#endif  // cmxIsFinite_h
