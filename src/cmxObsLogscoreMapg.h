//****************************************************************************
// cmxObsLogscoreMapg.h
//****************************************************************************

/**
 * @brief Captures global maximum observed log-score. MAP = maximum a
 *  posteriori, can also filter by Hi-C correlation
 */

#ifndef cmxObsLogscoreMapg_h
#define cmxObsLogscoreMapg_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxMapReactor.h"
#include "cmxObs.h"
#include "cmxObsCorrHicSnap.h"
#include "cmxObsLogscoreSnap.h"
#include "cmxSmartPtr.h"

#include <queue>
#include <vector>

//****************************************************************************
// cmxObsLogscoreMapg
//****************************************************************************

/**
 * Log-score global MAP observer - depends on log-score and Hi-C correlation
 *  snapshot observers
 */
template <typename t_Glue>
class cmxObsLogscoreMapg : public cmxObs<t_Glue> {
private:
    /**
     * Tuple for storing {log-score, model_index}
     */
    typedef std::pair<cmxReal, size_t> model_score_t;

    /**
     * Ranks models by log-score
     */
    typedef struct {
        /**
         * @return model a < model b iff a.log-score < b.log-score
         */
        inline bool operator()(const model_score_t a,
                               const model_score_t b) const {
            return a.first < b.first;
        }
    } model_ranker_t;

    /**
     * Priority-queue for candidate models
     */
    typedef std::priority_queue<model_score_t,
                                std::vector<model_score_t>,
                                model_ranker_t>
        model_pq_t;

public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Log-score snapshot observer type
     */
    typedef cmxObsLogscoreSnap<t_Glue> obs_logscore_snap_t;

    /**
     * Hi-C correlation snapshot observer type
     */
    typedef cmxObsCorrHicSnap<t_Glue> obs_corr_hic_snap_t;

    /**
     * Base map reactor type
     */
    typedef cmxMapReactor<t_Glue, cmxObsLogscoreMapg<t_Glue> > map_reactor_t;

    /**
     * Defaults
     */
    enum { default_start_iter = 0 };

    /**
     * Default constructor
     */
    cmxObsLogscoreMapg() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read start iteration
        config->read_into(m_start_iter, cmxOpt_obs_logscore_mapg_start_iter);
        // Read minimal Hi-C correlation
        config->read_into(m_min_corr_hic,
                          cmxOpt_obs_logscore_mapg_min_corr_hic);
        // Extract logscore snapshot observer - must occur before MAP observer
        if (!obs_t::find_raw(&mp_snap_logscore, sim, index)) {
            cmxLogf(
                "Error: log-score global MAP observer unable to find handle to "
                "log-score snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap_logscore);
        // Extract optional Hi-C correlation snapshot observer
        if (!obs_t::find_raw(&mp_snap_corr_hic, sim, index)) {
            cmxLogf(
                "Warning: log-score global MAP observer unable to find handle "
                "to Hi-C correlation snapshot observer.\n");
            mp_snap_corr_hic = NULL;
            m_min_corr_hic = -CMX_REAL_MAX;
        }
    }

    /**
     * Reset to default state
     */
    void clear() {
        mp_snap_logscore = NULL;
        mp_snap_corr_hic = NULL;
        m_reactors.clear();
        m_start_iter = default_start_iter;
        m_min_corr_hic = -CMX_REAL_MAX;
        m_map = -CMX_REAL_MAX;
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        // Use same period as configured logscore snapshot observer
        cmxAssert(mp_snap_logscore);
        cmxAssert(mp_snap_corr_hic);
        if ((iter < m_start_iter) || (!mp_snap_logscore->should_update(iter))) {
            // Early out if not yet at sampling interval
            return;
        }
        // Determine if new MAP score encountered
        const std::vector<cmxReal>& logscores =
            mp_snap_logscore->get_logscores();
        const size_t num_logscores = logscores.size();
        cmxAssert(num_logscores > 0);
        // Search for candidate models
        model_pq_t pq;
        for (size_t i = 0; i < num_logscores; ++i) {
            if (logscores[i] > m_map) {
                pq.push(model_score_t(logscores[i], i));
            }
        }

        // Allow soft fail if Hi-C correlation observer not configured
        if (!pq.empty() && mp_snap_corr_hic) {
            if (mp_snap_corr_hic->is_stale(iter)) {
                // Hi-C correlation is stale, capture new snapshot
                mp_snap_corr_hic->force_update(iter, sim);
            }
        }

        // Check if new potential "global" MAP found
        while (!pq.empty()) {
            const model_score_t best = pq.top();
            const cmxReal max_logscore = best.first;
            const size_t max_i = best.second;
            bool should_capture = true;
            if (mp_snap_corr_hic) {
                const std::vector<cmxReal>& corrs =
                    mp_snap_corr_hic->get_corrs();
                cmxAssertBounds(max_i, 0, corrs.size());
                const cmxReal max_corr_hic = corrs[max_i];
                // Only record MAP if min Hi-C correlation threshold is met
                should_capture = (max_corr_hic >= m_min_corr_hic);
            }
            if (should_capture) {
                // We found a new MAP estimate that meets minimal Hi-C
                // correlation!
                m_map = max_logscore;
                cmxAssertBounds(max_i, 0, sim.get_num_models());
                // Update reactors
                const size_t num_reactors = m_reactors.size();
                for (size_t i = 0; i < num_reactors; ++i) {
                    m_reactors[i]->update_react(iter, sim, max_i, m_map);
                }
                break;
            }
            // If we reached here, the minimum Hi-C correlation threshold was
            // not reached, remove current from consideration
            pq.pop();
        }
    }

    /**
     * Callback for when simulation has finished running
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        cmxLogf("MAP (global): log-score: %f\n", (double)m_map);
    }

    /**
     * Register reactor for updating when new MAP estimates are found
     */
    void add_reactor(map_reactor_t& reactor) { m_reactors.push_back(&reactor); }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_logscore_mapg)

private:
    /**
     * Handle to log-score snapshot observer - raw pointer is used as
     * lifetime of pointer does not exceed that of parent simulation's
     * shared_ptr
     */
    const obs_logscore_snap_t* mp_snap_logscore;

    /**
     * Handle to Hi-C correlation snapshot observer
     */
    obs_corr_hic_snap_t* mp_snap_corr_hic;

    /**
     * Set of registered reactors
     */
    std::vector<map_reactor_t*> m_reactors;

    /**
     * Iteration at which to begin MAP checking
     */
    cmxUInt m_start_iter;

    /**
     * MAP estimates are only recorded conditional on minimal Hi-C correlation
     *  threshold
     */
    cmxReal m_min_corr_hic;

    /**
     * Maximum observed value of tracked parameter
     */
    cmxReal m_map;
};

#endif  // cmxObsLogscoreMapg_h
