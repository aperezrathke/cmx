//****************************************************************************
// cmxObsBuilder.inl
//****************************************************************************

// Not meant to be included more than once
#ifdef cmxObsBuilder_inl
#error "Cmx Observer Builder implementation included multiple times!"
#endif  // cmxObsBuilder_inl
#define cmxObsBuilder_inl

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxAssert.h"
#include "cmxConfigUtils.h"
#include "cmxLogf.h"
#include "cmxObsBnetSpopMapc.h"
#include "cmxObsBnetSpopMapg.h"
#include "cmxObsBnetSpopRing.h"
#include "cmxObsBnetSpopSnap.h"
#include "cmxObsClustHistCons.h"
#include "cmxObsClustSizeSnap.h"
#include "cmxObsCorrHicCons.h"
#include "cmxObsCorrHicMapc.h"
#include "cmxObsCorrHicMapg.h"
#include "cmxObsCorrHicSnap.h"
#include "cmxObsCorrHicTrac.h"
#include "cmxObsLogscoreCons.h"
#include "cmxObsLogscoreMapc.h"
#include "cmxObsLogscoreMapg.h"
#include "cmxObsLogscoreSnap.h"
#include "cmxObsLogscoreTrac.h"
#include "cmxObsMixEntCons.h"
#include "cmxObsMixEntMapc.h"
#include "cmxObsMixEntMapg.h"
#include "cmxObsMixEntSnap.h"
#include "cmxObsMixEntTrac.h"
#include "cmxObsMixHistCons.h"
#include "cmxObsMixPmfSnap.h"
#include "cmxObsPaccBnetCons.h"
#include "cmxObsPaccPseuCons.h"
#include "cmxObsPseuFrameCons.h"
#include "cmxObsPseuFrameMapc.h"
#include "cmxObsPseuFrameMapg.h"
#include "cmxObsPseuHicMapc.h"
#include "cmxObsPseuHicMapg.h"

#include <algorithm>
#include <string>
#include <vector>

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * MAY ONLY BE CALLED FROM MAIN THREAD
 * Allocates and initializes observers from user configuration
 * @param config - user configuration
 * @param sim - parent simulation with initialized models
 */
template <typename t_Glue>
void cmxObsBuilder<t_Glue>::init(cmxSpConstConfig_t config,
                                 typename t_Glue::sim_t& sim) {

    // Handle to output sets of observers and reactors
    std::vector<sp_obs_t>& out_obs = sim.get_observers();
    std::vector<sp_obs_t>& out_reacts = sim.get_reactors();
    out_obs.clear();
    out_reacts.clear();

    // Allowed observer type names
    // @WARNING - must be same order as cmxObsEtype definition!
    const std::string OBS_NAMES[cmxObsEtype_num] = {
        std::string("logscore_snap"),   std::string("logscore_cons"),
        std::string("logscore_trac"),   std::string("corr_hic_snap"),
        std::string("corr_hic_cons"),   std::string("corr_hic_trac"),
        std::string("bnet_snap"),       std::string("bnet_ring"),
        std::string("clust_size_snap"), std::string("clust_hist_cons"),
        std::string("mix_pmf_snap"),    std::string("mix_hist_cons"),
        std::string("mix_ent_snap"),    std::string("mix_ent_cons"),
        std::string("mix_ent_trac"),    std::string("pacc_bnet_cons"),
        std::string("pacc_pseu_cons"),  std::string("logscore_mapc"),
        std::string("logscore_mapg"),   std::string("corr_hic_mapc"),
        std::string("corr_hic_mapg"),   std::string("bnet_mapc"),
        std::string("bnet_mapg"),       std::string("pseu_frame_cons"),
        std::string("pseu_frame_mapc"), std::string("pseu_frame_mapg"),
        std::string("pseu_hic_mapc"),   std::string("pseu_hic_mapg"),
        std::string("mix_ent_mapc"),    std::string("mix_ent_mapg")};
    const std::string* OBS_NAMES_START = &(OBS_NAMES[0]);
    const std::string* OBS_NAMES_END = &(OBS_NAMES[cmxObsEtype_num]);
    const cmxUInt mix_size = cmxConfigUtils::get_mix_size(config);
    cmxAssert(mix_size > CMX_TO_UINT(0));

    //////////////////////////////////////////////////////////////////////////
    // Determine user requested observers

    std::vector<std::string> user_obs;
    for (cmxUInt i = 0; i < cmxObsEtype_num; ++i) {
        std::string obs_name;
        if (config->read_into_vec(obs_name, cmxOpt_obs_enable, i)) {
            // Check observer name is registered type
            if (std::find(OBS_NAMES_START, OBS_NAMES_END, obs_name) !=
                OBS_NAMES_END) {
                // Observer type name is valid, add it to list
                user_obs.push_back(obs_name);
            } else {
                // Warning, unrecognized observer
                cmxLogf(
                    "Warning: Unrecognized observer type: '%s', ignoring.\n",
                    obs_name.c_str());
            }
        } else {
            // Enabled observers must be declared with sequential indices. As
            // soon as missing index encountered, break from loop
            break;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Convert user-strings to booleans

    std::vector<cmxBool> flags(cmxObsEtype_num);
    for (size_t i = 0; i < cmxObsEtype_num; ++i) {
        const cmxBool result =
            std::find(user_obs.begin(), user_obs.end(), OBS_NAMES[i]) !=
            user_obs.end();
        flags[i] = result;
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: log-score snapshot

    if (flags[cmxObsEtype_logscore_snap] || flags[cmxObsEtype_logscore_cons] ||
        flags[cmxObsEtype_logscore_trac] || flags[cmxObsEtype_logscore_mapc] ||
        flags[cmxObsEtype_logscore_mapg] || flags[cmxObsEtype_bnet_mapc] ||
        flags[cmxObsEtype_bnet_mapg] || flags[cmxObsEtype_corr_hic_mapc] ||
        flags[cmxObsEtype_corr_hic_mapg] ||
        flags[cmxObsEtype_pseu_frame_mapc] ||
        flags[cmxObsEtype_pseu_frame_mapg] ||
        flags[cmxObsEtype_pseu_hic_mapc] || flags[cmxObsEtype_pseu_hic_mapg] ||
        flags[cmxObsEtype_mix_ent_mapc] || flags[cmxObsEtype_mix_ent_mapg]) {
        typedef cmxObsLogscoreSnap<t_Glue> obs_logscore_snap_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_logscore_snap_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: log-score console

    if (flags[cmxObsEtype_logscore_cons]) {
        typedef cmxObsLogscoreCons<t_Glue> obs_logscore_cons_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_logscore_cons_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: log-score tracer

    if (flags[cmxObsEtype_logscore_trac]) {
        typedef cmxObsLogscoreTrac<t_Glue> obs_logscore_trac_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_logscore_trac_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Hi-C correlation snapshot

    if (flags[cmxObsEtype_corr_hic_snap] || flags[cmxObsEtype_corr_hic_cons] ||
        flags[cmxObsEtype_corr_hic_trac] || flags[cmxObsEtype_corr_hic_mapc] ||
        flags[cmxObsEtype_corr_hic_mapg]) {
        typedef cmxObsCorrHicSnap<t_Glue> obs_corr_hic_snap_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_corr_hic_snap_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Hi-C correlation console

    if (flags[cmxObsEtype_corr_hic_cons]) {
        typedef cmxObsCorrHicCons<t_Glue> obs_corr_hic_cons_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_corr_hic_cons_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Hi-C correlation tracer

    if (flags[cmxObsEtype_corr_hic_trac]) {
        typedef cmxObsCorrHicTrac<t_Glue> obs_corr_hic_trac_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_corr_hic_trac_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Bayesian network (single-population) snapshot

    if (flags[cmxObsEtype_bnet_snap] || flags[cmxObsEtype_bnet_mapc] ||
        flags[cmxObsEtype_bnet_mapg] || flags[cmxObsEtype_bnet_ring]) {
        if (mix_size == CMX_TO_UINT(1)) {
            typedef cmxObsBnetSpopSnap<t_Glue> obs_bnet_spop_snap_t;
            out_obs.push_back(cmxSmartPtr::make_shared<obs_bnet_spop_snap_t>());
        } else {
            // @TODO - Multi-population support
            cmxLogf(
                "Warning: Bayesian network (multi-population) snapshot "
                "observer not [yet] supported.\n");
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Bayesian network (single-population) ring-buffered history

    if (flags[cmxObsEtype_bnet_ring]) {
        if (mix_size == CMX_TO_UINT(1)) {
            typedef cmxObsBnetSpopRing<t_Glue> obs_bnet_spop_ring_t;
            out_obs.push_back(cmxSmartPtr::make_shared<obs_bnet_spop_ring_t>());
        } else {
            // @TODO - Multi-population support
            cmxLogf(
                "Warning: Bayesian network (multi-population) ring observer "
                "not [yet] supported.\n");
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Cluster (sorted) size snapshot

    if (flags[cmxObsEtype_clust_size_snap] ||
        flags[cmxObsEtype_clust_hist_cons]) {
        typedef cmxObsClustSizeSnap<t_Glue> obs_clust_size_snap_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_clust_size_snap_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Cluster histogram console

    if (flags[cmxObsEtype_clust_hist_cons]) {
        typedef cmxObsClustHistCons<t_Glue> obs_clust_hist_cons_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_clust_hist_cons_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Mixture PMF (sorted) snapshot

    if (flags[cmxObsEtype_mix_pmf_snap] || flags[cmxObsEtype_mix_hist_cons]) {
        // @TODO - Disable for single population?
        typedef cmxObsMixPmfSnap<t_Glue> obs_mix_pmf_snap_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_mix_pmf_snap_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Mixture PMF histogram (sorted) console

    if (flags[cmxObsEtype_mix_hist_cons]) {
        // @TODO - Disable for single population?
        typedef cmxObsMixHistCons<t_Glue> obs_mix_hist_cons_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_mix_hist_cons_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Mixture PMF entropy snapshot

    if (flags[cmxObsEtype_mix_ent_snap] || flags[cmxObsEtype_mix_ent_cons] ||
        flags[cmxObsEtype_mix_ent_trac] || flags[cmxObsEtype_mix_ent_mapc] ||
        flags[cmxObsEtype_mix_ent_mapg]) {
        // @TODO - Disable for single population?
        typedef cmxObsMixEntSnap<t_Glue> obs_mix_ent_snap_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_mix_ent_snap_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Mixture PMF entropy console

    if (flags[cmxObsEtype_mix_ent_cons]) {
        // @TODO - Disable for single population?
        typedef cmxObsMixEntCons<t_Glue> obs_mix_ent_cons_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_mix_ent_cons_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Mixture PMF entropy tracer

    if (flags[cmxObsEtype_mix_ent_trac]) {
        // @TODO - Disable for single population?
        typedef cmxObsMixEntTrac<t_Glue> obs_mix_ent_trac_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_mix_ent_trac_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Bayesian network acceptance probability console observer

#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    if (flags[cmxObsEtype_pacc_bnet_cons]) {
        typedef cmxObsPaccBnetCons<t_Glue> obs_pacc_bnet_cons_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_pacc_bnet_cons_t>());
    }
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

    //////////////////////////////////////////////////////////////////////////
    // Observer: Pseudo-population acceptance probability console observer

#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    if (flags[cmxObsEtype_pacc_pseu_cons]) {
        typedef cmxObsPaccPseuCons<t_Glue> obs_pacc_pseu_cons_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_pacc_pseu_cons_t>());
    }
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

    //////////////////////////////////////////////////////////////////////////
    // Observer: log-score chain MAP

    if (flags[cmxObsEtype_logscore_mapc] || flags[cmxObsEtype_corr_hic_mapc] ||
        flags[cmxObsEtype_pseu_frame_mapc] ||
        flags[cmxObsEtype_pseu_hic_mapc] || flags[cmxObsEtype_bnet_mapc] ||
        flags[cmxObsEtype_mix_ent_mapc]) {
        typedef cmxObsLogscoreMapc<t_Glue> obs_logscore_mapc_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_logscore_mapc_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: log-score global MAP

    if (flags[cmxObsEtype_logscore_mapg] || flags[cmxObsEtype_corr_hic_mapg] ||
        flags[cmxObsEtype_pseu_frame_mapg] ||
        flags[cmxObsEtype_pseu_hic_mapg] || flags[cmxObsEtype_bnet_mapg]) {
        typedef cmxObsLogscoreMapg<t_Glue> obs_logscore_mapg_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_logscore_mapg_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Hi-C correlation chain MAP

    if (flags[cmxObsEtype_corr_hic_mapc]) {
        typedef cmxObsCorrHicMapc<t_Glue> obs_corr_hic_mapc_t;
        out_reacts.push_back(cmxSmartPtr::make_shared<obs_corr_hic_mapc_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Hi-C correlation global MAP

    if (flags[cmxObsEtype_corr_hic_mapg]) {
        typedef cmxObsCorrHicMapg<t_Glue> obs_corr_hic_mapg_t;
        out_reacts.push_back(cmxSmartPtr::make_shared<obs_corr_hic_mapg_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Bayesian network (single-population) chain MAP

    if (flags[cmxObsEtype_bnet_mapc]) {
        if (mix_size == CMX_TO_UINT(1)) {
            typedef cmxObsBnetSpopMapc<t_Glue> obs_bnet_spop_mapc_t;
            out_reacts.push_back(
                cmxSmartPtr::make_shared<obs_bnet_spop_mapc_t>());
        } else {
            // @TODO - Multi-population support
            cmxLogf(
                "Warning: Bayesian network (multi-population) chain MAP "
                "observer not [yet] supported.\n");
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Bayesian network (single-population) global MAP

    if (flags[cmxObsEtype_bnet_mapg]) {
        if (mix_size == CMX_TO_UINT(1)) {
            typedef cmxObsBnetSpopMapg<t_Glue> obs_bnet_spop_mapg_t;
            out_reacts.push_back(
                cmxSmartPtr::make_shared<obs_bnet_spop_mapg_t>());
        } else {
            // @TODO - Multi-population support
            cmxLogf(
                "Warning: Bayesian network (multi-population) global MAP "
                "observer not [yet] supported.\n");
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Pseudo-population frame console

    if (flags[cmxObsEtype_pseu_frame_cons]) {
        typedef cmxObsPseuFrameCons<t_Glue> obs_pseu_frame_cons_t;
        out_obs.push_back(cmxSmartPtr::make_shared<obs_pseu_frame_cons_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Pseudo-population frame chain MAP

    if (flags[cmxObsEtype_pseu_frame_mapc]) {
        typedef cmxObsPseuFrameMapc<t_Glue> obs_pseu_frame_mapc_t;
        out_reacts.push_back(cmxSmartPtr::make_shared<obs_pseu_frame_mapc_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Pseudo-population frame global MAP

    if (flags[cmxObsEtype_pseu_frame_mapg]) {
        typedef cmxObsPseuFrameMapg<t_Glue> obs_pseu_frame_mapg_t;
        out_reacts.push_back(cmxSmartPtr::make_shared<obs_pseu_frame_mapg_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Pseudo-population Hi-C chain MAP

    if (flags[cmxObsEtype_pseu_hic_mapc]) {
        typedef cmxObsPseuHicMapc<t_Glue> obs_pseu_hic_mapc_t;
        out_reacts.push_back(cmxSmartPtr::make_shared<obs_pseu_hic_mapc_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Pseudo-population Hi-C global MAP

    if (flags[cmxObsEtype_pseu_hic_mapg]) {
        typedef cmxObsPseuHicMapg<t_Glue> obs_pseu_hic_mapg_t;
        out_reacts.push_back(cmxSmartPtr::make_shared<obs_pseu_hic_mapg_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Mixture PMF entropy chain MAP

    if (flags[cmxObsEtype_mix_ent_mapc]) {
        typedef cmxObsMixEntMapc<t_Glue> obs_mix_ent_mapc_t;
        out_reacts.push_back(cmxSmartPtr::make_shared<obs_mix_ent_mapc_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Observer: Mixture PMF entropy global MAP

    if (flags[cmxObsEtype_mix_ent_mapc]) {
        typedef cmxObsMixEntMapg<t_Glue> obs_mix_ent_mapg_t;
        out_reacts.push_back(cmxSmartPtr::make_shared<obs_mix_ent_mapg_t>());
    }

    //////////////////////////////////////////////////////////////////////////
    // Initialize allocated observers

    const size_t num_obs = out_obs.size();
    for (size_t i = 0; i < num_obs; ++i) {
        out_obs[i]->init(config, sim, i);
    }

    //////////////////////////////////////////////////////////////////////////
    // Initialize allocated reactors

    const size_t num_reacts = out_reacts.size();
    for (size_t i = 0; i < num_reacts; ++i) {
        out_reacts[i]->init(config, sim, i);
    }
}
