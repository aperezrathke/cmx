//****************************************************************************
// cmxClustMpopSharcoRo.h
//****************************************************************************

/**
 * @brief Read-only shared data for multi-population cluster association.
 */

#ifndef cmxClustMpopSharcoRo_h
#define cmxClustMpopSharcoRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxClustMpopSharcoRo
//****************************************************************************

/**
 * Multi-population cluster read-only shared resources
 */
class cmxClustMpopSharcoRo {
public:
    /**
     * Default constructor
     */
    cmxClustMpopSharcoRo() : m_mix_size(CMX_DEFAULT_MIX_SIZE) {}

    /**
     * Initialize from user configuration
     * @param config - user options
     */
    void init(const cmxSpConstConfig_t config) {
        m_mix_size = cmxConfigUtils::get_mix_size(config);
        cmxAssert(m_mix_size > 0);
    }

    /**
     * Reset read-only state
     */
    void clear() { m_mix_size = CMX_DEFAULT_MIX_SIZE; }

    /**
     * @return maximum number of mixture components
     */
    inline cmxUInt get_mix_size() const {
        cmxAssert(m_mix_size > CMX_TO_UINT(0));
        return m_mix_size;
    }

private:
    /**
     * Maximum number of mixture (i.e. cluster) components
     */
    cmxUInt m_mix_size;
};

#endif  // cmxClustMpopSharcoRo_h
