//****************************************************************************
// cmxModel.inl
//****************************************************************************

// Not meant to be included more than once
#ifdef uCmxModel_inl
#error "Cmx Model implementation included multiple times!"
#endif  // uCmxModel_inl
#define uCmxModel_inl

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

//****************************************************************************
// Defines
//****************************************************************************

/**
 * Macro to register all model components
 */

#ifdef CMX_MOCO_REGISTER_ALL
#error "CMX_MOCO_REGISTER_ALL MACRO ALREADY DEFINED!"
#endif  // CMX_MOCO_REGISTER_ALL

#define CMX_MOCO_REGISTER_ALL                                                 \
    do {                                                                      \
        cmxAssert(m_all_components.empty());                                  \
        cmxAssert(m_rv_components.empty());                                   \
        CMX_MOCO_REGISTER(Dp_stick_alpha, m_all_components, m_rv_components); \
        CMX_MOCO_REGISTER(Dp_stick, m_all_components, m_rv_components);       \
        CMX_MOCO_REGISTER(Bnet_dag_kappa, m_all_components, m_rv_components); \
        CMX_MOCO_REGISTER(Bnet_cpt_alpha, m_all_components, m_rv_components); \
        CMX_MOCO_REGISTER(                                                    \
            Bnet_infeas_lambda, m_all_components, m_rv_components);           \
        CMX_MOCO_REGISTER(Bnet, m_all_components, m_rv_components);           \
        CMX_MOCO_REGISTER(Clust, m_all_components, m_rv_components);          \
        CMX_MOCO_REGISTER(Pseudopop, m_all_components, m_rv_components);      \
        CMX_MOCO_REGISTER(                                                    \
            Contact_freqs_alpha, m_all_components, m_rv_components);          \
        CMX_MOCO_REGISTER(Contact_freqs, m_all_components, m_rv_components);  \
        cmxAssertBoundsInc(                                                   \
            m_rv_components.size(), 1, m_all_components.size());              \
    } while (0)

//****************************************************************************
// Implementation
//****************************************************************************

/**
 * Default constructor
 */
template <typename t_Glue>
cmxModel<t_Glue>::cmxModel() {
    // Register model components
    CMX_MOCO_REGISTER_ALL;
}

/**
 * Copy constructor
 */
template <typename t_Glue>
cmxModel<t_Glue>::cmxModel(const cmxModel& other)
    : m_Dp_stick_alpha(other.m_Dp_stick_alpha),
      m_Dp_stick(other.m_Dp_stick),
      m_Bnet_dag_kappa(other.m_Bnet_dag_kappa),
      m_Bnet_cpt_alpha(other.m_Bnet_cpt_alpha),
      m_Bnet_infeas_lambda(other.m_Bnet_infeas_lambda),
      m_Bnet(other.m_Bnet),
      m_Clust(other.m_Clust),
      m_Pseudopop(other.m_Pseudopop),
      m_Contact_freqs_alpha(other.m_Contact_freqs_alpha),
      m_Contact_freqs(other.m_Contact_freqs) {
    // Register model components
    CMX_MOCO_REGISTER_ALL;
}

/**
 * Assignment operator
 */
template <typename t_Glue>
cmxModel<t_Glue>& cmxModel<t_Glue>::operator=(const cmxModel& other) {
    if (&other == this) {
        // Early out if assigning to self
        return *this;
    }
    // Copy data members
    m_Dp_stick_alpha = other.m_Dp_stick_alpha;
    m_Dp_stick = other.m_Dp_stick;
    m_Bnet_dag_kappa = other.m_Bnet_dag_kappa;
    m_Bnet_cpt_alpha = other.m_Bnet_cpt_alpha;
    m_Bnet_infeas_lambda = other.m_Bnet_infeas_lambda;
    m_Bnet = other.m_Bnet;
    m_Clust = other.m_Clust;
    m_Pseudopop = other.m_Pseudopop;
    m_Contact_freqs_alpha = other.m_Contact_freqs_alpha;
    m_Contact_freqs = other.m_Contact_freqs;
    // Register model components
    m_rv_components.clear();
    m_all_components.clear();
    CMX_MOCO_REGISTER_ALL;
    return *this;
}

/**
 * Initializes model and all model components from user configuration
 * @param config - user configuration options
 * @param sim - parent simulation
 */
template <typename t_Glue>
void cmxModel<t_Glue>::init(cmxSpConstConfig_t config,
                            typename t_Glue::sim_t& sim CMX_THREAD_ID_PARAM) {
    // Initialize each child component
    for (cmxUInt i = 0; i < m_all_components.size(); ++i) {
        cmxAssert(m_all_components[i]);
        moco_t& c = *(m_all_components[i]);
        c.init(config, *this, sim CMX_THREAD_ID_ARG);
    }
}

/**
 * Initializes model components to random state, should be called after
 * init(); note: all components, not just 'random' components' can choose to
 * handle or ignore this call
 * @param config - user configuration options
 * @param sim - parent simulation
 */
template <typename t_Glue>
void cmxModel<t_Glue>::randomize(cmxSpConstConfig_t config,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {
    // Randomize each child component
    for (cmxUInt i = 0; i < m_all_components.size(); ++i) {
        cmxAssert(m_all_components[i]);
        moco_t& c = *(m_all_components[i]);
        c.randomize(config, *this, sim CMX_THREAD_ID_ARG);
    }
}

//****************************************************************************
// Defines (clean-up)
//****************************************************************************

/**
 * Tidy up pre-processor
 */
#undef CMX_MOCO_REGISTER_ALL
