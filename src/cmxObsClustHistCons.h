//****************************************************************************
// cmxObsClustHistCons.h
//****************************************************************************

/**
 * @brief Console observer for model cluster histogram
 */

#ifndef cmxObsClustHistCons_h
#define cmxObsClustHistCons_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxCaptClustSizeTypes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObsClustSizeSnap.h"
#include "cmxOpts.h"

#include <string>
#include <vector>

//****************************************************************************
// cmxObsClustHistCons
//****************************************************************************

/**
 * Cluster histogram console observer - reports cluster assignments (as an
 * ASCII histogram) to stdout at a regular, user-defined interval. Depends on
 * snapshot observer
 */
template <typename t_Glue>
class cmxObsClustHistCons : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Snapshot observer type
     */
    typedef cmxObsClustSizeSnap<t_Glue> obs_clust_size_snap_t;

    /**
     * Default console period
     */
    enum { default_period = 1 };

    /**
     * Number of stars to represent histogram, currently not configurable
     */
    enum { max_stars = 50 };

    /**
     * Default constructor
     */
    cmxObsClustHistCons() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_clust_hist_cons_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Extract snapshot observer - must occur before console observer
        if (!obs_t::find_raw(&mp_snap, sim, index)) {
            cmxLogf(
                "Error: cluster histogram console observer unable to find "
                "handle to snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_period = default_period;
        mp_snap = NULL;
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, reports to stdout
     * if iteration matches period
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if ((iter % m_period) != CMX_TO_UINT(0)) {
            // Early out if not yet at reporting interval
            return;
        }
        // Iterate over model cluster sizes
        const std::vector<cmxClustSizePqueue_t>& clust_sizes =
            mp_snap->get_clust_sizes();
        const size_t NUM_MODELS = clust_sizes.size();
        cmxAssertPosEq(NUM_MODELS, sim.get_num_models());
        const cmxReal TOTAL_PSUE_SIZEF = CMX_TO_REAL(
            sim.get_shared_ro().get_Pseudopop().get_pseudopop_size());
        for (size_t i_model = 0; i_model < NUM_MODELS; ++i_model) {
            cmxClustSizePqueue_t pqueue = clust_sizes[i_model];
            // Print histogram to stdout
            cmxLogf(
                "(ITER %d, MODEL %d) CLUST HIST =\n", (int)iter, (int)i_model);
            cmxAssert(!pqueue.empty());
            while (!pqueue.empty()) {
                const cmxClustSizeInfo_t nfo = pqueue.top();
                const cmxReal p = CMX_TO_REAL(nfo.m_size) / TOTAL_PSUE_SIZEF;
                const cmxReal n_starsf = CMX_TO_REAL(max_stars) * p;
                const size_t n_stars = static_cast<size_t>(n_starsf);
                const std::string stars(n_stars, '*');
                cmxLogf("\t%4d: %s %.4f\n",
                        ((int)nfo.m_index),
                        stars.c_str(),
                        ((double)p));
                pqueue.pop();
            }
        }
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_clust_hist_cons)

private:
    /**
     * Reports to stdout on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Handle to snapshot observer - raw pointer is used as lifetime
     * of pointer does not exceed that of parent simulation's shared_ptr
     */
    const obs_clust_size_snap_t* mp_snap;
};

#endif  // cmxObsClustHistCons_h
