//****************************************************************************
// cmxClustMpopMoco.h
//****************************************************************************

/**
 * @brief Cluster (i.e. mixture) assignments model component. A set of
 *  variables denoting the cluster membership for each sample in
 *  population.
 */

#ifndef cmxClustMpopMoco_h
#define cmxClustMpopMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxBnetBool.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxGlobals.h"
#include "cmxMoco.h"
#include "cmxModelTypes.h"
#include "cmxSelectorTower.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxClustMpopMoco
//****************************************************************************

/**
 * Expected Clust model component interface:
 *  get_mix_index(sample_index) - must return the associated cluster index
 *      for the parameter pseudo-population sample identifier
 *  get_pseudopop_indices(clust_index, sim) - must return indices of pseudo-
 *      population samples associated with parameter cluster index
 *  get_pseudopop_size(clust_index, sim) - must return number of pseudo-
 *      population samples associated with parameter cluster index
 */
template <typename t_Glue>
class cmxClustMpopMoco : public cmxMoco<t_Glue> {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Component is random and requires sampling
     */
    enum { is_random = cmxTRUE };

    // Model component interface

    /**
     * Initialization from user configuration. Model component should seek to
     * initialize to single population, independent null model where possible.
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void init(cmxSpConstConfig_t config,
                      model_t& model,
                      sim_t& sim CMX_THREAD_ID_PARAM) {
        // Determine pseudo-population size
        const cmxUInt pseudopop_size =
            cmxConfigUtils::get_pseudopop_size(config);
        cmxAssert(pseudopop_size > 0);
        // Determine mixture size
        const cmxUInt mix_size = cmxConfigUtils::get_mix_size(config);
        cmxAssert(mix_size > 0);
        // Reset mappings
        m_pseudopop2mix.clear();
        m_mix2pseudopop.clear();
        // Default assign all samples to first cluster
        m_pseudopop2mix.resize(pseudopop_size, 0);
        m_mix2pseudopop.resize(mix_size);
        m_mix2pseudopop[0].reserve(pseudopop_size);
        for (cmxUInt i = 0; i < pseudopop_size; ++i) {
            m_mix2pseudopop[0].push_back(i);
        }
    }

    /**
     * Initialize to a random state, should be called after init()
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void randomize(cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {
        const cmxUInt MIX_SIZE = sim.get_shared_ro().get_Clust().get_mix_size();
        cmxAssert(MIX_SIZE > 0);
        const cmxUInt PSEUDOPOP_SIZE =
            sim.get_shared_ro().get_Pseudopop().get_pseudopop_size();
        cmxAssert(PSEUDOPOP_SIZE > 0);
        // Reset cluster mappings
        m_pseudopop2mix.clear();
        m_pseudopop2mix.resize(PSEUDOPOP_SIZE);
        cmxAssertPosEq(PSEUDOPOP_SIZE, CMX_TO_UINT(m_pseudopop2mix.size()));
        m_mix2pseudopop.clear();
        m_mix2pseudopop.resize(MIX_SIZE);
        cmxAssertPosEq(MIX_SIZE, CMX_TO_UINT(m_mix2pseudopop.size()));
        // Get mixture PMF
        const cmxVecCol& mix_pmf = model.get_Dp_stick().get_mix_pmf();
        cmxAssertPosEq(CMX_TO_UINT(mix_pmf.n_elem), MIX_SIZE);
        // Sample from mixture PMF prior
        cmxRand& rng = cmxRng;
        for (cmxUInt i = 0; i < PSEUDOPOP_SIZE; ++i) {
            // Sample cluster index from categorical PMF
            const cmxUInt cluster_index =
                cmxSelectorTower::rand_select_index(mix_pmf, rng);
            cmxAssertBounds(cluster_index, CMX_TO_UINT(0), MIX_SIZE);
            m_pseudopop2mix[i] = cluster_index;
            m_mix2pseudopop[cluster_index].push_back(i);
        }
    }

    /**
     * Gibbs posterior update, should only be called if model component is
     * random! Generates a sample from the posterior distribution P(Xi | X-i)
     * where Xi is i-th random variable and X-i is set of of all random
     * variables except for Xi in the model.
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void Gibbs_posterior(model_t& model,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {
        /**
         * Implementation based on:
         *  Ishwaran, Hemant, and Mahmoud Zarepour. "Markov chain Monte Carlo
         *  in approximate Dirichlet and beta two-parameter process
         *  hierarchical models." Biometrika 87, no. 2 (2000) : 371 - 390.
         */
        // Get mixture size
        const cmxUInt MIX_SIZE = sim.get_shared_ro().get_Clust().get_mix_size();
        cmxAssert(MIX_SIZE > 0);
        // Get pseudo-population size
        const cmxUInt PSEUDOPOP_SIZE =
            sim.get_shared_ro().get_Pseudopop().get_pseudopop_size();
        cmxAssert(PSEUDOPOP_SIZE > 0);
        // Get mixture log-PMF
        const cmxVecCol& mix_lnpmf = model.get_Dp_stick().get_mix_lnpmf();
        cmxAssertPosEq(CMX_TO_MAT_SZ_T(MIX_SIZE), mix_lnpmf.n_elem);
        cmxAssertRealEq(cmxMatrixUtils::sum(cmxMatrixUtils::exp(mix_lnpmf)),
                        CMX_TO_MAT_SZ_T(1.0));
        // Get pseudo-population
        const cmxPseudopopFrame_t& pseudopop =
            model.get_Pseudopop().get_pseudopop_frame();
        cmxAssertPosEq(CMX_TO_UINT(pseudopop.n_cols), PSEUDOPOP_SIZE);
        // Reset cluster mappings
        cmxAssertPosEq(PSEUDOPOP_SIZE, CMX_TO_UINT(m_pseudopop2mix.size()));
        m_mix2pseudopop.clear();
        m_mix2pseudopop.resize(MIX_SIZE);
        cmxAssertPosEq(MIX_SIZE, CMX_TO_UINT(m_mix2pseudopop.size()));
        // Get handle to posterior PMF buffer
        cmxVecCol& mix_post_pmf = sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                                      .get_Clust()
                                      .get_mix_post_pmf_buf();
        // Sample from posterior
        cmxRand& rng = cmxRng;
        for (cmxUInt i_pseu = 0; i_pseu < PSEUDOPOP_SIZE; ++i_pseu) {
            mix_post_pmf = mix_lnpmf;
            const cmxPseudopopFrame_t::elem_type* const pseu_bits =
                pseudopop.colptr(CMX_TO_MAT_SZ_T(i_pseu));
            for (cmxUInt i_mix = 0; i_mix < MIX_SIZE; ++i_mix) {
                // Compute log-likelihood of sample under associated Bayes net
                const cmxBnetBool& bnet = model.get_Bnet().get_bnet(i_mix);
                const cmxReal lnprob_bnet = bnet.logprob(pseu_bits);
                // Modulate cluster probability by log-likelihood
                mix_post_pmf.at(i_mix) += lnprob_bnet;
            }
            // Numerically stabilize by subtracting max element
            const cmxReal max_elem = mix_post_pmf.max();
            mix_post_pmf -= max_elem;
            // Transform to inverse log-space
            mix_post_pmf = cmxMatrixUtils::exp(mix_post_pmf);
            // Normalize by partition constant
            const cmxReal inv_Z =
                CMX_TO_REAL(1.0) / cmxMatrixUtils::sum(mix_post_pmf);
            mix_post_pmf *= inv_Z;
            cmxAssertRealEq(CMX_TO_REAL(1.0),
                            cmxMatrixUtils::sum(mix_post_pmf));
            // Sample cluster index from categorical PMF
            const cmxUInt cluster_index =
                cmxSelectorTower::rand_select_index(mix_post_pmf, rng);
            cmxAssertBounds(cluster_index, CMX_TO_UINT(0), MIX_SIZE);
            m_pseudopop2mix[i_pseu] = cluster_index;
            m_mix2pseudopop[cluster_index].push_back(i_pseu);
        }
    }

    /**
     * Intended to assess Gibbs sampler convergence ideally showing that chain
     * is exploring high probability regions. This may be an expensive call to
     * make and should be used sparingly at regular intervals.
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    virtual cmxReal logscore(model_t& model, sim_t& sim) {
        // Get mixture size
        const cmxUInt MIX_SIZE = CMX_TO_UINT(m_mix2pseudopop.size());
        cmxAssertPosEq(MIX_SIZE,
                       sim.get_shared_ro().get_Clust().get_mix_size());
        // Get mixture log-PMF
        const cmxVecCol& mix_lnpmf = model.get_Dp_stick().get_mix_lnpmf();
        cmxAssertPosEq(CMX_TO_MAT_SZ_T(MIX_SIZE), mix_lnpmf.n_elem);
        // Tabulate sum of associated log stick-breaking probabilities
#ifdef CMX_BUILD_ENABLE_ASSERT
        // Keep track of total pseudo-population size
        cmxUInt total_pseudopop_size = CMX_TO_UINT(0);
#endif  // CMX_BUILD_ENABLE_ASSERT
        cmxReal score = CMX_TO_REAL(0.0);
        for (cmxUInt i = 0; i < MIX_SIZE; ++i) {
            const cmxReal PSEU_SIZE =
                CMX_TO_REAL(this->get_pseudopop_size(i, sim));
            score += PSEU_SIZE * mix_lnpmf.at(CMX_TO_MAT_SZ_T(i));
#ifdef CMX_BUILD_ENABLE_ASSERT
            total_pseudopop_size += this->get_pseudopop_size(i, sim);
#endif  // CMX_BUILD_ENABLE_ASSERT
        }
        cmxAssertPosEq(
            total_pseudopop_size,
            sim.get_shared_ro().get_Pseudopop().get_pseudopop_size());
        return score;
    }

    // Accessors

    /**
     * @return mixture assignment for parameter sample
     */
    inline cmxUInt get_mix_index(const cmxUInt sample_index) const {
        cmxAssertBounds(sample_index, 0, CMX_TO_UINT(m_pseudopop2mix.size()));
        return m_pseudopop2mix[sample_index];
    }

    /**
     * @return samples assigned to parameter cluster
     */
    inline const cmxPseudopopIndices_t& get_pseudopop_indices(
        const cmxUInt mix_index,
        const sim_t& sim) const {
        cmxAssertPosEq(CMX_TO_UINT(m_mix2pseudopop.size()),
                       sim.get_shared_ro().get_Clust().get_mix_size());
        cmxAssertBounds(mix_index, 0, CMX_TO_UINT(m_mix2pseudopop.size()));
        return m_mix2pseudopop[mix_index];
    }

    /**
     * @return number of samples assigned to parameter cluster
     */
    inline cmxUInt get_pseudopop_size(const cmxUInt mix_index,
                                      const sim_t& sim) const {
        return CMX_TO_UINT(this->get_pseudopop_indices(mix_index, sim).size());
    }

private:
    /**
     * Underlying mixture assignments - maps pseudo-population samples to
     * their associated cluster
     */
    std::vector<cmxUInt> m_pseudopop2mix;

    /**
     * Maps a cluster to its associated set of pseudo-population samples
     */
    std::vector<cmxPseudopopIndices_t> m_mix2pseudopop;
};

#endif  // cmxClustMpopMoco_h
