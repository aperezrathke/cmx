//****************************************************************************
// cmxMain.cpp
//
// Main program entry point
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAppHooks.h"
#include "cmxApps.h"
#include "cmxCmdOptsMap.h"
#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxIntervalLogger.h"
#include "cmxLogf.h"
#include "cmxOpts.h"

// Commandlet integration
#include "cmxCommandletHooks.inl"

// Test integration
#include "cmxTestHooks.inl"

//****************************************************************************
// Help utility
//****************************************************************************

/**
 * App prints usage information
 *
 * Usage:
 * -help | -app_help
 */
int cmxAppHelpMain(const cmxCmdOptsMap& cmd_opts) {
    cmxOpt_print_usage_all();

    // Signify we finished okay
    return cmxExitCode_normal;
}

//****************************************************************************
// App registration
//****************************************************************************

// Register applications here
const struct cmxAppInfo GApps[] = {
    {cmxOpt_get_cmd_switch(cmxOpt_help), &cmxAppHelpMain},
    {cmxOpt_get_cmd_switch(cmxOpt_app_help), &cmxAppHelpMain},
    {cmxOpt_get_cmd_switch(cmxOpt_app_spop_canon), &cmxAppSpopCanonMain},
    {cmxOpt_get_cmd_switch(cmxOpt_app_mpop_canon), &cmxAppMpopCanonMain}};

/**
 * Wrapper for executing main applications
 */
static int cmxRunApps(const cmxCmdOptsMap& cmd_opts) {
    //******************************
    // Main application starts here:
    //******************************
    CMX_CONDITIONAL_RUN_APPS(cmd_opts, GApps);
    // If we reach here, we didn't find an application to run
    return cmxExitCode_error;
}

/**
 * Display ASCII logo
 */
static void print_logo() {
    const char* LOGO =
        ".----------------.  .----------------.  .----------------.\n"
        "| .--------------. || .--------------. || .--------------. |\n"
        "| |     ______   | || | ____    ____ | || |  ____  ____  | |\n"
        "| |   .' ___  |  | || ||_   \\  /   _|| || | |_  _||_  _| | |\n"
        "| |  / .'   \\_|  | || |  |   \\/   |  | || |   \\ \\  / /   | |\n"
        "| |  | |         | || |  | |\\  /| |  | || |    > `' <    | |\n"
        "| |  \\ `.___.'\\  | || | _| |_\\/_| |_ | || |  _/ /'`\\ \\_  | |\n"
        "| |   `._____.'  | || ||_____||_____|| || | |____||____| | |\n"
        "| |              | || |              | || |              | |\n"
        "| '--------------' || '--------------' || '--------------' |\n"
        "'----------------'  '----------------'  '----------------'\n";
    cmxLogf("%s\n", LOGO);
}

//****************************************************************************
// Main
//****************************************************************************

int main(const int argc, const char** argv) {
    CMX_SCOPED_INTERVAL_LOGGER(cmxSTAT_TotalExecInterval);

    // Display logo
    print_logo();

    // Format command line options into <key, value> mappings
    cmxCmdOptsMap cmd_opts;
    if (!cmd_opts.parse(argc, argv)) {
        cmxLogf(
            "Unrecoverable error - unable to parse command line options.\n");
        return cmxExitCode_error;
    }

    // Commandlets!
    CMX_CONDITIONAL_RUN_COMMANDLETS(cmd_opts);

    // Tests!
    CMX_CONDITIONAL_RUN_TESTS(cmd_opts);

    // Run main application(s)
    return cmxRunApps(cmd_opts);
}
