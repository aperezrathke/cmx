//****************************************************************************
// cmxMcmcMh4gMixin.h
//****************************************************************************

/**
 * @brief Mixins for common double-buffering operations and utilities used by
 *  Metropolis[-Hastings] model components
 */

#ifndef cmxMcmcMh4gMixin_h
#define cmxMcmcMh4gMixin_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxGlue.h"
#include "cmxMcmcMh4g.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxMcmcMh4gMetroMixin
//****************************************************************************

/**
 * Mixin for Metropolis sampling, should only be used if proposal is
 *  symmetric. For asymmetric proposals, see cmxMcmcMh4gMhastMixin
 *
 * Mixins are meant to be inherited, assumes child class will provide
 * methods:
 *  - init_state(state_t&, config, model_t&, sim_t& THREAD_ID): initialize
 *      state, should seek to initialize to independent null model
 *  - randomize_state(state_t&, config, model_t&, sim_t& THREAD_ID): randomize
 *      state according to prior
 *
 * Template arguments:
 *  - t_Glue: Glue object as defined in cmxGlue
 *  - t_State: State object representing either a current or candidate
 *      Metropolis[-Hastings] state, assumed to have cmxReal data field(s):
 *      .logp_tar - probability of current state under target distribution
 *
 * In addition, the derived child class may need to give friend access to
 * mixin if init_state(...) and randomize_state(...) calls are private
 */
template <typename t_Glue, typename t_State>
class cmxMcmcMh4gMetroMixin {
private:
    /**
     * Avoid magic numbers, use double buffered states for Metropolis-Hastings
     */
    enum { buffer_size = 2 };

public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Perform single Metropolis step
     */
    template <typename t_model>
    inline static void Mh_step(t_model& model,
                               const typename t_model::Mh_args_t mh_args
                                   CMX_THREAD_ID_PARAM) {
        cmxMcmcMh4g::Mh_step<t_model, true /* use_metropolis */>(
            model, mh_args CMX_THREAD_ID_ARG);
    }

    /**
     * Run model for a fixed number of Metropolis steps (no thinning)
     * @param n_steps - number of steps to run MH sampler
     * @param model - the model to be sampled
     * @param mh_args - model-specific arguments
     */
    template <typename t_model>
    inline static void Mh_run(const cmxUInt n_steps,
                              t_model& model,
                              const typename t_model::Mh_args_t mh_args
                                  CMX_THREAD_ID_PARAM) {
        cmxMcmcMh4g::Mh_run<t_model, true /* use_metropolis */>(
            n_steps, model, mh_args CMX_THREAD_ID_ARG);
    }

    // Metropolis-Hastings MCMC interface - only defines methods
    // needed for symmetric proposal (Metropolis sampling)

    /**
     * Metropolis-Hastings interface
     * @return log probability of candidate under target distribution
     */
    inline cmxReal Mh_get_logp_tar_cand() const {
        return this->get_cand().logp_tar;
    }

    /**
     * Metropolis-Hastings interface
     * @return log probability of incumbent (i.e. current) under target
     *  distribution
     */
    inline cmxReal Mh_get_logp_tar_curr() const {
        return this->get_curr().logp_tar;
    }

    /**
     * Metropolis-Hastings interface
     * Callback for when a candidate has been accepted - swaps current and
     * candidate buffers
     */
    inline void Mh_on_accept_cand() {
        cmxAssertBounds(m_ix_curr, 0, buffer_size);
        // Swap index of current and candidate states (swap buffers)
        m_ix_curr = 1 - m_ix_curr;
#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
        // Update running acceptance probability tracker
        m_stats(CMX_TO_REAL(1.0));
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    }

    /**
     * Metropolis-Hastings interface
     * Callback for when a candidate has been rejected
     */
    inline void Mh_on_reject_cand() {
#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
        // Update running acceptance probability tracker
        m_stats(CMX_TO_REAL(0.0));
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    }

    // MCMC - Need to define full Metropolis-Hastings functions even though
    // they should not ever be called! (Need SFINAE)

    /**
     * Metropolis-Hastings interface
     * @return log probability of proposing incumbent (current) sample given
     *  the chain is at candidate sample
     */
    inline cmxReal Mh_get_logp_pro_curr_giv_cand() const {
        cmxAssert(false);
        return CMX_TO_REAL(0.0);
    }

    /**
     * Metropolis-Hastings interface
     * @return log probability of proposing candidate sample given the chain
     *  is at incumbent (current) sample
     */
    inline cmxReal Mh_get_logp_pro_cand_giv_curr() const {
        cmxAssert(false);
        return CMX_TO_REAL(0.0);
    }

public:
    // MCMC mixin interface

    /**
     * Metropolis[-Hastings] state type
     */
    typedef t_State state_t;

    // Constructors and initialization

    /**
     * Default constructor
     */
    cmxMcmcMh4gMetroMixin() : m_ix_curr(0) {}

    /**
     * Calls init_state() on current and candidate states. The method
     * init_state(state_t&, config, model_t&, sim_t& THREAD) is expected to
     * be defined in derived class. The init_state(...) method should seek to
     * initialize to an independent, null model where possible.
     * @param d - object which inherits mixin
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    template <typename t_deriv>
    void init_states(t_deriv& d,
                     cmxSpConstConfig_t config,
                     model_t& model,
                     sim_t& sim CMX_THREAD_ID_PARAM) {
        d.init_state(this->get_curr(), config, model, sim CMX_THREAD_ID_ARG);
        d.init_state(this->get_cand(), config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Calls randomize_state() on current and candidate states. The method
     * randomize_state(state_t&, config, model_t&, sim_t& THREAD) is expected
     * to be defined in derived class. The randomize_state(...) method should
     * sample a state from a prior distribution.
     * @param d - object which inherits mixin
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    template <typename t_deriv>
    void randomize_states(t_deriv& d,
                          cmxSpConstConfig_t config,
                          model_t& model,
                          sim_t& sim CMX_THREAD_ID_PARAM) {
        d.randomize_state(
            this->get_curr(), config, model, sim CMX_THREAD_ID_ARG);
        d.randomize_state(
            this->get_cand(), config, model, sim CMX_THREAD_ID_ARG);
    }

    // Accessors

    /**
     * @return index of current state
     */
    inline cmxUInt get_ix_curr() const {
        cmxAssertBounds(m_ix_curr, 0, buffer_size);
        return m_ix_curr;
    }

    /**
     * @return index of candidate state
     */
    inline cmxUInt get_ix_cand() const { return 1 - this->get_ix_curr(); }

    /**
     * @return [const] current state
     */
    inline state_t& get_curr() { return this->get_state(this->get_ix_curr()); }
    inline const state_t& get_curr() const {
        return this->get_state(this->get_ix_curr());
    }

    /**
     * @return [const] candidate state
     */
    inline state_t& get_cand() { return this->get_state(this->get_ix_cand()); }
    inline const state_t& get_cand() const {
        return this->get_state(this->get_ix_cand());
    }

    /**
     * @return [const] state at parameter index
     */
    inline state_t& get_state(const cmxUInt ix) {
        cmxAssertBounds(ix, 0, buffer_size);
        return m_state[ix];
    }
    inline const state_t& get_state(const cmxUInt ix) const {
        cmxAssertBounds(ix, 0, buffer_size);
        return m_state[ix];
    }

    /**
     * @return const handle to tabulated stats
     */
#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    inline const cmxRunStat_t& get_stats() const { return m_stats; }
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

private:
    /**
     * Running stats tracker
     */
#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    cmxRunStat_t m_stats;
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

    /**
     * Index of current state
     */
    cmxUInt m_ix_curr;

    /**
     * Double-buffered current and candidate states
     */
    state_t m_state[buffer_size];
};

//****************************************************************************
// cmxMcmcMh4gMhastMixin
//****************************************************************************

/**
 * Mixin for Metropolis-Hasting sampling, should only be used if proposal is
 *  asymmetric. For symmetric proposals, see cmxMcmcMh4gMetroMixin
 *
 * Mixins are meant to be inherited, assumptions of child class and
 *  description of template arguments are same as cmxMcmcMh4gMetroMixin except
 *  where detailed below.

 * Template arguments:
 *  - t_State: State object representing either a current or candidate
 *      Metropolis-Hastings state, assumed to have cmxReal data fields:
 *      .logp_tar - probability of current state under target distribution
 *      .logp_pro - probability of proposing a state; specifically, in the
 *          candidate state, the probability of proposing current state; in
 *          the current state, the probability of proposing candidate state
 */
template <typename t_Glue, typename t_State>
class cmxMcmcMh4gMhastMixin : public cmxMcmcMh4gMetroMixin<t_Glue, t_State> {
protected:
    /**
     * Metropolis-Hastings state type
     */
    typedef t_State state_t;

public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    // @WARNING - THIS IS NOT USING VIRTUAL FUNCTION OVERLOADING, THIS IS
    // RELYING ON FUNCTION HIDING BEHAVIOR, COMPILER MUST KNOW WHICH TYPE
    // (cmxMcmcMh4gMhastMixin vs cmxMcmcMh4gMetroMixin) IS BEING CALLED!

    /**
     * Perform single Metropolis-Hastings step
     */
    template <typename t_model>
    inline static void Mh_step(t_model& model,
                               const typename t_model::Mh_args_t mh_args
                                   CMX_THREAD_ID_PARAM) {
        cmxMcmcMh4g::Mh_step<t_model, false /* use_metropolis */>(
            model, mh_args CMX_THREAD_ID_ARG);
    }

    /**
     * Run model for a fixed number of Metropolis-Hastings steps (no thinning)
     * @param n_steps - number of steps to run MH sampler
     * @param model - the model to be sampled
     * @param mh_args - model-specific arguments
     */
    template <typename t_model>
    inline static void Mh_run(const cmxUInt n_steps,
                              t_model& model,
                              const typename t_model::Mh_args_t mh_args
                                  CMX_THREAD_ID_PARAM) {
        cmxMcmcMh4g::Mh_run<t_model, false /* use_metropolis */>(
            n_steps, model, mh_args CMX_THREAD_ID_ARG);
    }

    // Metropolis-Hastings MCMC interface - defines additional methods
    // needed for asymmetric proposal

    /**
     * Metropolis-Hastings interface
     * @return log probability of proposing incumbent (current) sample given
     *  the chain is at candidate sample
     */
    inline cmxReal Mh_get_logp_pro_curr_giv_cand() const {
        return this->get_cand().logp_pro;
    }

    /**
     * Metropolis-Hastings interface
     * @return log probability of proposing candidate sample given the chain
     *  is at incumbent (current) sample
     */
    inline cmxReal Mh_get_logp_pro_cand_giv_curr() const {
        return this->get_curr().logp_pro;
    }
};

#endif  // cmxMcmcMh4gMixin_h
