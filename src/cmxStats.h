//****************************************************************************
// cmxStats.h
//****************************************************************************

/**
 * @brief Simple stats structures for profiling and tracking counts
 */

#ifndef cmxStats_h
#define cmxStats_h

#include "cmxBuild.h"

// For convenience - include interval logging
#include "cmxIntervalLogger.h"

#ifdef CMX_BUILD_ENABLE_STATS

#include "cmxAssert.h"
#include "cmxLogf.h"
#include "cmxThread.h"
#include "cmxTime.h"
#include "cmxTypes.h"

/**
 * Enumerated unique identifiers for all timer stats
 */
enum cmxAllTimerStats {
    cmxSTAT_GlobalInitTime = 0,
    cmxSTAT_BnetThinDagCycleCheckTime,
    cmxSTAT_BnetThinDagMoveFinderTime,
    cmxSTAT_BnetThinDagTopoSortTime,
    cmxSTAT_PseudpopResampleTime,
    cmxSTAT_PseudpopRefreshFreqsTime,
    cmxSTAT_TimerMAX
};

/**
 * Enumerated unique identifiers for all counter stats
 */
enum cmxAllCounterStats { cmxSTAT_GlobalInitCount = 0, cmxSTAT_CounterMAX };

#define CMX_SCOPED_STAT_TIMER(stat_id) \
    cmxStats::ScopedStatTimer ScopedTimer_##stat_id(stat_id)

#define CMX_INC_STAT_COUNTER(stat_id) cmxStats::GCounterStats[stat_id].inc_by(1)

#define CMX_INC_STAT_COUNTER_BY(stat_id, amount) \
    cmxStats::GCounterStats[stat_id].inc_by(amount)

namespace cmxStats {

/**
 * For timing function calls
 */
class TimerStat {
public:
    TimerStat() : m_total_time(0.0) {
#ifdef CMX_BUILD_ENABLE_THREADS
        cmxAssert(m_total_time.is_lock_free());
#endif  // CMX_BUILD_ENABLE_THREADS
    }

    inline void inc_total_time(const double t) {
        CMX_ATOMIC_ADD_DOUBLE(m_total_time, t);
    }
    inline double get_total_time() const { return m_total_time; }

private:
    /**
     * The total time of this stat
     */
    cmxAtomicDouble_t m_total_time;
};

/**
 * For counting how many times an event occurs
 */
class CounterStat {
public:
    CounterStat() : m_counter(0) {
#ifdef CMX_BUILD_ENABLE_THREADS
        cmxAssert(m_counter.is_lock_free());
#endif  // CMX_BUILD_ENABLE_THREADS
    }

    inline void inc_by(const cmxUInt amount) { m_counter += amount; }
    inline cmxUInt get_counter() const { return m_counter; }

private:
    cmxAtomicUInt m_counter;
};

/**
 * All the timer stat instances
 */
extern TimerStat GTimerStats[cmxSTAT_TimerMAX];

/**
 * All the counter stat instances
 */
extern CounterStat GCounterStats[cmxSTAT_CounterMAX];

/**
 * A synchronous timer which updates on destruction
 */
class ScopedStatTimer {
public:
    explicit ScopedStatTimer(const cmxAllTimerStats stat_id)
        : m_stat_id(stat_id), m_start_time(cmx_time_secs_wall()) {}

    ~ScopedStatTimer() {
        GTimerStats[m_stat_id].inc_total_time(cmx_time_secs_wall() -
                                              m_start_time);
    }

private:
    const cmxAllTimerStats m_stat_id;
    const double m_start_time;
};

/**
 * Report stats - print to stdout
 */
extern void report_stats();

}  // namespace cmxStats

#define CMX_STATS_REPORT cmxStats::report_stats()

#else  // !CMX__BUILD_ENABLE_STATS

#define CMX_SCOPED_STAT_TIMER(stat_id) ((void)0)
#define CMX_INC_STAT_COUNTER(stat_id) ((void)0)
#define CMX_INC_STAT_COUNTER_BY(stat_id, amount) ((void)0)
#define CMX_STATS_REPORT ((void)0)

#endif  // CMX_BUILD_ENABLE_STATS

#endif  // cmxStats_h
