//****************************************************************************
// cmxBuild.h
//****************************************************************************

/**
 * @brief Global build defines
 */

#ifndef cmxBuild_h
#define cmxBuild_h

/**
 * OS detection
 */
#ifdef _WIN32
#   define CMX_BUILD_OS_WINDOWS
#else
#   define CMX_BUILD_OS_UNIX
#endif  // _WIN32

/**
 * Build types
 */
#ifndef CMX_BUILD_TYPE_DEBUG
// _DEBUG is defined by Visual Studio (non-ANSI compliant)
// NDEBUG is defined by CMake for GCC release builds
// NDEBUG is part of C++ ANSI standard for disabling assertions
#   if defined(_DEBUG) || (defined(CMX_BUILD_OS_UNIX) && !defined(NDEBUG))
#       define CMX_BUILD_TYPE_DEBUG
#   else
// Switch to force enable debug build
//#       define CMX_BUILD_TYPE_DEBUG
#   endif
#endif  // CMX_BUILD_TYPE_DEBUG

/**
 * Compiler detection
 */
#ifdef _MSC_VER
#   define CMX_BUILD_COMPILER_MSVC
#elif (defined(__ICC) || defined(__ICL) || defined(__INTEL_COMPILER))
#   define CMX_BUILD_COMPILER_ICC
#elif (defined(__GNUC__))
#   define CMX_BUILD_COMPILER_GCC
#else
#   error Unrecognized compiler
#endif

/**
 * C++11
 */
// http://stackoverflow.com/questions/5047971/how-do-i-check-for-c11-support
#ifndef CMX_BUILD_CXX_11
#   ifdef CMX_BUILD_COMPILER_MSVC
#       if (__cplusplus >= 199711L)
#           define CMX_BUILD_CXX_11
#       endif
#   elif (__cplusplus >= 201103L)
#       define CMX_BUILD_CXX_11
#   endif
#endif  // CMX_BUILD_CXX_11

/**
 * This master switch toggles assertion checking
 * (will enable even if NDEBUG is defined)
 */
//#define CMX_BUILD_ENABLE_ASSERT

// Allow assertions to be on by default for debug builds and off o/w
#ifndef CMX_BUILD_ENABLE_ASSERT
#   ifdef CMX_BUILD_TYPE_DEBUG
#       define CMX_BUILD_ENABLE_ASSERT
#   else
#       ifndef NDEBUG
#           define NDEBUG
#       endif  // NDEBUG
#   endif  // CMX_BUILD_TYPE_DEBUG
#endif  // CMX_BUILD_ENABLE_ASSERT

/**
 * Performs extra string parsing checks to verify data imported properly.
 * These extra checks can be significantly slower and should only be enabled
 * if data integrity is of concern.
 */
#ifndef CMX_BUILD_ENABLE_ROBUST_PARSER_CHECKS
#   ifdef CMX_BUILD_ENABLE_ASSERT
//#       define CMX_BUILD_ENABLE_ROBUST_PARSER_CHECKS
#   endif // CMX_BUILD_ENABLE_ASSERT
#endif // CMX_BUILD_ENABLE_ROBUST_PARSER_CHECKS

#ifndef CMX_BUILD_ENABLE_BNET_CHECK_ORDER
#   ifdef CMX_BUILD_ENABLE_ASSERT
// Uncomment line below to enable expensive Bayesian network topology checks
//#       define CMX_BUILD_ENABLE_BNET_CHECK_ORDER
#   endif // CMX_BUILD_ENABLE_ASSERT
#endif // CMX_BUILD_ENABLE_BNET_CHECK_ORDER

/**
 * Enables logging utilities
 */
#ifndef CMX_BUILD_ENABLE_LOGGING
// Comment out line below to disable logging
#   define CMX_BUILD_ENABLE_LOGGING
#endif  // CMX_BUILD_ENABLE_LOGGING

/**
 * Switch to toggle stats profiling
 */
#ifndef CMX_BUILD_ENABLE_STATS
#   ifdef CMX_BUILD_TYPE_DEBUG
#       define CMX_BUILD_ENABLE_STATS
#   else
// Uncomment line below to enable stats in release
//#       define CMX_BUILD_ENABLE_STATS
#   endif  // CMX_BUILD_TYPE_DEBUG
#endif  // CMX_BUILD_ENABLE_STATS

/**
 * Interval logging allows use of timers which auto report
 * durations to console, hence dependence on logging
 */
#if !defined(CMX_BUILD_ENABLE_INTERVAL_LOGGING) && \
    defined(CMX_BUILD_ENABLE_LOGGING)
#   if defined(CMX_BUILD_ENABLE_STATS)
#       define CMX_BUILD_ENABLE_INTERVAL_LOGGING
#   else
// Uncomment line below to override enable even when stats are disabled
//#       define CMX_BUILD_ENABLE_INTERVAL_LOGGING
#   endif  // CMX_BUILD_ENABLE_STATS
#endif  // CMX_BUILD_ENABLE_INTERVAL_LOGGING

/**
 * This switch toggles if commandlets should be enabled
 */
#ifndef CMX_BUILD_ENABLE_COMMANDLETS
#   if defined(CMX_BUILD_TYPE_DEBUG)
#       define CMX_BUILD_ENABLE_COMMANDLETS
#   else
// Uncomment line below to enable commandlets in release
//#      define CMX_BUILD_ENABLE_COMMANDLETS
#   endif  // CMX_BUILD_TYPE_DEBUG
#endif  // CMX_BUILD_ENABLE_COMMANDLETS

/**
 * This switch toggles if tests should be enabled
 */
#ifndef CMX_BUILD_ENABLE_TESTS
#   if defined(CMX_BUILD_TYPE_DEBUG)
#       define CMX_BUILD_ENABLE_TESTS
#   else
// Uncomment line below to enable tests in release
//#       define CMX_BUILD_ENABLE_TESTS
#   endif  // CMX_BUILD_TYPE_DEBUG
#endif  // CMX_BUILD_ENABLE_TEST

/**
 * This switch toggles if armadillo library should be built with extra debug
 * information such as bounds checking.
 */
// Allow armadillo debug information for debug builds and off o/w
#ifndef CMX_BUILD_ARMADILLO_DEBUG
#   ifdef CMX_BUILD_TYPE_DEBUG
#       define CMX_BUILD_ARMADILLO_DEBUG
#   else
#       define ARMA_NO_DEBUG
#   endif  // CMX_BUILD_TYPE_DEBUG
#endif  // CMX_BUILD_ARMADILLO_DEBUG

/**
 * Switch to toggle seeding the random number generator
 */
#ifndef CMX_BUILD_SEED_RAND
#   if !defined(CMX_BUILD_TYPE_DEBUG)
#       define CMX_BUILD_SEED_RAND
#   endif
#endif  // CMX_BUILD_SEED_RAND

/**
 * Allow threaded utilities
 */
#ifndef CMX_BUILD_ENABLE_THREADS
#   ifdef CMX_BUILD_COMPILER_MSVC
// (Un) comment below to toggle usage of threading in visual studio
//#       define CMX_BUILD_ENABLE_THREADS
#   else
// Note, for Linux builds, we typically prefer the CMake build system to
// configure this flag; therefore, it should be undefined by default.
// However, to override build system behavior, simply enable it here.
//#       define CMX_BUILD_ENABLE_THREADS
#   endif  // CMX_BUILD_COMPILER_MSVC
#endif  // CMX_BUILD_ENABLE_THREADS

#ifndef CMX_BUILD_ENABLE_BOOST_MEM_ALIGN
// (Un) comment below to toggle usage of Boost for aligned allocations
//#   define CMX_BUILD_ENABLE_BOOST_MEM_ALIGN
#endif  // CMX_BUILD_ENABLE_BOOST_MEM_ALIGN

/**
 * Switch controls whether to track acceptance probability for Metropolis
 *  [-Hastings] model components. Can be used to assess how well sampler is
 *  exploring state space
 */
#ifndef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
#   ifdef CMX_BUILD_TYPE_DEBUG
// (Un) comment below to toggle MCMC acceptance tracking in debug build
#       define CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
#   else
// Switch to toggle acceptance tracking for non-debug builds
//#       define CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
#   endif // CMX_BUILD_TYPE_DEBUG
#endif // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

/**
 * Default (shape, scale) for Gamma prior on Dirichlet process alpha from:
 *
 *  Sudderth, Erik Blaine. "Graphical models for visual object recognition and
 *      tracking." PhD diss., Massachusetts Institute of Technology, 2006.
 *
 * (Section 2.5, pg. 109) - assuming text gives Gamma scale instead of rate
 */

/**
 * Default shape term for Gamma(shape, scale) prior on Dirichlet process alpha
 */
#define CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SHAPE CMX_TO_REAL(0.2)

/**
 * Default scale term for Gamma(shape, scale) prior on Dirichlet process alpha
 */
#define CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SCALE CMX_TO_REAL(0.1)

/**
 * Default Dirichlet process alpha concentration parameter, monotonic to the
 * the expected number of clusters. Further, we set it be the expected value
 * assuming a Gamma prior = shape * scale
 */
#define CMX_DEFAULT_DP_STICK_ALPHA              \
    ((CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SHAPE) * \
     (CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SCALE))

/**
 * Dirichlet distributions are parameterized by a vector of pseudo-counts;
 * however, each individual pseudo-count, 'alpha', must be positive. To
 * handle cases where a computed alpha is <= 0, we simply fall back to a small
 * but numerically stable constant
 */
#define CMX_DIRICHLET_MIN_ALPHA CMX_TO_REAL(0.00001)

/**
 * Default value of hyper-parameter for prior on Bayesian network structure
 */
#define CMX_DEFAULT_BNET_DAG_KAPPA CMX_TO_REAL(0.9)

/**
 * Default value of hyper-parameter for prior on Bayesian network CPTs
 */
#define CMX_DEFAULT_BNET_CPT_ALPHA CMX_TO_REAL(1.0)

/**
 * Default block size for multi-population Bayesian network MCMC proposal
 */
#define CMX_DEFAULT_BNET_MPOP_BLOCK_SIZE CMX_TO_UINT(5)

/**
 * Default multi-population Bayesian network weighted blend where:
 *  0 -> uniform selection
 *  1 -> weighted selection
 *  (0,1) -> anything in-between is a linear blend of both approaches
 */
#define CMX_DEFAULT_BNET_MPOP_BLOCK_WEIGHTED_BLEND CMX_TO_REAL(0.5)

/**
 * Default number of models to run
 */
#define CMX_DEFAULT_NUM_MODELS CMX_TO_UINT(5)

/**
 * Prefix to denote command line arguments meant for child configurations
 */
#define CMX_CONFIG_CHILD_CMDARG_PREFIX "."

/**
 * Default number of mixture components. Value based on truncated Dirichlet
 * process as described in:
 *
 *  Ishwaran, Hemant, and Mahmoud Zarepour. "Markov chain Monte Carlo in
 *  approximate Dirichlet and beta two-parameter process hierarchical
 *  models." Biometrika 87, no. 2 (2000): 371-390.
 *
 * which states that for most DP processes, alpha is in (0,10] and hence a
 * truncation size of 50 produces negligible approximation error.
 */
#define CMX_DEFAULT_MIX_SIZE CMX_TO_UINT(50)

/**
 * Default pseudo-population size.
 */
#define CMX_DEFAULT_PSEUDOPOP_SIZE CMX_TO_UINT(1000)

/**
 * The default block size used for sampling pseudo-population. The block size
 *  is random sub-population size selected at each MCMC proposal
 */
#define CMX_DEFAULT_PSEUDOPOP_BLOCK_SIZE CMX_TO_UINT(5)

/**
 * Default start number of balls in pseudo-population's Polya urn sampler
 */
#define CMX_DEFAULT_PSEUDOPOP_POLYA_ALPHA CMX_TO_REAL(1.0)

/**
 * Default number of balls added to pseudo-population's Polya urn sampler
 */
#define CMX_DEFAULT_PSEUDOPOP_POLYA_DELTA CMX_TO_REAL(0.0)

/**
 * Default cutoff for deferring to Polya urn proposal under Bpoly mode
 */
#define CMX_DEFAULT_PSEUDOPOP_BPOLY_CORR_THRESH CMX_TO_REAL(0.95)

/**
 * Default alpha hyper-parameter for empirical Hi-C likelihood
 */
#define CMX_DEFAULT_CONTACT_FREQS_ALPHA CMX_TO_REAL(3.0)

/**
 * Default scale factor associated with the infeasible (negative) corpus. A
 * positive value penalizes the posterior score proportional to the log
 * probability mass associated to each infeasible configuration.
 */
#define CMX_DEFAULT_BNET_INFEAS_SCALE CMX_TO_REAL(0.5)

/**
 * Default ceiling factor associated with the infeasible (negative) corpus.
 * Defines the maximum reward that can be granted for assigning low
 * probability mass to the infeasible (negative) corpus.
 */
#define CMX_DEFAULT_BNET_INFEAS_CEIL CMX_TO_REAL(100.0)

#endif  // cmxBuild_h
