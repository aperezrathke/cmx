//****************************************************************************
// cmxSelectorTower.h
//****************************************************************************

/**
 * @brief - Samples a vector element according to the discrete probability
 *  distribution defined by the vector
 */

#ifndef cmxSelectorTower_h
#define cmxSelectorTower_h

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxRand.h"
#include "cmxTypes.h"

/**
 * Basic idea:
 *  1) Compute CDF of empirical distribution
 *  2) Generate a uniform random variable U in [0,1)
 *  3) Select first element with CDF <= U
 */
class cmxSelectorTower {
public:
    /**
     * @param p_select - a vector of probabilities - must sum to 1.0 and must
     *  have at least 1 element
     * @param rng - handle to random number generator
     * @return an index [0, size-1] where each index i has probability
     *  p_select[i] of being chosen
     */
    static inline cmxUInt rand_select_index(const cmxVecCol& p_select,
                                            cmxRand& rng) {
        const cmxUInt n_choices = p_select.n_elem;
        cmxAssertRealEq(cmxMatrixUtils::sum(p_select), CMX_TO_REAL(1.0));
        cmxAssert(n_choices > 0);

        // Uniformly select a real number in [0, 1)
        const cmxReal r_choice = rng.unif_real();
        cmxAssert(r_choice <= cmxMatrixUtils::sum(p_select));

        // Used to identify which bucket r_choice falls within
        cmxReal p_sum = CMX_TO_REAL(0.0);

        // Determine which index to select
        for (cmxUInt i = 0; i < (n_choices - 1); ++i) {
            p_sum += p_select.at(i);
            if (r_choice <= p_sum) {
                // Element found, return it
                return i;
            }
        }

        // Else, last element is chosen
        cmxAssert(r_choice <= (p_sum + p_select.at(n_choices - 1)));
        return (n_choices - 1);
    }

    /**
     * @param p_select - a vector of possibly unnormalized probabilities - must
     *  sum to 'z' and must have at least 1 element
     * @param z - the normalizing constant = sum(p_select)
     * @param rng - handle to random number generator
     * @return an index [0, size-1] where each index i has probability
     *  p_select[i] / z of being chosen
     */
    static inline cmxUInt rand_select_index(const cmxVecCol& p_select,
                                            const cmxReal z,
                                            cmxRand& rng) {
        const cmxUInt n_choices = p_select.n_elem;
        cmxAssertRealEq(cmxMatrixUtils::sum(p_select), z);
        cmxAssert(n_choices > 0);

        // Uniformly select a real number in [0, z)
        const cmxReal r_choice = rng.unif_real(0, z);
        cmxAssert(r_choice <= z);

        // Used to identify which bucket r_choice falls within
        cmxReal p_sum = CMX_TO_REAL(0.0);

        // Determine which index to select
        for (cmxUInt i = 0; i < (n_choices - 1); ++i) {
            p_sum += p_select.at(i);
            if (r_choice <= p_sum) {
                // Element found, return it
                return i;
            }
        }

        // Else, last element is chosen
        cmxAssert(r_choice <= (p_sum + p_select.at(n_choices - 1)));
        return (n_choices - 1);
    }
};

#endif  // cmxSelectorTower_h
