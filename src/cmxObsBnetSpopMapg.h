//****************************************************************************
// cmxObsBnetSpopMapg.h
//****************************************************************************

/**
 * @brief Records global maximum a posteriori (MAP) estimate for single-
 *  population Bayesian network model
 */

#ifndef cmxObsBnetSpopMapg_h
#define cmxObsBnetSpopMapg_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetBool.h"
#include "cmxExitCodes.h"
#include "cmxExportBnet.h"
#include "cmxExportCore.h"
#include "cmxFilesystem.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObs.h"
#include "cmxObsBnetSpopSnap.h"
#include "cmxObsLogscoreMapg.h"
#include "cmxSmartPtr.h"

#include <string>

//****************************************************************************
// cmxObsBnetSpopMapg
//****************************************************************************

/**
 * Global MAP observer - captures global (across all MCMC chains) maximum a
 *  posteriori estimate of observed parameter
 */
template <typename t_Glue>
class cmxObsBnetSpopMapg : public cmxObsLogscoreMapg<t_Glue>::map_reactor_t {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Snapshot observer type
     */
    typedef cmxObsBnetSpopSnap<t_Glue> obs_bnet_spop_snap_t;

    /**
     * Default constructor
     */
    cmxObsBnetSpopMapg() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within reactors array
     */
    virtual void init_react(cmxSpConstConfig_t config,
                            sim_t& sim,
                            const size_t index) {
        cmxAssert(sim.get_shared_ro().get_Clust().get_mix_size() ==
                  CMX_TO_UINT(1));
        // Reset
        this->clear();
        // Extract snapshot observer
        if (!obs_t::find_raw(&mp_snap, sim)) {
            cmxLogf(
                "Error: Bayesian network (single-population) global MAP "
                "observer unable to find handle to snapshot observer. "
                "Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
        // Determine export prefix
        m_fprefix = cmxExport::get_fpath(
            config, "" /*subdir*/, "map.bnet" /*file_id*/, "" /*ext*/);
    }

    /**
     * Reset to default state
     */
    void clear() {
        mp_snap = NULL;
        m_map = cmxBnetBool();
    }

    /**
     * Reactor update managed my logscore MAP observer, when a new MAP
     * estimate is encountered, this method will be called
     * @param iter - Typically post burn-in iteration number (0-based)
     * @param sim - Outer simulation
     * @param i_model - Index of MAP model
     * @param logscore - new MAP logscore associated to MAP model
     */
    virtual void update_react(const cmxUInt iter,
                              sim_t& sim,
                              const size_t i_model,
                              const cmxReal logscore) {
        cmxAssert(mp_snap);
        if (mp_snap->is_stale(iter)) {
            // Snapshot is stale, capture new snapshot
            mp_snap->force_update(iter, sim);
        }
        // Update MAP estimate
        cmxAssertBounds(i_model, 0, mp_snap->get_num_bnets());
        m_map = mp_snap->get_bnets()[i_model];
    }

    /**
     * Callback for when simulation has finished running
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        // @WARNING, using Bnet order as proxy for network validity
        if (m_fprefix.empty() || m_map.get_order().empty()) {
            // Avoid export if not configured or network is invalid
            return;
        }

        // Export src2dst CSV
        std::string fpath = m_fprefix + ".src2dst.csv";
        cmxLogf("Exporting global MAP estimate: %s\n", fpath.c_str());
        cmxExport::write_bnet_dag_adjmap_csv(m_map.get_dag().get_src2dst_map(),
                                             fpath);

        // Export src2dst Graphviz
        fpath = m_fprefix + ".src2dst.gv";
        cmxLogf("Exporting global MAP estimate: %s\n", fpath.c_str());
        cmxExport::write_bnet_dag_adjmap_gv(m_map.get_dag().get_src2dst_map(),
                                            fpath);

        // Export dst2src
        fpath = m_fprefix + ".dst2src.csv";
        cmxLogf("Exporting global MAP estimate: %s\n", fpath.c_str());
        cmxExport::write_bnet_dag_adjmap_csv(m_map.get_dag().get_dst2src_map(),
                                             fpath);

        // Export order
        fpath = m_fprefix + ".order.csv";
        cmxLogf("Exporting global MAP estimate: %s\n", fpath.c_str());
        cmxExport::write_bnet_order_csv(m_map.get_order(), fpath);

        // Export CPTs
        fpath = m_fprefix + ".cpt.csv";
        cmxLogf("Exporting global MAP estimate: %s\n", fpath.c_str());
        cmxExport::write_bnet_bool_cpt_csv(m_map.get_cpts(), fpath);
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_bnet_mapg)

private:
    /**
     * Handle to snapshot observer - raw pointer is used as lifetime of
     * pointer does not exceed that of parent simulation's shared_ptr
     */
    obs_bnet_spop_snap_t* mp_snap;

    /**
     * MAP estimate export path prefix
     */
    std::string m_fprefix;

    /**
     * MAP (maximum a posteriori) estimate
     */
    cmxBnetBool m_map;
};

#endif  // cmxObsBnetSpopMapg_h
