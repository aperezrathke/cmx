//****************************************************************************
// cmxHashMap.h
//****************************************************************************

#ifndef cmxHashMap_h
#define cmxHashMap_h

/**
 * @brief An associative array implemented as a hash table
 */

#include "cmxBuild.h"
#include "cmxTypes.h"

/**
 * Select between standard vs boost implementations
 */
#ifdef CMX_BUILD_CXX_11
#   include <unordered_map>
#   define CMX_HASH_MAP std::unordered_map
#else
#   include <boost/unordered_map.hpp>
#   define CMX_HASH_MAP boost::unordered_map
#endif  // CMX_BUILD_CXX_11

/**
 * [May] pre-allocate storage for 'n' elements
 */
#define CMX_HASH_RESERVE(hash_, n_) (hash_).reserve(n_)

/**
 * Hash map from key = unsigned integer to value = unsigned integer
 */
typedef CMX_HASH_MAP<cmxUInt, cmxUInt> cmxUIntToUIntMap;

#endif  // cmxHashMap_h
