//****************************************************************************
// cmxBnetThinDag.h
//****************************************************************************

/**
 * @brief Class for maintaining a "thin" directed acyclic graph (DAG)
 *  representation. Makes assumptions about how edges can be added and
 *  removed and therefore may not be suitable for general purpose use.
 */

#ifndef cmxBnetThinDag_h
#define cmxBnetThinDag_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetDagTypes.h"
#include "cmxBnetThinEdgeRemoval.h"
#include "cmxStats.h"
#include "cmxTypes.h"

#include <vector>

#ifdef CMX_BUILD_ENABLE_ASSERT
// Needed for asserting an added edge doesn't already exist
#include <algorithm>
#endif  // CMX_BUILD_ENABLE_ASSERT

//****************************************************************************
// cmxBnetThinDag
//****************************************************************************

/**
 * Maintains adjacency list, stores the directed edges. This is called a
 * "thin" representation because of the following assumptions of the client:
 *  - no self edges will ever be proposed for addition
 *  - no already existing edge will ever be proposed for addition
 *  - no non-existing edge will ever be proposed for removal
 *  - edge addition will not create a cycle - see can_add_edge()
 */
class cmxBnetThinDag {
public:
    // Typedefs

    /**
     * List of vertex identifiers
     */
    typedef cmxBnetDagVxList_t VxList_t;

    /**
     * Represents list of edges for each vertex
     */
    typedef cmxBnetDagAdjMap_t AdjMap_t;

    // Topology

    /**
     * Test if edge can be added to DAG without creating a cycle.
     * Implementation based on exercise 22.4-5 of:
     *  Cormen, Thomas H. Introduction to algorithms. MIT press, 2009.
     * @WARNING: ASSUMES EDGE (v_src, v_dst) DOES NOT ALREADY EXIST!
     * @WARNING: ASSUMES v_src != v_dst
     * @WARNING: ASSUMES total number of vertices is greater than 1
     * @param v_src - source vertex identifier
     * @param v_dst - destination vertex identifier
     * @param in_deg_buf - scratch buffer for storing in-degrees at each
     *  source vertex (reserve to size N where N is number of vertices)
     * @param stack_buf - scratch buffer used during cycle checking (reserve
     *  to size N where N is number of vertices)
     * @return cmxTRUE only if edge (v_src, v_dst) does not produce a cycle,
     *  o/w cmxFALSE
     */
    inline cmxBool can_add_edge(const cmxUInt v_src,
                                const cmxUInt v_dst,
                                std::vector<cmxUInt>& in_deg_buf,
                                std::vector<cmxUInt>& stack_buf) const {
        CMX_SCOPED_STAT_TIMER(cmxSTAT_BnetThinDagCycleCheckTime);

        cmxAssertPosEq(m_src2dst_map.size(), m_dst2src_map.size());
        cmxAssert(!internal_edge_exists(v_src, v_dst));
        cmxAssert(v_src != v_dst);

        // Number of vertices
        const cmxUInt v_num = this->get_v_num();
        cmxAssert(v_num > 1);

        ////////////////////////////////////////////////////////////
        // Initialize the in-degrees at each vertex

        in_deg_buf.clear();
        // i: identifier for source vertex
        cmxUInt i = 0;
        for (; i < v_num; ++i) {
            cmxAssert(CMX_TO_UINT(m_dst2src_map[i].size()) <= v_num);
            // The in-degree is the same as the number of parents of a child
            in_deg_buf.push_back(CMX_TO_UINT(m_dst2src_map[i].size()));
        }
        // Account for additional parent at destination vertex
        ++in_deg_buf[v_dst];

        ////////////////////////////////////////////////////////////
        // Initialize search stack for nodes with 0 in-degree

        stack_buf.clear();
        for (i = 0; i < v_num; ++i) {
            if (CMX_TO_UINT(0) == in_deg_buf[i]) {
                stack_buf.push_back(i);
            }
        }

        // Count emitted nodes, if this matches v_num, then we have no cycles!
        cmxUInt v_emitted = 0;

        ////////////////////////////////////////////////////////////
        // Pre-phase prior to accounting for candidate edge

        // v_iter: iterator over source vertex's (i) destination list
        // j: identifier for destination vertex
        cmxUInt v_iter, j;
        while (stack_buf.size()) {
            i = stack_buf.back();
            stack_buf.pop_back();
            ++v_emitted;

            // Decrement in-degrees of all destination vertices
            const cmxUInt v_dst_num = CMX_TO_UINT(m_src2dst_map[i].size());
            for (v_iter = 0; v_iter < v_dst_num; ++v_iter) {
                j = m_src2dst_map[i][v_iter];
                cmxAssert(internal_edge_exists(i, j));
                cmxAssert(in_deg_buf[j] > 0);
                if (CMX_TO_UINT(0) == (--in_deg_buf[j])) {
                    stack_buf.push_back(j);
                }
            }
        }

        ////////////////////////////////////////////////////////////
        // Account for candidate edge

        cmxAssert(in_deg_buf[v_dst] > 0);
        if (CMX_TO_UINT(0) == in_deg_buf[v_src]) {
            if (CMX_TO_UINT(0) == (--in_deg_buf[v_dst])) {
                stack_buf.push_back(v_dst);
            }
        }

        ////////////////////////////////////////////////////////////
        // Post-phase after accounting for candidate edge

        while (stack_buf.size()) {
            i = stack_buf.back();
            stack_buf.pop_back();
            ++v_emitted;

            // Decrement in-degrees of all destination vertices
            const cmxUInt v_dst_num = CMX_TO_UINT(m_src2dst_map[i].size());
            for (v_iter = 0; v_iter < v_dst_num; ++v_iter) {
                j = m_src2dst_map[i][v_iter];
                cmxAssert(internal_edge_exists(i, j));
                if (CMX_TO_UINT(0) == (--in_deg_buf[j])) {
                    stack_buf.push_back(j);
                }
            }
        }

        cmxAssert(v_emitted <= v_num);
        return v_emitted == v_num;
    }

    /**
     * DAG topological sort - an ordering of the vertices such that parents
     * are listed before child vertices.
     * Implementation based on exercise 22.4-5 of:
     *  Cormen, Thomas H. Introduction to algorithms. MIT press, 2009.
     * @WARNING: ASSUMES NO CYCLES EXIST (GRAPH IS A DAG), if this is not the
     *  case, the output vertex list will be less than the total # of vertices
     * @param out_order - output buffer containing vertex identifiers in a
     *  topological order such that all parent vertices are listed before
     *  child vertices. (reserve to size N where N is number of vertices)
     * @param in_deg_buf - scratch buffer for storing in-degrees at each
     *  source vertex (reserve to size N where N is number of vertices)
     * @param stack_buf - scratch buffer used during cycle checking (reserve
     *  to size N where N is number of vertices)
     */
    inline void topo_sort(std::vector<cmxUInt>& out_order,
                          std::vector<cmxUInt>& in_deg_buf,
                          std::vector<cmxUInt>& stack_buf) const {
        CMX_SCOPED_STAT_TIMER(cmxSTAT_BnetThinDagTopoSortTime);

        // Breadth-first search (BFS) and depth-first search (DFS) can provide
        // first-in-first-out (FIFO) and last-in-first-out (LIFO) topological
        // orderings respectively. This algorithm uses a stack and therefore
        // is a DFS/LIFO ordering. If a BFS/FIFO order is desired, consider
        // instead using a std::dequeue or a boost::circular_buffer for
        // efficient push_back() and pop_front() functionality.

        cmxAssertPosEq(m_src2dst_map.size(), m_dst2src_map.size());

        // Number of vertices
        const cmxUInt v_num = this->get_v_num();

        ////////////////////////////////////////////////////////////
        // Initialize the in-degrees at each vertex

        in_deg_buf.clear();
        // i: identifier for source vertex
        cmxUInt i = 0;
        for (; i < v_num; ++i) {
            cmxAssert(CMX_TO_UINT(m_dst2src_map[i].size()) <= v_num);
            // The in-degree is the same as the number of parents of a child
            in_deg_buf.push_back(CMX_TO_UINT(m_dst2src_map[i].size()));
        }

        ////////////////////////////////////////////////////////////
        // Initialize search stack for nodes with 0 in-degree

        stack_buf.clear();
        for (i = 0; i < v_num; ++i) {
            if (CMX_TO_UINT(0) == in_deg_buf[i]) {
                stack_buf.push_back(i);
            }
        }

        ////////////////////////////////////////////////////////////
        // Order vertices such that parents < children

        out_order.clear();
        // v_iter: iterator over source vertex's (i) destination list
        // j: identifier for destination vertex
        cmxUInt v_iter, j;
        while (stack_buf.size()) {
            i = stack_buf.back();
            stack_buf.pop_back();
            out_order.push_back(i);

            // Decrement in-degrees of all destination vertices
            const cmxUInt v_dst_num = CMX_TO_UINT(m_src2dst_map[i].size());
            for (v_iter = 0; v_iter < v_dst_num; ++v_iter) {
                j = m_src2dst_map[i][v_iter];
                cmxAssert(internal_edge_exists(i, j));
                cmxAssert(in_deg_buf[j] > 0);
                if (CMX_TO_UINT(0) == (--in_deg_buf[j])) {
                    stack_buf.push_back(j);
                }
            }
        }

        // If this assert trips, then graph is not a DAG!
        cmxAssert(out_order.size() == v_num);
    }

    // Edge manipulation

    // Note: querying if edge (v_src, v_dst) exists is O(V); therefore this
    // class does not expose an API for it. (See cmxBnetThinEdgeStatesMu)

    /**
     * NOTE INTERFACE MISMATCH BETWEEN add_edge(...) and remove_edge(...). The
     *  method "add_edge" takes arguments v_src and v_dst, whereas
     *  "remove_edge" takes v_src and additional info needed to remove the
     *  edge. Basically, method add_edge *returns* info which can be used to
     *  remove that added edge in O(1) time.
     *
     * Example usage:
     *
     *  // Add edge from vertex 'v_i' to vertex 'v_j'
     *  cmxBnetThinEdgeRemovalInfo_t eri = { 0 };
     *  add_edge(v_i, v_j, eri);
     *  // Remove edge from vertex 'v_i' to vertex 'v_j'
     *  cmxBnetThinEdgePostRemovalInfo_t epri = { 0 };
     *  if (remove_edge(v_i, eri, epri) needs post update)
     *      { // update info according to epri ... }
     */

    /**
     * Creates edge from vertex v_src to vertex v_dst
     * @WARNING: ASSUMES EDGE DOES NOT ALREADY EXIST!
     * @WARNING: DOES NOT CHECK FOR CYCLES! see can_add_edge(...)
     * @WARNING: ASSUMES v_src != v_dst
     * @param v_src - source vertex identifier
     * @param v_dst - destination vertex identifier
     * @param out_eri - additional data needed for future edge removal in O(1)
     */
    inline void add_edge(const cmxUInt v_src,
                         const cmxUInt v_dst,
                         cmxBnetThinEdgeRemovalInfo_t& out_eri) {
        cmxAssert(internal_can_add_edge(v_src, v_dst));
        // Update v_src's destination vertex list
        out_eri.v_dst_ix =
            internal_add_edge_update(v_src, v_dst, m_src2dst_map);
        // Update v_dst's source vertex list
        out_eri.v_src_ix =
            internal_add_edge_update(v_dst, v_src, m_dst2src_map);
    }

    /**
     * Removes edge entries both within v_src's destination vertex list and
     * v_dst's (inferred from eri param) source vertex list.
     * @WARNING: ASSUMES EDGE ACTUALLY EXISTS!
     * This cumbersome interface is to facilitate O(1) edge removal!
     * @param v_src - source vertex identifier
     * @param eri - edge removal info created by original add_edge(...) call
     * @param out_epri - edge post removal information. If a field is not
     *   needed, it will be set to CMX_BNET_EDGE_NA, else that field specifies
     *   how to update the edge removal information after edge removal.
     */
    inline void remove_edge(const cmxUInt v_src,
                            const cmxBnetThinEdgeRemovalInfo_t eri,
                            cmxBnetThinEdgePostRemovalInfo_t& out_epri) {
        // Update v_src's destination vertex list
        cmxUInt v_dst;
        out_epri.v_dst_swap = internal_remove_edge_update(
            v_src, eri.v_dst_ix, m_src2dst_map, v_dst);
        // Update v_dst's source vertex list
        cmxUInt v_unused;
        out_epri.v_src_swap = internal_remove_edge_update(
            v_dst, eri.v_src_ix, m_dst2src_map, v_unused);
        cmxAssert(v_unused == v_src);
    }

    // Vertex manipulation - can only add vertices!

    /**
     * Adds a single vertex to DAG with no incoming our outgoing edges
     * @return unique vertex identifier for newly added vertex
     */
    inline cmxUInt add_vertex() {
        cmxAssert(m_src2dst_map.size() == m_dst2src_map.size());
        m_src2dst_map.push_back(VxList_t());
        m_dst2src_map.push_back(VxList_t());
        return CMX_TO_UINT(m_src2dst_map.size() - 1);
    }

    /**
     * Adds multiple vertices to DAG, each with no incoming our outgoing
     * edges. The vertices will be sequentially assigned 'num' unique
     * contiguous identifiers in integral range [lo, hi]. Note, if DAG is
     * empty (no vertices), then 'lo' will be 0 and 'hi' will be (num-1).
     * @param num - the number of vertices to add to DAG
     * @param lo - the lowest vertex identifier among newly added vertices
     * @param hi - the highest vertex identifier among newly added vertices
     * @param should_clear - If cmxTRUE, all vertices AND edges will be erased
     *  prior to adding new set of vertices
     */
    inline void add_vertices(const cmxUInt num,
                             cmxUInt& lo,
                             cmxUInt& hi,
                             const cmxBool should_clear = cmxFALSE) {
        cmxAssert(num > 0);
        cmxAssert(m_src2dst_map.size() == m_dst2src_map.size());
        if (should_clear) {
            m_src2dst_map.clear();
            m_dst2src_map.clear();
        }
        lo = CMX_TO_UINT(m_src2dst_map.size());
        hi = lo + num - 1;
        m_src2dst_map.resize(hi + 1);
        m_dst2src_map.resize(hi + 1);
        cmxAssert(num <= CMX_TO_UINT(m_src2dst_map.size()));
    }

    // Read-only accessors

    /**
     * @return number of vertices in DAG
     */
    inline cmxUInt get_v_num() const {
        cmxAssert(m_src2dst_map.size() == m_dst2src_map.size());
        return CMX_TO_UINT(m_src2dst_map.size());
    }

    /**
     * @return mapping from source vertices to destination vertices
     */
    inline const AdjMap_t& get_src2dst_map() const { return m_src2dst_map; }

    /**
     * @return mapping from destination vertices to source vertices
     */
    inline const AdjMap_t& get_dst2src_map() const { return m_dst2src_map; }

    /**
     * @return const handle to adjacency maps
     */
    inline cmxBnetDagConstHandle_t get_chandle() const {
        return {m_src2dst_map, m_dst2src_map};
    }

private:
    /**
     * Updates adjacency map when adding an edge. This method is meant to be
     * called both on the source-to-destination map (parent-to-child) and the
     * destination-to-source map (child-to-parent).
     * @WARNING: ASSUMES EDGE DOES NOT ALREADY EXIST!
     * @WARNING: DOES NOT CHECK FOR CYCLES!
     * @WARNING: ASSUMES v_key != v_val
     * @param v_key - v_key's vertex list within adj will be updated
     * @param v_val - vertex entry to add to v_key's vertex list
     * @param adj - the adjacency mapping containing v_key's vertex list
     * @return index within v_key's vertex list at which v_val was entered
     *  (for future O(1) edge removal)
     */
    inline static cmxUInt internal_add_edge_update(const cmxUInt v_key,
                                                   const cmxUInt v_val,
                                                   AdjMap_t& adj) {
        cmxAssert(v_key != v_val);
        // Bounds checking
        cmxAssertBounds(v_key, CMX_TO_UINT(0), CMX_TO_UINT(adj.size()));
        cmxAssertBounds(v_val, CMX_TO_UINT(0), CMX_TO_UINT(adj.size()));
        VxList_t& v_list = adj[v_key];
        // v_val must not already exist in v_key's vertex list
        cmxAssert(!internal_v_list_contains(v_list, v_val));
        // Add entry to v_key's vertex list
        v_list.push_back(v_val);
        // Check size is still consistent
        cmxAssert(v_list.size() <= adj.size());
        cmxAssert(CMX_TO_UINT(v_list.size()) != CMX_BNET_EDGE_NA);
        // Return index of added entry
        return CMX_TO_UINT(v_list.size() - 1);
    }

    /**
     * Updates adjacency map when removing an edge. This method is meant to be
     * called both on the source-to-destination map (parent-to-child) and the
     * destination-to-source map (child-to-parent).
     * @WARNING: ASSUMES EDGE ALREADY EXISTS!
     * @param v_key - v_key's vertex list within adj will be updated
     * @param v_ix - index of vertex entry to remove from v_key's vertex list
     * @param adj - the adjacency mapping containing v_key's vertex list
     * @param v_val_removed - the value removed from v_key's vertex list
     * @return new value at v_ix location else CMX_BNET_EDGE_NA if v_key's
     *  vertex list is now empty
     */
    inline static cmxUInt internal_remove_edge_update(const cmxUInt v_key,
                                                      const cmxUInt v_ix,
                                                      AdjMap_t& adj,
                                                      cmxUInt& v_val_removed) {
        // Bounds checking
        cmxAssertBounds(v_key, CMX_TO_UINT(0), CMX_TO_UINT(adj.size()));
        VxList_t& v_list = adj[v_key];
        cmxAssertBounds(v_ix, CMX_TO_UINT(0), CMX_TO_UINT(v_list.size()));
        // Guard against using a reserved value
        cmxAssert(v_ix != CMX_BNET_EDGE_NA);
        v_val_removed = v_list[v_ix];
        cmxAssertBounds(v_val_removed, CMX_TO_UINT(0), CMX_TO_UINT(adj.size()));
        v_list[v_ix] = v_list.back();
        v_list.pop_back();
        return (CMX_TO_UINT(v_list.size()) == v_ix) ? CMX_BNET_EDGE_NA
                                                    : v_list[v_ix];
    }

#ifdef CMX_BUILD_ENABLE_ASSERT

    /**
     * Also checks v_list has all unique elements
     * @return cmxTRUE if list has element, cmxFALSE o/w
     */
    inline static cmxBool internal_v_list_contains(const VxList_t& v_list,
                                                   const cmxUInt v_val) {
        cmxAssert(internal_v_list_unique(v_list));
        return v_list.cend() !=
               std::find(v_list.cbegin(), v_list.cend(), v_val);
    }

    /**
     * @return cmxTRUE if all list elements are unique, cmxFALSE o/w
     */
    inline static cmxBool internal_v_list_unique(const VxList_t& v_list) {
        // https://stackoverflow.com/questions/2769174/determining-if-an-unordered-vectort-has-all-unique-elements
        VxList_t tmp = v_list;
        std::sort(tmp.begin(), tmp.end());
        return (tmp.cend() == std::adjacent_find(tmp.cbegin(), tmp.cend()));
    }

    /**
     * @return cmxTRUE if edge state is valid, cmxFALSE o/w
     */
    inline cmxBool internal_edge_exists(const cmxUInt v_src,
                                        const cmxUInt v_dst) const {
        cmxAssert(v_src != v_dst);
        cmxAssertPosEq(m_src2dst_map.size(), m_dst2src_map.size());
        cmxAssertBounds(v_src, 0, CMX_TO_UINT(m_src2dst_map.size()));
        cmxAssertBounds(v_dst, 0, CMX_TO_UINT(m_dst2src_map.size()));
        const cmxBool is_okay_src2dst =
            internal_v_list_contains(m_src2dst_map[v_src], v_dst);
        const cmxBool is_okay_dst2src =
            internal_v_list_contains(m_dst2src_map[v_dst], v_src);
        cmxAssert(is_okay_src2dst == is_okay_dst2src);
        return is_okay_src2dst && is_okay_dst2src;
    }

    /**
     * @return cmxTRUE if DAG is will remain acyclic, cmxFALSE o/w
     */
    inline cmxBool internal_can_add_edge(const cmxUInt v_src,
                                         const cmxUInt v_dst) const {
        std::vector<cmxUInt> in_deg_buf;
        in_deg_buf.reserve(this->get_v_num());
        std::vector<cmxUInt> stack_buf;
        stack_buf.reserve(this->get_v_num());
        return this->can_add_edge(v_src, v_dst, in_deg_buf, stack_buf);
    }

#endif  // CMX_BUILD_ENABLE_ASSERT

    // @WARNING: THESE MAPS ARE NOT TOPOLOGICALLY SORTED

    /**
     * Mapping from a source vertex to its set of destination vertices. This
     * could also be understood as mapping parent vertices to their set of
     * child vertices. Can be used to determine vertex out-degrees.
     */
    AdjMap_t m_src2dst_map;

    /**
     * Mapping from a destination vertex to its set of source vertices. This
     * could also be understood as mapping child vertices to their set of
     * parent vertices. Can be used to determine vertex in-degrees.
     */
    AdjMap_t m_dst2src_map;
};

#endif  // cmxBnetThinDag_h
