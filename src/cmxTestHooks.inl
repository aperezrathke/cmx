//****************************************************************************
// cmxTestHooks.inl
//****************************************************************************

/**
 * General work flow for adding a test commandlet:
 * 1) Create a new commandlet header file cmxTest<name>.h
 * 2) Within test commandlet header file, follow header guard and build flags
 *      policy as in other test commandlets:
 *      - throw error if header included multiple times
 *      - wrap code in #ifdef CMX_BUILD_ENABLE_TESTS
 * 3) Within test commandlet header file, wrap utility methods and class in
 *      anonymous namespace
 * 4) Within test commandlet header file, define cmxTest<name>Main(opts)
 *      method (outside of anonymous namespace)
 * 5) In cmxOpts.h, within the test commandlet options demarcated by build flag,
 *      add cmxOpt_test_<name> to the set of enums
 * 6) In cmxOpts.cpp, in the section of CMX_OPT_INFOS demarcated by build flag,
 *      add line DECLARE_CMD_UNARY_OPT("test_<name>", "<description>").
 *      - make sure this is in same order as enum structure in cmxOpts.h
 * 7.) In cmxTestHooks.inl, include test commandlet header file
 * 8.) In cmxTestHooks.inl, add to the GAppInfo structure the test commandlet
 *      details
 */

// Not meant to be included more than once
#ifdef cmxTestHooks_inl
#   error "Test Hooks included multiple times!"
#endif // cmxTestHooks_inl
#define cmxTestHooks_inl

// Implementation of test commandlet hooks extracted to separate file to avoid
// cluttering cmxMain.cpp

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_TESTS

#include "cmxAppHooks.h"
#include "cmxExitCodes.h"

// Include additional tests here
#include "cmxTestBnetThinDag.h"
#include "cmxTestDirichletDistribution.h"
#include "cmxTestDpStick.h"
#include "cmxTestGibbsMpop.h"
#include "cmxTestGibbsSpop.h"
#include "cmxTestGlobalInit.h"
#include "cmxTestHash.h"
#ifdef CMX_BUILD_ENABLE_THREADS
#   include "cmxTestParallelMapper.h"
#endif // CMX_BUILD_ENABLE_THREADS
#include "cmxTestRng.h"

// Register test commandlets here
static struct cmxAppInfo GTests[] =
{
      { cmxOpt_get_cmd_switch(cmxOpt_test_bnet_thin_dag), &cmxTestBnetThinDagMain }
    , { cmxOpt_get_cmd_switch(cmxOpt_test_dir_distrib), &cmxTestDirichletDistributionMain }
    , { cmxOpt_get_cmd_switch(cmxOpt_test_dp_stick), &cmxTestDpStickMain }
    , { cmxOpt_get_cmd_switch(cmxOpt_test_gibbs_mpop), &cmxTestGibbsMpopMain }
    , { cmxOpt_get_cmd_switch(cmxOpt_test_gibbs_spop), &cmxTestGibbsSpopMain }
    , { cmxOpt_get_cmd_switch(cmxOpt_test_global_init), &cmxTestGlobalInitMain }
    , { cmxOpt_get_cmd_switch(cmxOpt_test_hash), &cmxTestHashMain }
    , { cmxOpt_get_cmd_switch(cmxOpt_test_rng), &cmxTestRngMain }
#ifdef CMX_BUILD_ENABLE_THREADS
    , { cmxOpt_get_cmd_switch(cmxOpt_test_parallel_mapper), &cmxTestParallelMapperMain }
#endif // CMX_BUILD_ENABLE_THREADS
};

// Tests are enabled, route to the proper hook:
// Parse command line and run any tests specified
#define CMX_CONDITIONAL_RUN_TESTS( cmd_opts ) CMX_CONDITIONAL_RUN_APPS( cmd_opts, GTests )

#else

// Tests are not enabled, strip away call to hook
#define CMX_CONDITIONAL_RUN_TESTS( cmd_opts ) ((void)0)

#endif // CMX_BUILD_ENABLE_TESTS
