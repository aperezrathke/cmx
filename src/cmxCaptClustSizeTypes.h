//****************************************************************************
// cmxCaptClustSizeTypes.h
//****************************************************************************

/**
 * @brief Types used for capturing model cluster sizes
 */

#ifndef cmxCaptClustSizeTypes_h
#define cmxCaptClustSizeTypes_h

#include "cmxBuild.h"
#include "cmxTypes.h"

#include <queue>
#include <vector>

/**
 * POD for organizing clusters by size
 */
typedef struct {
    // Cluster size
    cmxUInt m_size;
    // Index of cluster
    cmxUInt m_index;
} cmxClustSizeInfo_t;

/**
 * Cluster comparison operation for organizing by size
 */
class cmxClustSizeOrg {
public:
    inline bool operator()(const cmxClustSizeInfo_t a,
                           const cmxClustSizeInfo_t b) {
        // Break size ties according to index
        return (a.m_size == b.m_size) ? (a.m_index > b.m_index)
                                      : (a.m_size < b.m_size);
    }
};

/**
 * Priority queue for organizing clusters by size
 */
typedef std::priority_queue<cmxClustSizeInfo_t,
                            std::vector<cmxClustSizeInfo_t>,
                            cmxClustSizeOrg>
    cmxClustSizePqueue_t;

/**
 * Utility to reset and fill priority queue of cluster sizes
 * @param pqueue - Priority queue to populate with non-empty cluster sizes
 * @param model - Parent model
 * @param sim - Outer simulation
 */
template <typename t_model, typename t_sim>
void cmx_clust_size_fill(cmxClustSizePqueue_t& pqueue,
                         const t_model& model,
                         const t_sim& sim) {
    // Determine number of mixture components
    const cmxUInt MIX_SIZE = sim.get_shared_ro().get_Clust().get_mix_size();
    cmxAssert(MIX_SIZE > CMX_TO_UINT(1));
    // Initialize priority queue
    pqueue = cmxClustSizePqueue_t();
    // Iterate over mixture components
    for (cmxUInt i_mix = 0; i_mix < MIX_SIZE; ++i_mix) {
        const cmxUInt pseu_size =
            model.get_Clust().get_pseudopop_size(i_mix, sim);
        // Only retain non-empty clusters
        if (pseu_size > CMX_TO_UINT(0)) {
            cmxClustSizeInfo_t nfo;
            nfo.m_size = pseu_size;
            nfo.m_index = i_mix;
            pqueue.push(nfo);
        }
    }  // End iteration over mixture components
}

#endif  // cmxCaptClustSizeTypes_h
