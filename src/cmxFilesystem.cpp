//****************************************************************************
// cmxFilesystem.cpp
//****************************************************************************

#include "cmxBuild.h"
#include "cmxFilesystem.h"

#include <boost/version.hpp>

/**
 * Creates any directories needed for parameter path
 * @param dir_path - path to directory
 * @return result of cmxFs::create_directories(...) for dir_path
 */
bool cmxFs_create_dirs(const cmxFs::path& dir_path) {
    return cmxFs::create_directories(dir_path);
}

/**
 * Creates any parent directories needed for parameter file path
 * @param file_path - path to file
 * @return result of cmxFs::create_directories(...) for parent directories
 */
bool cmxFs_create_parent_dirs(const cmxFs::path& file_path) {
    const cmxFs::path outdir = cmxFs::absolute(file_path.parent_path());
    return cmxFs::create_directories(outdir);
}

/**
 * Creates lexically normal (removes extra path separators and dots) directory
 * path without trailing path separator slash(es)
 * @param dir_path - directory path to normalize
 */
void cmxFs_normalize_dir_path(cmxFs::path& dir_path) {
    // @HACK - append non-existing file to path so that we may later resolve
    // normalized directory path using parent_path(). See discussion :
    // https://stackoverflow.com/questions/36941934/parent-path-with-or-without-trailing-slash/50493087#50493087
    dir_path /= "FILE.TXT";
    // Remove unneeded dots and slashes
#if BOOST_VERSION >= 106000
    dir_path = dir_path.lexically_normal();
#else
    // @HACK - THIS IS A DEPRECATED FUNCTION INCLUDED BY DEFAULT. NEEDED FOR
    // HELPING RESOLVE DOTS IN PATH STRING
    dir_path.normalize();
#endif  // BOOST_VERSION >= 106000
    // Remove trailing slash from original path!
    dir_path = dir_path.parent_path();
}

/**
 * Overloaded version to work with string argument
 */
void cmxFs_normalize_dir_path(std::string& dir_path) {
    cmxFs::path dir_path_(dir_path);
    cmxFs_normalize_dir_path(dir_path_);
    dir_path = dir_path_.string();
}
