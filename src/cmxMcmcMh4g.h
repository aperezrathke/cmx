//****************************************************************************
// cmxMcmcMh4g.h
//****************************************************************************

/**
 * @brief Metropolis-Hastings for use within Gibbs sampler (Mh4g). Idea is to
 *  have a relatively simple Markov-chain Monte Carlo sampler as it's meant
 *  to be included within a parent Gibbs sampler.
 */

#ifndef cmxMcmcMh4g_h
#define cmxMcmcMh4g_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxGlobals.h"
#include "cmxRand.h"
#include "cmxStaticAssert.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <limits>
#include <math.h>

//****************************************************************************
// cmxMcmcMh4g
//****************************************************************************

/**
 * Metropolis-Hasting for Gibbs (Mh4g). A lightweight MCMC sampler meant to be
 * embedded within a parent Gibbs sampler.
 *
 * The templated model type must support interface:
 *  t_model::Mh_prime_cand(): prime candidate sample
 *  t_model::Mh_get_logp_tar_cand(): log probability of candidate under the
 *      target distribution
 *  t_model::Mh_get_logp_tar_curr(): log probability of incumbent (i.e.
 *      current) sample under the target distribution
 *  t_model::Mh_get_logp_pro_curr_giv_cand(): log probability of proposing
 *      incumbent sample given the candidate sample
 *  t_model::Mh_get_logp_pro_cand_giv_curr(): log probability of proposing
 *      candidate sample given the incumbent sample
 *  t_model::Mh_on_accept_cand(): callback for when candidate move was
 *      accepted and model should update its internal state accordingly.
 *  t_model::Mh_on_reject_cand(): callback for when candidate move was
 *      rejected - allows model to tabulate statistics such as mixing rate
 *
 *  t_model::Mh_args_t - Any model-specific arguments to pass to the
 *      t_model::Mh*() interface (passed by value!)
 */
class cmxMcmcMh4g {
    /**
     * Sampler can theoretically generate log(0) = -inf. Want to make sure
     * that code supports -inf as a number and also supports common sense
     * operations on -inf values (eg <, >, etc.).
     * https://stackoverflow.com/questions/41160276/does-c-stds-inf-behave-exactly-as-common-sensical-infinity
     */
    CMX_STATIC_ASSERT(std::numeric_limits<cmxReal>::is_iec559);
    CMX_STATIC_ASSERT(std::numeric_limits<cmxReal>::has_infinity);

public:
    /**
     * Runs Metropolis-Hastings sampler for a single step. If template boolean
     * 'use_metropolis' is 'true', then Metropolis acceptance criterion will
     * be used and proposal bias will not be computed.
     * @param model - the model to be sampled
     * @param mh_args - model-specific arguments
     */
    template <typename t_model, bool use_metropolis = false>
    inline static void Mh_step(t_model& model,
                               const typename t_model::Mh_args_t mh_args
                                   CMX_THREAD_ID_PARAM) {
        // Inform model to prepare a new candidate
        model.Mh_prime_cand(mh_args CMX_THREAD_ID_ARG);

        // Calculate acceptance probability (log-space)
        // Note: compiler should optimize away this branch
        const cmxReal log_p_accept =
            use_metropolis
                // Metropolis acceptance criterion
                ? (model.Mh_get_logp_tar_cand() - model.Mh_get_logp_tar_curr())
                // Metropolis-Hastings acceptance criterion
                : (model.Mh_get_logp_tar_cand() +
                   model.Mh_get_logp_pro_curr_giv_cand() -
                   model.Mh_get_logp_tar_curr() -
                   model.Mh_get_logp_pro_cand_giv_curr());

        // Test if move is accepted
        cmxRand& rng = cmxRng;
        const cmxReal log_test = log(rng.unif_real());
        if (log_test < std::min(CMX_TO_REAL(0.0), log_p_accept)) {
            // Inform model of acceptance
            model.Mh_on_accept_cand(mh_args);
        } else {
            // Inform model of rejection
            model.Mh_on_reject_cand(mh_args);
        }
    }

    /**
     * Run model for a fixed number of Metropolis-Hastings steps (no thinning)
     * @param n_steps - number of steps to run MH sampler
     * @param model - the model to be sampled
     * @param mh_args - model-specific arguments
     */
    template <typename t_model, bool use_metropolis = false>
    inline static void Mh_run(const cmxUInt n_steps,
                              t_model& model,
                              const typename t_model::Mh_args_t mh_args
                                  CMX_THREAD_ID_PARAM) {
        for (cmxUInt i = 0; i < n_steps; ++i) {
            cmxMcmcMh4g::Mh_step<t_model, use_metropolis>(
                model, mh_args CMX_THREAD_ID_ARG);
        }
    }

private:
    // Disallow instantiation, copy, and assignment for now
    cmxMcmcMh4g();
    cmxMcmcMh4g(const cmxMcmcMh4g&);
    cmxMcmcMh4g& operator=(const cmxMcmcMh4g&);
};

#endif  // cmxMcmcMh4g
