//****************************************************************************
// cmxCaptLogscoreParallel.h
//****************************************************************************

/**
 * @brief Parallel batch capture of model log-scores
 */

#ifndef cmxCaptLogscoreParallel_h
#define cmxCaptLogscoreParallel_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_THREADS

#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxCaptLogscoreParallel
//****************************************************************************

/**
 * Performs batched capture of model log-scores (parallel version)
 */
template <typename t_Glue>
class cmxCaptLogscoreParallel {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Model component base type
     */
    typedef typename model_t::moco_t moco_t;

private:
    /**
     * Defines the chunk size - the amount of work each thread grabs and
     * must complete before grabbing another chunk of work
     */
    enum { inc_by = 1 };

    /**
     * Work object to process set of models
     */
    class Work : public cmxMappableWork {
    public:
        /**
         * Constructor
         */
        Work(std::vector<cmxReal>& out_logscores_presized, sim_t& sim)
            : m_cached_size(CMX_TO_UINT(out_logscores_presized.size())),
              m_logscores(out_logscores_presized),
              m_sim(sim) {
            cmxAssertPosEq(m_logscores.size(), sim.get_models().size());
            cmxAssertPosEq((size_t)m_cached_size, m_logscores.size());
        }

        /**
         * Defines what work is to be performed for the parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         * @return TRUE if work still to be done, FALSE if finished
         */
        virtual cmxBool do_range(const cmxUInt ix_start,
                                 const cmxUInt count,
                                 const cmxUInt thread_id) {
            // Perform Gibbs MCMC for chunk
            const cmxUInt end = std::min(ix_start + count, m_cached_size);
            std::vector<model_t>& models = m_sim.get_models();
            cmxAssertBoundsInc(end, CMX_TO_UINT(0), CMX_TO_UINT(models.size()));
            cmxAssertPosEq(m_cached_size, CMX_TO_UINT(models.size()));
            cmxAssertPosEq(models.size(), m_logscores.size());
            for (cmxUInt i = ix_start; i < end; ++i) {
                cmxAssertBounds(i, CMX_TO_UINT(0), CMX_TO_UINT(models.size()));
                model_t& model = models[i];
                std::vector<moco_t*>& mocos = model.get_moco_all();
                const size_t num_mocos = mocos.size();
                cmxReal logscore = CMX_TO_REAL(0.0);
                // Iterate over model components
                for (size_t j = 0; j < num_mocos; ++j) {
                    moco_t* const p_moco = mocos[j];
                    cmxAssert(p_moco);
                    logscore += p_moco->logscore(model, m_sim);
                }  // End iteration over random variables
                cmxAssertBounds(
                    i, CMX_TO_UINT(0), CMX_TO_UINT(m_logscores.size()));
                // Update model's log-score
                m_logscores[i] = logscore;
            }
            return end != m_cached_size;
        }

    private:
        // Cached number of models
        const cmxUInt m_cached_size;
        // Handle to output log-scores at each model
        std::vector<cmxReal>& m_logscores;
        // Handle to outer simulation
        sim_t& m_sim;
    };

public:
    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD, this is different from serial
     * version which may be called from any thread
     * Capture log-scores of all models belonging to simulation
     * @param out_logscores_presized - pre-sized vector of output log-scores,
     *  MUST BE SAME SIZE AS NUMBER OF MODELS!
     * @param sim - Outer simulation containing models
     */
    static void capture(std::vector<cmxReal>& out_logscores_presized,
                        sim_t& sim CMX_THREAD_ID_PARAM) {
        // @TODO - if calling from non-main thread, then default
        // to serial method. Only main thread should be using worker queue.

        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        cmxAssert(CMX_THREAD_ID_0_ARG == CMX_MAIN_THREAD_ID_0_ARG);

        // Create work object
        Work w(out_logscores_presized, sim);

        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will likely
        // deadlock as we only have a single, global parallel mapper.
        cmxParMap.wait_until_work_finished(w, inc_by);
    }
};

#else  // !defined (CMX_BUILD_ENABLE_THREADS)

// Default to serial method
#ifdef CMX_BUILD_COMPILER_MSVC
#pragma message( \
    "Warning: parallel logscore batch capture disabled. Defaulting to serial.")
#elif defined(CMX_BUILD_COMPILER_GCC)
#pragma message \
    "Warning: parallel logscore batch capture disabled. Defaulting to serial."
#endif  // compiler check

#include "cmxCaptLogscoreSerial.h"

// "Typedef" parallel runner to serial version
#define cmxCaptLogscoreParallel cmxCaptLogscoreSerial

#endif  // CMX_BUILD_ENABLE_THREADS

#endif  // cmxCaptLogscoreParallel_h
