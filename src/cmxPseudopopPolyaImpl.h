//****************************************************************************
// cmxPseudopopPolyaImpl.h
//****************************************************************************

/**
 * @brief Pseudo-population implementation uses Polya urn as proposal
 */

/**
 * Pseudo-population Impl must define:
 *
 *  ::mcmc_mixin_t - The MCMC mixin type (Metropolis vs Metropolis-Hastings)
 *  ::Mh_prime_cand(...) - Primes current and candidate states for MCMC
 *  ::randomize(...) - Randomizes parameter MCMC state
 */

#ifndef cmxPseudopopPolyaImpl_h
#define cmxPseudopopPolyaImpl_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxGlobals.h"
#include "cmxGlue.h"
#include "cmxMcmcMh4gMixin.h"
#include "cmxModelTypes.h"
#include "cmxPseudopopCore.h"
#include "cmxRand.h"
#include "cmxShufflerFisherYates.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxPseudopopPolyaImpl
//****************************************************************************

/**
 * Uses Polya urn as proposal distribution for candidate pseudo-population.
 * Each row of the pseudo-population bit matrix is modeled as an independent
 * Polya urn with color proportions according to the corresponding empirical
 * Hi-C frequency.
 *
 * The pseudo-population log posterior probability is given by:
 *
 *      (1) logpost = loglike(Hi-C|Pseudopop) + log(P(Pseudopop|Bnet)) + logZ
 *
 *  where P(Pseudopop|Bnet) is the probability of forward-sampling the
 *  pseudo-population according to each individual pseudo-sample's assigned
 *  Bayesian network (and logZ is the log of the normalizing constant which
 *  isn't computed).
 *
 * For Metropolis-Hastings, the proposal distribution of any state is always
 * log(P(Pseudopop|Polya)) (i.e. the probability of sampling according
 * to a Polya urn centered at each empirical Hi-C frequency). Hence, the log
 * Metropolis-Hastings acceptance criterion is given by:
 *
 *      (2) logp_accept =
 *              logp_tar_cand + logp_pro_curr - logp_tar_curr - logp_pro_cand
 *
 *  where logp_tar_cand = logpost(candidate),
 *        logp_pro_curr = log(P(current|Polya))
 *        logp_tar_curr = logpost(current),
 *        logp_pro_cand = log(P(candidate|Polya))
 *
 *  and 'current' is the current pseudo-population state and 'candidate' is
 *  the proposed candidate pseudo-population state.
 */
template <typename t_Glue>
class cmxPseudopopPolyaImpl {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * MCMC state type
     */
    typedef cmxPseudopopMocoStateMhast_t state_t;

    /**
     * Impl interface - MCMC mixin type
     */
    typedef cmxMcmcMh4gMhastMixin<glue_t, state_t> mcmc_mixin_t;

    /**
     * Pseudo-population core utilities
     */
    typedef cmxPseudopopCore<t_Glue> core_t;

    /**
     * Typedefs for utilized model component(s)
     */
    typedef typename glue_t::Contact_freqs_t contact_freqs_moco_t;

    /**
     * Metropolis-Hastings prime (i.e. propose) candidate sample
     */
    static void Mh_prime_cand(state_t& cand,
                              state_t& curr,
                              const cmxUIVecCol& indices,
                              const model_t& model,
                              sim_t& sim CMX_THREAD_ID_PARAM) {
        // Pre-conditions
        cmxAssertPosEq(cand.frame.n_cols, curr.frame.n_cols);
        cmxAssertPosEq(cand.frame.n_rows, curr.frame.n_rows);
        cmxAssertPosEq(cand.freqs.n_elem, curr.freqs.n_elem);
        cmxAssert(core_t::check_block_indices(cand.frame, indices));

        ////////////////////////////////////////////////////////////
        // Candidate: sample new state

        // Handle to random number generator
        cmxRand& rng = cmxRng;
        // Generate bit matrix
        core_t::sample_polya_trunc(cand.frame, indices, model, sim, rng);
        // Refresh contact frequencies
        core_t::refresh(cand.freqs, cand.frame, sim CMX_THREAD_ID_ARG);

        ////////////////////////////////////////////////////////////
        // Candidate: determine probability of proposing current

        cand.logp_pro = core_t::logscore_polya(curr.frame, indices, model, sim);

        ////////////////////////////////////////////////////////////
        // Candidate: determine target probability

        // Score candidate state according to Hi-C
        const contact_freqs_moco_t& contact_freqs_moco =
            model.get_Contact_freqs();
        cand.logp_tar = cand.logp_tar_hic = contact_freqs_moco.logscore_using(
            cand.frame, cand.freqs, model, sim);
        // Modulate by likelihood according to parent Bayesian network(s)
        cand.logp_tar += core_t::logscore_bfwd(cand.frame, indices, model, sim);

        ////////////////////////////////////////////////////////////
        // Current: determine probability of proposing candidate

        curr.logp_pro = core_t::logscore_polya(cand.frame, indices, model, sim);

        ////////////////////////////////////////////////////////////
        // Current: determine target probability

        if (!core_t::use_cached_hic_loglike) {
            // Re-score current state according to Hi-C
            curr.logp_tar_hic = contact_freqs_moco.logscore_using(
                curr.frame, curr.freqs, model, sim);
        } else {
            // Verify correct value is cached
            cmxAssert(curr.logp_tar_hic ==
                      contact_freqs_moco.logscore_using(
                          curr.frame, curr.freqs, model, sim));
        }
        curr.logp_tar = curr.logp_tar_hic;
        curr.logp_tar += core_t::logscore_bfwd(curr.frame, indices, model, sim);
    }

    /**
     * Randomize state - sample from prior
     * @param s - Metropolis-Hastings state to randomize from prior
     * @param config - user configuration
     * @param model - parent model
     * @param sim - outer simulation
     */
    static void randomize(state_t& s,
                          cmxSpConstConfig_t config,
                          const model_t& model,
                          sim_t& sim CMX_THREAD_ID_PARAM) {

        ////////////////////////////////////////////////////////////
        // Sample new state

        // Get index vector 0:PSEUDOPOP_SIZE-1
        cmxUIVecCol indices;
        cmxShufflerFisherYatesCore::get_sorted_indices(indices, s.frame.n_cols);
        // Handle to random number generator
        cmxRand& rng = cmxRng;
        // Sample from parent Bayesian network(s)
        core_t::sample_bfwd_trunc(s.frame, indices, model, sim, rng);
        // Refresh contact frequencies
        core_t::refresh(s.freqs, s.frame, sim CMX_THREAD_ID_ARG);

        ////////////////////////////////////////////////////////////
        // Determine probability of proposing

        // Note: logp_pro is probability of proposing alternate state,
        //  just set to self proposal bias for now
        s.logp_pro = core_t::logscore_polya(s.frame, indices, model, sim);

        ////////////////////////////////////////////////////////////
        // Determine target probability

        // Score according to Hi-C
        const contact_freqs_moco_t& contact_freqs_moco =
            model.get_Contact_freqs();
        s.logp_tar = s.logp_tar_hic =
            contact_freqs_moco.logscore_using(s.frame, s.freqs, model, sim);
        // Modulate by likelihood according to parent Bayesian network(s)
        s.logp_tar += core_t::logscore_bfwd(s.frame, indices, model, sim);
    }
};

#endif  // cmxPseudopopPolyaImpl_h
