//****************************************************************************
// cmxObsLogscoreCons.h
//****************************************************************************

/**
 * @brief Console observer for model log-score, where the log-score is
 *  the log of the possibly unnormalized posterior density. Console observers
 *  report values to stdout and typically rely on Snap ('Snapshot') observers.
 */

#ifndef cmxObsLogscoreCons_h
#define cmxObsLogscoreCons_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObsLogscoreSnap.h"
#include "cmxSmartPtr.h"

//****************************************************************************
// cmxObsLogscoreCons
//****************************************************************************

/**
 * Log-score console observer - reports log-score to stdout at a regular,
 * user-defined interval. Depends on log-score snapshot observer
 */
template <typename t_Glue>
class cmxObsLogscoreCons : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Log-score snapshot observer type
     */
    typedef cmxObsLogscoreSnap<t_Glue> obs_logscore_snap_t;

    /**
     * Default console period
     */
    enum { default_period = 1 };

    /**
     * Default constructor
     */
    cmxObsLogscoreCons() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_logscore_cons_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Extract snapshot observer - must occur before console observer
        if (!obs_t::find_raw(&mp_logscore_snap, sim, index)) {
            cmxLogf(
                "Error: log-score console observer unable to find handle to "
                "log-score snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_logscore_snap);
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_period = default_period;
        mp_logscore_snap = NULL;
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, reports posterior
     * log-score for all models to stdout if iteration matches period
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if ((iter % m_period) != CMX_TO_UINT(0)) {
            // Early out if not yet at sampling interval
            return;
        }
        // Report latest log-score snapshot to stdout
        cmxAssert(mp_logscore_snap);
        const std::vector<cmxReal>& logscores =
            mp_logscore_snap->get_logscores();
        const size_t num_logscores = logscores.size();
        for (size_t i = 0; i < num_logscores; ++i) {
            cmxLogf("(ITER %d, MODEL %d) LOG-SCORE = %f\n",
                    (int)iter,
                    (int)i,
                    logscores[i]);
        }
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_logscore_cons)

private:
    /**
     * Log-score is reported to stdout on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Handle to log-score snapshot observer - raw pointer is used as lifetime
     * of pointer does not exceed that of parent simulation's shared_ptr
     */
    const obs_logscore_snap_t* mp_logscore_snap;
};

#endif  // cmxObsLogscoreCons_h
