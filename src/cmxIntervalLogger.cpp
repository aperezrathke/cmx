//****************************************************************************
// cmxIntervalLogger.cpp
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_INTERVAL_LOGGING

#include "cmxIntervalLogger.h"

namespace cmxStats {
    /**
     * The static descriptions used for interval logging
     */
    const char* GCounterIntervalLoggerDescs[cmxSTAT_IntervalLoggerMax] = {
          "Total execution time:"
    };

} // end of cmxStats namespace

#endif  // CMX_BUILD_ENABLE_INTERVAL_LOGGING
