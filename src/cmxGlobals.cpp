//****************************************************************************
// cmxGlobals.cpp
//****************************************************************************

/**
 * Houses global data that is configurable at run time
 */

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxRand.h"
#include "cmxStats.h"
#include "cmxThread.h"

/**
 * Namespace to house global data
 */
namespace cmxG {
/**
 * If threads enabled, this value will hold the number of worker threads
 * available (the main thread is not included in this count). To access:
 * cmxG::NUM_WORKER_THREADS_VAR_NAME (will cause build errors if threads not
 * enabled).
 */
CMX_DECLARE_NUM_WORKER_THREADS

/**
 * If threads enabled, this declares an extern for a parallel mapper. To
 * access: cmxG:PARALLEL_MAPPER_VAR_NAME (will cause build errors if threads
 * not enabled).
 */
CMX_DECLARE_PARALLEL_MAPPER

/**
 * Random number generator (each thread has its own instance)
 */
CMX_DECLARE_TLS_DATA(cmxRand, rng);

/**
 * Initialize global data from runtime configuration
 */
void init(cmxSpConstConfig_t cfg) {
    CMX_SCOPED_STAT_TIMER(cmxSTAT_GlobalInitTime);
    CMX_INC_STAT_COUNTER(cmxSTAT_GlobalInitCount);
#ifdef CMX_BUILD_ENABLE_THREADS
    // Initialize thread count
    CMX_NUM_WORKER_THREADS_VAR_NAME = CMX_DEFAULT_NUM_THREADS;
    cfg->read_into(CMX_NUM_WORKER_THREADS_VAR_NAME, cmxOpt_num_worker_threads);
    cmxAssert(CMX_NUM_WORKER_THREADS_VAR_NAME > 0);
    CMX_NUM_WORKER_THREADS_VAR_NAME = (CMX_NUM_WORKER_THREADS_VAR_NAME < 1)
                                          ? 1
                                          : CMX_NUM_WORKER_THREADS_VAR_NAME;

    // Initialize parallel mapper
    CMX_PARALLEL_MAPPER_VAR_NAME.init(CMX_NUM_WORKER_THREADS_VAR_NAME);
#endif  // CMX_BUILD_ENABLE_THREADS

    // Initialize random number generator (rng)
    CMX_INIT_TLS_DATA_0_PARAM(rng);
    // @HACK - Force Intel compilers to seed RNG
    CMX_TLS_DATA_FOR_EACH_0_PARAM(rng, reseed);
}

/**
 * Initializes global data to default values
 */
void default_init() {
    CMX_SCOPED_STAT_TIMER(cmxSTAT_GlobalInitTime);
    CMX_INC_STAT_COUNTER(cmxSTAT_GlobalInitCount);
#ifdef CMX_BUILD_ENABLE_THREADS
    // Initialize thread count
    CMX_NUM_WORKER_THREADS_VAR_NAME = CMX_DEFAULT_NUM_THREADS;

    // Initialize parallel mapper
    CMX_PARALLEL_MAPPER_VAR_NAME.init(CMX_NUM_WORKER_THREADS_VAR_NAME);
#endif  // CMX_BUILD_ENABLE_THREADS

    // Initialize random number generator (rng)
    CMX_INIT_TLS_DATA_0_PARAM(rng);
    // @HACK - Force Intel compilers to seed RNG
    CMX_TLS_DATA_FOR_EACH_0_PARAM(rng, reseed);
}

/**
 * If global data is initialized, teardown() must be called before main()
 * exits!
 * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
 * This seems to be a deadlock bug caused by Microsoft's implementation of
 * std::thread
 */
void teardown() {
#ifdef CMX_BUILD_ENABLE_THREADS
    CMX_PARALLEL_MAPPER_VAR_NAME.teardown();
#endif  // CMX_BUILD_ENABLE_THREADS
}

}  // namespace cmxG
