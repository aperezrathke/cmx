//****************************************************************************
// cmxConfigUtils.h
//****************************************************************************

/**
 * @brief Reads shared model parameters from configuration object. The
 *  parameters may be invalid, in which case an error message is displayed and
 *  the program is terminated. Also, the parameters may not be user-specified,
 *  in which case defaults are used.
 */

#ifndef cmxConfigUtils_h
#define cmxConfigUtils_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxLogf.h"
#include "cmxOpts.h"

//****************************************************************************
// cmxConfigUtils
//****************************************************************************

class cmxConfigUtils {
public:
    /**
     * @return number of independent models (i.e. Gibbs chains)
     */
    inline static cmxUInt get_num_models(cmxSpConstConfig_t config) {
        return get_positive_int(config,
                                CMX_DEFAULT_NUM_MODELS,
                                cmxOpt_num_models,
                                cmxExitCode_invalid_num_models,
                                "Error, number of models must be integer > 0.");
    }

    /**
     * @return number of mixture components from configuration
     */
    inline static cmxUInt get_mix_size(cmxSpConstConfig_t config) {
        return get_positive_int(config,
                                CMX_DEFAULT_MIX_SIZE,
                                cmxOpt_mix_size,
                                cmxExitCode_invalid_mix_size,
                                "Error, mixture size must be integer > 0.");
    }

    /**
     * @return pseudo-population size from configuration
     */
    inline static cmxUInt get_pseudopop_size(cmxSpConstConfig_t config) {
        return get_positive_int(
            config,
            CMX_DEFAULT_PSEUDOPOP_SIZE,
            cmxOpt_pseudopop_size,
            cmxExitCode_invalid_pseudopop_size,
            "Error, pseudo-population size must be integer > 0.");
    }

    /**
     * @return pseudo-population block size from configuration
     */
    inline static cmxUInt get_pseudopop_block_size(cmxSpConstConfig_t config) {
        cmxUInt block_size = get_positive_int(
            config,
            CMX_DEFAULT_PSEUDOPOP_BLOCK_SIZE,
            cmxOpt_pseudopop_block_size,
            cmxExitCode_invalid_pseudopop_block_size,
            "Error, pseudo-population block size must be integer > 0.");
        block_size = std::min(block_size, get_pseudopop_size(config));
        return block_size;
    }

    /**
     * @return Multi-population Bayesian network block size from configuration
     */
    inline static cmxUInt get_Bnet_mpop_block_size(cmxSpConstConfig_t config) {
        cmxUInt block_size = get_positive_int(
            config,
            CMX_DEFAULT_BNET_MPOP_BLOCK_SIZE,
            cmxOpt_Bnet_mpop_block_size,
            cmxExitCode_invalid_bnet_mpop_block_size,
            "Error, pseudo-population block size must be integer > 0.");
        block_size = std::min(block_size, get_mix_size(config));
        return block_size;
    }

private:
    /**
     * @return value of positive integer from configuration
     */
    inline static cmxUInt get_positive_int(cmxSpConstConfig_t config,
                                           const cmxUInt default_value,
                                           const cmxOptE opt,
                                           const cmxExitCode exit_code,
                                           const char* error_msg) {
        cmxAssert(error_msg);
        cmxUInt i = default_value;
        config->read_into(i, opt);
        if (i < 1) {
            cmxLogf("%s\n", error_msg);
            exit(exit_code);
        }
        cmxAssert(i > 0);
        return i;
    }
};

#endif  // cmxConfigUtils_h
