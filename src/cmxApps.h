//****************************************************************************
// cmxApps.h
//****************************************************************************

#ifndef cmxApps_h
#define cmxApps_h

#include "cmxBuild.h"
#include "cmxCmdOptsMap.h"

/**
 * App prints usage information
 *
 * Usage:
 * -help | -app_help
 */
extern int cmxAppHelpMain(const cmxCmdOptsMap& cmd_opts);

/**
 * App runs canonical, single-population simulation
 *
 * Usage:
 *  -app_spop_canon
 */
extern int cmxAppSpopCanonMain(const cmxCmdOptsMap& cmd_opts);

/**
 * App runs canonical, multi-population simulation
 *
 * Usage:
 *  -app_mpop_canon
 */
extern int cmxAppMpopCanonMain(const cmxCmdOptsMap& cmd_opts);

#endif  // cmxApps_h
