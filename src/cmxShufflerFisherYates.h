//****************************************************************************
// cmxShufflerFisherYates.h
//****************************************************************************

/**
 * @brief - Generates unbiased permutations
 */

/**
 * @WARNING - THIS UTILITY WILL NOT WORK WITH 0-LENGTH VECTORS! THE CLIENT
 *  IS RESPONSIBLE FOR PASSING IN NON-EMPTY VECTORS. THERE ARE ASSERTS TO
 *  TO CATCH THIS EDGE CASE BUT ASSERTS ARE NOT ALWAYS ENABLED - SUCH AS
 *  FOR RELEASE BUILDS.
 */

/**
 * Note, template argument 't_ivec_arma' assumes an integer vector type that
 *  has interface consistent with the Armadillo matrix library:
 *
 *  http://arma.sourceforge.net/
 */

#ifndef cmxShufflerFisherYates_h
#define cmxShufflerFisherYates_h

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxRand.h"
#include "cmxTypes.h"

#include <algorithm>

/**
 * Utility for generating a Fisher-Yates shuffle. See:
 *   https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
 *   https://eli.thegreenplace.net/2010/05/28/the-intuition-behind-fisher-yates-shuffling/
 *   https://stackoverflow.com/questions/9345087/choose-m-elements-randomly-from-a-vector-containing-n-elements
 * A Fisher-Yates shuffle generates an unbiased permutation. In other words,
 * all permutations are equally likely. This algorithm is also more efficient
 * when you only require a partial permutation of an index set.
 */
class cmxShufflerFisherYatesCore {
public:
    /**
     * Generates a sorted index vector from [0, n-1]. In other words, the
     * output vector will have elements in order: 0 1 ... (n-1)
     * @param out - unsigned integer vector for storing sorted index set
     * @param n - the index range to sort [0, (n-1)]
     */
    template <typename t_ivec_arma = cmxUIVecCol>
    inline static void get_sorted_indices(t_ivec_arma& out_sorted,
                                          const cmxUInt n) {
        cmxAssert(n > CMX_TO_UINT(0));
        out_sorted = cmxMatrixUtils::linspace<t_ivec_arma>(
            CMX_TO_MAT_SZ_T(0), CMX_TO_MAT_SZ_T(n - 1), CMX_TO_MAT_SZ_T(n));
        cmxAssert(out_sorted.is_sorted());
        cmxAssert(out_sorted.n_elem == CMX_TO_MAT_SZ_T(n));
    }

    /**
     * Performs complete permutation of all elements
     * @param out_perm - output permuted index vector
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = cmxUIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm, cmxRand& rng) {
        cmxAssert(out_perm.n_elem > CMX_TO_MAT_SZ_T(0));
        shuffle(out_perm,
                CMX_TO_UINT(0),
                CMX_TO_UINT(out_perm.n_elem - 1),
                CMX_TO_UINT(out_perm.n_elem - 1),
                rng);
    }

    /**
     * Performs partial permutation of first 'm' elements. Considers all
     * elements of candidates for the permutation.
     * @param out_perm - output permuted index vector up to first m elements
     * @param m - only the first 'm' elements will be permuted where
     *  0 < m <= n == out_perm.n_elem
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = cmxUIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm,
                               const cmxUInt m,
                               cmxRand& rng) {
        cmxAssertBoundsInc(m, CMX_TO_UINT(1), CMX_TO_UINT(out_perm.n_elem));
        shuffle(out_perm,
                CMX_TO_UINT(0),
                m - CMX_TO_UINT(1),
                CMX_TO_UINT(out_perm.n_elem - 1),
                rng);
    }

    /**
     * Performs partial permutation of elements in range [ix_min, ix_max] with
     * all elements in [ix_min, ix_pool] as possible candidates and the
     * following inequalities hold: ix_min <= ix_max and ix_max <= ix_pool
     * @param out_perm - output permuted index vector, only elements in range
     *  [ix_min, ix_max] are considered permuted; however, elements in
     *  [ix_max+1, ix_pool] may be modified but are not considered as part of
     *  final permutation.
     * @param ix_min - lower bound of range to permute, must be <= ix_max
     * @param ix_max - upper bound of range to permute, must be >= ix_min and
     *  <= ix_pool
     * @param ix_pool - upper bound of candidate elements considered for
     *  permuting, considered elements are those in range [ix_min, ix_pool],
     *  must be >= ix_max
     * @param rng - random number generator
     */
    template <typename t_ivec_arma = cmxUIVecCol>
    inline static void shuffle(t_ivec_arma& out_perm,
                               const cmxUInt ix_min,
                               const cmxUInt ix_max,
                               const cmxUInt ix_pool,
                               cmxRand& rng) {
        cmxAssert(out_perm.n_elem > CMX_TO_MAT_SZ_T(0));
        cmxAssertBounds(ix_min, CMX_TO_UINT(0), CMX_TO_UINT(out_perm.n_elem));
        cmxAssertBounds(ix_max, CMX_TO_UINT(0), CMX_TO_UINT(out_perm.n_elem));
        cmxAssertBounds(ix_pool, CMX_TO_UINT(0), CMX_TO_UINT(out_perm.n_elem));
        cmxAssert(ix_min <= ix_max);
        cmxAssert(ix_max <= ix_pool);
        cmxAssert((ix_pool - ix_min) > CMX_TO_UINT(0));
        for (cmxUInt i = ix_min; i <= ix_max; ++i) {
            const cmxUInt ix_swap = rng.unif_int(i, ix_pool);
            cmxAssertBoundsInc(ix_swap, i, ix_pool);
            std::swap(out_perm.at(CMX_TO_MAT_SZ_T(i)),
                      out_perm.at(CMX_TO_MAT_SZ_T(ix_swap)));
        }
    }
};

/**
 * Encapsulated Fisher-Yates object, maintains internal index buffers
 */
template <typename t_ivec_arma = cmxUIVecCol>
class cmxShufflerFisherYates {
public:
    /**
     * Default constructor - uninitialized!
     */
    cmxShufflerFisherYates() {}

    /**
     * Constructor
     * @param n - number of indices that need shuffling
     */
    explicit cmxShufflerFisherYates(const cmxUInt n) : sorted(n), perm(n) {
        cmxAssert(n > CMX_TO_UINT(0));
        cmxShufflerFisherYatesCore::get_sorted_indices(sorted, n);
    }

    /**
     * Initialize shuffler with 'n' elements, where n > 0
     * @param n - number of indices that may be shuffled
     */
    void init(const cmxUInt n) {
        cmxAssert(n > CMX_TO_UINT(0));
        sorted.set_size(n);
        perm.set_size(n);
        cmxShufflerFisherYatesCore::get_sorted_indices(sorted, n);
    }

    /**
     * Obtain partially shuffled index set, compute shuffle for first 'm'
     * elements only (with all elements as possible candidates)
     * @param m - first 0 < m <= n elements of index set will be shuffled
     * @param rng - random number generator
     * @return index set with first m elements shuffled
     */
    const t_ivec_arma& shuffle(const cmxUInt m, cmxRand& rng) {
        cmxAssert(m > CMX_TO_UINT(0));
        cmxAssert(m <= CMX_TO_UINT(sorted.n_elem));
        perm = sorted;
        cmxShufflerFisherYatesCore::shuffle(perm, m, rng);
        return this->get_view(m);
    }

    /**
     * @WARNING - DO NOT CALL THIS METHOD UNLESS YOU KNOW WHAT YOUR ARE DOING!
     * @return View of internal permutation buffer as size 'm'
     */
    inline const t_ivec_arma& get_view(const cmxUInt m) {
        cmxAssert(m > CMX_TO_UINT(0));
        cmxAssert(m <= CMX_TO_UINT(perm.n_elem));
        // return aliased view for vector of length 'm'
        new (&perm_view) t_ivec_arma(perm.memptr(),
                                     CMX_TO_MAT_SZ_T(m),
                                     false, /* copy */
                                     true); /* strict*/
        return perm_view;
    }

private:
    // Sorted index set with elements [0 to n-1]
    t_ivec_arma sorted;
    // Scratch buffer for permuting sorted index set
    t_ivec_arma perm;
    // Aliased view of permuted buffer of size m
    t_ivec_arma perm_view;
};

#endif  // cmxShufflerFisherYates_h
