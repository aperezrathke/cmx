//****************************************************************************
// cmxBnetBoolCt.h
//****************************************************************************

/**
 * @brief Table with entries conditional on the boolean state (0|1) of the
 *  parent and child nodes. Can be used as a conditional probability table
 *  (CPT) or more generally for storing any scalar value (e.g. counts)
 */

#ifndef cmxBnetBoolCt_h
#define cmxBnetBoolCt_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxBnetBoolCt
//****************************************************************************

/**
 * Represents data as a 2-D Matrix, where row index 0 stores values
 * conditional on child node being 0, row index 1 stores values conditional on
 * child node being 1. The number of columns is given by expression:
 *      2^(# of parents ) == 1 << (# of parents).
 * Columns are stored in order of the parent values by simply converting the
 * binary parent values into a decimal integer. Therefore, 0th column stores
 * entries conditional on all parent nodes = 0, and the last column stores
 * entries conditional on all parent nodes = 1.
 *
 */
class cmxBnetBoolCt {
public:
    /**
     * Row enumeration
     */
    enum eRowIx { row_false = 0, row_true = 1, num_rows = 2 };

    /**
     * Handle to column data - a column contains all child values conditional
     * on a specific instance of the parents' states. The handle classes are
     * intended to provide some safety in accessing these values, they are
     * bounds checked if asserts are enabled.
     */

    /**
     * Mutable handle to column data
     */
    class handle {
    public:
        /**
         * @return mutable reference to value at row index
         */
        inline cmxReal& operator()(enum eRowIx ix) {
            cmxAssertBounds(ix, 0, num_rows);
            return m_col[ix];
        }
        /**
         * @return non-mutable value at row index
         */
        inline cmxReal operator()(enum eRowIx ix) const {
            cmxAssertBounds(ix, 0, num_rows);
            return m_col[ix];
        }

    private:
        cmxReal* m_col;
        friend class cmxBnetBoolCt;
    };

    /**
     * Non-mutable handle to column data (read-only)
     */
    class const_handle {
    public:
        /**
         * @return non-mutable value at row index
         */
        inline cmxReal operator()(enum eRowIx ix) const {
            cmxAssertBounds(ix, 0, num_rows);
            return m_col[ix];
        }

    private:
        const cmxReal* m_col;
        friend class cmxBnetBoolCt;
    };

    /**
     * Mutable accessor
     * @param parents - boolean state vector for parent nodes
     * @return handle to column associated to state vector
     */
    handle operator()(const std::vector<cmxBool>& parents) {
        const cmxMatSz_t index = this->get_col_index(parents);
        handle h;
        h.m_col = m_t.colptr(index);
        return h;
    }

    /**
     * Non-mutable accessor
     * @param parents - boolean state vector for parent nodes
     * @return const handle to column associated to state vector
     */
    const_handle operator()(const std::vector<cmxBool>& parents) const {
        const cmxMatSz_t ix_col = this->get_col_index(parents);
        const_handle h;
        h.m_col = m_t.colptr(ix_col);
        return h;
    }

    /**
     * Set number of parent nodes, each parent node is assumed to be boolean.
     * (Any previous data is not guaranteed to be preserved)
     * @param num_parents - number of boolean parents
     */
    void set_num_parents(const cmxMatSz_t num_parents) {
        // Compute 2^(# of parents)
        const cmxMatSz_t num_cols = CMX_TO_MAT_SZ_T(1) << num_parents;
        m_t.set_size(num_rows, num_cols);
    }

    /**
     * Directly set number of columns
     * @param num_cols - number of columns to allocate, must be power of 2!
     */
    void set_num_cols(const cmxMatSz_t num_cols) {
        cmxAssert(CMX_UINT_IS_POW_2(num_cols));
        m_t.set_size(num_rows, num_cols);
    }

    /**
     * @return number of columns in conditional table, corresponds to:
     *  2^(# of parents) == # of parent states
     */
    inline cmxUInt num_cols() const { return CMX_TO_UINT(m_t.n_cols); }

    /**
     * @param index - column index
     * @return [Const] column pointer
     */
    inline cmxReal* colptr(const cmxUInt index) {
        cmxAssertBounds(index, 0, CMX_TO_UINT(m_t.n_cols));
        return m_t.colptr(CMX_TO_MAT_SZ_T(index));
    }
    inline const cmxReal* colptr(const cmxUInt index) const {
        cmxAssertBounds(index, 0, CMX_TO_UINT(m_t.n_cols));
        return m_t.colptr(CMX_TO_MAT_SZ_T(index));
    }

    /**
     * @return Const handle to conditional table data
     */
    inline const cmxMatrix& table() const { return m_t; }

    /**
     * Fill underlying table with all zeros
     */
    void zeros() { m_t.zeros(); }

    /**
     * Resize underlying table to accommodate boolean parent nodes, and fill
     * resulting table with all zeros.
     */
    void zeros(const cmxMatSz_t num_parents) {
        // Compute 2^(# of parents)
        const cmxMatSz_t num_cols = CMX_TO_MAT_SZ_T(1) << num_parents;
        m_t.zeros(num_rows, num_cols);
    }

    /**
     * Fill underlying table will parameter value
     */
    void fill(const cmxReal value) { m_t.fill(value); }

    /**
     * Resize table to accommodate boolean parent nodes, and fill resulting
     * table with parameter value
     */
    void fill(const cmxMatSz_t num_parents, const cmxReal value) {
        // Compute 2^(# of parents)
        const cmxMatSz_t num_cols = CMX_TO_MAT_SZ_T(1) << num_parents;
        m_t.set_size(num_rows, num_cols);
        m_t.fill(value);
    }

private:
    /**
     * Maps parent boolean state vector to column indices
     */
    cmxMatSz_t get_col_index(const std::vector<cmxBool>& parents) const {
        const cmxMatSz_t num_parents = CMX_TO_MAT_SZ_T(parents.size());
        cmxAssert((CMX_TO_MAT_SZ_T(1) << num_parents) == m_t.n_cols);
        // Determine column offset based on parent states
        cmxMatSz_t index = 0;
        for (cmxMatSz_t i = 0; i < num_parents; ++i) {
            const cmxBool s = parents[i];
            cmxAssert((s == cmxTRUE) || (s == cmxFALSE));
            index |= (s << i);
            cmxAssertBounds(index, 0, m_t.n_cols);
        }
        cmxAssertBounds(index, 0, m_t.n_cols);
        return index;
    }

    /**
     * Internal matrix representation
     */
    cmxMatrix m_t;
};

#endif  // cmxBnetBoolCt_h
