//****************************************************************************
// cmxTestHash.h
//****************************************************************************

/**
 * Commandlet for testing the hash interface
 *
 * Usage:
 * -test_hash
 */

#ifdef cmxTestHash_h
#   error "Test Hash included multiple times!"
#endif  // cmxTestHash_h
#define cmxTestHash_h

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_TESTS

#include "cmxAssert.h"
#include "cmxCmdOptsMap.h"
#include "cmxExitCodes.h"
#include "cmxHashMap.h"
#include "cmxTypes.h"

#include <iostream>
#include <limits>

/**
 * Entry point for test
 */
int cmxTestHashMain(const cmxCmdOptsMap& cmd_opts) {
    // Hash map from key = unsigned integer to value = unsigned integer
    typedef CMX_HASH_MAP<unsigned int, unsigned int> cmxUIntToUIntMap;
    cmxUIntToUIntMap hash_map;
    typedef cmxUIntToUIntMap::key_type key_t;
    typedef cmxUIntToUIntMap::value_type::second_type val_t;

    std::cout << "Running Test Hash." << std::endl;

    const cmxUInt MIN_SIZE = 12000;
    CMX_HASH_RESERVE(hash_map, MIN_SIZE);
    std::cout << "Starting bucket count: " << hash_map.bucket_count()
              << std::endl;
    cmxAssert(hash_map.bucket_count() >= MIN_SIZE);

    const key_t KEYS[] = {
        2,
        4,
        101,
        0xDEADBEEF,
        std::numeric_limits<cmxUIntToUIntMap::key_type>::max(),
        std::numeric_limits<cmxUIntToUIntMap::key_type>::max() - 1};

    const val_t VALS[] = {100, 200, 5000, 7331, 0xDEADBEEF, 0x8BADF00D};

    const cmxUInt N = sizeof(KEYS) / sizeof(key_t);
    cmxAssert((sizeof(VALS) / sizeof(val_t)) == N);

    cmxUInt i = 0;
    for (i = 0; i < N; ++i) {
        hash_map[KEYS[i]] = VALS[i];
        std::cout << "Inserted (" << KEYS[i] << ", " << hash_map[KEYS[i]] << ")"
                  << std::endl;
    }

    for (i = 0; i < N; ++i) {
        cmxAssert(hash_map.find(KEYS[i]) != hash_map.end());
    }

    cmxAssert(hash_map.find(0x1CEB00DA) == hash_map.end());

    std::cout << "Ending bucket count: " << hash_map.bucket_count()
              << std::endl;
    cmxAssert(hash_map.bucket_count() >= MIN_SIZE);

    return cmxExitCode_normal;
}

#endif  // CMX_BUILD_ENABLE_TESTS
