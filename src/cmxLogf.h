//****************************************************************************
// cmxLogf.h
//****************************************************************************

#ifndef cmxLogf_h
#define cmxLogf_h

#include "cmxBuild.h"

/**
 * A logging macro useful for debugging
 */
#ifdef CMX_BUILD_ENABLE_LOGGING
#   include <stdio.h>
    // Flush stdout so that batch scripts can keep us updated
#   define cmxLogf(...)          \
        do {                     \
            printf(__VA_ARGS__); \
            fflush(stdout);      \
        } while (0)
#else
#   define cmxLogf(...)
#endif  // CMX_BUILD_ENABLE_LOGGING

#endif  // cmxLogf_h
