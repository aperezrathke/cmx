//****************************************************************************
// cmxNullMoco.h
//****************************************************************************

/**
 * @brief Null model component for use as placeholder when component is not
 *  available and/or necessary
 */

#ifndef cmxNullMoco_h
#define cmxNullMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxGlue.h"
#include "cmxMoco.h"

//****************************************************************************
// cmxNullMoco
//****************************************************************************

/**
 * Null model component, always deterministic
 */
template <typename t_Glue>
class cmxNullMoco : public cmxMoco<t_Glue> {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Component is deterministic and requires no sampling
     */
    enum { is_random = cmxFALSE };
};

#endif  // cmxNullMoco_h
