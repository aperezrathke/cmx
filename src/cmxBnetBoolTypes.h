//****************************************************************************
// cmxBnetBoolTypes.h
//****************************************************************************

/**
 * @brief Utility structures used by boolean Bayesian network
 */

#ifndef cmxBnetBoolTypes_h
#define cmxBnetBoolTypes_h

/**
 * We are assuming an adjacency list interface/representation for our DAGs. We
 * refer to this structure as an "adjacency map" or AdjMap_t. For an excellent
 * overview of graph algorithms and data structures, please refer to the Boost
 * graph library (BGL) documentation:
 *
 *  http://www.boost.org/doc/libs/1_65_1/libs/graph/doc/index.html
 *
 * In particular, the graph theory overview provides a nice review of graph
 * representation data structures and their various trade offs:
 *
 *  http://www.boost.org/doc/libs/1_65_1/libs/graph/doc/graph_theory_review.html
 *
 * Since we intend our DAGs to be sparse, an adjacency list representation is
 * the best way to go in terms of memory as it is O(V + E). Also, refer to our
 * "thin DAG" implementation which allocates extra memory to additionally
 * allow O(1) edge removal. (Essentially, our thin DAG has most if not all
 * benefits of adjacency matrix, but still benefits from sparsity).
 */

#include "cmxBuild.h"
#include "cmxBnetBool.h"
#include "cmxBnetThinEdgeStatesMu.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// Alpha transforms
//****************************************************************************

/**
 * Alpha transforms take a global scalar 'alpha' count parameter and transform
 * it according to a prior on the conditional probability table parameters
 */

/**
 * Identity alpha transform policy, simply returns the alpha count unchanged.
 * Note, when alpha is 1, this corresponds to the 'K2' score of Cooper and
 * Herskovits (1991)
 */
class cmxBnetBoolAlphaTransIdent {
public:
    /**
     * @param alpha - 'global' alpha to transform
     * @param num_parent_states - unused
     * @return alpha (identity transform)
     */
    inline static cmxReal trans(const cmxReal alpha,
                                const cmxUInt num_parent_states) {
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        return alpha;
    }
};

/**
 * BDeu score from Buntine 1991
 */
class cmxBnetBoolAlphaTransBdeu {
public:
    /**
     * @param alpha - 'global' alpha to transform
     * @param num_parent_states - number of parent states, must be power of 2
     * @return alpha / (2 * # of parent states), corresponds to Bdeu score of
     *  Buntine 1991 when all network nodes are boolean
     */
    inline static cmxReal trans(const cmxReal alpha,
                                const cmxUInt num_parent_states) {
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        // Check number of parent states is power of 2
        cmxAssert(CMX_UINT_IS_POW_2(num_parent_states));
        return alpha / CMX_TO_REAL(2 * num_parent_states);
    }
};

//****************************************************************************
// cmxBnetBoolMocoStateMh_t
//****************************************************************************

/**
 * Metropolis-within-Gibbs state for Gibbs updating.
 */
typedef struct {
    /**
     * Core boolean Bayesian network which can be used for generating a
     * psuedo-population and/or evaluating probability of a pseudo-population
     */
    cmxBnetBool bnet;

    /**
     * Used for keeping track of edge state within DAG, for removing edges in
     * O(1) time, and for determining legal move set.
     */
    cmxBnetThinEdgeStatesMu dag_mu;

    /**
     * The set of legal edge moves that can be applied to current state (i.e.
     * moves which do not induce a cycle). Determines the reachable states.
     */
    std::vector<cmxUInt> dag_moves;

    /**
     * Number of active edges
     */
    cmxUInt dag_num_edges;

    /**
     * The log probability of proposing any other state from this state
     */
    cmxReal logp_pro;

    /**
     * The log probability of this state under the target distribution
     */
    cmxReal logp_tar;

} cmxBnetBoolMocoStateMh_t;

#endif  // cmxBnetBoolTypes_h
