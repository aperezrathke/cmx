//****************************************************************************
// cmxBnetDagTypes.h
//****************************************************************************

/**
 * @brief Expose concrete types used for directed acyclic graph (DAG)
 *  as many classes need to utilize this data representation.
 */

#ifndef cmxBnetDagTypes_h
#define cmxBnetDagTypes_h

/**
 * We are assuming an adjacency list interface/representation for our DAGs. We
 * refer to this structure as an "adjacency map" or AdjMap_t. For an excellent
 * overview of graph algorithms and data structures, please refer to the Boost
 * graph library (BGL) documentation:
 *
 *  http://www.boost.org/doc/libs/1_65_1/libs/graph/doc/index.html
 *
 * In particular, the graph theory overview provides a nice review of graph
 * representation data structures and their various trade offs:
 *
 *  http://www.boost.org/doc/libs/1_65_1/libs/graph/doc/graph_theory_review.html
 *
 * Since we intend our DAGs to be sparse, an adjacency list representation is
 * the best way to go in terms of memory as it is O(V + E). Also, refer to our
 * "thin DAG" implementation which allocates extra memory to additionally
 * allow O(1) edge removal. (Essentially, our thin DAG has most if not all
 * benefits of adjacency matrix, but still benefits from sparsity).
 */

#include "cmxBuild.h"
#include "cmxTypes.h"

#include <vector>

/**
 * A list of vertex identifiers
 */
typedef std::vector<cmxUInt> cmxBnetDagVxList_t;

/**
 * Adjacency mapping implemented as a jagged array interface.
 */
typedef std::vector<cmxBnetDagVxList_t> cmxBnetDagAdjMap_t;

/**
 * Const handle for sharing source-to-destination (i.e. parent-to-child) and
 * destination-to-source (i.e. child-to-parent) adjacency maps
 */
typedef struct {
    const cmxBnetDagAdjMap_t& src2dst;
    const cmxBnetDagAdjMap_t& dst2src;
} cmxBnetDagConstHandle_t;

#endif  // cmxBnetDagTypes_h
