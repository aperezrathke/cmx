//****************************************************************************
// cmxObsCorrHicSnap.h
//****************************************************************************

/**
 * @brief Snapshot observer for correlation of pseudo-population with
 *  empirical Hi-C. Snapshot observers compute the value of some statistic
 *  according to their sampling frequency and should only store the most
 *  recent value. Other observer types such as Cons ('Console') and Trac
 *  ('Tracer') can use the snapshot value to log to stdout and generate trace
 *  plots respectively.
 */

#ifndef cmxObsCorrHicSnap_h
#define cmxObsCorrHicSnap_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxCaptCorrHicParallel.h"
#include "cmxGlue.h"
#include "cmxModelTypes.h"
#include "cmxObs.h"
#include "cmxThread.h"

//****************************************************************************
// cmxObsCorrHicSnap
//****************************************************************************

/**
 * Hi-C correlation snapshot observer - stores latest snapshot of Hi-C
 * correlation at each model. The snapshot is updated at a regular,
 * user-defined interval.
 */
template <typename t_Glue>
class cmxObsCorrHicSnap : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Default snapshot period
     */
    enum { default_period = 1 };

    /**
     * Default constructor
     */
    cmxObsCorrHicSnap() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_corr_hic_snap_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Allocate snapshots buffer
        cmxAssert(sim.get_num_models() > 0);
        m_corrs.resize(sim.get_num_models(), CMX_TO_REAL(0.0));
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_period = default_period;
        m_last_snap_iter = CMX_UINT_MAX;
        m_corrs.clear();
    }

    /**
     * @return true if snapshot does not match iteration
     */
    inline cmxBool is_stale(const cmxUInt iter) const {
        return iter != m_last_snap_iter;
    }

    /**
     * Mark current capture as stale without clearing
     */
    virtual void mark_stale() { m_last_snap_iter = CMX_UINT_MAX; }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, captures Hi-C
     * correlation with pseudo-population for all models if iteration matches
     * period
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if (this->should_update(iter)) {
            // Capture new snapshot
            this->force_update(iter, sim);
        }
    }

    /**
     * @param iter - 0-based iteration that has just completed
     * @param sim - outer simulation
     * Updates snapshot regardless of iteration
     */
    void force_update(const cmxUInt iter, sim_t& sim) {
        cmxAssert(m_last_snap_iter != iter);
        m_last_snap_iter = iter;
        const std::vector<model_t>& models = sim.get_models();
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();
        calc_corr_hic(m_corrs, models, hic);
    }

    /**
     * Utility to calculate model correlations with empirical Hi-C
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * @param out_corrs_presized - pre-sized vector of output correlations,
     *  MUST BE SAME SIZE AS models!
     * @param models - set of models, must be same size as out_corrs_presized
     * @param hic - empirical (observed) Hi-C frequencies
     */
    template <typename t_Capt = cmxCaptCorrHicParallel<glue_t> >
    static void calc_corr_hic(std::vector<cmxReal>& out_corrs_presized,
                              const std::vector<model_t>& models,
                              const cmxContactFreqs_t& hic) {
        // Defer to batch capture utility
        typedef t_Capt capt_t;
        capt_t::capture(out_corrs_presized, models, hic CMX_MAIN_THREAD_ID_ARG);
    }

    /**
     * @return read-only view of model Hi-C correlation snapshots
     */
    inline const std::vector<cmxReal>& get_corrs() const { return m_corrs; }

    /**
     * @return number of tracked Hi-C correlations
     */
    inline size_t get_num_corrs() const { return m_corrs.size(); }

    /**
     * Other [dependent] observers may be interested in period of snapshot
     *  observer
     * @return period of this observer
     */
    inline cmxUInt get_period() const { return m_period; }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_corr_hic_snap)

private:
    /**
     * @return TRUE if update is scheduled for parameter iteration, FALSE o/w
     */
    inline cmxBool should_update(const cmxUInt iter) const {
        return ((iter % m_period) == CMX_TO_UINT(0)) && is_stale(iter);
    }

    /**
     * Snapshot is updated on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Iteration at which last snapshot was captured
     */
    cmxUInt m_last_snap_iter;

    /**
     * Snapshot Hi-C correlations at each model
     */
    std::vector<cmxReal> m_corrs;
};

#endif  // cmxObsCorrHicSnap_h
