//****************************************************************************
// cmxArmadillo.h
//****************************************************************************

/**
 * @brief Armadillo include which incorporates build flags
 */

#ifndef cmxArmadillo_h
#define cmxArmadillo_h

#include "cmxBuild.h"
#include "cmxTypes.h"

// Check if we don't want armadillo debug information (e.g. during release)
// Also, define NDEBUG before including this external library just in case it
// still has internal asserts that aren't disabled by ARMA_NO_DEBUG.
#ifndef CMX_BUILD_ARMADILLO_DEBUG
#   ifndef ARMA_NO_DEBUG
#       define ARMA_NO_DEBUG // tell armadillo to avoid debug checks
#   endif
#   ifndef NDEBUG // for good measure, disable asserts for this unit
#       define CMX_NDEBUG_TRANSIENT // this means enable asserts after include
#       define NDEBUG
#   endif // NDEBUG
#   include <armadillo>
#   ifdef CMX_NDEBUG_TRANSIENT // NDEBUG wasn't defined before, so undef it
#       undef CMX_NDEBUG_TRANSIENT
#       undef NDEBUG
#   endif // CMX_NDEBUG_TRANSIENT
#else
#   include <armadillo> // else, lets include extra debug checks
#endif // CMX_BUILD_ARMADILLO_DEBUG

// Wrap namespace
#define cmxMatrixUtils arma

/**
 * Matrix for real values
 */
typedef cmxMatrixUtils::Mat<cmxReal> cmxMatrix;

/**
 * Matrix for unsigned integers
 */
typedef cmxMatrixUtils::Mat<cmxUInt> cmxUIMatrix;

/**
 * Column vector for real values
 */
typedef cmxMatrixUtils::Col<cmxReal> cmxVecCol;

/**
 * Row vector for real values
 */
typedef cmxMatrixUtils::Row<cmxReal> cmxVecRow;

/**
 * Column vector for unsigned integers
 */
typedef cmxMatrixUtils::Col<cmxUInt> cmxUIVecCol;

/**
 * Column vector for boolean values
 */
typedef cmxMatrixUtils::Col<cmxBool> cmxBoolVecCol;

/**
 * Matrix indexing type
 */
typedef cmxMatrixUtils::uword cmxMatSz_t;

/**
 * Macro for casting to matrix indexing type
 */
#define CMX_TO_MAT_SZ_T(i) static_cast<cmxMatSz_t>((i))

#endif  // cmxArmadillo_h
