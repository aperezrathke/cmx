//****************************************************************************
// cmxModelTypes.h
//****************************************************************************

/**
 * @brief Types used for representing the core model component states
 */

#ifndef cmxModelTypes_h
#define cmxModelTypes_h

#include "cmxBuild.h"
#include "cmxTypes.h"

/**
 * Bayesian network DAG representation
 */
#include "cmxBnetDagTypes.h"

// @TODO - Bayesian network CPT representation

/**
 * List of indices into pseudo-population. If pseudo-population is a column-
 * major matrix, then this is a list of column indices
 */
typedef std::vector<cmxUInt> cmxPseudopopIndices_t;

/**
 * Pseudo-population representation, a column-major matrix (i.e. each column
 * represents a single member of the pseudo-population)
 */
typedef cmxUIMatrix cmxPseudopopFrame_t;

/**
 * Linearized representation of aggregated contact frequencies in [0,1] from
 * either observed Hi-C or from the pseudo-population
 */
typedef cmxVecCol cmxContactFreqs_t;

/**
 * Running stat tracker type
 */
#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
typedef cmxMatrixUtils::running_stat<cmxReal> cmxRunStat_t;
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

#endif  // cmxModelTypes_h
