//****************************************************************************
// cmxDpStickMpopMoco.h
//****************************************************************************

/**
 * Truncated Dirichlet stick breaking process
 *
 * See Section 4, pg. 383 of:
 *
 *  Ishwaran, Hemant, and Mahmoud Zarepour. "Markov chain Monte Carlo in
 *      approximate Dirichlet and beta two-parameter process hierarchical
 *      models." Biometrika 87, no. 2 (2000): 371-390.
 *
 *  A stick-breaking truncation of size N = 50 is sufficient for simulating
 *  Dirichlet process concentration alphas in range (0, 10] (pg. 385 & Fig. 5).
 */

#ifndef cmxDpStickMpopMoco_h
#define cmxDpStickMpopMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxDirichletDistribution.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxRand.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxDpStickMpopMoco
//****************************************************************************

/**
 * Multi-population mixture PMF sampled according to a truncated Dirichlet
 * process as described in Ishwaran text
 */
template <typename t_Glue>
class cmxDpStickMpopMoco : public cmxMoco<t_Glue> {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Component is random and requires sampling
     */
    enum { is_random = cmxTRUE };

    /**
     * Default constructor
     */
    cmxDpStickMpopMoco()
        : m_pmf(CMX_DEFAULT_MIX_SIZE, cmxMatrixUtils::fill::zeros),
          m_lnpmf(CMX_DEFAULT_MIX_SIZE, cmxMatrixUtils::fill::zeros) {}

    /**
     * Initialization from user configuration. Model component should seek to
     * initialize to single population, independent null model where possible.
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void init(cmxSpConstConfig_t config,
                      model_t& model,
                      sim_t& sim CMX_THREAD_ID_PARAM) {
        // Assign almost all probability mass to first cluster and very small
        // probability to remaining clusters for numerical stability
        const cmxUInt MIX_SIZE = cmxConfigUtils::get_mix_size(config);
        cmxAssert(MIX_SIZE >= CMX_TO_UINT(1));
        m_pmf.set_size(MIX_SIZE);
        m_pmf.fill(CMX_REAL_EPS);
        const cmxReal p_rem =
            CMX_TO_REAL(MIX_SIZE - CMX_TO_UINT(1)) * CMX_REAL_EPS;
        m_pmf.at(0) = CMX_TO_REAL(1.0) - p_rem;
        cmxAssert(is_valid_pmf(m_pmf));
        m_lnpmf = cmxMatrixUtils::log(m_pmf);
        cmxAssert(cmxMatrixUtils::is_finite(m_lnpmf));
    }

    /**
     * Initialize to a random state, should be called after init()
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void randomize(cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {
        // Get concentration parameter
        const cmxReal alpha = model.get_Dp_stick_alpha().get_scalar(sim);
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        // Sample from prior
        cmxRand& rng = cmxRng;
        this->sample_betas_prior(rng, alpha);
        this->calc_mix_pmf_from_betas();
    }

    /**
     * Virtual Gibbs posterior update, should only be called if model
     * component is random! Generates a sample from the posterior distribution
     * P(Xi | X-i) where Xi is i-th random variable and X-i is set of of all
     * random variables except for Xi in the model.
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void Gibbs_posterior(model_t& model,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {
        // Get concentration parameter
        const cmxReal alpha = model.get_Dp_stick_alpha().get_scalar(sim);
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        // Sample from posterior
        cmxRand& rng = cmxRng;
        this->sample_betas_posterior(rng, alpha, model, sim);
        this->calc_mix_pmf_from_betas();
    }

    /**
     * Intended to assess Gibbs sampler convergence ideally showing that chain
     * is exploring high probability regions. This may be an expensive call to
     * make and should be used sparingly at regular intervals.
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    virtual cmxReal logscore(model_t& model, sim_t& sim) {
        // Get concentration parameter
        const cmxReal alpha = model.get_Dp_stick_alpha().get_scalar(sim);
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        // Cache mixture size - 2
        cmxAssertPosEq(this->get_mix_pmf().size(),
                       sim.get_shared_ro().get_Clust().get_mix_size());
        // Use integer to avoid unsigned underflow for single population model
        const int MIX_SIZE_MINUS_2 = int(m_pmf.n_elem) - 2;
        // Stick-breaking process follows generalized Dirichlet distribution
        // with a_i = 1, b_i = alpha concentration for all i (See Ishwaran
        // text, section 4, pg. 383, eq. 26)
        cmxReal score =
            (alpha - CMX_TO_REAL(1.0)) * this->get_mix_tail_lnprob();
        cmxReal cmf = CMX_TO_REAL(0.0);
        for (int i = 0; i < MIX_SIZE_MINUS_2; ++i) {
            cmf += m_pmf.at(CMX_TO_MAT_SZ_T(i));
            score -= log(CMX_TO_REAL(1.0) - cmf);
        }
        // Add normalizing constant, note the following:
        //  The Gamma factorial term with all a_i = 1 and b_i = alpha:
        //      Gamma(a_k + b_k) / (Gamma(a_k) * Gamma(b_k))
        //          = Gamma(1 + alpha) / (Gamma(1) * Gamma(alpha))
        //          = Gamma(1 + alpha) / Gamma(alpha) since Gamma(1) = 0 != 1
        //          = alpha
        //      Therefore, the Gamma factorial product sequence :
        //          Pi_k=1_to_(N - 1)
        //              { Gamma(a_k + b_k) / (Gamma(a_k) * Gamma(b_k)) }
        //          = Pi_k=1_to_(N - 1) { alpha }
        //          = alpha ^ (N - 1)
        // (SEE ALSO notes in cmxDpStickAlphaGammaMoco.h)
        //  => log(alpha ^ (N - 1)) = (N - 1) * log(alpha)
        const cmxMatSz_t MIX_SIZE_MINUS_1 = m_pmf.n_elem - CMX_TO_MAT_SZ_T(1);
        score += (log(alpha) * CMX_TO_REAL(MIX_SIZE_MINUS_1));
        return score;
    }

    // Accessor(s)

    /**
     * @return Mixture probability mass function (PMF)
     */
    inline const cmxVecCol& get_mix_pmf() const {
        cmxAssert(is_valid_pmf(m_pmf));
        return m_pmf;
    }

    /**
     * @return Mixture probability mass function (PMF), log-transformed
     */
    inline const cmxVecCol& get_mix_lnpmf() const {
        cmxAssert(this->check_consistency());
        return m_lnpmf;
    }

    /**
     * @return Cached log tail probability of truncated stick-breaking process
     */
    inline cmxReal get_mix_tail_lnprob() const {
        return this->get_mix_lnpmf().at(m_lnpmf.n_elem - CMX_TO_MAT_SZ_T(1));
    }

    /**
     * Interface for querying mixture component weight (probability mass)
     * @return Probability mass assigned to 'mix_index' component
     */
    inline cmxReal get_mix_weight(const cmxUInt mix_index) const {
        cmxAssertBounds(mix_index, CMX_TO_UINT(0), CMX_TO_UINT(m_pmf.size()));
        return this->get_mix_pmf().at(CMX_TO_MAT_SZ_T(mix_index));
    }

    /**
     * Interface for querying mixture entropy
     * @return Entropy of mixture PMF
     */
    inline cmxReal get_mix_ent() const {
        const cmxReal neg_ent =
            cmxMatrixUtils::dot(this->get_mix_pmf(), this->get_mix_lnpmf());
        return -neg_ent;
    }

private:
#ifdef CMX_BUILD_ENABLE_ASSERT
    /**
     * @return TRUE if vector of probability masses is proper, FALSE o/w
     */
    static bool is_valid_pmf(const cmxVecCol& pmf) {
        cmxAssert(!pmf.empty());
        cmxReal cmf = CMX_TO_REAL(0.0);
        const cmxMatSz_t MIX_SIZE = pmf.n_elem;
        for (cmxMatSz_t i = 0; i < MIX_SIZE; ++i) {
            cmxAssertRealBoundsInc(
                pmf.at(i), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            cmf += pmf.at(i);
        }
        const bool is_valid = CMX_REAL_CMP_EQ(cmf, CMX_TO_REAL(1.0));
        cmxAssert(is_valid);
        return is_valid;
    }

    /**
     * @return m_lnpmf == log(m_pmf)
     */
    bool check_consistency() const {
        cmxAssertPosEq(m_pmf.n_elem, m_lnpmf.n_elem);
        cmxAssert(cmxMatrixUtils::is_finite(m_lnpmf));
        // @HACK!
        // For some reason, the following code seems to be tripping erroneous
        // asserts in Windows Release builds (was not able to repro in Debug):
        //
        //  cmxAssert(cmxMatrixUtils::all(
        //      cmxMatrixUtils::log(this->get_mix_pmf()) == m_lnpmf));
        //
        // Am wondering if sometimes log is computed using vector operations
        // and in other cases using scalar operations? If so, this maybe could
        // lead to small (negligible) floating point differences. Hence, we
        // are manually checking that each element is within the real-valued
        // tolerance using a loop.
        const cmxVecCol tmp_lnpmf = cmxMatrixUtils::log(this->get_mix_pmf());
        for (cmxMatSz_t i = 0; i < tmp_lnpmf.n_elem; ++i) {
            const cmxReal a = m_lnpmf.at(i);
            const cmxReal b = tmp_lnpmf.at(i);
            if (!CMX_REAL_CMP_EQ(a, b)) {
                cmxAssert(false && "Dp Stick Log PMF Mismatch!");
                return false;
            }
        }
        return true;
    }
#endif  // CMX_BUILD_ENABLE_ASSERT

    /**
     * Samples N-1 betas (and assigns last element to 1) from posterior Beta
     * distribution to model truncated Dirichlet process
     * @param rng - Random number generator
     * @param alpha - Dirichlet stick-breaking process concentration parameter
     * @param model - owning model
     * @param sim - outer simulation
     */
    void sample_betas_posterior(cmxRand& rng,
                                const cmxReal alpha,
                                model_t& model,
                                sim_t& sim) {
        cmxAssertPosEq(m_pmf.size(),
                       sim.get_shared_ro().get_Clust().get_mix_size());
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        const cmxMatSz_t MIX_SIZE_MINUS_1 = m_pmf.n_elem - CMX_TO_MAT_SZ_T(1);
        const cmxUInt NUM_BETA_PARAMS = 2;
        cmxReal v_alpha[NUM_BETA_PARAMS];
        cmxReal v_theta[NUM_BETA_PARAMS];
        const cmxUInt PSEUDOPOP_SIZE =
            sim.get_shared_ro().get_Pseudopop().get_pseudopop_size();
        cmxAssert(PSEUDOPOP_SIZE > CMX_TO_UINT(1));
        cmxUInt running_pseudopop_size = CMX_TO_UINT(0);
        for (cmxMatSz_t i = 0; i < MIX_SIZE_MINUS_1; ++i) {
            // Obtain size of cluster
            const cmxUInt k_size =
                model.get_Clust().get_pseudopop_size(CMX_TO_UINT(i), sim);
            // Determine posterior parameters according to Ishwaran text,
            // section 4, pg. 383
            running_pseudopop_size += k_size;
            cmxAssert(running_pseudopop_size <= PSEUDOPOP_SIZE);
            v_alpha[0] = CMX_TO_REAL(k_size + 1);
            v_alpha[1] =
                alpha + CMX_TO_REAL(PSEUDOPOP_SIZE - running_pseudopop_size);
            cmxDirichletDistribution::sample(
                rng, NUM_BETA_PARAMS, v_alpha, v_theta);
            m_pmf.at(i) = v_theta[0];
            cmxAssertBoundsInc(m_pmf.at(i), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        }
        cmxAssertPosEq(
            (running_pseudopop_size +
             model.get_Clust().get_pseudopop_size(MIX_SIZE_MINUS_1, sim)),
            PSEUDOPOP_SIZE);
        m_pmf.at(MIX_SIZE_MINUS_1) = CMX_TO_REAL(1.0);
    }

    /**
     * Samples N-1 betas (and assigns last element to 1) to model truncated
     * Dirichlet process from prior Beta distribution
     * @param rng - Random number generator
     * @param alpha - Dirichlet stick-breaking process concentration parameter
     */
    void sample_betas_prior(cmxRand& rng, const cmxReal alpha) {
        cmxAssert(!m_pmf.empty());
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        const cmxMatSz_t MIX_SIZE_MINUS_1 = m_pmf.n_elem - CMX_TO_MAT_SZ_T(1);
        const cmxUInt NUM_BETA_PARAMS = 2;
        const cmxReal V_ALPHA[NUM_BETA_PARAMS] = {CMX_TO_REAL(1.0), alpha};
        cmxReal v_theta[NUM_BETA_PARAMS];
        for (cmxMatSz_t i = 0; i < MIX_SIZE_MINUS_1; ++i) {
            cmxDirichletDistribution::sample(
                rng, NUM_BETA_PARAMS, V_ALPHA, v_theta);
            m_pmf.at(i) = v_theta[0];
            cmxAssertBoundsInc(m_pmf.at(i), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        }
        m_pmf.at(MIX_SIZE_MINUS_1) = CMX_TO_REAL(1.0);
    }

    /**
     * Computes mixture probability masses using samples from Beta prior or
     * posterior Beta distribution assumed stored in 'm_pmf' data member; will
     * also cache mixture log probability
     */
    void calc_mix_pmf_from_betas() {
        // Truncated stick-breaking process
        cmxAssert(!m_pmf.empty());
        cmxAssertRealBoundsInc(m_pmf.at(0), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        const cmxMatSz_t MIX_SIZE_MINUS_1 = m_pmf.n_elem - CMX_TO_MAT_SZ_T(1);
        cmxReal cmf = CMX_TO_REAL(0.0);
        for (cmxMatSz_t i = CMX_TO_MAT_SZ_T(0); i < MIX_SIZE_MINUS_1; ++i) {
            cmxAssertRealBoundsInc(
                m_pmf.at(i), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            m_pmf.at(i) = (CMX_TO_REAL(1.0) - cmf) * m_pmf.at(i);
            cmxAssertRealBoundsInc(
                m_pmf.at(i), CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            cmf += m_pmf.at(i);
        }
        cmxAssertRealBoundsInc(cmf, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
        m_pmf.at(MIX_SIZE_MINUS_1) = (CMX_TO_REAL(1.0) - cmf);
        // For numerical stability, we subtract a small epsilon from highest
        // probability cluster and assign to remaining clusters; this avoids
        // 0 probability clusters which have log-probability(0) = -inf which
        // leads to all sorts of downstream issues! Assuming negligible bias
        // is introduced by this procedure
        {
            const cmxMatSz_t i_max = cmxMatrixUtils::index_max(m_pmf);
            cmxAssertBounds(i_max, CMX_TO_MAT_SZ_T(0), m_pmf.n_elem);
            m_pmf += CMX_REAL_EPS;
            m_pmf.at(i_max) -= CMX_TO_REAL(m_pmf.n_elem) * CMX_REAL_EPS;
            cmxAssert(m_pmf.at(i_max) > CMX_TO_REAL(0.0));
        }
        cmxAssert(is_valid_pmf(m_pmf));
        // Compute log-transform
        m_lnpmf = cmxMatrixUtils::log(m_pmf);
        cmxAssert(cmxMatrixUtils::is_finite(m_lnpmf));
    }

    /**
     * Mixture PMF sampled according to truncated Dirichlet process
     */
    cmxVecCol m_pmf;

    /**
     * Log mixture PMF
     */
    cmxVecCol m_lnpmf;
};

#endif  // cmxDpStickMpopMoco_h
