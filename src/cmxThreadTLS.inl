//****************************************************************************
// cmxThreadTLS.inl
//****************************************************************************

/**
 * Used for (re)setting TLS accessor macros based on build conditions
 */

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_THREADS

// Wrapping multi-line macros in do { ... } while(0) so that they behave
// similar to single-line statements.
// http://www.pixelstech.net/article/1390482950-do-%7B-%7D-while-%280%29-in-macros

/**
 * Undefine any previously existing macros
 */

#ifdef CMX_DECLARE_TLS_DATA
#   undef CMX_DECLARE_TLS_DATA
#endif  // CMX_DECLARE_TLS_DATA

#ifdef CMX_ACCESS_TLS_DATA
#   undef CMX_ACCESS_TLS_DATA
#endif  // CMX_ACCESS_TLS_DATA

#ifdef CMX_INIT_TLS_DATA_0_PARAM
#   undef CMX_INIT_TLS_DATA_0_PARAM
#endif  // CMX_INIT_TLS_DATA_0_PARAM

#ifdef CMX_INIT_TLS_DATA_FOR_EACH_1_PARAM
#   undef CMX_INIT_TLS_DATA_FOR_EACH_1_PARAM
#endif  // CMX_INIT_TLS_DATA_FOR_EACH_1_PARAM

#ifdef CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM
#   undef CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM
#endif  // CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM

#ifdef CMX_TLS_DATA_FOR_EACH_0_PARAM
#   undef CMX_TLS_DATA_FOR_EACH_0_PARAM
#endif  // CMX_TLS_DATA_FOR_EACH_0_PARAM

#define CMX_DECLARE_TLS_DATA(type, name) std::vector<type> name

#define CMX_ACCESS_TLS_DATA(name) name[CMX_THREAD_ID_VAR_NAME]

/**
 * @Warning
 * TLS Data is allocated under the assumption that the worker threads and main
 * thread are never running/accessing TLS at the same time. If this is ever
 * *NOT* the case - then we must allocate an extra slot for the main thread,
 * else it just reuses the TLS data allocated for the 0th worker thread.
 *
 * Also, if main and worker are accessing TLS, then the thread ids of the
 * worker threads must start from 1 (and not 0).
 */

#define CMX_INIT_TLS_DATA_0_PARAM(name)                       \
    do {                                                      \
        name.clear();                                         \
        cmxAssert(cmxG::CMX_NUM_WORKER_THREADS_VAR_NAME > 0); \
        name.resize(cmxG::CMX_NUM_WORKER_THREADS_VAR_NAME);   \
    } while (0)

#define CMX_INIT_TLS_DATA_FOR_EACH_1_PARAM(name, memb_func, param_1) \
    do {                                                             \
        CMX_INIT_TLS_DATA_0_PARAM(name);                             \
        for (cmxUInt it_tls_ = 0;                                    \
             it_tls_ < cmxG::CMX_NUM_WORKER_THREADS_VAR_NAME;        \
             ++it_tls_) {                                            \
            name[it_tls_].memb_func(param_1);                        \
        }                                                            \
    } while (0)

#define CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM(name, memb_func, param_1, param_2) \
    do {                                                                      \
        CMX_INIT_TLS_DATA_0_PARAM(name);                                      \
        for (cmxUInt it_tls_ = 0;                                             \
             it_tls_ < cmxG::CMX_NUM_WORKER_THREADS_VAR_NAME;                 \
             ++it_tls_) {                                                     \
            name[it_tls_].memb_func(param_1, param_2);                        \
        }                                                                     \
    } while (0)

#define CMX_TLS_DATA_FOR_EACH_0_PARAM(name, memb_func)        \
    do {                                                      \
        for (cmxUInt it_tls_ = 0;                             \
             it_tls_ < cmxG::CMX_NUM_WORKER_THREADS_VAR_NAME; \
             ++it_tls_) {                                     \
            name[it_tls_].memb_func();                        \
        }                                                     \
    } while (0)

#else

#include "cmxThreadNullTLS.inl"

#endif  // CMX_BUILD_ENABLE_THREADS
