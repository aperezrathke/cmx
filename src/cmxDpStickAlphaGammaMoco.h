//****************************************************************************
// cmxDpStickAlphaGammaMoco.h
//****************************************************************************

/**
 * Gamma-distributed alpha concentration parameter (> 0) for generalized
 * Dirichlet stick-breaking process (truncated) approximation to infinite
 * Dirichlet process.
 *
 * From:
 *
 * Murugiah, Siva, and Trevor Sweeting. "Selecting the precision parameter
 *  prior in Dirichlet process mixture models." Journal of Statistical
 *  Planning and Inference 142, no. 7 (2012): 1947-1959.
 *
 * which cites:
 *
 * Antoniak, Charles E. "Mixtures of Dirichlet processes with applications to
 *  Bayesian nonparametric problems." The annals of statistics (1974):
 *  1152-1174.
 *
 * These sources state:
 *
 *  For concentration alpha and pseudo-population of size M, the expected
 *  number of clusters K is:
 *
 *  E(K|M,alpha) (approx)= alpha * ln{(M+alpha)/alpha}
 *
 * Therefore, the expected number of clusters is monotonic relative to alpha.
 *
 * See also:
 *
 *  Sudderth, Erik Blaine. "Graphical models for visual object recognition
 *      and tracking." PhD diss., Massachusetts Institute of Technology, 2006.
 *
 * which states: K -> alpha * log{M} as M -> infinity (Ch.2, pg. 108)
 *
 * According to MCMC approximation for Dirichlet process:
 *
 *  Ishwaran, Hemant, and Mahmoud Zarepour. "Markov chain Monte Carlo in
 *      approximate Dirichlet and beta two-parameter process hierarchical
 *      models." Biometrika 87, no. 2 (2000): 371-390.
 *
 *  A stick-breaking truncation of size N = 50 is sufficient for simulating
 *  alphas in range (0, 10] (pg. 385 and Fig. 5).
 */

/**
 * From Ishwaran & Zarepour, Section 4.5, the posterior P(alpha|p) - where
 * alpha is concentration parameter and p is set of mixture weights from a
 * generalized Dirichlet process stick breaking process - has form:
 *
 *  f(alpha|p) prop_to alpha^(N-1)(p_N)^(alpha-1)*f(alpha); where N is
 *      truncation size, p_N is Nth mixture weight, and f(alpha) is prior on
 *      alpha
 *
 * Claim: If f(alpha) is a Gamma(shape=v1,rate=v2) prior, then posterior:
 *
 *  alpha|p ~ Gamma(shape=N+v1-1, rate=v2-ln{p_N}), where Gamma distribution
 *      is of form:
 *          {(rate^shape)/(Gamma(shape))}*{alpha^(shape-1)}*exp{-rate*alpha}
 *      where Gamma is the continuous factorial function
 *
 * Proof:
 *
 *  1) f(alpha|p) prop_to alpha^(N-1)(p_N)^(alpha-1)*f(alpha); this part is
 *      trivial, just plug a_i=1, and b_i = alpha into Eq. 26 of Ishwaran text
 *
 *      i. Note the Gamma factorial term with all a_i = 1 and b_i = alpha:
 *          Gamma(a_k + b_k) / ( Gamma(a_k) * Gamma(b_k) )
 *              = Gamma(1 + alpha) / ( Gamma(1) * Gamma(alpha) )
 *              = Gamma(1 + alpha) / Gamma(alpha) since Gamma(1) = 0! = 1
 *              = alpha
 *          Therefore, the Gamma factorial product sequence:
 *              Pi_k=1_to_(N-1)
 *                  { Gamma(a_k + b_k) / ( Gamma(a_k) * Gamma(b_k) ) }
 *              = Pi_k=1_to_(N-1) { alpha }
 *              = alpha^(N-1)
 *      ii. Note the product sequence with all a_i = 1:
 *          p_1^(a_1 - 1) ... p_(N-1)^(a_(N-1) - 1)
 *              = p_1^0 ... p_(N-1)^0
 *              = 1
 *      iii. Note the product sequence with all a_i = 1 and b_i = alpha:
 *          (1 - P_1)^(b_1 - (a_2 + b2))
 *                  ... (1 - P_(N-2))^(b_(N-2) - (a_(N-1) + b_(N-1))
 *              = (1 - P_1)^(-1) ... (1 - P_(N-2)^(-1)) has no alpha
 *                  and therefore gets folded into constant
 *
 *  2) Apply prior f(alpha) =
 *          Gamma(shape=v1,rate=v2) prop_to {alpha^(v1-1)}*exp{-v2 * alpha}
 *      => f(alpha|p) prop_to
 *              {alpha^(N-1)}*{alpha^(v1-1)}*{p_N^(alpha-1)}*exp{-v2*alpha}
 *
 *  3) Combine alpha terms:
 *      => f(alpha|p) prop_to
 *              {alpha^([N+v1-1]-1)}*{p_N^(alpha-1)}*exp{-v2*alpha}
 *
 *  4) Replace {p_N^(alpha-1)} as exp{(alpha-1)ln{p_N}}:
 *      => f(alpha|p) prop_to
 *              {alpha^([N+v1-1]-1)}*exp{(alpha-1)*ln{p_N} - v2*alpha}
 *
 *  5) Manipulate exp terms:
 *      => f(alpha|p) prop_to
 *              {alpha^([N+v1-1]-1)}*exp{(alpha-1)ln{p_N}}*exp{-v2*alpha}
 *      => f(alpha|p) prop_to
 *              {alpha^([N+v1-1]-1)}*exp{(alpha*ln{p_N}-ln{p_N}-v2*alpha}
 *      => f(alpha|p) prop_to
 *              {alpha^([N+v1-1]-1)}*exp{(alpha*(ln{p_N}-v2)}*exp{-ln{p_N}}
 *
 *  6) The term exp{-ln{p_N}} is a constant with no dependence on alpha and
 *      therefore is simply folded into the partition constant and can be
 *      removed from subsequent expressions:
 *      => f(alpha|p) prop_to
 *              {alpha^([N+v1-1]-1)}*exp{(alpha*(ln{p_N}-v2)}
 *
 *  7) Put in standard form, multiply exponent by (-1)*(-1):
 *      => f(alpha|p) prop_to
 *              {alpha^([N+v1-1]-1)}*exp{((-1)*alpha*(ln{p_N}-v2)*(-1)}
 *      => f(alpha|p) prop_to
 *              {alpha^([N+v1-1]-1)}*exp{(-alpha*(v2 - ln{p_N})}
 *
 * This is the improper density for a Gamma(shape=N+v1-1, rate=v2-ln{p_N})
 *
 * Note, that certain libraries (e.g. Boost) assume Gamma distribution with a
 * 'scale' instead of 'rate', see:
 *      https://en.wikipedia.org/wiki/Gamma_distribution
 * and therefore expect to receive '1/rate' as input for 'scale' term.
 */

/**
 * Typically, 'vague|uninformative' terms for shape (v1) and rate (v2) are
 * chosen such that the prior is relatively flat with high variance.
 *
 * From Sudderth's thesis, alpha ~ Gamma(shape=0.2, rate=0.1) empirically
 * performed well. Note, Boost C++ library expects scale = 1/rate as input
 */

#ifndef cmxDpStickAlphaGammaMoco_h
#define cmxDpStickAlphaGammaMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxRand.h"
#include "cmxTypes.h"

// For evaluating log-score of the alpha parameter
#include <boost/math/distributions/gamma.hpp>

//****************************************************************************
// cmxDpStickAlphaGammaMoco
//****************************************************************************

/**
 * Simulation-level read-only shared data, provides user configured hyper-
 * parameters (shape, scale) for Gamma prior on Dirichlet process alpha where
 *
 *  E[Gamma(shape,scale)] = shape * scale
 *  Var[Gamma(shape,scale)] = shape * scale^2
 */
class cmxDpStickAlphaGammaSharcoRo {
public:
    /**
     * Default constructor
     */
    cmxDpStickAlphaGammaSharcoRo()
        : m_shape(CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SHAPE),
          m_scale(CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SCALE) {}

    /**
     * Initializes shared resource(s) - captures user-supplied alpha
     * @param config - user options
     */
    void init(cmxSpConstConfig_t config) {
        // Reset state
        this->clear();
        cmxAssert(m_shape == CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SHAPE);
        cmxAssert(m_scale == CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SCALE);
        // Read user configuration for shape
        config->read_into(m_shape, cmxOpt_Dp_stick_alpha_gamma_shape);
        // Error if non-positive shape encountered
        if (m_shape <= CMX_TO_REAL(0.0)) {
            cmxLogf(
                "Error: Dp stick alpha gamma shape must be > 0! Exiting.\n");
            exit(cmxExitCode_invalid_dp_stick_alpha);
        }
        // Read user configuration for scale
        config->read_into(m_scale, cmxOpt_Dp_stick_alpha_gamma_scale);
        // Error if non-positive shape encountered
        if (m_scale <= CMX_TO_REAL(0.0)) {
            cmxLogf(
                "Error: Dp stick alpha gamma scale must be > 0! Exiting.\n");
            exit(cmxExitCode_invalid_dp_stick_alpha);
        }
    }

    /**
     * Reset read-only state
     */
    void clear() {
        m_shape = CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SHAPE;
        m_scale = CMX_DEFAULT_DP_STICK_ALPHA_GAMMA_SCALE;
    }

    /**
     * @return user configured Gamma shape
     */
    inline cmxReal get_shape() const {
        cmxAssert(m_shape > CMX_TO_REAL(0.0));
        return m_shape;
    }

    /**
     * @return user configured Gamma scale
     */
    inline cmxReal get_scale() const {
        cmxAssert(m_scale > CMX_TO_REAL(0.0));
        return m_scale;
    }

private:
    /**
     * Gamma distribution parameters shape and scale, where
     *
     * E[Gamma(shape,scale)] = shape * scale
     * Var[Gamma(shape,scale)] = shape * scale^2
     *
     * and shape > 0, scale > 0
     */
    cmxReal m_shape;
    cmxReal m_scale;
};

/**
 * Root-level random node representing alpha concentration for a (generalized)
 * Dirichlet stick-breaking process which is monotonic to expected number of
 * clusters. This model component assumes alpha ~ Gamma(shape, scale) as this
 * is conjugate to the stick-breaking process!
 */
template <typename t_Glue>
class cmxDpStickAlphaGammaMoco : public cmxMoco<t_Glue> {
private:
    /**
     * Gamma distribution type for sampling
     */
    typedef cmxRandom::gamma_distribution<cmxReal> gamma_sampler_t;

    /**
     * Gamma distribution type for evaluating pdf
     */
    typedef boost::math::gamma_distribution<cmxReal> gamma_pdf_t;

public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Component is random and requires sampling
     */
    enum { is_random = cmxTRUE };

    /**
     * Default constructor
     */
    cmxDpStickAlphaGammaMoco() : m_alpha(CMX_DEFAULT_DP_STICK_ALPHA) {}

    /**
     * Initialization from user configuration if necessary
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void init(cmxSpConstConfig_t config,
                      model_t& model,
                      sim_t& sim CMX_THREAD_ID_PARAM) {
        m_alpha = CMX_DEFAULT_DP_STICK_ALPHA;
        cmxAssert(m_alpha > CMX_TO_REAL(0.0));
    }

    /**
     * Initialize to a random state, should be called after init()
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void randomize(cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {
        // Query read-only shared data for Gamma shape and scale
        const cmxReal shape = this->get_prior_shape(sim);
        const cmxReal scale = this->get_prior_scale(sim);
        // Sample from Gamma(shape, scale) prior
        cmxRand& rng = cmxRng;
        gamma_sampler_t gamma_sampler(shape, scale);
        m_alpha = rng.sample(gamma_sampler);
        cmxAssert(m_alpha > CMX_TO_REAL(0.0));
    }

    /**
     * Virtual Gibbs posterior update, should only be called if model
     * component is random! Generates a sample from the posterior distribution
     * P(Xi | X-i) where Xi is i-th random variable and X-i is set of of all
     * random variables except for Xi in the model.
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void Gibbs_posterior(model_t& model,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {
        /**
         * Sample from Gamma posterior as given in Section 4.5, pg. 387
         *
         *  Ishwaran, Hemant, and Mahmoud Zarepour. "Markov chain Monte Carlo in
         *      approximate Dirichlet and beta two - parameter process
         * hierarchical models." Biometrika 87, no. 2 (2000): 371-390.
         *
         * Note, (see proof at top of file) Gamma distribution in Ishwaran
         * text appears to be for the Gamma(shape, rate) form and therefore we
         * need to use scale = 1/rate when passing to our C++ library methods
         *
         * See:
         *
         *  http://www.cplusplus.com/reference/random/gamma_distribution/
         *  https://en.wikipedia.org/wiki/Gamma_distribution
         *
         * Posterior has form:
         *
         *  alpha|p ~ Gamma(shape'=N+v1-1, rate'=v2-ln{p_N}) where v1, v2 are
         *      the prior's shape and rate respectively, N is mixture size,
         *      and p_N is the tail probability assigned to the last cluster
         *
         * To convert posterior rate' to posterior scale':
         *      scale' = 1/rate'
         *             = 1/(v2 - ln{p_N})
         *             = 1/((1/scale) - ln{p_N})
         *             = scale/(1 - scale*ln{p_N}) (source: Wolfram Alpha;
         *                                          multiply by scale/scale)
         *          where scale is the prior's user-defined scale and last
         *          expression has less divisions when starting with a scale
         */

        //////////////////////////////////////////////////////////////////////
        // Compute posterior shape

        const cmxUInt mix_size = sim.get_shared_ro().get_Clust().get_mix_size();
        cmxAssert(mix_size > CMX_TO_UINT(0));
        const cmxReal prior_shape = this->get_prior_shape(sim);
        const cmxReal post_shape =
            CMX_TO_REAL(mix_size) + prior_shape - CMX_TO_REAL(1.0);
        cmxAssert(post_shape > CMX_TO_REAL(0.0));

        //////////////////////////////////////////////////////////////////////
        // Compute posterior scale

        const cmxReal prior_scale = this->get_prior_scale(sim);
        // Query log tail probability assigned to last cluster
        const cmxReal tail_lnprob = model.get_Dp_stick().get_mix_tail_lnprob();
        cmxAssert(tail_lnprob <= CMX_TO_REAL(0.0));
        const cmxReal post_scale =
            prior_scale / (CMX_TO_REAL(1.0) - (prior_scale * tail_lnprob));
        cmxAssert(post_scale > CMX_TO_REAL(0.0));

        //////////////////////////////////////////////////////////////////////
        // Sample from posterior

        cmxRand& rng = cmxRng;
        gamma_sampler_t gamma_sampler(post_shape, post_scale);
        m_alpha = rng.sample(gamma_sampler);
        cmxAssert(m_alpha > CMX_TO_REAL(0.0));
    }

    /**
     * Intended to assess Gibbs sampler convergence ideally showing that chain
     * is exploring high probability regions. This may be an expensive call to
     * make and should be used sparingly at regular intervals.
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    virtual cmxReal logscore(model_t& model, sim_t& sim) {
        const gamma_pdf_t gamma_pdf(this->get_prior_shape(sim),
                                    this->get_prior_scale(sim));
        const cmxReal score =
            log(boost::math::pdf(gamma_pdf, this->get_scalar(sim)));
        return score;
    }

    // Accessor(s)

    /**
     * @return sampled alpha concentration parameter
     */
    inline cmxReal get_scalar(const sim_t& sim) const {
        cmxAssert(m_alpha > CMX_TO_REAL(0.0));
        return m_alpha;
    }

private:
    /**
     * @return read-only shape term from Gamma(shape, scale) prior
     */
    static inline cmxReal get_prior_shape(const sim_t& sim) {
        const cmxReal shape =
            sim.get_shared_ro().get_Dp_stick_alpha().get_shape();
        cmxAssert(shape > CMX_TO_REAL(0.0));
        return shape;
    }

    /**
     * @return read-only scale term from Gamma(shape, scale) prior
     */
    static inline cmxReal get_prior_scale(const sim_t& sim) {
        const cmxReal scale =
            sim.get_shared_ro().get_Dp_stick_alpha().get_scale();
        cmxAssert(scale > CMX_TO_REAL(0.0));
        return scale;
    }

    /**
     * Stochastic Dirichlet process concentration parameter
     */
    cmxReal m_alpha;
};

#endif  // cmxDpStickAlphaGammaMoco_h
