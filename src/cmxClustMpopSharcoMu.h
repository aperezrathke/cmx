//****************************************************************************
// cmxClustMpopSharcoMu.h
//****************************************************************************

/**
 * @brief Mutable shared buffers for multi-population cluster association.
 */

#ifndef cmxClustMpopSharcoMu_h
#define cmxClustMpopSharcoMu_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxConfigUtils.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxClustMpopSharcoMu
//****************************************************************************

/**
 * Multi-population cluster mutable shared buffer(s)
 */
class cmxClustMpopSharcoMu {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     * @param sim - outer simulation
     */
    template <typename sim_t>
    void init(cmxSpConstConfig_t config, const sim_t& sim) {
        const cmxUInt MIX_SIZE = cmxConfigUtils::get_mix_size(config);
        cmxAssert(MIX_SIZE > CMX_TO_UINT(0));
        // Pre-allocate posterior PMF buffer
        m_mix_post_pmf_buf.zeros(CMX_TO_MAT_SZ_T(MIX_SIZE));
    }

    /**
     * Clear shared buffer(s)
     */
    void clear() { m_mix_post_pmf_buf.clear(); }

    /**
     * @return Handle to scratch buffer for computing posterior mixture
     *  probabilities at each categorical cluster assignment random variable
     */
    inline cmxVecCol& get_mix_post_pmf_buf() const {
        return m_mix_post_pmf_buf;
    }

private:
    /**
     * Scratch buffer for computer posterior mixture probabilities at each
     *  categorical cluster assignment random variable
     */
    mutable cmxVecCol m_mix_post_pmf_buf;
};

#endif  // cmxClustMpopSharcoMu_h
