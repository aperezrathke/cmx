//****************************************************************************
// cmxBnetDagKappaDetrMoco.h
//****************************************************************************

/**
 * @brief Real-valued scalar hyper-parameter for edge prior on Bayesian
 *  network DAG structure.
 *      Detr -> 'Deterministic', this component is non-random
 */

#ifndef cmxBnetDagKappaDetrMoco_h
#define cmxBnetDagKappaDetrMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxScalarDetrMoco.h"
#include "cmxTypes.h"

#include <math.h>

//****************************************************************************
// cmxBnetDagKappaDetrMoco
//****************************************************************************

/**
 * Simulation-level read-only shared data, provides user configured DAG kappa
 * (a hyper-parameter intended for prior on DAG network number of edges)
 */
class cmxBnetDagKappaDetrSharcoRo {
public:
    /**
     * Default constructor
     */
    cmxBnetDagKappaDetrSharcoRo()
        : m_log_kappa(log(CMX_DEFAULT_BNET_DAG_KAPPA)) {}

    /**
     * Initializes shared resource(s) - caches log transformed kappa
     * @param config - user options
     */
    void init(cmxSpConstConfig_t config) {
        m_log_kappa = CMX_DEFAULT_BNET_DAG_KAPPA;
        config->read_into(m_log_kappa, cmxOpt_Bnet_dag_kappa);
        if ((m_log_kappa <= CMX_TO_REAL(0.0)) ||
            (m_log_kappa > CMX_TO_REAL(1.0))) {
            cmxLogf(
                "Warning: Bayesian network DAG kappa must be in range "
                "(0,1].\n\t-Setting to default: %f\n",
                ((double)(CMX_DEFAULT_BNET_DAG_KAPPA)));
            m_log_kappa = CMX_DEFAULT_BNET_DAG_KAPPA;
        }
        cmxAssert(m_log_kappa > CMX_TO_REAL(0.0));
        cmxAssert(m_log_kappa <= CMX_TO_REAL(1.0));
        m_log_kappa = log(m_log_kappa);
        cmxAssert(m_log_kappa <= CMX_TO_REAL(0.0));
    }

    /**
     * Clear state
     */
    void clear() { m_log_kappa = CMX_DEFAULT_BNET_DAG_KAPPA; }

    /**
     * @return user configured log-transformed kappa scalar
     */
    inline cmxReal get_log_scalar() const { return m_log_kappa; }

private:
    /**
     * log-transformed DAG kappa scalar hyper-parameter
     */
    cmxReal m_log_kappa;
};

/**
 * Utility for querying global read-only kappa value
 */
template <typename t_Glue>
class cmxBnetDagKappaDetrSharedQuery {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);
    /**
     * @param sim - outer simulation
     * @return log-transformed real-valued scalar hyper-parameter
     */
    static inline cmxReal get_scalar(const sim_t& sim) {
        const cmxReal log_kappa =
            sim.get_shared_ro().get_Bnet_dag_kappa().get_log_scalar();
        /**
         * Based on:
         *  Heckerman, David, Dan Geiger, and David M. Chickering. "Learning
         *  Bayesian networks: The combination of knowledge and statistical
         *  data." Machine learning 20, no. 3 (1995): 197-243.
         *
         * Specifically pg. 226, section 6, equation 42
         */
        cmxAssert(log_kappa <= CMX_TO_REAL(0.0));
        return log_kappa;
    }
};

/**
 * Hyper-parameter in (0,1] for edge prior on DAG edges
 *  - t_Glue: Glue object as defined by cmxGlue
 */
template <typename t_Glue>
class cmxBnetDagKappaDetrMoco
    : public cmxScalarDetrMoco<t_Glue,
                               cmxBnetDagKappaDetrSharedQuery<t_Glue> > {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);
};

#endif  // cmxBnetDagKappaDetrMoco_h
