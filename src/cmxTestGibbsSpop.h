//****************************************************************************
// cmxTestGibbsSpop.h
//****************************************************************************

/**
 * Tests single-population Gibbs sampler
 *
 * Usage:
 *  -test_gibbs_spop [--conf <file_path>] [...] [--<opt_name> <opt_value>]
 *
 * If default config is okay (assume running under vstudio), then simply:
 *  -test_gibbs_spop
 */

#ifdef cmxTestGibbsSpop
#error "Single-population Gibbs Test included multiple times!"
#endif  // cmxTestGibbsSpop_h
#define cmxTestGibbsSpop_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_TESTS

#include "cmxAssert.h"
#include "cmxAutoTimer.h"
#include "cmxCmdOptsMap.h"
#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxGlobals.h"
#include "cmxLogf.h"
#include "cmxThread.h"
#include "cmxTypes.h"

// Glue: Core
#include "cmxModel.h"
#include "cmxSharedMu.h"
#include "cmxSharedRo.h"
#include "cmxSim.h"

// Glue: Controller
#include "cmxController.h"
#include "cmxMbrParallel.h"

// Glue: Observer
#include "cmxObs.h"

// Glue: Model components
#include "cmxBnetBoolMoco.h"
#include "cmxBnetCptAlphaDetrMoco.h"
#include "cmxBnetDagKappaDetrMoco.h"
#include "cmxBnetInfeasLambdaDetrMoco.h"
#include "cmxClustSpopMoco.h"
#include "cmxContactFreqsAlphaDetrMoco.h"
#include "cmxContactFreqsMoco.h"
#include "cmxDpStickSpopMoco.h"
#include "cmxNullMoco.h"
#include "cmxPseudopopMoco.h"

// Glue: Read-only shared components
#include "cmxBnetInfeasSharcoRo.h"
#include "cmxBnetThinDagSharcoRo.h"
#include "cmxClustSpopSharcoRo.h"
#include "cmxContactFreqsSharcoRo.h"
#include "cmxNullSharcoRo.h"
#include "cmxPseudopopSharcoRo.h"

// Glue: Mutable shared components
#include "cmxBnetBoolCptSharcoMu.h"
#include "cmxBnetThinDagSharcoMu.h"
#include "cmxNullSharcoMu.h"
#include "cmxPseudopopSharcoMu.h"

// Primer
#include "cmxPrimerCorrHic.h"

#include <string>

//****************************************************************************
// Utilities
//****************************************************************************

namespace cmxTestGibbsSpop {

/**
 * Read-only shared
 */
class GlueSharedRo {
public:
    typedef cmxNullSharcoRo Dp_stick_alpha_t;
    typedef cmxBnetDagKappaDetrSharcoRo Bnet_dag_kappa_t;
    typedef cmxBnetCptAlphaDetrSharcoRo Bnet_cpt_alpha_t;
    typedef cmxBnetInfeasLambdaDetrSharcoRo Bnet_infeas_lambda_t;
    typedef cmxBnetInfeasSharcoRo Bnet_infeas_t;
    typedef cmxBnetThinDagSharcoRo Bnet_dag_t;
    typedef cmxNullSharcoRo Bnet_mpop_t;
    typedef cmxClustSpopSharcoRo Clust_t;
    typedef cmxPseudopopSharcoRo Pseudopop_t;
    typedef cmxContactFreqsAlphaDetrSharcoRo Contact_freqs_alpha_t;
    typedef cmxContactFreqsSharcoRo Contact_freqs_t;
};

/**
 * Mutable shared
 */
class GlueSharedMu {
public:
    typedef cmxBnetThinDagSharcoMu Bnet_dag_t;
    typedef cmxBnetBoolCptSharcoMu Bnet_cpt_t;
    typedef cmxNullSharcoMu Bnet_mpop_t;
    typedef cmxNullSharcoMu Clust_t;
    typedef cmxPseudopopSharcoMu Pseudopop_t;
};

/**
 * Master
 */
class GlueMaster {
public:
    // Core simulation utilities
    typedef cmxSim<GlueMaster> sim_t;
    typedef cmxSharedRo<GlueSharedRo> shared_ro_t;
    typedef cmxSharedMu<GlueSharedMu> shared_mu_t;
    typedef cmxModel<GlueMaster> model_t;
    // Controller simulation utilities
    typedef cmxController<GlueMaster> controller_t;
    typedef cmxMbrParallel<GlueMaster> model_batch_runner_t;
    // Observer simulation utilities
    typedef cmxObs<GlueMaster> obs_t;
    // Model components
    typedef cmxNullMoco<GlueMaster> Dp_stick_alpha_t;
    typedef cmxDpStickSpopMoco<GlueMaster> Dp_stick_t;
    typedef cmxBnetDagKappaDetrMoco<GlueMaster> Bnet_dag_kappa_t;
    typedef cmxBnetCptAlphaDetrMoco<GlueMaster> Bnet_cpt_alpha_t;
    typedef cmxBnetInfeasLambdaDetrMoco<GlueMaster> Bnet_infeas_lambda_t;
    typedef cmxBnetBoolMoco<GlueMaster> Bnet_t;
    typedef cmxClustSpopMoco<GlueMaster> Clust_t;
    typedef cmxPseudopopMoco<GlueMaster> Pseudopop_t;
    typedef cmxContactFreqsAlphaDetrMoco<GlueMaster> Contact_freqs_alpha_t;
    typedef cmxContactFreqsMoco<GlueMaster> Contact_freqs_t;
};

/**
 * Initialize command line options, defer to default path if not specified
 */
void init_cmd(cmxCmdOptsMap& out_cmd, const cmxCmdOptsMap& in_cmd) {
    out_cmd = in_cmd;
    // Default INI path
    const std::string ini_path = "../test/gibbs.spop/spop.ini";
    // Check if key present, set to default if not specified
    const std::string conf_key(cmxOpt_get_cmd_switch(cmxOpt_conf));
    out_cmd.set_option_if_absent(conf_key, ini_path);
}

}  // namespace cmxTestGibbsSpop

//****************************************************************************
// Main
//****************************************************************************

/**
 * Entry point for test
 */
int cmxTestGibbsSpopMain(const cmxCmdOptsMap& cmd_opts_) {
    // Auto-timer for capturing total-exec time
    cmxAutoTimer profiler;
    cmxLogf("Running Test Gibbs Single-population.\n");
    // Simulation type
    typedef cmxTestGibbsSpop::GlueMaster::sim_t sim_t;
    // Primer type
    typedef cmxPrimerCorrHic<cmxTestGibbsSpop::GlueMaster> primer_t;
    // BEGIN TEST SPECIFIC
    // Initialize command line with default configuration path if unspecified
    cmxCmdOptsMap cmd_opts;
    cmxTestGibbsSpop::init_cmd(cmd_opts, cmd_opts_);
    // END TEST SPECIFIC
    // Create configuration
    cmxSpConfig_t config = cmxSmartPtr::make_shared<cmxConfig>();
    // Parse command line and any INI configuration (override defaults)
    config->init(cmd_opts);
    // Ensure single-population mixture size
    config->set_option(cmxOpt_mix_size, CMX_TO_UINT(1));
    // Output configuration
    config->print();
    // Initialize globals
    cmxG::init(config);
    // Report thread count
    CMX_LOG_NUM_WORKER_THREADS;
    // Initialize simulation
    sim_t sim;
    sim.init(config);
    cmxLogf("Finished initializing %d models.\n", (int)sim.get_num_models());
    sim.observers_force_update(CMX_TO_UINT(0));
    // Prime simulation
    primer_t::prime(sim, config);
    // Run simulation
    const cmxBool result = sim.run();
    cmxLogf("Simulation completed with status: %d\n", (int)result);
    // Report stats
    CMX_STATS_REPORT;
    // Destroy globals
    cmxG::teardown();
    // Return exit status
    return cmxExitCode_normal;
}

#endif  // CMX_BUILD_ENABLE_TESTS
