//****************************************************************************
// cmxObsBnetSpopMapc.h
//****************************************************************************

/**
 * @brief Records maximum a posteriori (MAP) estimate for single-
 *  population Bayesian network model at each MCMC model chain
 */

#ifndef cmxObsBnetSpopMapc_h
#define cmxObsBnetSpopMapc_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetBool.h"
#include "cmxExitCodes.h"
#include "cmxExportBnet.h"
#include "cmxExportCore.h"
#include "cmxFilesystem.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObs.h"
#include "cmxObsBnetSpopSnap.h"
#include "cmxObsLogscoreMapc.h"
#include "cmxSmartPtr.h"

#include <string>

//****************************************************************************
// cmxObsBnetSpopMapc
//****************************************************************************

/**
 * Chain MAP observer - captures maximum a posteriori estimate of
 *  observed parameter at each MCMC chain
 */
template <typename t_Glue>
class cmxObsBnetSpopMapc : public cmxObsLogscoreMapc<t_Glue>::map_reactor_t {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Log-score chain MAP observer type
     */
    typedef cmxObsLogscoreMapc<t_Glue> obs_logscore_map_t;

    /**
     * Snapshot observer type
     */
    typedef cmxObsBnetSpopSnap<t_Glue> obs_bnet_spop_snap_t;

    /**
     * Default constructor
     */
    cmxObsBnetSpopMapc() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within reactors array
     */
    virtual void init_react(cmxSpConstConfig_t config,
                            sim_t& sim,
                            const size_t index) {
        cmxAssert(sim.get_shared_ro().get_Clust().get_mix_size() ==
                  CMX_TO_UINT(1));
        // Reset
        this->clear();
        // Extract snapshot observer
        if (!obs_t::find_raw(&mp_snap, sim)) {
            cmxLogf(
                "Error: Bayesian network (single-population) chain MAP "
                "observer unable to find handle to snapshot observer. "
                "Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
        // Allocate buffer
        cmxAssert(sim.get_num_models() > 0);
        m_maps.resize(sim.get_num_models());
    }

    /**
     * Reset to default state
     */
    void clear() {
        mp_snap = NULL;
        m_maps.clear();
    }

    /**
     * Reactor update managed my logscore MAP observer, when a new MAP
     * estimate is encountered, this method will be called
     * @param iter - Typically post burn-in iteration number (0-based)
     * @param sim - Outer simulation
     * @param i_model - Index of MAP model
     * @param logscore - new MAP logscore associated to MAP model
     */
    virtual void update_react(const cmxUInt iter,
                              sim_t& sim,
                              const size_t i_model,
                              const cmxReal logscore) {
        cmxAssert(mp_snap);
        if (mp_snap->is_stale(iter)) {
            // Snapshot is stale, capture new snapshot
            mp_snap->force_update(iter, sim);
        }
        // Update MAP estimate
        cmxAssertBounds(i_model, 0, mp_snap->get_num_bnets());
        cmxAssertPosEq(mp_snap->get_num_bnets(), m_maps.size());
        m_maps[i_model] = mp_snap->get_bnets()[i_model];
    }

    /**
     * Callback for when simulation has finished running
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        if (m_maps.empty()) {
            // Avoid export if not configured or network is invalid
            return;
        }
        const obs_logscore_map_t& logscore_map = this->get_logscore_map();
        cmxLogf(
            "Exporting Bayesian network (single-population) chain MAP "
            "estimates...\n");
        cmxSpConstConfig_t config(sim.get_config());
        const size_t num_maps = m_maps.size();
        for (size_t i = 0; i < num_maps; ++i) {
            // Skip if no MAP capture available for this MCMC model chain
            if (!logscore_map.has_capture(i)) {
                continue;
            }
            // Determine export prefix
            const std::string fprefix =
                cmxExport::get_fpath(config,
                                     i,          /*index*/
                                     "",         /*subdir*/
                                     "map.bnet", /*file_id*/
                                     "");        /*ext*/
            // Export src2dst CSV
            std::string fpath = fprefix + ".src2dst.csv";
            cmxExport::write_bnet_dag_adjmap_csv(
                m_maps[i].get_dag().get_src2dst_map(), fpath);

            // Export src2dst Graphviz
            fpath = fprefix + ".src2dst.gv";
            cmxExport::write_bnet_dag_adjmap_gv(
                m_maps[i].get_dag().get_src2dst_map(), fpath);

            // Export dst2src
            fpath = fprefix + ".dst2src.csv";
            cmxExport::write_bnet_dag_adjmap_csv(
                m_maps[i].get_dag().get_dst2src_map(), fpath);

            // Export order
            fpath = fprefix + ".order.csv";
            cmxExport::write_bnet_order_csv(m_maps[i].get_order(), fpath);

            // Export CPTs
            fpath = fprefix + ".cpt.csv";
            cmxExport::write_bnet_bool_cpt_csv(m_maps[i].get_cpts(), fpath);
        }
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_bnet_mapc)

private:
    /**
     * Handle to snapshot observer - raw pointer is used as lifetime of
     * pointer does not exceed that of parent simulation's shared_ptr
     */
    obs_bnet_spop_snap_t* mp_snap;

    /**
     * MAP (maximum a posteriori) estimates
     */
    std::vector<cmxBnetBool> m_maps;
};

#endif  // cmxObsBnetSpopMapc_h
