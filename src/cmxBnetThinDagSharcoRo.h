//****************************************************************************
// cmxBnetThinDagSharcoRo.h
//****************************************************************************

/**
 * @brief Read-only shared resources used by thin DAGs.
 */

/**
 * Note the following design framework:
 *  - Shared mutable (i.e. writable) data buffers should be replicated across
 *      threads (thread-local storage - TLS) to avoid race conditions
 *  - Shared read-only (i.e. non-writable|non-mutable) data should be stored
 *      as single instance at simulation level to avoid unnecessary
 *      duplication across threads and therefore conserve memory
 */
#ifndef cmxBnetThinDagSharcoRo_h
#define cmxBnetThinDagSharcoRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetThinEdgeStatesRo.h"
#include "cmxConfig.h"
#include "cmxContactFreqsSharcoRo.h"
#include "cmxExitCodes.h"
#include "cmxLogf.h"
#include "cmxModelTypes.h"
#include "cmxOpts.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetThinDagSharcoRo
//****************************************************************************

/**
 * Shared read-only data resources for thin DAGs
 */
class cmxBnetThinDagSharcoRo {
public:
    /**
     * Initialize from user configuration
     * @param config - user options
     */
    void init(cmxSpConstConfig_t config) {
        // Determine v_num from Hi-C frequencies
        cmxContactFreqs_t hic;
        cmxContactFreqsSharcoRo::load_hic_exit_on_fail(config, hic);
        cmxAssert(hic.n_elem > 1);
        const cmxUInt v_num = CMX_TO_UINT(hic.n_elem);
        // Determine path to allowed edges matrix
        std::string edges_path;
        if (!config->resolve_path(edges_path, cmxOpt_Bnet_dag_edges_path)) {
            cmxLogf(
                "Error: file path to Bayesian network allowed edges matrix not "
                "specified. See %s for usage. Exiting\n",
                cmxOpt_get_cmd_switch(cmxOpt_help));
            exit(cmxExitCode_invalid_bnet_dag_edges);
        }
        // Load allowed edges matrix
        cmxUIMatrix props;
        if (!props.load(edges_path, cmxMatrixUtils::csv_ascii)) {
            cmxLogf(
                "Error: unable to load Bayesian network allowed edges matrix "
                "from file %s. Exiting\n",
                edges_path.c_str());
            exit(cmxExitCode_invalid_bnet_dag_edges);
        }
        // Check dimensions
        if (props.n_rows != cmxBnetThinEdgeStatesRo::cmxBnetThinEdge_ro_num) {
            cmxLogf(
                "Error: Bayesian network allowed edges matrix expected %d rows "
                "but loaded %d rows. Exiting.\n",
                (int)cmxBnetThinEdgeStatesRo::cmxBnetThinEdge_ro_num,
                (int)props.n_rows);
            exit(cmxExitCode_invalid_bnet_dag_edges);
        }
        // Check indexing
        for (cmxMatSz_t i = 0; i < props.n_cols; ++i) {
            check_index(props,
                        cmxBnetThinEdgeStatesRo::cmxBnetThinEdge_ro_src,
                        i,
                        v_num);
            check_index(props,
                        cmxBnetThinEdgeStatesRo::cmxBnetThinEdge_ro_dst,
                        i,
                        v_num);
        }
        // Data integrity checks passed, feed to read-only thin edge manager
        m_ero.init(props, v_num);
    }

    /**
     * Clear (reset) read-only resources
     */
    void clear() { m_ero.clear(); }

    /**
     * @return Const handle to read-only edge information
     */
    inline const cmxBnetThinEdgeStatesRo& get_ero() const { return m_ero; }

private:
    /**
     * Utility checks user matrix has proper 0-based index at (i_row, i_col),
     *  will exit on failure
     */
    static void check_index(const cmxUIMatrix props,
                            const cmxBnetThinEdgeStatesRo::EdgePropertyRo i_row,
                            const cmxMatSz_t i_col,
                            const cmxUInt v_num) {
        cmxAssertBounds(
            CMX_TO_MAT_SZ_T(i_row), CMX_TO_MAT_SZ_T(0), props.n_rows);
        cmxAssertBounds(i_col, CMX_TO_MAT_SZ_T(0), props.n_cols);
        const cmxUInt index = props.at(i_row, i_col);
        if (index >= v_num) {
            cmxLogf(
                "Error: Bayesian network allowed edges must use 0-based "
                "indexing. Encountered element(%d, %d) = %d > than max allowed "
                "index %d. Exiting.\n",
                (int)i_row,
                (int)i_col,
                (int)index,
                (int)v_num);
            exit(cmxExitCode_invalid_bnet_dag_edges);
        }
    }

    /**
     * Edge read-only (ero) information
     */
    cmxBnetThinEdgeStatesRo m_ero;
};

#endif  // cmxBnetThinDagSharcoRo_h
