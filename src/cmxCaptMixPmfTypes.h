//****************************************************************************
// cmxCaptMixPmfTypes.h
//****************************************************************************

/**
 * @brief Types used for capturing model mixture weights
 */

#ifndef cmxCaptMixPmfTypes_h
#define cmxCaptMixPmfTypes_h

#include "cmxBuild.h"
#include "cmxTypes.h"

#include <queue>
#include <vector>

/**
 * POD for organizing clusters by size
 */
typedef struct {
    // Mixture weight
    cmxReal m_weight;
    // Index of cluster (i.e. mixture component)
    cmxUInt m_index;
} cmxMixPmfInfo_t;

/**
 * Mixture comparison operation for organizing mixture components by weight
 */
class cmxMixPmfOrg {
public:
    inline bool operator()(const cmxMixPmfInfo_t a, const cmxMixPmfInfo_t b) {
        // Break size ties according to index
        return (a.m_weight == b.m_weight) ? (a.m_index > b.m_index)
                                          : (a.m_weight < b.m_weight);
    }
};

/**
 * Priority queue for organizing clusters by size
 */
typedef std::
    priority_queue<cmxMixPmfInfo_t, std::vector<cmxMixPmfInfo_t>, cmxMixPmfOrg>
        cmxMixPmfPqueue_t;

/**
 * Utility to reset and fill priority queue of mixture weights
 * @param pqueue - Priority queue to populate with mixture weights
 * @param model - Parent model
 * @param sim - Outer simulation
 */
template <typename t_model, typename t_sim>
void cmx_mix_pmf_fill(cmxMixPmfPqueue_t& pqueue,
                      const t_model& model,
                      const t_sim& sim) {
    // Determine number of mixture components
    const cmxUInt MIX_SIZE = sim.get_shared_ro().get_Clust().get_mix_size();
    cmxAssert(MIX_SIZE > CMX_TO_UINT(1));
    // Initialize priority queue
    pqueue = cmxMixPmfPqueue_t();
    // Iterate over mixture components
    for (cmxUInt i_mix = 0; i_mix < MIX_SIZE; ++i_mix) {
        const cmxReal mix_weight = model.get_Dp_stick().get_mix_weight(i_mix);
        // @TODO - consider minimum weight retention threshold
        cmxMixPmfInfo_t nfo;
        nfo.m_weight = mix_weight;
        nfo.m_index = i_mix;
        pqueue.push(nfo);
    }  // End iteration over mixture components
}

#endif  // cmxCaptMixPmfTypes_h
