//****************************************************************************
// cmxConfig.h
//
// Class for unifying command line and file (eg INI) user options
//****************************************************************************

#ifndef cmxConfig_h
#define cmxConfig_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxCmdOptsMap.h"
#include "cmxConfigFile.h"
#include "cmxOpts.h"
#include "cmxSmartPtr.h"
#include "cmxTypes.h"

#include <string>
#include <vector>

/**
 * Reads and stores users configuration parameters.
 *
 * Provides interface so that rest of application does not have to worry about
 * whether its options are coming from the command line or from a INI file
 */
class cmxConfig : public cmxSmartPtr::enable_shared_from_this<cmxConfig> {
public:
    /**
     * Typedefs
     */
    typedef cmxSmartPtr::shared_ptr<cmxConfig> cmxSpConfig_t;
    typedef cmxSmartPtr::shared_ptr<const cmxConfig> cmxSpConstConfig_t;
    typedef cmxSmartPtr::weak_ptr<cmxConfig> cmxWpConfig_t;
    typedef cmxSmartPtr::weak_ptr<const cmxConfig> cmxWpConstConfig_t;

    //************************************************************************
    // Public methods
    //************************************************************************

    /**
     * Clears configuration options
     */
    void clear();

    /**
     * Initializes from command line
     *
     * @param cmd_opts - command line options
     * @param should_clear - If TRUE, erases existing state in this object
     */
    void init(const cmxCmdOptsMap& cmd_opts,
              const cmxBool should_clear = cmxTRUE);

    /**
     * Outputs configuration options to stdout
     */
    void print() const;

    /**
     * Search for value mapped to option key
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param out_val - The output value mapped to parameter opt_key,
     *      unmodified if opt_key does not exist in command options map
     * @param ix - The option identifier
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    template <class T>
    cmxBool read_into(T& out_val,
                      const cmxOptE ix,
                      const cmxBool check_parent = cmxTRUE) const {
        // Check command line first
        if (m_cmd_opts.read_into(out_val, cmxOpt_get_cmd_switch(ix))) {
            return cmxTRUE;
        }
        // Else check INI file
        else if (m_cfg_file.read_into(out_val, cmxOpt_get_ini_key(ix))) {
            return cmxTRUE;
        }
        // Else check INI archetype
        else if (m_cfg_arch.read_into(out_val, cmxOpt_get_ini_key(ix))) {
            return cmxTRUE;
        }
        // Else check the parent configuration
        else if (check_parent) {
            cmxSpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->read_into(
                    out_val, ix, cmxTRUE /*check_parent*/);
            }
        }

        // Key not found, return false
        return cmxFALSE;
    }

    /**
     * @WARNING - DOES NOT PERFORM PATH RESOLUTION!
     * @out_val - Searches for first parameter value mapped to a matching key
     *  in 'ixs' array where all command line matches have precedence over INI
     * @param ixs - Set of option keys to search for mapped value, the key
     *  priority is as follows: All command line variants have higher priority
     *  than INI, then earlier indices within array have priority over later
     * @param check_parent - If true, then parent configuration (if valid) will
     *  be recursively checked for the set of option keys
     * @return index of option read or ixs.size() if not found
     */
    size_t read_into(std::string& out_val,
                     const std::vector<cmxOptE>& ixs,
                     const cmxBool check_parent = cmxTRUE) const;

    /**
     * PERFORMS PATH RESOLUTION!
     * @out_val - Searches for first parameter value mapped to a matching key
     *  in 'ixs' array where all command line matches have precedence over INI
     * @param ixs - Set of option keys to search for mapped value, the key
     *  priority is as follows: All command line variants have higher priority
     *  than INI, then earlier indices within array have priority over later
     * @param should_resolve - Parallel vector of same size as 'ixs' with i-th
     *  element TRUE if option is a path that must be resolved, FALSE o/w
     * @param check_parent - If true, then parent configuration (if valid) will
     *  be recursively checked for the set of option keys
     * @return index of option read or ixs.size() if not found
     */
    size_t read_into(std::string& out_val,
                     const std::vector<cmxOptE>& ixs,
                     const std::vector<cmxBool>& should_resolve,
                     const cmxBool check_parent = cmxTRUE) const;

    /**
     * Search for value mapped to option vector key. Vector options are of INI
     * format: <opt_name>_<off> = <value> where 'off' is a non-negative integer
     * index. This method returns a SINGLE value mapped to the option offset.
     * Vector options are assumed to use 0-based indexing and be declared
     * sequentially (no skips).
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param out_val - The output value mapped to parameter opt_key,
     *      unmodified if opt_key does not exist in command options map
     * @param ix - The option prefix identifier
     * @param off - The option offset
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    template <class T>
    cmxBool read_into_vec(T& out_val,
                          const cmxOptE ix,
                          const cmxUInt off,
                          const cmxBool check_parent = cmxTRUE) const {
        // Check command line first
        const std::string suffix(std::string("_") + cmx2Str(off));
        if (m_cmd_opts.read_into(out_val, cmxOpt_get_cmd_switch(ix) + suffix)) {
            return cmxTRUE;
        }
        // Else check INI file
        /* else */
        if (m_cfg_file.read_into(out_val, cmxOpt_get_ini_key(ix) + suffix)) {
            return cmxTRUE;
        }
        // Else check INI archetype
        /* else */
        if (m_cfg_arch.read_into(out_val, cmxOpt_get_ini_key(ix) + suffix)) {
            return cmxTRUE;
        }
        // Else check the parent configuration
        /* else */
        if (check_parent) {
            cmxSpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->read_into_vec(
                    out_val, ix, off, cmxTRUE /*check_parent*/);
            }
        }

        // Key not found, return false
        return cmxFALSE;
    }

    /**
     * Obtain path value resolved according to the rules (in order of
     * precedence)
     *  - if not found, path argument is unmodified
     *  - if from command line, output path is unmodified
     *  - if from config file and absolute, output path is unmodified
     *  - if from config file and relative, output path is resolved relative
     *      to config file directory
     *
     * Assumes parse() has been called for internal command line and ini file
     * maps.
     *
     * @param path - Output path, unmodified if key not found
     * @param ix - Option identifier
     * @return TRUE if key exists, FALSE otherwise. If key exists and value
     *  is from command line, then output path argument is same as command
     *  line. However, if key exists and value is from configuration file,
     *  then relative paths are resolved to be relative to the configuration
     *  file directory. Note that command line values override any
     *  configuration file values.
     */
    cmxBool resolve_path(std::string& path,
                         const cmxOptE ix,
                         const cmxBool check_parent = cmxTRUE) const;

    /**
     * Resolve path for vectorized options
     * @param path - Output path obtained using resolve_path() semantics
     * @param ix - Option identifier
     * @param off - The option offset
     * @return TRUE if key exists, FALSE o/w
     */
    cmxBool resolve_path_vec(std::string& path,
                             const cmxOptE ix,
                             const cmxUInt off,
                             const cmxBool check_parent = cmxTRUE) const;

    /**
     * @param ix - The option identifier
     * @param check_parent - If TRUE, parent configuration (if valid)
     *      will be checked for the option if key not found in child. If
     *      FALSE, the parent will not be checked for parameter option.
     * @return TRUE if key exists, FALSE otherwise
     */
    inline cmxBool key_exists(const cmxOptE ix,
                              const cmxBool check_parent = cmxTRUE) const {
        // Check command line first
        if (m_cmd_opts.key_exists(cmxOpt_get_cmd_switch(ix))) {
            return cmxTRUE;
        }
        // Else check INI file
        else if (m_cfg_file.key_exists(cmxOpt_get_ini_key(ix))) {
            return cmxTRUE;
        }
        // Else check INI archetype
        else if (m_cfg_arch.key_exists(cmxOpt_get_ini_key(ix))) {
            return cmxTRUE;
        }
        // Else, check the parent configuration
        else if (check_parent) {
            cmxSpConstConfig_t sptr_parent = m_parent.lock();
            if (sptr_parent) {
                return sptr_parent->key_exists(ix, cmxTRUE /*check_parent*/);
            }
        }
        // Key not found, return false
        return cmxFALSE;
    }

    /**
     * Allow clients to set options programmatically as well. Note: all
     * options are cleared if init() is called with should_clear = TRUE.
     *
     * @param ix - The option identifier
     * @param val - The value to assign
     */
    template <typename t_>
    void set_option(const cmxOptE ix, const t_& val) {
        // Store in command line options as those always take priority
        m_cmd_opts.set_option(cmxOpt_get_cmd_switch(ix), val);
    }

    /**
     * Overload for char*
     * Allow clients to set options programmatically as well. Note: all
     * options are cleared if init() is called with should_clear = TRUE.
     *
     * @param ix - The option identifier
     * @param val - The value to assign
     */
    void set_option(const cmxOptE ix, const char* val) {
        // Store in command line options as those always take priority
        m_cmd_opts.set_option(cmxOpt_get_cmd_switch(ix), std::string(val));
    }

    /**
     * Set option if not already set (can be used for setting defaults)
     * @param ix - The option identifier
     * @param val - The value to assign
     * @return TRUE if option was set, FALSE otherwise (e.g. if option already
     *  specified)
     */
    template <typename t_>
    inline cmxBool set_option_if_absent(const cmxOptE ix, const t_& val) {
        if (!key_exists(ix)) {
            // Option was not previously set
            set_option(ix, val);
            // Option was successfully set
            return cmxTRUE;
        }
        // Option was not set as it exists already
        return cmxFALSE;
    }

    /**
     * @return pointer to child configuration
     */
    inline cmxSpConfig_t get_child() const { return m_child; };

    /**
     * Overwrite child configuration
     * @param child - child configuration
     */
    void set_child(cmxSpConfig_t child);

private:
    /**
     * Internal print adjusted for child level
     */
    void print(const cmxUInt level) const;

    /**
     * Checks if child configuration should be initialized
     */
    void conditional_init_child();

    /**
     * Actual initialization for a child configuration
     * @param parent - The parent configuration
     * @param cfg_file - Child INI file
     * Note: child command line options are extracted from parent command line
     */
    void init(cmxSpConstConfig_t parent, const cmxConfigFile& cfg_file);

    /**
     * Command line options mapping:
     * Used for storing <key=string, value=string> tuples obtained from
     * command line
     */
    cmxCmdOptsMap m_cmd_opts;

    /**
     * Ini file options mapping:
     * Used for storing <key=string, value=string> tuples obtained from
     * ini line
     */
    cmxConfigFile m_cfg_file;

    /**
     * INI file archetype:
     * Settings not found in m_cfg_file will be deferred to archetype
     */
    cmxConfigFile m_cfg_arch;

    /**
     * Pointer to possible parent configuration. If a key,value pair is
     * missing in this config, the parent is checked for the value.
     * Using weak_ptr to prevent circular reference issues.
     */
    cmxWpConstConfig_t m_parent;

    /**
     * The parent configuration owns the memory for any child configurations.
     * If all parent configurations are destroyed, then the child
     * configuration will be destroyed as well.
     * Note: the parent configuration does not defer to the child
     * configuration for any key, value pairs. However, client code may
     * utilize the child configurations for initializing sub-simulations of
     * the client program.
     */
    cmxSpConfig_t m_child;
};

/**
 * Typedefs
 */
typedef cmxConfig::cmxSpConfig_t cmxSpConfig_t;
typedef cmxConfig::cmxWpConfig_t cmxWpConfig_t;
typedef cmxConfig::cmxSpConstConfig_t cmxSpConstConfig_t;
typedef cmxConfig::cmxWpConstConfig_t cmxWpConstConfig_t;

#endif  // cmxConfig_h
