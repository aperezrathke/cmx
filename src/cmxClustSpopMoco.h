//****************************************************************************
// cmxClustSpopMoco.h
//****************************************************************************

/**
 * @brief Cluster assignment for single population, which is really a trivial
 *  variable just meant to allow single population model to be a special case
 *  of the multi-population model.
 */

#ifndef cmxClustSpopMoco_h
#define cmxClustSpopMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxModelTypes.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxClustSpopMoco
//****************************************************************************

/**
 * Expected Clust model component interface:
 *  get_mix_index(sample_index) - must return the associated cluster index
 *      for the parameter pseudo-population sample identifier
 *  get_pseudopop_indices(clust_index, sim) - must return indices of pseudo-
 *      population samples associated with parameter cluster index
 */
template <typename t_Glue>
class cmxClustSpopMoco : public cmxMoco<t_Glue> {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    // This component is always deterministic!
    enum { is_random = cmxFALSE };

    // Accessors

    /**
     * @return mixture assignment for parameter sample
     */
    inline cmxUInt get_mix_index(const cmxUInt sample_index) const { return 0; }

    /**
     * @return samples assigned to parameter cluster
     */
    inline const cmxPseudopopIndices_t& get_pseudopop_indices(
        const cmxUInt mix_index,
        const sim_t& sim) const {
        // Assume single population mixture
        cmxAssert(0 == mix_index);
        cmxAssert(1 == sim.get_shared_ro().get_Clust().get_mix_size());
        return sim.get_shared_ro().get_Clust().get_pseudopop_indices_spop();
    }

    /**
     * @return number of samples assigned to parameter cluster
     */
    inline cmxUInt get_pseudopop_size(const cmxUInt mix_index,
                                      const sim_t& sim) const {
        return CMX_TO_UINT(this->get_pseudopop_indices(mix_index, sim).size());
    }
};

#endif  // cmxClustSpopMoco_h
