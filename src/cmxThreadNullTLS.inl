//****************************************************************************
// cmxThreadNullTLS.inl
//****************************************************************************

/**
 * WARNING: OVERWRITES THREAD LOCAL STORAGE (TLS) MACROS TO NULL
 * IMPLEMENTATION.
 *
 * This can be useful if you want to a serial implementation of a class that
 * will only ever run on a single thread but in a multi-threaded environment.
 *
 * MAKE SURE YOU KNOW WHAT YOU ARE DOING WHEN INCLUDING THIS FILE AS THESE
 * MACROS WILL OVERWRITE ANY COMMON DEFINES WITHIN cmxThread.h!!!
 */

/**
 * Undefine any previously existing macros
 */

#ifdef CMX_DECLARE_TLS_DATA
#   undef CMX_DECLARE_TLS_DATA
#endif  // CMX_DECLARE_TLS_DATA

#ifdef CMX_ACCESS_TLS_DATA
#   undef CMX_ACCESS_TLS_DATA
#endif  // CMX_ACCESS_TLS_DATA

#ifdef CMX_INIT_TLS_DATA_0_PARAM
#   undef CMX_INIT_TLS_DATA_0_PARAM
#endif  // CMX_INIT_TLS_DATA_0_PARAM

#ifdef CMX_INIT_TLS_DATA_FOR_EACH_1_PARAM
#   undef CMX_INIT_TLS_DATA_FOR_EACH_1_PARAM
#endif  // CMX_INIT_TLS_DATA_FOR_EACH_1_PARAM

#ifdef CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM
#   undef CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM
#endif  // CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM

#ifdef CMX_TLS_DATA_FOR_EACH_0_PARAM
#   undef CMX_TLS_DATA_FOR_EACH_0_PARAM
#endif  // CMX_TLS_DATA_FOR_EACH_0_PARAM

/**
 * Redefine macros
 */

#define CMX_DECLARE_TLS_DATA(type, name) type name

#define CMX_ACCESS_TLS_DATA(name) name

#define CMX_INIT_TLS_DATA_0_PARAM(name) ((void)0)

#define CMX_INIT_TLS_DATA_FOR_EACH_1_PARAM(name, memb_func, param_1) \
    name.memb_func(param_1)

#define CMX_INIT_TLS_DATA_FOR_EACH_2_PARAM(name, memb_func, param_1, param_2) \
    name.memb_func(param_1, param_2)

#define CMX_TLS_DATA_FOR_EACH_0_PARAM(name, memb_func) name.memb_func()
