//****************************************************************************
// cmxBnetThinEdgeStatesRo.h
//****************************************************************************

/**
 * @brief Class stores read-only states for the legal edges within a thin
 *  directed acyclic graph (DAG). The "read-only" aspect means that these
 *  state fields are only initialized once and thereafter remain constant
 *  (immutable) for the duration of the program execution.
 */

#ifndef cmxBnetThinEdgeStatesRo_h
#define cmxBnetThinEdgeStatesRo_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetThinEdgeRemoval.h"
#include "cmxHashMap.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetThinEdgeStatesRo
//****************************************************************************

/**
 * Read-only properties for legal edges. This data is meant to only be
 * initialized once and thereafter should be non-mutable. This allows the
 * state to be shared across threads if using a threaded implementation.
 */
class cmxBnetThinEdgeStatesRo {
public:
    /**
     * Enum used for querying legal edge read-only properties
     */
    enum EdgePropertyRo {
        // Source vertex
        cmxBnetThinEdge_ro_src = 0,
        // Destination vertex
        cmxBnetThinEdge_ro_dst,
        // Number of read-only edge properties
        cmxBnetThinEdge_ro_num
    };

    /**
     * Default constructor
     */
    cmxBnetThinEdgeStatesRo() : m_v_num(0) {}

    /**
     * Initialize from column major matrix.
     * @param props - Each column of the matrix specifies an allowed edge.
     *  Therefore, the matrix dimensions (row x col) must be
     *  cmxBnetThinEdge_ro_num x N where N is the number of allowed edges.
     *  Each row represents the corresponding property as defined by
     *  EdgePropertyRo enumeration.
     * @param v_num - total number of vertices
     */
    void init(const cmxUIMatrix& props, const cmxUInt v_num) {
        // Verify format
        cmxAssert(v_num > 1);
        cmxAssert(props.n_rows == cmxBnetThinEdge_ro_num);
        cmxAssert(props.n_cols > 0);
        // Copy number of vertices
        m_v_num = v_num;
        // Copy properties
        m_props = props;
        // Initialize keys
        m_edge_1D_to_prop_key_map.clear();
        cmxUInt key_1D;
        for (cmxUInt i = 0; i < m_props.n_cols; ++i) {
            key_1D = this->get_edge_1D(m_props.at(cmxBnetThinEdge_ro_src, i),
                                       m_props.at(cmxBnetThinEdge_ro_dst, i));
            m_edge_1D_to_prop_key_map[key_1D] = i;
        }
    }

    /**
     * Reset read-only state
     */
    void clear() {
        m_v_num = CMX_TO_UINT(0);
        m_edge_1D_to_prop_key_map.clear();
        m_props.clear();
    }

    /**
     * Determines the key which can be used for querying legal edge properties
     * for the *legal* edge from vertex v_src to vertex v_dst.
     * @WARNING: ASSUMES EDGE (v_src, v_dst) IS A LEGAL EDGE!!!
     * @param v_src - source vertex identifier
     * @param v_dst - destination vertex identifier
     * @return key which can be used to query legal edge properties
     */
    inline cmxUInt get_key(const cmxUInt v_src, const cmxUInt v_dst) const {
        cmxAssert(m_v_num > 1);
        // Transform edge from 2-D (v_src, v_dst) to 1-D format
        const cmxUInt edge_1D = this->get_edge_1D(v_src, v_dst);
        // If key does not exist, then something is wrong!
        cmxAssert(m_edge_1D_to_prop_key_map.count(edge_1D));
        // Return key which can be used for querying properties
        return m_edge_1D_to_prop_key_map.find(edge_1D)->second;
    }

    /**
     * Can be used to query if edge (v_src, v_dst) is a legal edge
     * @param v_src - source vertex identifier
     * @param v_dst - destination vertex identifier
     * @param out_key - if edge (v_src, v_dst) is legal, this will contain the
     *  key which can be used for querying the edge's properties
     * @return uTRUE if edge (v_src, v_dst) is legal and also its property key
     *  in out_key, uFALSE o/w
     */
    inline cmxBool has_key(const cmxUInt v_src,
                           const cmxUInt v_dst,
                           cmxUInt& out_key) const {
        cmxAssert(m_v_num > 1);
        // Transform edge from 2-D (v_src, v_dst) to 1-D format
        const cmxUInt edge_1D = this->get_edge_1D(v_src, v_dst);
        // Search for edge among set of legal edges
        cmxUIntToUIntMap::const_iterator itr =
            m_edge_1D_to_prop_key_map.find(edge_1D);
        if (itr != m_edge_1D_to_prop_key_map.cend()) {
            // Output key which can be used for querying properties
            out_key = itr->second;
            // Return true indicating we found a key => edge is legal
            return cmxTRUE;
        }
        // Return false indicating we did not find a key => edge is not legal
        return cmxFALSE;
    }

    /**
     * @param key - unique legal edge identifier
     * @return source vertex identifier for legal edge at key
     */
    inline cmxUInt get_v_src(const cmxUInt key) const {
        return this->get_prop(key, cmxBnetThinEdge_ro_src);
    }

    /**
     * @param key - unique legal edge identifier
     * @return destination vertex identifier for legal edge at key
     */
    inline cmxUInt get_v_dst(const cmxUInt key) const {
        return this->get_prop(key, cmxBnetThinEdge_ro_dst);
    }

    /**
     * @WARNING - ASSUMED TO MATCH SAME VALUE AS CORRESPONDING DAG
     * @WARNING - THIS WILL LIKELY NEED TO QUERY FULL HI-C DATA TO DETERMINE
     *  NUMBER OF VERTS AS IT'S LIKELY SOME VERTS WILL NOT HAVE ANY OUTGOING
     *  EDGES AND THEREFORE WILL NOT BE REFLECTED IN ALLOWED EDGES DATA SET!
     * @return total number of vertices
     */
    inline cmxUInt get_v_num() const { return m_v_num; }

    /**
     * @return number of possible edges
     */
    inline cmxUInt get_e_num() const {
        return CMX_TO_UINT(this->m_props.n_cols);
    }

private:
    /**
     * Utility maps 2-D edge (v_src, v_dst) into a 1-D linear representation
     */
    inline cmxUInt get_edge_1D(const cmxUInt v_src, const cmxUInt v_dst) const {
        // Verify not using reserved value(s)
        cmxAssert(v_src != CMX_BNET_EDGE_NA);
        cmxAssert(v_dst != CMX_BNET_EDGE_NA);
        cmxAssert(m_v_num != CMX_BNET_EDGE_NA);
        // Bounds checking, implicitly assumes m_v_num > 0
        cmxAssertBounds(v_src, CMX_TO_UINT(0), m_v_num);
        cmxAssertBounds(v_dst, CMX_TO_UINT(0), m_v_num);
        // Compute index as if using an adjacency matrix
        return (v_src * m_v_num) + v_dst;
    }

    /**
     * Accessor for querying legal edge properties
     * @param key - unique legal edge identifier
     * @param prop - the property category identifier
     * @return property for legal edge identified by key
     */
    inline cmxUInt get_prop(const cmxUInt key,
                            const enum EdgePropertyRo prop) const {
        cmxAssertBounds(key, CMX_TO_UINT(0), m_props.n_cols);
        cmxAssertBounds(CMX_TO_MAT_SZ_T(prop), 0, m_props.n_rows);
        cmxAssert(m_props.n_rows == cmxBnetThinEdge_ro_num);
        return m_props.at(prop, key);
    }

    /**
     * Number of vertices. Stored for convenience
     */
    cmxUInt m_v_num;

    /**
     * Used for mapping 2-D (v_src, v_dst) edge representation to column index
     * containing edge properties.
     *  - key: 1-D transform of 2-D edge representation (see get_edge_1D(...))
     *  - value: column index within m_props
     */
    cmxUIntToUIntMap m_edge_1D_to_prop_key_map;

    /**
     * A cmxBnetThinEdge_ro_num x N matrix where N is number of legal edges.
     * Each column is the read-only edge properties for a single edge.
     * SAME ORDER AS CORRESPONDING LEGAL EDGE MUTABLE DATA!
     */
    cmxUIMatrix m_props;
};

#endif  // cmxBnetThinEdgeStatesRo_h
