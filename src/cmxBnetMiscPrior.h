//****************************************************************************
// cmxBnetMiscPrior.h
//****************************************************************************

/**
 * @brief Miscellaneous priors on Bayesian network component
 */

#ifndef cmxBnetMiscPrior_h
#define cmxBnetMiscPrior_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetBool.h"
#include "cmxGlue.h"
#include "cmxModelTypes.h"
#include "cmxTypes.h"

#include <math.h>

//****************************************************************************
// Priors
//****************************************************************************

/**
 * Null prior - deterministic and does not affect log-score, can be used as a
 *  proxy placeholder or to disable a component misc prior
 */
template <typename t_Glue>
class cmxBnetNullPrior {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Prior is deterministic
     */
    enum { is_random = cmxFALSE };

    /**
     * Null prior - all arguments unused
     * @param bnet - Boolean Bayesian network
     * @param num_edges - number of edges in Bayesian network
     * @param model - parent model
     * @param sim - outer simulation
     * @return 0 (null log-score)
     */
    inline static cmxReal logscore(const cmxBnetBool& bnet,
                                   const cmxUInt num_edges,
                                   const model_t& model,
                                   const sim_t& sim) {
        return CMX_TO_REAL(0.0);
    }
};

/**
 * H95 -> Heckerman, 1995
 *
 * Bayesian network structure (DAG) edge prior based on:
 *  Heckerman, David, Dan Geiger, and David M. Chickering. "Learning
 *  Bayesian networks: The combination of knowledge and statistical
 *  data." Machine learning 20, no. 3 (1995): 197-243.
 *
 * Specifically pg. 226, section 6, equation 42
 */
template <typename t_Glue>
class cmxBnetDagEdgeH95Prior {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Hyper-parameter model component type
     */
    typedef typename model_t::Bnet_dag_kappa_t bnet_dag_kappa_t;

    /**
     * Prior score may only be cached if hyper-parameter(s) are deterministic
     */
    enum { is_random = bnet_dag_kappa_t::is_random };

    /**
     * @param bnet - [ignored] Boolean Bayesian network
     * @param num_edges - number of edges in Bayesian network
     * @param model - parent model
     * @param sim - outer simulation
     * @return log-score of Bayesian network edge structure
     */
    inline static cmxReal logscore(const cmxBnetBool& bnet,
                                   const cmxUInt num_edges,
                                   const model_t& model,
                                   const sim_t& sim) {
        // Edge prior is proportional to:
        //  P(# of edges) = kappa ^ (# of edges) where kappa is in (0, 1], in
        //  log space, this results in log P = (# of edges) * log(kappa)

        // Get hyper-parameter
        cmxReal kappa = model.get_Bnet_dag_kappa().get_scalar(sim);
        if (is_random) {
            // Hyper-parameter is stochastic, therefore we must compute log
            // transform
            cmxAssert(kappa > CMX_TO_REAL(0.0));
            cmxAssert(kappa <= CMX_TO_REAL(1.0));
            kappa = log(kappa);
        }
        // Note, if hyper-parameter is deterministic, assume that scalar has
        // already been log transformed
        cmxAssert(kappa <= CMX_TO_REAL(0.0));
        return CMX_TO_REAL(num_edges) * kappa;
    }
};

/**
 * Prior on prohibited (infeasible) samples. The log score returned is:
 *
 *  log-score =  min{ -scale * max_i{ logprob(sample_i|bnet) }, ceil }
 *      where scale is >= 0, ceil is >= 0, and sample_i belongs to a set of
 *      infeasible samples.
 *
 * The log-score is the negative scaled log-probability of generating a sample
 * from the infeasible set given the Bayesian network. To keep things
 * numerically stable and to prevent this term from dominating, 'ceil' is
 * introduced to limit the effect of the penalty.
 *
 * Note, the logarithmic-sum-of-exponentials:
 *
 *      ln[sum_i{e^(logprob(sample_i|bnet))}] = ln[sum_i{prob(sample_i|bnet)}]
 *
 *  is well approximated by max_i{ logprob(sample_i|bnet) }, and saves us from
 *  evaluating many additional calls to exp(...). Hence, the returned score
 *  is actually an approximation of:
 *
 *      min{ -scale * ln[sum_i{prob(sample_i|bnet)}], ceil }
 *
 *  See: https://en.wikipedia.org/wiki/LogSumExp
 *  for an explanation and bounds on the logarithmic-sum-of-exponentials.
 */
template <typename t_Glue>
class cmxBnetInfeasNegpPrior {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Hyper-parameter model component type
     */
    typedef typename model_t::Bnet_infeas_lambda_t bnet_infeas_lambda_t;

    /**
     * Prior score may only be cached if hyper-parameter(s) are deterministic
     */
    enum { is_random = bnet_infeas_lambda_t::is_random };

    /**
     * @param bnet - [ignored] Boolean Bayesian network
     * @param num_edges - number of edges in Bayesian network
     * @param model - parent model
     * @param sim - outer simulation
     * @return log-score penalty based on probability of infeasible samples
     */
    inline static cmxReal logscore(const cmxBnetBool& bnet,
                                   const cmxUInt num_edges,
                                   const model_t& model,
                                   const sim_t& sim) {
        // Handle to infeasible set
        const cmxPseudopopFrame_t& infeas_pop =
            sim.get_shared_ro().get_Bnet_infeas().get_infeas_pop();
        // Estimate log probability of infeasible set
        cmxReal max_logp = -CMX_REAL_MAX;
        for (cmxMatSz_t i = 0; i < infeas_pop.n_cols; ++i) {
            cmxAssertPosEq(CMX_TO_MAT_SZ_T(bnet.get_cpts().size()),
                           infeas_pop.n_rows);
            const cmxBool* const bits = infeas_pop.colptr(i);
            const cmxReal logp = bnet.logprob(bits);
            cmxAssert(logp <= CMX_TO_REAL(0.0));
            max_logp = std::max(logp, max_logp);
        }
        // Get hyper-parameter scaling factor
        const cmxReal lambda_scale =
            model.get_Bnet_infeas_lambda().get_scale(sim);
        // ASSUMING LAMBDA SCALE IS NON-NEGATIVE!
        cmxAssert(lambda_scale >= CMX_TO_REAL(0.0));
        // Get hyper-parameter reward ceiling
        const cmxReal lambda_ceil =
            model.get_Bnet_infeas_lambda().get_ceil(sim);
        // ASSUMING LAMBDA CEIL IS NON-NEGATIVE!
        cmxAssert(lambda_ceil >= CMX_TO_REAL(0.0));
        // Multiplying two floating-point values may be numerically unstable.
        // To help mitigate this potential issue, we assume (scale * ceil) is
        // numerically stable; therefore, any max(logprob) < -ceil, can be
        // capped to -ceil instead and result will be unchanged; this capping
        // should only be applied if scale >= 1.0.
        max_logp = (lambda_scale >= CMX_TO_REAL(1.0))
                       ? std::max(max_logp, -lambda_ceil)
                       : max_logp;
        const cmxReal score = std::min((-lambda_scale * max_logp), lambda_ceil);
        return score;
    }
};

//****************************************************************************
// cmxBnetMiscPrior
//****************************************************************************

/**
 * Template arguments:
 *  t_Glue - glue object as defined in cmxGlue
 *  t_ScoreDagEdge - DAG edge prior
 *  t_ScoreInfeas - Prior on prohibited (infeasible) samples
 *
 * Both template priors must provide interface -
 *  ::logscore(bnet, num_edges, model, sim)
 */
template <typename t_Glue,
          typename t_ScoreDagEdge = cmxBnetDagEdgeH95Prior<t_Glue>,
          typename t_ScoreInfeas = cmxBnetInfeasNegpPrior<t_Glue> >
class cmxBnetMiscPrior {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Edge prior type
     */
    typedef t_ScoreDagEdge score_dag_edge_t;

    /**
     * Prohibited (infeasible) sample prior type
     */
    typedef t_ScoreInfeas score_infeas_t;

    /**
     * If any prior hyper-parameters are non-deterministic, let model know
     * that prior score cannot be cached and must be re-computed each time
     */
    enum {
        is_random = (score_dag_edge_t::is_random || score_infeas_t::is_random)
    };

    /**
     * Bayesian network score - applies all miscellaneous priors besides
     * the Dirichlet (Beta) prior on the CPTs which is explicitly handled by
     * Bayesian network model component
     * @param bnet - Boolean Bayesian network
     * @param num_edges - number of edges in Bayesian network
     * @param model - parent model
     * @param sim - outer simulation
     * @return log-score of miscellaneous priors on Bayesian network
     */
    static cmxReal logscore(const cmxBnetBool& bnet,
                            const cmxUInt num_edges,
                            const model_t& model,
                            const sim_t& sim) {
        // Defer to component priors
        cmxReal score = score_dag_edge_t::logscore(bnet, num_edges, model, sim);
        score += score_infeas_t::logscore(bnet, num_edges, model, sim);
        return score;
    }
};

#endif  // cmxBnetMiscPrior_h
