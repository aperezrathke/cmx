//****************************************************************************
// cmxSim.h
//****************************************************************************

/**
 * @brief Simulates parallel Gibbs chains, stores simulation level shared data,
 *  and manages model observers
 */

#ifndef cmxSim_h
#define cmxSim_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxController.h"
#include "cmxGlue.h"
#include "cmxObs.h"
#include "cmxSmartPtr.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxSim
//****************************************************************************

/**
 * Model simulator, manages parallel Gibbs chains, assesses convergence,
 * monitors model states, etc. The template t_Glue has same format as defined
 * in "cmxGlue".
 */
template <
    // Core model types
    typename t_Glue>
class cmxSim {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Default constructor
     */
    cmxSim() { clear(); }

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Initialize from user configuration
     * @param config - user configuration
     */
    void init(cmxSpConstConfig_t config);

    /**
     * Randomize all model chains - can only be called after init. Currently,
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * @param config - user configuration
     */
    void randomize(cmxSpConstConfig_t config);

    /**
     * Reset to default state
     */
    void clear();

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Runs entire simulation to completion.
     * Assumes simulation has been initialized.
     * @return cmxTRUE if simulation successful, cmxFALSE o/w
     */
    cmxBool run();

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Notifies 'active' observers that iteration 'iter' has completed, note
     * that 'passive' observers (aka'reactors') are *not* notified!
     * @param iter - 0-based iteration counter that has completed
     */
    void observers_update(const cmxUInt iter);

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Marks active observers 'stale' and then updates
     * @param iter - 0-based iteration counter that has completed
     */
    void observers_force_update(const cmxUInt iter);

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Notifies 'active' observers that they should set their current state
     *  as stale without clearing history
     */
    void observers_mark_stale();

    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD
     * Notifies *both* 'active' and 'passive' observers (aka 'reactors') that
     * simulation has finished
     */
    void observers_finish_all();

    // Accessors

    /**
     * @return View of configuration object
     */
    inline cmxSpConstConfig_t get_config() const { return m_config; }

    /**
     * @return [Const] handle to simulation shared read-only resources
     */
    inline shared_ro_t& get_shared_ro() { return m_shared_ro; }
    inline const shared_ro_t& get_shared_ro() const { return m_shared_ro; }

    /**
     * @return [Const] handle to simulation shared mutable resources
     */
    inline shared_mu_t& get_shared_mu(CMX_THREAD_ID_0_PARAM) {
        return CMX_ACCESS_TLS_DATA(m_shared_mu);
    }
    inline const shared_mu_t& get_shared_mu(CMX_THREAD_ID_0_PARAM) const {
        return CMX_ACCESS_TLS_DATA(m_shared_mu);
    }

    /**
     * @return [Const] set of models
     */
    inline std::vector<model_t>& get_models() { return m_models; }
    inline const std::vector<model_t>& get_models() const { return m_models; }

    /**
     * @return number of models
     */
    inline size_t get_num_models() const { return m_models.size(); }

    /**
     * @return [Const] set of "active" observers
     */
    inline std::vector<sp_obs_t>& get_observers() { return m_observers; }
    inline const std::vector<sp_obs_t>& get_observers() const {
        return m_observers;
    }

    /**
     * @return number of "active" observers
     */
    inline size_t get_num_observers() const { return m_observers.size(); }

    /**
     * @return [Const] set of "passive" observers, aka "reactors"
     */
    inline std::vector<sp_obs_t>& get_reactors() { return m_reactors; }
    inline const std::vector<sp_obs_t>& get_reactors() const {
        return m_reactors;
    }

    /**
     * @return number of reactors
     */
    inline size_t get_num_reactors() const { return m_reactors.size(); }

private:
    /**
     * Our configuration object
     */
    cmxSpConstConfig_t m_config;

    /**
     * Simulation-level read-only shared data common to all models
     */
    shared_ro_t m_shared_ro;

    /**
     * Simulation-level mutable shared data common to all models
     */
    CMX_DECLARE_TLS_DATA(shared_mu_t, m_shared_mu);

    /**
     * Set of models, each independently simulated
     */
    std::vector<model_t> m_models;

    /**
     * Controller responsible for simulating models and performing
     * administrative tasks such as burn-in, assessing convergence, determining
     * when model has finished, etc.
     */
    sp_controller_t mp_controller;

    /**
     * Set of "active" observers for monitoring model data streams. "Active"
     * observers recieve update() calls after every post burn-in iteration
     */
    std::vector<sp_obs_t> m_observers;

    /**
     * Set of "passive" observers which respond to updates from "active"
     *  observers, they do not receive direct update() calls from controller
     */
    std::vector<sp_obs_t> m_reactors;
};

// Include implementation as we are a template class
#include "cmxSim.inl"

#endif  // cmxSim_h
