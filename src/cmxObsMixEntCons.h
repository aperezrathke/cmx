//****************************************************************************
// cmxObsMixEntCons.h
//****************************************************************************

/**
 * @brief Console observer for model mixture entropies.
 */

#ifndef cmxObsMixEntCons_h
#define cmxObsMixEntCons_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObsMixEntSnap.h"
#include "cmxSmartPtr.h"

//****************************************************************************
// cmxObsMixEntCons
//****************************************************************************

/**
 * Log-score console observer - reports log-score to stdout at a regular,
 * user-defined interval. Depends on log-score snapshot observer
 */
template <typename t_Glue>
class cmxObsMixEntCons : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Snapshot observer type
     */
    typedef cmxObsMixEntSnap<t_Glue> obs_mix_ent_snap_t;

    /**
     * Default console period
     */
    enum { default_period = 1 };

    /**
     * Default constructor
     */
    cmxObsMixEntCons() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Reset
        this->clear();
        // Read snapshot period
        config->read_into(m_period, cmxOpt_obs_mix_ent_cons_period);
        // Ensure period is positive
        m_period = std::max(CMX_TO_UINT(1), m_period);
        // Extract snapshot observer - must occur before console observer
        if (!obs_t::find_raw(&mp_snap, sim, index)) {
            cmxLogf(
                "Error: mixture entropy console observer unable to find handle "
                "to snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
    }

    /**
     * Reset to default state
     */
    void clear() {
        m_period = default_period;
        mp_snap = NULL;
    }

    /**
     * @WARNING - MAY ONLY BE CALLED FROM MAIN THREAD!
     * Callback for when simulation has updated all models, reports to stdout
     * if iteration matches period
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {
        if ((iter % m_period) != CMX_TO_UINT(0)) {
            // Early out if not yet at sampling interval
            return;
        }
        // Report latest snapshot to stdout
        cmxAssert(mp_snap);
        const cmxVecCol& mix_ents = mp_snap->get_mix_ents();
        const cmxMatSz_t n = mix_ents.n_elem;
        for (cmxMatSz_t i = CMX_TO_MAT_SZ_T(0); i < n; ++i) {
            cmxLogf("(ITER %d, MODEL %d) MIX ENTROPY = %f\n",
                    (int)iter,
                    (int)i,
                    mix_ents.at(i));
        }
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_mix_ent_cons)

private:
    /**
     * Reports to stdout on multiples of this period
     */
    cmxUInt m_period;

    /**
     * Handle to snapshot observer - raw pointer is used as lifetime
     * of pointer does not exceed that of parent simulation's shared_ptr
     */
    const obs_mix_ent_snap_t* mp_snap;
};

#endif  // cmxObsMixEntCons_h
