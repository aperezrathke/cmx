//****************************************************************************
// cmxMbrParallel.h
//****************************************************************************

/**
 * @brief Parallel (threaded) model batch runner - performs batched Gibbs MCMC
 *  updating for a set of models. Mbr -> *M*odel *b*atch *r*unner
 */

#ifndef cmxMbrParallel_h
#define cmxMbrParallel_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_THREADS

#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxMcmcGibbsRev.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxMbrParallel
//****************************************************************************

/**
 * Performs batched Gibbs MCMC updating for set of models (parallel version)
 */
template <typename t_Glue>
class cmxMbrParallel {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

private:
    /**
     * Defines the chunk size - the amount of work each thread grabs and
     * must complete before grabbing another chunk of work
     */
    enum { inc_by = 1 };

    /**
     * Work object to process set of models
     */
    class Work : public cmxMappableWork {
    public:
        /**
         * Constructor
         */
        Work(const cmxUInt num_steps, std::vector<model_t>& models, sim_t& sim)
            : m_cached_size(CMX_TO_UINT(models.size())),
              m_num_steps(num_steps),
              m_models(models),
              m_sim(sim) {}

        /**
         * Defines what work is to be performed for the parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         * @return TRUE if work still to be done, FALSE if finished
         */
        virtual cmxBool do_range(const cmxUInt ix_start,
                                 const cmxUInt count,
                                 const cmxUInt thread_id) {
            // Perform Gibbs MCMC for chunk
            const cmxUInt end = std::min(ix_start + count, m_cached_size);
            for (cmxUInt i = ix_start; i < end; ++i) {
                cmxAssertBounds(
                    i, CMX_TO_UINT(0), CMX_TO_UINT(m_models.size()));
                model_t& model = m_models[i];
                cmxMcmcGibbsRev::run(m_num_steps, model, m_sim, thread_id);
            }
            return end != m_cached_size;
        }

    private:
        // Cached number of models
        const cmxUInt m_cached_size;
        // Number of Gibbs MCMC steps to perform
        const cmxUInt m_num_steps;
        // Handle to set of models to batch update
        std::vector<model_t>& m_models;
        // Handle to outer simulation
        sim_t& m_sim;
    };

public:
    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD, this is different from serial
     * version which may be called from any thread
     * Performs Gibbs sampling MCMC for parameter set of models
     * @param num_steps - Number of Gibbs update steps to run for each model
     * @param models - Set of models to be Gibbs updated
     * @param sim - Outer simulation
     */
    static void run(const cmxUInt num_steps,
                    std::vector<model_t>& models,
                    sim_t& sim CMX_THREAD_ID_PARAM) {
        // @TODO - if calling from non-main thread, then default
        // to serial method. Only main thread should be using worker queue.

        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        cmxAssert(CMX_THREAD_ID_0_ARG == CMX_MAIN_THREAD_ID_0_ARG);

        // Create work object
        Work w(num_steps, models, sim);

        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will likely
        // deadlock as we only have a single, global parallel mapper.
        cmxParMap.wait_until_work_finished(w, inc_by);
    }
};

#else  // !defined (CMX_BUILD_ENABLE_THREADS)

// Default to serial method
#ifdef CMX_BUILD_COMPILER_MSVC
#pragma message( \
    "Warning: parallel model batch runner disabled. Defaulting to serial.")
#elif defined(CMX_BUILD_COMPILER_GCC)
#pragma message \
    "Warning: parallel model batch runner disabled. Defaulting to serial."
#endif  // compiler check

#include "cmxMbrSerial.h"

// "Typedef" parallel runner to serial version
#define cmxMbrParallel cmxMbrSerial

#endif  // CMX_BUILD_ENABLE_THREADS

#endif  // cmxMbrParallel_h
