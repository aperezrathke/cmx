//****************************************************************************
// cmxCaptCorrHicParallel.h
//****************************************************************************

/**
 * @brief Parallel batch capture of model Hi-C correlations
 */

#ifndef cmxCaptCorrHicParallel_h
#define cmxCaptCorrHicParallel_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_THREADS

#include "cmxAssert.h"
#include "cmxGlobals.h"
#include "cmxGlue.h"
#include "cmxModelTypes.h"
#include "cmxPearsonCorr.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxCaptCorrHicParallel
//****************************************************************************

/**
 * Performs batched capture of model Hi-C correlations (parallel version)
 */
template <typename t_Glue>
class cmxCaptCorrHicParallel {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

private:
    /**
     * Defines the chunk size - the amount of work each thread grabs and
     * must complete before grabbing another chunk of work
     */
    enum { inc_by = 1 };

    /**
     * Work object to process set of models
     */
    class Work : public cmxMappableWork {
    public:
        /**
         * Constructor
         */
        Work(std::vector<cmxReal>& out_corrs_presized,
             const std::vector<model_t>& models,
             const cmxContactFreqs_t& hic)
            : m_num_models(CMX_TO_UINT(models.size())),
              m_corrs(out_corrs_presized),
              m_models(models),
              m_hic(hic) {
            cmxAssertPosEq(m_corrs.size(), m_models.size());
            cmxAssertPosEq((size_t)m_num_models, m_corrs.size());
        }

        /**
         * Defines what work is to be performed for the parameter range
         * @param ix_start - the starting index to perform work at
         * @param count - the number of work units to perform beginning at
         *  ix_start
         * @param thread_id - a unique calling thread identifier
         * @return TRUE if work still to be done, FALSE if finished
         */
        virtual cmxBool do_range(const cmxUInt ix_start,
                                 const cmxUInt count,
                                 const cmxUInt thread_id) {
            // Perform work for chunk
            const cmxUInt end = std::min(ix_start + count, m_num_models);
            // Iterate over range
            for (cmxUInt i = ix_start; i < end; ++i) {
                cmxAssertBounds(((size_t)i), 0, m_models.size());
                const model_t& model = m_models[i];
                const cmxContactFreqs_t& pseu_hic =
                    model.get_Pseudopop().get_pseudopop_freqs();
                cmxAssertPosEq(m_hic.n_elem, pseu_hic.n_elem);
                // Compute correlation between empirical and pseudo Hi-C
                const cmxReal corr = cmxPearsonCorr::get(
                    m_hic.memptr(), pseu_hic.memptr(), m_hic.n_elem);
                cmxAssertBounds(((size_t)i), 0, m_corrs.size());
                m_corrs[i] = corr;
            }  // End iteration over models
            return end != m_num_models;
        }

    private:
        // Cached number of models
        const cmxUInt m_num_models;
        // Handle to output Hi-C correlations at each model
        std::vector<cmxReal>& m_corrs;
        // Handle to models
        const std::vector<model_t>& m_models;
        // Handle to empirical Hi-C contact frequencies
        const cmxContactFreqs_t& m_hic;
    };

public:
    /**
     * MAY ONLY BE CALLED FROM MAIN THREAD, this is different from serial
     * version which may be called from any thread
     * Capture Hi-C correlations of all models belonging to simulation
     * @param out_corrs_presized - pre-sized vector of output correlations,
     *  MUST BE SAME SIZE AS models!
     * @param models - set of models, must be same size as out_corrs_presized
     * @param hic - empirical (observed) Hi-C frequencies
     */
    static void capture(std::vector<cmxReal>& out_corrs_presized,
                        const std::vector<model_t>& models,
                        const cmxContactFreqs_t& hic CMX_THREAD_ID_PARAM) {
        // @TODO - if calling from non-main thread, then default
        // to serial method. Only main thread should be using worker queue.

        // This is not a fool-proof check as worker 0 and main thread share
        // the same thread id, but this will hopefully trap many of the cases
        // where this method is called from a non-main thread.
        cmxAssert(CMX_THREAD_ID_0_ARG == CMX_MAIN_THREAD_ID_0_ARG);

        // Create work object
        Work w(out_corrs_presized, models, hic);

        // This call is the reason we can only use this method from the main
        // thread. Any main thread work will be overwritten and we will likely
        // deadlock as we only have a single, global parallel mapper.
        cmxParMap.wait_until_work_finished(w, inc_by);
    }
};

#else  // !defined (CMX_BUILD_ENABLE_THREADS)

// Default to serial method
#ifdef CMX_BUILD_COMPILER_MSVC
#pragma message( \
    "Warning: parallel Hi-C correlation batch capture disabled. Defaulting to serial.")
#elif defined(CMX_BUILD_COMPILER_GCC)
#pragma message \
    "Warning: parallel Hi-C correlation batch capture disabled. Defaulting to serial."
#endif  // compiler check

#include "cmxCaptCorrHicSerial.h"

// "Typedef" parallel runner to serial version
#define cmxCaptCorrHicParallel cmxCaptCorrHicSerial

#endif  // CMX_BUILD_ENABLE_THREADS

#endif  // cmxCaptCorrHicParallel_h
