//****************************************************************************
// cmxObs.h
//****************************************************************************

/**
 * @brief Observer interface for tracking simulation data
 */

#ifndef cmxObs_h
#define cmxObs_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxController.h"
#include "cmxGlue.h"
#include "cmxSmartPtr.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// Enums
//****************************************************************************

/**
 * Enumerated observer types, the basic classes are:
 *  snap - Snapshot, records current state of observed value according to some
 *      typically regular interval
 *  cons - Console, logs the value of a snapshot observer to stdout
 *  trac - Tracer, records a history of a snapshot observer and then
 *      typically writes trace to disk
 *
 * Types must be registered here for use with cmxObsBuilder. The derived child
 * type must use CMX_OBS_DECLARE_ETYPE(...) macro to provide type interface.
 *
 * All dependent observer types must be listed *AFTER* their parent type, only
 * acyclic dependencies allowed (where a 'dependency' means the observer has
 * a reference/pointer to some other, i.e. parent, observer type)
 *
 * @WARNING - MUST BE SAME ORDER AS cmxObsBuilder::init() OBSERVER_NAMES array
 */
enum cmxObsEtype {
    cmxObsEtype_logscore_snap = 0,
    cmxObsEtype_logscore_cons,
    cmxObsEtype_logscore_trac,
    cmxObsEtype_corr_hic_snap,
    cmxObsEtype_corr_hic_cons,
    cmxObsEtype_corr_hic_trac,
    cmxObsEtype_bnet_snap,
    cmxObsEtype_bnet_ring,
    cmxObsEtype_clust_size_snap,
    cmxObsEtype_clust_hist_cons,
    cmxObsEtype_mix_pmf_snap,
    cmxObsEtype_mix_hist_cons,
    cmxObsEtype_mix_ent_snap,
    cmxObsEtype_mix_ent_cons,
    cmxObsEtype_mix_ent_trac,
    cmxObsEtype_pacc_bnet_cons,
    cmxObsEtype_pacc_pseu_cons,
    cmxObsEtype_logscore_mapc,
    cmxObsEtype_logscore_mapg,
    cmxObsEtype_corr_hic_mapc,
    cmxObsEtype_corr_hic_mapg,
    cmxObsEtype_bnet_mapc,
    cmxObsEtype_bnet_mapg,
    cmxObsEtype_pseu_frame_cons,
    cmxObsEtype_pseu_frame_mapc,
    cmxObsEtype_pseu_frame_mapg,
    cmxObsEtype_pseu_hic_mapc,
    cmxObsEtype_pseu_hic_mapg,
    cmxObsEtype_mix_ent_mapc,
    cmxObsEtype_mix_ent_mapg,
    cmxObsEtype_num
};

//****************************************************************************
// Macros
//****************************************************************************

/**
 * Dynamic type system for observers, assumes public scope. Adds run-time type
 * checking with etype_static() and etype() overload.
 * @param ety_ - Enumerated cmxObsEtype
 */
#define CMX_OBS_DECLARE_ETYPE(ety_)                           \
    inline static cmxObsEtype etype_static() { return ety_; } \
    virtual cmxObsEtype etype() const { return ety_; }

//****************************************************************************
// cmxObs
//****************************************************************************

/**
 * Observer pattern (https://en.wikipedia.org/wiki/Observer_pattern) for
 * monitoring simulation data streams. The template argument t_Glue has
 * same format as "cmxGlue".
 *
 * See:
 *  Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides (1994). Design
 *      Patterns: Elements of Reusable Object-Oriented Software. Addison
 *      Wesley. pp. 293ff. ISBN 0-201-63361-2
 */
template <typename t_Glue>
class cmxObs {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Virtual destructor
     */
    virtual ~cmxObs() {}

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within observers array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {}

    /**
     * Callback for when simulation has updated all models
     * @param iter - 0-based iteration that has just completed
     * @param sim - parent simulation
     */
    virtual void update(const cmxUInt iter, sim_t& sim) = 0;

    /**
     * Callback for when simulation has finished running, can be used for
     * logging final observer status to stdout or hard disk
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {}

    /**
     * This method should be overloaded in derived observers using
     * the CMX_OBS_DECLARE_ETYPE macro
     * @return Enumerated observer type
     */
    virtual cmxObsEtype etype() const = 0;

    /**
     * Marks current capture as stale without clearing
     */
    virtual void mark_stale() {}

    /**
     * Find [first] raw pointer handle to observer type, searches among ALL
     * registered observers. The template observer type 't_obs' is assumed to
     * have defined t_obs::etype_static() which returns a cmxObsEtype and is
     * used to identify the derived observer type.
     *
     * @WARNING: This method is intended for use by objects whose lifetimes
     * are owned by the parameter simulation such as observers and controllers
     * - it is safe to store raw observer pointers internally within these
     * objects as they will never access the pointer after the simulation has
     * been destroyed (since their lifetime is tied to the simulation). For
     * all other situations in which the raw pointer lifetime may exceed the
     * the scope of the owning simulation shared_ptr, a shared_ptr or weak_ptr
     * is likely required.
     *
     * @param raw - output raw pointer
     * @param sim - parent simulation
     * @param cmxTRUE if observer of template type found, cmxFALSE o/w
     */
    template <typename t_obs>
    static cmxBool find_raw(t_obs** pp_raw, const sim_t& sim) {
        return find_raw(pp_raw, sim, sim.get_observers().size());
    }

    /**
     * Find [first] raw pointer handle to observer type, searches among
     * registered observers in range [0, stop). The template observer type
     * 't_obs' is assumed to have defined t_obs::etype_static() which returns
     * a cmxObsEtype and is used to identify the derived observer type.
     *
     * @WARNING: This method is intended for use by objects whose lifetimes
     * are owned by the parameter simulation such as observers and controllers
     * - it is safe to store raw observer pointers internally within these
     * objects as they will never access the pointer after the simulation has
     * been destroyed (since their lifetime is tied to the simulation). For
     * all other situations in which the raw pointer lifetime may exceed the
     * the scope of the owning simulation shared_ptr, a shared_ptr or weak_ptr
     * is likely required.
     *
     * @param raw - output raw pointer
     * @param sim - parent simulation
     * @param stop - stop index to end search at (exclusive)
     * @param cmxTRUE if observer of template type found, cmxFALSE o/w
     */
    template <typename t_obs>
    static cmxBool find_raw(t_obs** pp_raw,
                            const sim_t& sim,
                            const size_t stop) {
        cmxAssert(pp_raw);
        const std::vector<sp_obs_t>& observers = sim.get_observers();
        cmxAssertBoundsInc(stop, 0, observers.size());
        const cmxObsEtype et = t_obs::etype_static();
        cmxAssert(et != cmxObsEtype_num);
        for (size_t i = 0; i < stop; ++i) {
            cmxAssert(static_cast<bool>(observers[i]));
            if (observers[i]->etype() == et) {
                // Success, matching observer type found!
                *pp_raw = static_cast<t_obs*>(observers[i].get());
                return cmxTRUE;
            }
        }
        // Failed, matching observer type not found!
        return cmxFALSE;
    }
};

#endif  // cmxObs_h
