//****************************************************************************
// cmxContactFreqsAlphaDetrMoco.h
//****************************************************************************

/**
 * @brief Real-valued scalar hyper-parameter for empirical Hi-C likelihood
 */

#ifndef cmxContactFreqsAlphaDetrMoco_h
#define cmxContactFreqsAlphaDetrMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxOpts.h"
#include "cmxScalarDetrMoco.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxContactFreqsAlphaDetrMoco
//****************************************************************************

/**
 * Simulation-level read-only shared data, provides user configured alpha
 * (a hyper-parameter for likelihood of empirical Hi-C)
 */
class cmxContactFreqsAlphaDetrSharcoRo {
public:
    /**
     * Default constructor
     */
    cmxContactFreqsAlphaDetrSharcoRo()
        : m_alpha(CMX_DEFAULT_CONTACT_FREQS_ALPHA) {}

    /**
     * Initializes shared resource(s) - captures user-supplied alpha
     * @param config - user options
     */
    void init(cmxSpConstConfig_t config) {
        m_alpha = CMX_DEFAULT_CONTACT_FREQS_ALPHA;
        config->read_into(m_alpha, cmxOpt_hic_alpha);
        // @TODO - add switch checking if alpha is in valid range (i.e., check
        // if underlying score is Dirichlet, in which case, alpha must be > 0)
    }

    /**
     * Reset read-only state
     */
    void clear() { m_alpha = CMX_DEFAULT_CONTACT_FREQS_ALPHA; }

    /**
     * @return user configured alpha scalar
     */
    inline cmxReal get_scalar() const { return m_alpha; }

private:
    /**
     * Empirical Hi-C likelihood hyper-parameter
     */
    cmxReal m_alpha;
};

/**
 * Utility for querying global read-only empirical Hi-C alpha parameter
 */
template <typename t_Glue>
class cmxContactFreqsAlphaDetrSharedQuery {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);
    /**
     * @param sim - outer simulation
     * @return real-valued scalar hyper-parameter
     */
    static inline cmxReal get_scalar(const sim_t& sim) {
        const cmxReal alpha =
            sim.get_shared_ro().get_Contact_freqs_alpha().get_scalar();
        return alpha;
    }
};

/**
 * Root-level deterministic scalar node representing hyper-parameter for
 *  empirical Hi-C likelihood
 */
template <typename t_Glue>
class cmxContactFreqsAlphaDetrMoco
    : public cmxScalarDetrMoco<t_Glue,
                               cmxContactFreqsAlphaDetrSharedQuery<t_Glue> > {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);
};

#endif  // cmxContactFreqsAlphaDetrMoco_h
