//****************************************************************************
// cmxObsMixEntMapc.h
//****************************************************************************

/**
 * @brief Records maximum a posteriori (MAP) estimate for mixture entropies
 *  at each MCMC model chain
 */

#ifndef cmxObsMixEntMapc_h
#define cmxObsMixEntMapc_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxObs.h"
#include "cmxObsLogscoreMapc.h"
#include "cmxObsMixEntSnap.h"
#include "cmxSmartPtr.h"

//****************************************************************************
// cmxObsMixEntMapc
//****************************************************************************

/**
 * Chain MAP observer - captures maximum a posteriori estimate of
 *  observed parameter at each MCMC chain
 */
template <typename t_Glue>
class cmxObsMixEntMapc : public cmxObsLogscoreMapc<t_Glue>::map_reactor_t {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Log-score chain MAP observer type
     */
    typedef cmxObsLogscoreMapc<t_Glue> obs_logscore_map_t;

    /**
     * Snapshot observer type
     */
    typedef cmxObsMixEntSnap<t_Glue> obs_mix_ent_snap_t;

    /**
     * Default constructor
     */
    cmxObsMixEntMapc() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within reactors array
     */
    virtual void init_react(cmxSpConstConfig_t config,
                            sim_t& sim,
                            const size_t index) {
        // Reset
        this->clear();
        // Extract snapshot observer
        if (!obs_t::find_raw(&mp_snap, sim)) {
            cmxLogf(
                "Error: mixture entropy chain MAP observer unable to find "
                "handle to snapshot observer. Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_snap);
        // Allocate buffer
        cmxAssert(sim.get_num_models() > 0);
        m_maps.resize(sim.get_num_models(), CMX_TO_REAL(0.0));
    }

    /**
     * Reset to default state
     */
    void clear() {
        mp_snap = NULL;
        m_maps.clear();
    }

    /**
     * Reactor update managed by logscore MAP observer, when a new MAP
     * estimate is encountered, this method will be called
     * @param iter - Typically post burn-in iteration number (0-based)
     * @param sim - Outer simulation
     * @param i_model - Index of MAP model
     * @param logscore - new MAP logscore associated to MAP model
     */
    virtual void update_react(const cmxUInt iter,
                              sim_t& sim,
                              const size_t i_model,
                              const cmxReal logscore) {
        cmxAssert(mp_snap);
        if (mp_snap->is_stale(iter)) {
            // Snapshot is stale, capture new snapshot
            mp_snap->force_update(iter, sim);
        }
        // Update MAP estimate
        const cmxVecCol& mix_ents = mp_snap->get_mix_ents();
        cmxAssertBounds(
            CMX_TO_MAT_SZ_T(i_model), CMX_TO_MAT_SZ_T(0), mix_ents.n_elem);
        cmxAssertPosEq(mix_ents.n_elem, CMX_TO_MAT_SZ_T(m_maps.size()));
        m_maps[i_model] = mix_ents.at(CMX_TO_MAT_SZ_T(i_model));
    }

    /**
     * Callback for when simulation has finished running
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        const obs_logscore_map_t& logscore_map = this->get_logscore_map();
        cmxLogf("Exporting mixture entropy chain MAP estimates...\n");
        logscore_map.export_csv(m_maps,
                                "map.chain.ent", /*file_id*/
                                false,           /*b_warn*/
                                sim);
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_mix_ent_mapc)

private:
    /**
     * Handle to snapshot observer - raw pointer is used as lifetime of
     * pointer does not exceed that of parent simulation's shared_ptr
     */
    obs_mix_ent_snap_t* mp_snap;

    /**
     * MAP (maximum a posteriori) estimates
     */
    std::vector<cmxReal> m_maps;
};

#endif  // cmxObsMixEntMapc_h
