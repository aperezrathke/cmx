//****************************************************************************
// cmxBnetBoolRandomizer.h
//****************************************************************************

/**
 * @brief Model component for Bayesian network with only boolean nodes
 */

#ifndef cmxBnetBoolRandomizer_h
#define cmxBnetBoolRandomizer_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxMcmcMh4gMixin.h"
#include "cmxOpts.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetBoolRandomizer
//****************************************************************************

/**
 * Template arguments:
 *  t_BnetBoolMoco - boolean Bayesian network model component
 */
template <typename t_BnetBoolMoco>
class cmxBnetBoolRandomizer : public t_BnetBoolMoco::mcmc_mixin_t {
private:
    /**
     * Bayesian network model component
     */
    typedef t_BnetBoolMoco bnet_bool_moco_t;

public:
    typedef typename bnet_bool_moco_t::glue_t t_Glue;
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * MCMC mixin type
     */
    typedef typename bnet_bool_moco_t::mcmc_mixin_t mcmc_mixin_t;

    /**
     * Metropolis argument
     */
    typedef typename bnet_bool_moco_t::Mh_args_t Mh_args_t;

    /**
     * Metropolis[-Hastings] state type
     */
    typedef typename bnet_bool_moco_t::state_t state_t;

    /**
     * Default number of Metropolis-Hastings steps to use for state
     * randomization
     */
    enum { default_randomize_steps = 100 };

    /**
     * Metropolis-Hastings interface
     * Proposes new candidate state and updates target probabilities of
     * current and candidate states given the current state of parent model
     */
    void Mh_prime_cand(const Mh_args_t mh_args CMX_THREAD_ID_PARAM) {
        // Defer to common preamble
        bnet_bool_moco_t::Mh_preamble(*this, mh_args CMX_THREAD_ID_ARG);
    }

    /**
     * Callback for when current state is swapped by MCMC
     */
    inline void Mh_on_accept_cand(const Mh_args_t mh_args) {
        // Forward call to parent
        this->mcmc_mixin_t::Mh_on_accept_cand();
    }

    /**
     * Callback for when current state is retained by MCMC
     */
    inline void Mh_on_reject_cand(const Mh_args_t mh_args) {
        // Forward call to parent
        this->mcmc_mixin_t::Mh_on_reject_cand();
    }

    /**
     * Randomize external model component state - sample from prior
     */
    static void randomize_moco_state(state_t& s,
                                     cmxSpConstConfig_t config,
                                     model_t& model,
                                     sim_t& sim CMX_THREAD_ID_PARAM) {
        // Initialize from parameter state
        cmxBnetBoolRandomizer rzr;
        rzr.get_curr() = s;
        rzr.get_cand() = s;
        cmxUInt num_steps = default_randomize_steps;
        config->read_into(num_steps, cmxOpt_Bnet_randomize_steps);
        // Randomize by sampling from prior
        // Note, block_index is unused, set to default value
        const cmxUInt block_index = CMX_TO_UINT(0);
        Mh_args_t mh_args = {block_index, &model, &sim};
        mcmc_mixin_t::Mh_run(num_steps, rzr, mh_args CMX_THREAD_ID_ARG);
        // Emit randomized state
        s = rzr.get_curr();
    }

    /**
     * Interface expected by Mh_preamble, only uses alpha counts to allow
     * sampling from prior
     * @param cpt_counts - output buffer for storing tabular counts
     * @param s - Metropolis-Hastings state for which to update counts
     * @param block_index - location identifier unused!
     * @param model - parent model
     * @param sim - outer simulation
     */
    static void tally_cpt_counts(std::vector<cmxBnetBoolCt>& cpt_counts,
                                 const state_t& s,
                                 const cmxUInt block_index,
                                 const model_t& model,
                                 const sim_t& sim) {
        bnet_bool_moco_t::fill_cpt_alpha(cpt_counts, s, model, sim);
    }

    /**
     * Allow mixin access to private init_state(...) and randomize_state(..)
     * members
     */
    friend mcmc_mixin_t;
};

#endif  // cmxBnetBoolRandomizer_h
