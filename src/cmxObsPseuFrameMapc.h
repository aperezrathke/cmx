//****************************************************************************
// cmxObsPseuFrameMapc.h
//****************************************************************************

/**
 * @brief Records maximum a posteriori (MAP) estimate at each MCMC chain for
 *  latent Pseudo-population single-cell states
 */

#ifndef cmxObsPseuFrameMapc_h
#define cmxObsPseuFrameMapc_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxExitCodes.h"
#include "cmxExportCore.h"
#include "cmxFilesystem.h"
#include "cmxGlue.h"
#include "cmxLogf.h"
#include "cmxModelTypes.h"
#include "cmxObs.h"
#include "cmxObsLogscoreMapc.h"
#include "cmxSmartPtr.h"

#include <fstream>
#include <string>

//****************************************************************************
// cmxObsPseuFrameMapc
//****************************************************************************

/**
 * Chain MAP observer - captures maximum a posteriori estimate of
 *  observed parameter at each MCMC chain
 */
template <typename t_Glue>
class cmxObsPseuFrameMapc : public cmxObsLogscoreMapc<t_Glue>::map_reactor_t {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Log-score chain MAP observer type
     */
    typedef cmxObsLogscoreMapc<t_Glue> obs_logscore_map_t;

    /**
     * Default constructor
     */
    cmxObsPseuFrameMapc() { this->clear(); }

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within reactors array
     */
    virtual void init_react(cmxSpConstConfig_t config,
                            sim_t& sim,
                            const size_t index) {
        // Reset
        this->clear();
        // Allocate buffer
        cmxAssert(sim.get_num_models() > 0);
        m_maps.resize(sim.get_num_models());
    }

    /**
     * Reset to default state
     */
    void clear() { m_maps.clear(); }

    /**
     * Reactor update managed by logscore MAP observer, when a new MAP
     * estimate is encountered, this method will be called
     * @param iter - Typically post burn-in iteration number (0-based)
     * @param sim - Outer simulation
     * @param i_model - Index of MAP model
     * @param logscore - new MAP logscore associated to MAP model
     */
    virtual void update_react(const cmxUInt iter,
                              sim_t& sim,
                              const size_t i_model,
                              const cmxReal logscore) {
        // Update MAP estimate
        cmxAssertBounds(i_model, 0, sim.get_num_models());
        cmxAssertPosEq(sim.get_num_models(), m_maps.size());
        m_maps[i_model] =
            sim.get_models()[i_model].get_Pseudopop().get_pseudopop_frame();
    }

    /**
     * Callback for when simulation has finished running
     * @param sim - parent simulation
     */
    virtual void finish(sim_t& sim) {
        // Determine if export enabled
        cmxBool b_export = CMX_TO_UINT(0);
        cmxSpConstConfig_t config(sim.get_config());
        config->read_into(b_export, cmxOpt_obs_pseu_frame_mapc_export);
        if (!b_export || m_maps.empty()) {
            // Early out if user did not specify export or no data present
            return;
        }
        const obs_logscore_map_t& logscore_map = this->get_logscore_map();
        cmxLogf("Exporting pseudo frame chain MAP estimates...\n");
        const size_t num_maps = m_maps.size();
        for (size_t i = 0; i < num_maps; ++i) {
            // Skip if no MAP capture available for this MCMC model chain
            if (!logscore_map.has_capture(i)) {
                continue;
            }
            // Determine export path
            const std::string fpath =
                cmxExport::get_fpath(config,
                                     i,                /*index*/
                                     "",               /*subdir*/
                                     "map.pseu.frame", /*file_id*/
                                     "csv");           /*ext*/
            // Make sure output directory exists
            cmxFs_create_parent_dirs(fpath);
            const cmxPseudopopFrame_t& chain_map = m_maps[i];
            // Export MAP estimate to disk
            if (!chain_map.save(fpath, cmxMatrixUtils::csv_ascii, false)) {
                // Inform user that export path is invalid
                cmxLogf(
                    "Warning: pseudo frame chain MAP observer unable to export "
                    "to path: %s\n",
                    fpath.c_str());
            }
        }
    }

    /**
     * Dynamic type system for observers
     */
    CMX_OBS_DECLARE_ETYPE(cmxObsEtype_pseu_frame_mapc)

private:
    /**
     * MAP (maximum a posteriori) estimates
     */
    std::vector<cmxPseudopopFrame_t> m_maps;
};

#endif  // cmxObsPseuFrameMapc_h
