//****************************************************************************
// cmxCaptMixPmfSerial.h
//****************************************************************************

/**
 * @brief Serial (non-parallel) batch capture of model mixture weights
 */

#ifndef cmxCaptMixPmfSerial_h
#define cmxCaptMixPmfSerial_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxCaptMixPmfTypes.h"
#include "cmxGlue.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxCaptMixPmfSerial
//****************************************************************************

/**
 * Performs batched capture of model mixture weights (serial version)
 */
template <typename t_Glue>
class cmxCaptMixPmfSerial {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Capture sorted mixture weights at each simulated model
     * @param out - vector of output sorted mixture weights, will correspond
     *  to each simulated model
     * @param sim - Parent simulation
     */
    static void capture(std::vector<cmxMixPmfPqueue_t>& out,
                        const sim_t& sim CMX_THREAD_ID_PARAM) {
        const std::vector<model_t>& models = sim.get_models();
        const size_t NUM_MODELS = models.size();
        out.resize(NUM_MODELS);
        cmxAssertPosEq(models.size(), out.size());
        // Iterate over models
        for (size_t i_model = 0; i_model < NUM_MODELS; ++i_model) {
            const model_t& model = models[i_model];
            cmxMixPmfPqueue_t& pqueue = out[i_model];
            cmx_mix_pmf_fill(pqueue, model, sim);
        }  // End iteration over models
    }
};

#endif  // cmxCaptMixPmfSerial_h
