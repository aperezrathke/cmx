//****************************************************************************
// cmxTestBnetThinDag.h
//****************************************************************************

/**
 * Commandlet for testing a thin Bnet DAG structure
 *
 * Usage:
 * -test_bnet_thin_dag
 */

#ifdef cmxTestBnetThinDag_h
#error "Bnet Thin Dag Test included multiple times!"
#endif  // cmxTestBnetThinDag_h
#define cmxTestBnetThinDag_h

#include "cmxBuild.h"

#ifdef CMX_BUILD_ENABLE_TESTS

#include "cmxAssert.h"
#include "cmxBnetThinDag.h"
#include "cmxBnetThinEdgeStatesMu.h"
#include "cmxBnetThinEdgeStatesRo.h"
#include "cmxCmdOptsMap.h"
#include "cmxExitCodes.h"
#include "cmxLogf.h"
#include "cmxTypes.h"

#include <algorithm>

/**
 * Anonymous namespace
 */
namespace {

/**
 *  Actual namespace to avoid naming clashes
 */
namespace cmxTestBnetThinDag {
/**
 * Log test result
 */
void report_test(const char* desc, const cmxBool result) {
    cmxLogf("%s? %s\n", desc, result == cmxTRUE ? "passed" : "failed");
}

/**
 * Print an adjacency map
 */
void print_adj_map(const cmxBnetThinDag::AdjMap_t& adj_map) {
    for (cmxUInt i = 0; i < adj_map.size(); ++i) {
        cmxLogf("%u:", i);
        const cmxBnetThinDag::VxList_t& vx_lst = adj_map[i];
        for (cmxUInt j = 0; j < vx_lst.size(); ++j) {
            cmxLogf(" %u", vx_lst[j]);
        }
        cmxLogf("\n");
    }
}

/**
 * Prints src-2-dst and dst-2-src maps
 */
void print_graph_state(const cmxBnetThinDag& dag) {
    cmxLogf("SRC-2-DST MAP (PARENT TO CHILD):\n");
    const cmxBnetThinDag::AdjMap_t& src2dst = dag.get_src2dst_map();
    print_adj_map(src2dst);
    cmxLogf("DST-2-SRC MAP (CHILD TO PARENT):\n");
    const cmxBnetThinDag::AdjMap_t& dst2src = dag.get_dst2src_map();
    print_adj_map(dst2src);
}

/**
 * Utility to check if edge exists within DAG
 */
cmxBool has_edge(const cmxBnetThinDag& dag,
                 const cmxUInt v_src,
                 const cmxUInt v_dst,
                 const cmxBool expected) {
    const cmxBnetThinDag::AdjMap_t& src2dst = dag.get_src2dst_map();
    const cmxBnetThinDag::VxList_t& src_vlst = src2dst[v_src];
    const cmxBnetThinDag::VxList_t::const_iterator src_itr =
        std::find(src_vlst.begin(), src_vlst.end(), v_dst);
    const cmxBool src_found = src_itr != src_vlst.end();

    const cmxBnetThinDag::AdjMap_t& dst2src = dag.get_dst2src_map();
    const cmxBnetThinDag::VxList_t& dst_vlst = dst2src[v_dst];
    const cmxBnetThinDag::VxList_t::const_iterator dst_itr =
        std::find(dst_vlst.begin(), dst_vlst.end(), v_src);
    const cmxBool dst_found = dst_itr != dst_vlst.end();

    cmxAssert(src_found == expected);
    cmxAssert(dst_found == expected);
    return src_found && dst_found;
}
}  // namespace cmxTestBnetThinDag

}  // end of anonymous namespace

/**
 * Entry point for test
 */
int cmxTestBnetThinDagMain(const cmxCmdOptsMap& cmd_opts) {
    using namespace cmxTestBnetThinDag;
    cmxLogf("Running Test Bnet Thin Dag.\n");

    cmxBool success = cmxTRUE;

    /**
     * Test construction of acyclic graph, "+" is an arrowhead:
     *
     *         0
     *       / | \
     *      +  |  |
     *      1  |  |
     *    / |  |  |
     *   |  +  |  |
     *   |  2  |  |
     *   |   \ |  |
     *   |    ++  |
     *   |     3  |
     *   |      \ |
     *   |       ++
     *   |        5
     *   |       /
     *   |      /
     *   |     /
     *   |    /
     *   |   /
     *   |  /
     *   | /
     *   ++
     *   4
     */

    /////////////////////////////////////////////////////
    // TEST: add_vertices(...)

    cmxBnetThinDag dag;
    cmxUInt lo, hi;

    const cmxUInt NUM_VERTS_1 = 6;
    dag.add_vertices(NUM_VERTS_1, lo, hi);
    cmxVerify(success &= (0 == lo));
    cmxVerify(success &= ((NUM_VERTS_1 - 1) == hi));
    cmxVerify(success &= (dag.get_v_num() == NUM_VERTS_1));
    report_test("Test add vertices", success);

    /////////////////////////////////////////////////////
    // TEST: add_edge(...)

    std::vector<cmxUInt> in_deg_buf;
    in_deg_buf.reserve(NUM_VERTS_1);
    std::vector<cmxUInt> stack_buf;
    stack_buf.reserve(NUM_VERTS_1);

    // Edges to create
    const cmxUInt EDGES_1[][2] = {
        {0, 1},  // 0
        {0, 3},  // 1
        {0, 5},  // 2
        {1, 2},  // 3
        {1, 4},  // 4
        {2, 3},  // 5
        {3, 5},  // 6
        {5, 4}   // 7
    };
    const cmxUInt NUM_EDGES_1 = sizeof(EDGES_1) / sizeof(EDGES_1[0]);
    cmxLogf("Attempting construction of DAG with %u vertices and %u edges.\n",
            NUM_VERTS_1,
            NUM_EDGES_1);

    // Create read-only and mutable properties
    const cmxUIMatrix EDGE_PROPS_1(&(EDGES_1[0][0]),  // aux_mem
                                   2,                 // n_rows
                                   NUM_EDGES_1        // n_cols
    );
    EDGE_PROPS_1.print("Edges that will be added (columns):");
    cmxBnetThinEdgeStatesRo ro;
    ro.init(EDGE_PROPS_1, NUM_VERTS_1);
    cmxVerify(success &= (ro.get_v_num() == dag.get_v_num()));
    cmxBnetThinEdgeStatesMu mu;
    mu.init(ro);

    // Add edges
    cmxBnetThinEdgeRemovalInfo_t eri;
    cmxUInt tmp_key;
    for (cmxUInt i = 0; i < NUM_EDGES_1; ++i) {
        const cmxUInt src = EDGES_1[i][0];
        const cmxUInt dst = EDGES_1[i][1];
        cmxAssert(EDGE_PROPS_1.at(
                      cmxBnetThinEdgeStatesRo::cmxBnetThinEdge_ro_src, i) ==
                  src);
        cmxAssert(EDGE_PROPS_1.at(
                      cmxBnetThinEdgeStatesRo::cmxBnetThinEdge_ro_dst, i) ==
                  dst);
        cmxVerify(success &= dag.can_add_edge(src, dst, in_deg_buf, stack_buf));
        dag.add_edge(src, dst, eri);
        cmxVerify(success &= ro.has_key(src, dst, tmp_key));
        cmxVerify(success &= (i == tmp_key));
        mu.on_add_edge(i, eri);
        cmxVerify(success &= mu.has_edge(i));
    }
    report_test("Test DAG add edges", success);

    /////////////////////////////////////////////////////
    // TEST: has_edge(...)

    // Verify edges are within DAG
    for (cmxUInt i = 0; i < NUM_EDGES_1; ++i) {
        const cmxUInt src = EDGES_1[i][0];
        const cmxUInt dst = EDGES_1[i][1];
        cmxVerify(success &= has_edge(dag, src, dst, cmxTRUE));
    }
    report_test("Test DAG has edges", success);

    /////////////////////////////////////////////////////
    // TEST: cannot add reverse edge (v_dst, v_src)

    // Verify cannot add reverse edge from v_dst to v_src
    for (cmxUInt i = 0; i < NUM_EDGES_1; ++i) {
        const cmxUInt src = EDGES_1[i][0];
        const cmxUInt dst = EDGES_1[i][1];
        cmxVerify(success &=
                  !dag.can_add_edge(dst, src, in_deg_buf, stack_buf));
    }
    report_test("Test reverse edge cycles avoided", success);

    /////////////////////////////////////////////////////
    // TEST: avoid non-trivial cycles

    const cmxUInt EDGES_2[][2] = {
        {2, 0},  // 0
        {3, 1},  // 1
        {4, 0},  // 2
        {4, 3},  // 3
        {5, 1},  // 4
        {5, 2}   // 5
    };
    const cmxUInt NUM_EDGES_2 = sizeof(EDGES_2) / sizeof(EDGES_2[0]);

    for (cmxUInt i = 0; i < NUM_EDGES_2; ++i) {
        // Attempt to add directed edge (will create a cycle)
        const cmxUInt src = EDGES_2[i][0];
        const cmxUInt dst = EDGES_2[i][1];
        cmxVerify(success &=
                  !dag.can_add_edge(src, dst, in_deg_buf, stack_buf));
        // Also check that unregistered edges don't have keys
        cmxVerify(success &= !ro.has_key(src, dst, tmp_key));
    }
    report_test("Test non-trivial cycles avoided", success);

    /////////////////////////////////////////////////////
    // TEST: topological sort

    std::vector<cmxUInt> order;
    order.reserve(NUM_VERTS_1);
    dag.topo_sort(order, in_deg_buf, stack_buf);

    cmxLogf("Topological sort:");
    for (cmxUInt i = 0; i < order.size(); ++i) {
        cmxLogf(" %u", order[i]);
    }
    cmxLogf("\n");
    cmxVerify(success &= order.size() == NUM_VERTS_1);
    cmxVerify(success &= order.back() == (NUM_VERTS_1 - 2));
    report_test("Test topological sort", success);

    print_graph_state(dag);

    /////////////////////////////////////////////////////
    // TEST: remove all edges

    cmxBnetThinDag dag2 = dag;
    cmxBnetThinEdgePostRemovalInfo_t epri;
    for (cmxUInt i = 0; i < NUM_EDGES_1; ++i) {
        const cmxUInt src = EDGES_1[i][0];
        const cmxUInt dst = EDGES_1[i][1];
        cmxLogf("...removing edge (%u, %u)\n", src, dst);
        cmxVerify(success &= has_edge(dag2, src, dst, cmxTRUE));
        mu.get_eri(i, eri);
        dag2.remove_edge(src, eri, epri);
        cmxVerify(success &= !has_edge(dag2, src, dst, cmxFALSE));
        mu.on_remove_edge(i, epri, ro);
        cmxVerify(success &= !mu.has_edge(i));
    }
    report_test("Test DAG remove all edges", success);

    print_graph_state(dag2);

    /////////////////////////////////////////////////////
    // TEST: add all edges back

    for (cmxUInt i = 0; i < NUM_EDGES_1; ++i) {
        const cmxUInt src = EDGES_1[i][0];
        const cmxUInt dst = EDGES_1[i][1];
        cmxVerify(success &=
                  dag2.can_add_edge(src, dst, in_deg_buf, stack_buf));
        dag2.add_edge(src, dst, eri);
        cmxVerify(success &= ro.has_key(src, dst, tmp_key));
        cmxVerify(success &= (i == tmp_key));
        mu.on_add_edge(i, eri);
        cmxVerify(success &= mu.has_edge(i));
    }
    report_test("Test DAG re-add edges", success);

    print_graph_state(dag2);

    /////////////////////////////////////////////////////
    // TEST: remove edge to allow prev forbidden edge

    // Remove edge (2,3)
    cmxBnetThinDag dag3 = dag2;
    cmxVerify(success &= has_edge(dag3, 2, 3, cmxTRUE));
    cmxVerify(success &= !dag3.can_add_edge(5, 2, in_deg_buf, stack_buf));
    tmp_key = ro.get_key(2, 3);
    mu.get_eri(tmp_key, eri);
    dag3.remove_edge(2, eri, epri);
    cmxVerify(success &= !has_edge(dag3, 2, 3, cmxFALSE));
    // Verify can now add edge (5,2)
    cmxVerify(success &= dag3.can_add_edge(5, 2, in_deg_buf, stack_buf));
    report_test("Test now can add forbidden edge", success);

    CMX_STATS_REPORT;

    return success ? cmxExitCode_normal : cmxExitCode_error;
}

#endif  // CMX_BUILD_ENABLE_TESTS
