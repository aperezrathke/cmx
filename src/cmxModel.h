//****************************************************************************
// cmxModel.h
//****************************************************************************

/**
 * @brief Chromatin hierarchical model. Model based on truncated Dirichlet
 *  process (corresponds to Generalized Dirichlet) as reported in:
 *
 *  Ishwaran, Hemant, and Mahmoud Zarepour. "Markov chain Monte Carlo in
 *      approximate Dirichlet and beta two-parameter process hierarchical
 *      models." Biometrika 87, no. 2 (2000): 371-390.
 *
 *  See also:
 *
 *  Paisley, John. "A Tutorial on the Dirichlet Process for Engineers
 *      Technical Report."
 */

#ifndef cmxModel_h
#define cmxModel_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxConfig.h"
#include "cmxGlue.h"
#include "cmxMoco.h"
#include "cmxThread.h"
#include "cmxTypes.h"

#include <vector>

//****************************************************************************
// cmxModel
//****************************************************************************

/**
 * Current implementation assumes model only runs on a single thread, if this
 * is not the case, then member methods with a thread identifier parameter
 * may have to be revisited.
 */

/**
 * Chromatin hierarchical model. Note, this template can be used for both
 * single and multiple population (i.e. mixture) models. For single
 * population, simply set the mixture components to a deterministic
 * cmxMoco type and they will avoid being sampled with negligible
 * runtime overhead.
 *
 * Template argument t_Glue must contain the following typedefs
 *  1a) Core simulation utilities
 *      - sim_t: Parent simulation type
 *      - sim_shared_t: Parent simulation-level shared data
 *      - model_t: This model type
 *      - model_shared_t: Model-level shared data
 *  1b) Controller simulation utilities
 *      - controller_t: Base controller type
 *      - model_batch_runner_t: Utility for batch updating models
 *  1c) Observer simulation utilities
 *      - obs_t: Base observer type
 *  2) Model components supporting the "Moco" interface:
 *      - Dp_stick_alpha_t: Dirichlet process stick-breaking alpha parameter;
 *          may be a fixed deterministic value or a prior such as Gamma or
 *          Uniform
 *      - Dp_stick_t: Dirichlet process stick-breaking component
 *      - Bnet_dag_kappa_t: Bayesian network DAG edge prior
 *      - Bnet_cpt_alpha_t: Bayesian network conditional probability table
 *          (CPT) prior
 *      - Bnet_infeas_lambda: Hyper-parameter for prior on infeasible samples
 *      - Bnet_t: Bayesian network tuple (DAG, CPT)
 *      - Clust_t: Cluster association indicator variables
 *      - Pseudopop_t: Pseudo-population
 *      - Contact_freqs_alpha_t: Alpha parameter for contact frequency matrix
 *          may be a fixed deterministic value or a prior such as Uniform
 *      - Contact_freqs_t: Linearized contact matrix, observed Hi-C, defines
 *          likelihood of contact frequency matrix given pseudo-population
 *
 * See model component declarations for further information.
 */
template <typename t_Glue>
class cmxModel {
    // Inject model components

    /**
     * Dirichlet process alpha parameter for stick-breaking process
     */
    CMX_MOCO_DECLARE(t_Glue, Dp_stick_alpha);

    /**
     * Stick-breaking component of Dirichlet process
     */
    CMX_MOCO_DECLARE(t_Glue, Dp_stick);

    /**
     * Hyper-parameter(s) for edge prior on Bayesian network structure
     */
    CMX_MOCO_DECLARE(t_Glue, Bnet_dag_kappa);

    /**
     * Hyper-parameter for prior on Bayesian CPTs
     */
    CMX_MOCO_DECLARE(t_Glue, Bnet_cpt_alpha);

    /**
     * Hyper-parameter(s) for prohibited (infeasible) samples
     */
    CMX_MOCO_DECLARE(t_Glue, Bnet_infeas_lambda);

    /**
     * Bayesian network probabilistic graphical model defined as a (DAG, CPT)
     *  tuple where DAG = directed acyclic graph, CPT = conditional
     *  probability table.
     */
    CMX_MOCO_DECLARE(t_Glue, Bnet);

    /**
     * Block-sampled cluster association categorical variables
     */
    CMX_MOCO_DECLARE(t_Glue, Clust);

    /**
     * Block-sampled chromatin single-cell samples
     */
    CMX_MOCO_DECLARE(t_Glue, Pseudopop);

    /**
     * Alpha parameter for prior on contact frequency matrix
     */
    CMX_MOCO_DECLARE(t_Glue, Contact_freqs_alpha);

    /**
     * Contact frequency matrix (e.g. Hi-C)
     */
    CMX_MOCO_DECLARE(t_Glue, Contact_freqs);

public:
    // Additional typedefs

    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Gibbs sampler argument is reference to parent simulation
     */
    typedef sim_t& Gibbs_args_t;

    /**
     * Model component base type
     */
    typedef cmxMoco<t_Glue> moco_t;

    // Constructors

    /**
     * Default constructor
     */
    cmxModel();

    /**
     * Copy constructor
     */
    cmxModel(const cmxModel&);

    /**
     * Assignment operator
     */
    cmxModel& operator=(const cmxModel&);

    /**
     * Initializes model and all model components from user configuration
     * @param config - user configuration options
     * @param sim - parent simulation
     */
    void init(cmxSpConstConfig_t config, sim_t& sim CMX_THREAD_ID_PARAM);

    /**
     * Initializes model components to random state, should be called after
     * init(); note: all components, not just 'random' components' can choose
     * to handle or ignore this call
     * @param config - user configuration options
     * @param sim - parent simulation
     */
    void randomize(cmxSpConstConfig_t config, sim_t& sim CMX_THREAD_ID_PARAM);

    // Gibbs interface

    /**
     * @return number of random variables that can be sampled
     */
    inline cmxUInt Gibbs_get_num_random() const {
        cmxAssert(!m_rv_components.empty());
        return CMX_TO_UINT(m_rv_components.size());
    }

    /**
     * Performs the Gibbs posterior update for random variable 'v'
     * @param v - identifier of random variable to update
     * @param sim - reference to parent simulation
     */
    inline void Gibbs_update(const cmxUInt v,
                             const Gibbs_args_t sim CMX_THREAD_ID_PARAM) {
        cmxAssertBounds(v, 0, CMX_TO_UINT(m_rv_components.size()));
        cmxAssert(m_rv_components[v]);
        m_rv_components[v]->Gibbs_posterior(*this, sim CMX_THREAD_ID_ARG);
    }

    // Accessors

    /**
     * @return [Const] set of all model components
     */
    inline std::vector<moco_t*>& get_moco_all() { return m_all_components; }
    inline const std::vector<moco_t*>& get_moco_all() const {
        return m_all_components;
    }

    /**
     * @return [Const] set of random model components
     */
    inline std::vector<moco_t*>& get_moco_rv() { return m_rv_components; }
    inline const std::vector<moco_t*>& get_moco_rv() const {
        return m_rv_components;
    }

private:
    // Data members

    /**
     * Master list of all components, both random and deterministic
     */
    std::vector<moco_t*> m_all_components;

    /**
     * List of random variable components (non-observed)
     */
    std::vector<moco_t*> m_rv_components;
};

// Include implementation as we are a template class
#include "cmxModel.inl"

#endif  // cmxModel_h
