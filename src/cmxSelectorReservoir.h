//****************************************************************************
// cmxSelectorReservoir.h
//****************************************************************************

/**
 * @brief - Generates selections according to a weighted reservoir sampler
 */

/**
 * Reference(s):
 *
 *  Efraimidis, Pavlos S., and Paul G. Spirakis. "Weighted random sampling
 *      with a reservoir." Information Processing Letters 97, no. 5 (2006):
 *      181-185.
 *
 * Wikipedia:
 *
 *  https://en.wikipedia.org/wiki/Reservoir_sampling#Algorithm_A-Res
 */

/**
 * @WARNING - THIS UTILITY WILL NOT WORK WITH 0-LENGTH VECTORS! THE CLIENT
 *  IS RESPONSIBLE FOR PASSING IN NON-EMPTY VECTORS. THERE ARE ASSERTS TO
 *  TO CATCH THIS EDGE CASE BUT ASSERTS ARE NOT ALWAYS ENABLED - SUCH AS
 *  FOR RELEASE BUILDS.
 */

#ifndef cmxSelectorReservoir_h
#define cmxSelectorReservoir_h

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxRand.h"
#include "cmxTypes.h"

#include <queue>

/**
 * Utility for generating a weighted reservoir selection. Assume an urn with N
 *  balls and K colors - each ball is one of the K colors. Algorithm gives a
 *  random selection of the colors *equivalent* to:
 *
 *      1) Randomly draw a ball from the urn and mark color of ball chosen
 *      2) Remove all balls with same color from urn
 *      3) Repeat steps 1-2 until target selection size reached (at most K)
 *
 *  Note, this algorithm does not actually perform steps 1-3, but instead
 *  generates a [0,1] uniform random number v_i for each color and
 *  then raises that number to 1/w_i where w_i is the weight of each color
 *  (e.g. the number of balls of that color). It then selects the max K values
 *  of v_i*(1/w_i) as the resulting selection of size K
 */
class cmxSelectorReservoir {
private:
    /**
     * Tuple associating weighted random key with element index
     */
    typedef struct {
        // Element key
        cmxReal m_key;
        // Element index
        cmxUInt m_index;
    } tuple_t;

    /**
     * Comparison operator for min-priority queue
     */
    typedef struct {
        inline bool operator()(const tuple_t a, const tuple_t b) {
            return a.m_key > b.m_key;
        }
    } compare_t;

    /**
     * Min priority queue for keeping track of k-largest elements
     */
    typedef std::priority_queue<tuple_t, std::vector<tuple_t>, compare_t>
        pqueue_t;

public:
    /**
     * Performs weighted selection of k elements
     * @param out - output index vector of size 'k'
     * @param k - size of selection, where 1 <= k <= w.n_elem
     * @param w - vector of non-negative weights
     * @param rng - handle to random number generator
     */
    static inline void rand_select_indices(cmxUIVecCol& out,
                                           const cmxUInt k,
                                           const cmxVecCol& w,
                                           cmxRand& rng) {
        cmxAssert(k > CMX_TO_UINT(0));
        cmxAssert(CMX_TO_MAT_SZ_T(k) <= w.n_elem);
        out.set_size(CMX_TO_MAT_SZ_T(k));

        // Priority queue containing top-k largest elements
        pqueue_t pq;

        // Initialize with first k elements
        tuple_t tup;
        cmxMatSz_t i = CMX_TO_MAT_SZ_T(0);
        for (; i < CMX_TO_MAT_SZ_T(k); ++i) {
            cmxAssert(w.at(i) >= CMX_TO_REAL(0.0));
            // Pad by CMX_REAL_EPS to avoid log(0) and divide-by-zero
            const cmxReal v =
                log(rng.unif_real() + CMX_REAL_EPS) *
                (CMX_TO_REAL(1.0) / std::min(w.at(i), CMX_REAL_EPS));
            // Update priority queue
            tup.m_key = v;
            tup.m_index = i;
            pq.push(tup);
        }

        // Iterate over remaining elements
        cmxAssert(i == k);
        for (; i < w.n_elem; ++i) {
            cmxAssert(w.at(i) >= CMX_TO_REAL(0.0));
            // Pad by CMX_REAL_EPS to avoid log(0) and divide-by-zero
            const cmxReal v =
                log(rng.unif_real() + CMX_REAL_EPS) *
                (CMX_TO_REAL(1.0) / std::min(w.at(i), CMX_REAL_EPS));
            if (pq.top().m_key < v) {
                // Update priority queue
                tup.m_key = v;
                tup.m_index = i;
                pq.pop();
                pq.push(tup);
            }
        }

        // Extract top-k elements
        cmxAssert(pq.size() == ((size_t)k));
        for (i = CMX_TO_MAT_SZ_T(0); i < CMX_TO_MAT_SZ_T(k); ++i) {
            cmxAssert(!pq.empty());
            out.at(i) = pq.top().m_index;
            cmxAssertBounds(
                CMX_TO_MAT_SZ_T(out.at(i)), CMX_TO_MAT_SZ_T(0), w.n_elem);
            pq.pop();
        }
        cmxAssert(i == k);
    }
};

#endif  // cmxSelectorReservoir_h
