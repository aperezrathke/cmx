//****************************************************************************
// cmxStats.cpp
//****************************************************************************

#include "cmxBuild.h"
#include "cmxStats.h"

#ifdef CMX_BUILD_ENABLE_STATS

#include "cmxAssert.h"
#include "cmxLogf.h"

#include <string>

namespace cmxStats {

#ifdef CMX_STATS_DECLARE_NAME
    #error CMX_STATS_DECLARE_NAME already defined!
#endif  // CMX_STATS_DECLARE_NAME

#define CMX_STATS_DECLARE_NAME(statId) #statId

/**
 * All timer stat instances
 */
TimerStat GTimerStats[cmxSTAT_TimerMAX];

/**
 * The name of each timer stat
 */
const char* GTimerStatsNames[cmxSTAT_TimerMAX] = {
    CMX_STATS_DECLARE_NAME(cmxSTAT_GlobalInitTime),
    CMX_STATS_DECLARE_NAME(cmxSTAT_BnetThinDagCycleCheckTime),
    CMX_STATS_DECLARE_NAME(cmxSTAT_BnetThinDagMoveFinderTime),
    CMX_STATS_DECLARE_NAME(cmxSTAT_BnetThinDagTopoSortTime),
    CMX_STATS_DECLARE_NAME(cmxSTAT_PseudpopRefreshFreqsTime)
};

/**
 * All counter stat instances
 */
CounterStat GCounterStats[cmxSTAT_CounterMAX];

/**
 * The name of each counter stat
 */
const char* GCounterStatsNames[cmxSTAT_CounterMAX] = {
    CMX_STATS_DECLARE_NAME(cmxSTAT_GlobalInitCount)
};

#undef CMX_STATS_DECLARE_NAME

/**
 * Report to standard out
 */
void report_stats() {
    // Header
    cmxLogf("STATS REPORT\n");
    cmxLogf("------------\n");

    // Timers
    cmxLogf("Timer stats:\n");
    unsigned int i = 0;
    for (i = 0; i < cmxSTAT_TimerMAX; ++i) {
        cmxLogf("\t%s: %f minutes\n",
                GTimerStatsNames[i],
                (((double)GTimerStats[i].get_total_time()) / 60.0));
    }

    // Counters
    cmxLogf("Counter stats:\n");
    for (i = 0; i < cmxSTAT_CounterMAX; ++i) {
        cmxLogf("\t%s: %u\n",
                GCounterStatsNames[i],
                GCounterStats[i].get_counter());
    }
}

}  // namespace cmxStats

#endif  // CMX_BUILD_ENABLE_STATS
