//****************************************************************************
// cmxBnetBoolMoco.h
//****************************************************************************

/**
 * @brief Model component for Bayesian network with only boolean nodes
 */

#ifndef cmxBnetBoolMoco_h
#define cmxBnetBoolMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetBool.h"
#include "cmxBnetBoolRandomizer.h"
#include "cmxBnetBoolTypes.h"
#include "cmxBnetMiscPrior.h"
#include "cmxBnetThinDagMoveFinderSerial.h"
#include "cmxConfig.h"
#include "cmxDirichletDistribution.h"
#include "cmxGlobals.h"
#include "cmxGlue.h"
#include "cmxMcmcMh4gMixin.h"
#include "cmxMoco.h"
#include "cmxModelTypes.h"
#include "cmxRand.h"
#include "cmxThread.h"
#include "cmxTypes.h"

//****************************************************************************
// cmxBnetBoolMoco
//****************************************************************************

/**
 * Template arguments:
 *  t_Glue - glue object as defined in cmxGlue
 *  t_MiscPrior - stateless policy for determining 'miscellaneous' prior of a
 *      boolean network, the term 'miscellaneous' refers to all priors *not*
 *      including the Dirichlet (Beta) on the conditional probability table
 *      which is explicitly handled, must provide interface:
 *          ::logscore(bnet, num_edges, model, sim)
 *          ::is_random enum which is true if any prior components are
 *              stochastic and therefore undergo Gibbs updating
 *  t_AlphaTrans - stateless policy for querying the alpha count used for
 *      Beta priors on each conditional probability table (CPT), must provide
 *      interface:
 *          ::trans(alpha, num_parent_states) - return transformed alpha count
 *              for CPT with parameter number of parent states (power of 2)
 */
template <typename t_Glue,
          typename t_MiscPrior = cmxBnetMiscPrior<t_Glue>,
          typename t_AlphaTrans = cmxBnetBoolAlphaTransIdent>
class cmxBnetBoolMoco
    : public cmxMoco<t_Glue>,
      public cmxMcmcMh4gMhastMixin<t_Glue, cmxBnetBoolMocoStateMh_t> {
private:
    /**
     * Randomizer type
     */
    typedef cmxBnetBoolRandomizer<cmxBnetBoolMoco> randomizer_t;

public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * MCMC mixin type
     */
    typedef cmxMcmcMh4gMhastMixin<t_Glue, cmxBnetBoolMocoStateMh_t>
        mcmc_mixin_t;

    /**
     * Metropolis[-Hastings] state type
     */
    typedef cmxBnetBoolMocoStateMh_t state_t;

    /**
     * Miscellaneous prior on Bayesian network, must *not* include Dirichlet
     * prior on CPT parameters as this is already explicitly handled by
     * model component
     */
    typedef t_MiscPrior misc_prior_t;

    /**
     * Alpha transform type
     */
    typedef t_AlphaTrans alpha_trans_t;

    /**
     * Metropolis argument
     */
    typedef struct {
        cmxUInt block_index;
        model_t* p_model;
        sim_t* p_sim;
    } Mh_args_t;

    /**
     * Component is random and requires sampling
     */
    enum { is_random = cmxTRUE };

    /**
     * Initialization from user configuration. Model component should seek to
     * initialize to single population, independent null model where possible.
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void init(cmxSpConstConfig_t config,
                      model_t& model,
                      sim_t& sim CMX_THREAD_ID_PARAM) {
        this->init_states(*this, config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Initialize to a random state, should be called after init()
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void randomize(cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {
        this->randomize_states(*this, config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Gibbs posterior update, should only be called if model component is
     * random! Generates a sample from the posterior distribution P(Xi | X-i)
     * where Xi is i-th random variable and X-i is set of of all random
     * variables except for Xi in the model.
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void Gibbs_posterior(model_t& model,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {
        // To satisfy detailed balance, perform single Metropolis step
        Mh_args_t mh_args = {CMX_TO_UINT(0), &model, &sim};
        mcmc_mixin_t::Mh_step(*this, mh_args CMX_THREAD_ID_ARG);
    }

    /**
     * Intended to assess Gibbs sampler convergence ideally showing that chain
     * is exploring high probability regions. This may be an expensive call to
     * make and should be used sparingly at regular intervals.
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    virtual cmxReal logscore(model_t& model, sim_t& sim) {
        const state_t& curr = this->get_curr();
        cmxReal score = CMX_TO_REAL(0.0);

        // Score CPT parameters
        const std::vector<cmxBnetBoolCt>& cpts = curr.bnet.get_cpts();
        const cmxUInt num_cpts = CMX_TO_UINT(cpts.size());
        cmxAssertPosEq(num_cpts, curr.bnet.get_dag().get_v_num());
        const cmxReal alpha = model.get_Bnet_cpt_alpha().get_scalar(sim);
        cmxReal alpha_counts[cmxBnetBoolCt::num_rows];
        for (cmxUInt itr_cpt = 0; itr_cpt < num_cpts; ++itr_cpt) {
            const cmxBnetBoolCt& cpt = cpts[itr_cpt];
            const cmxUInt num_cols = cpt.num_cols();
            cmxAssert(CMX_UINT_IS_POW_2(num_cols));
            const cmxReal alpha_count = alpha_trans_t::trans(alpha, num_cols);
            alpha_counts[cmxBnetBoolCt::row_false] =
                alpha_counts[cmxBnetBoolCt::row_true] = alpha_count;
            for (cmxUInt itr_col = 0; itr_col < num_cols; ++itr_col) {
                const cmxReal* const p_distrib = cpt.colptr(itr_col);
                cmxAssert(cmxBnetBool::check_is_pmf(
                    p_distrib[cmxBnetBoolCt::row_false],
                    p_distrib[cmxBnetBoolCt::row_true]));
                // Score according to Beta prior
                score += cmxDirichletDistribution::lnpdf(
                    cmxBnetBoolCt::num_rows, alpha_counts, p_distrib);
            }
        }

        // Score according to miscellaneous prior (e.g. edge counts,
        // infeasible set probability, etc.)
        if (misc_prior_t::is_random) {
            this->get_curr().logp_tar = misc_prior_t::logscore(
                curr.bnet, curr.dag_num_edges, model, sim);
        } else {
            // Miscellaneous prior is deterministic, verify value is cached
            cmxAssert(curr.logp_tar ==
                      misc_prior_t::logscore(
                          curr.bnet, curr.dag_num_edges, model, sim));
        }
        score += curr.logp_tar;

        return score;
    }

    /**
     * Metropolis-Hastings interface
     * Proposes new candidate state and updates target probabilities of
     * current and candidate states given the current state of parent model
     */
    void Mh_prime_cand(const Mh_args_t mh_args CMX_THREAD_ID_PARAM) {
        // Defer to common preamble
        Mh_preamble(*this, mh_args CMX_THREAD_ID_ARG);
    }

    /**
     * Callback for when current state is swapped by MCMC
     */
    inline void Mh_on_accept_cand(const Mh_args_t mh_args) {
        // Forward call to parent
        this->mcmc_mixin_t::Mh_on_accept_cand();
    }

    /**
     * Callback for when current state is retained by MCMC
     */
    inline void Mh_on_reject_cand(const Mh_args_t mh_args) {
        // Forward call to parent
        this->mcmc_mixin_t::Mh_on_reject_cand();
    }

    /**
     * @param block_index_unused - block index parameter to keep interface
     *  consistent with multi-population model (ignored)
     * @return handle to active dependency model
     */
    inline const cmxBnetBool& get_bnet(
        const cmxUInt block_index_unused = CMX_TO_UINT(0)) const {
        return this->get_curr().bnet;
    }

private:
    /**
     * Initialize state from user configuration, initializes to independent
     * (no edge) null model
     */
    void init_state(state_t& s,
                    cmxSpConstConfig_t config,
                    model_t& model,
                    sim_t& sim CMX_THREAD_ID_PARAM) {
        // Read-only edge data assumed stored in sim-level shared
        const cmxBnetThinEdgeStatesRo& dag_ro =
            sim.get_shared_ro().get_Bnet_dag().get_ero();
        const cmxUInt num_v = dag_ro.get_v_num();
        cmxAssert(num_v > CMX_TO_UINT(1));

        // Initialize DAG
        cmxUInt lo, hi;
        cmxBnetThinDag& dag = s.bnet.get_dag();
        dag.add_vertices(num_v, lo, hi, cmxTRUE /*clear*/);

        cmxAssert(CMX_TO_UINT(0) == lo);
        cmxAssert((num_v - 1) == hi);

        // Initialize mutable edge states
        s.dag_mu.init(dag_ro);

        // Initialize move set - all moves should be legal, but just to be extra
        // safe, lets double check using our move finder. If initialization is a
        // performance concern and we can be sure that initial DAG has no edges,
        // then simply set all moves to be legal.
        cmxBnetThinDagMoveFinderSerial::get_moves(
            dag,
            s.dag_mu,
            dag_ro,
            s.dag_moves,
            sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                .get_Bnet_dag()
                .get_in_deg_buf(),
            sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                .get_Bnet_dag()
                .get_stack_buf());

        // Set edge count
        s.dag_num_edges = CMX_TO_UINT(0);

        // Initialize topological sort
        topo_sort_state(s, sim CMX_THREAD_ID_ARG);

        // Initialize CPTs to observed contact frequencies
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();
        cmxAssertPosEq(CMX_TO_UINT(hic.n_elem), num_v);

        // Initialize CPTs
        std::vector<cmxBnetBoolCt>& cpts = s.bnet.get_cpts();
        cpts.resize(num_v);
        for (cmxUInt itr_cpt = 0; itr_cpt < num_v; ++itr_cpt) {
            // Empirical Hi-C frequency in [0,1]
            const cmxReal p_hic = hic.at(itr_cpt);
            cmxAssertBoundsInc(p_hic, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            // Set node frequencies to match empirical Hi-C
            cmxBnetBoolCt& cpt = cpts[itr_cpt];
            cpt.set_num_cols(CMX_TO_MAT_SZ_T(1));
            cmxReal* const p_col = cpt.colptr(0);
            p_col[cmxBnetBoolCt::row_true] = p_hic;
            p_col[cmxBnetBoolCt::row_false] = CMX_TO_REAL(1.0) - p_hic;
        }

        // Initialize proposal probability
        s.logp_pro = -log(CMX_TO_REAL(s.dag_moves.size()));

        // Initialize target probability
        s.logp_tar =
            misc_prior_t::logscore(s.bnet, s.dag_num_edges, model, sim);
    }

    /**
     * Randomize state - sample from prior
     * @param s - Metropolis-Hastings state to randomize from prior
     * @param config - user configuration
     * @param model - parent model
     * @param sim - outer simulation
     */
    void randomize_state(state_t& s,
                         cmxSpConstConfig_t config,
                         model_t& model,
                         sim_t& sim CMX_THREAD_ID_PARAM) {
        randomizer_t::randomize_moco_state(
            s, config, model, sim CMX_THREAD_ID_ARG);
    }

    /**
     * Initialize CPT count tables with alpha prior counts
     * @param cpt_counts - output buffer for storing tabular counts
     * @param s - Metropolis-Hastings state for which to update counts
     * @param model - parent model
     * @param sim - outer simulation
     */
    static void fill_cpt_alpha(std::vector<cmxBnetBoolCt>& cpt_counts,
                               const state_t& s,
                               const model_t& model,
                               const sim_t& sim) {
        // Mapping from destination (child) node to source (parent) node
        const cmxBnetDagAdjMap_t& dst2src = s.bnet.get_dag().get_dst2src_map();
        const cmxUInt num_v = CMX_TO_UINT(dst2src.size());

        // Resize set of count tables
        cpt_counts.resize(num_v);

        // Initialize each count table to proper size and with prior number of
        // 'alpha counts' according to alpha transform policy
        const cmxReal alpha = model.get_Bnet_cpt_alpha().get_scalar(sim);
        cmxAssert(alpha > CMX_TO_REAL(0.0));
        cmxAssertPosEq(dst2src.size(), cpt_counts.size());
        for (cmxUInt itr_v = 0; itr_v < num_v; ++itr_v) {
            cmxBnetBoolCt& ct = cpt_counts[itr_v];
            const cmxUInt num_parents = CMX_TO_UINT(dst2src[itr_v].size());
            cmxAssert(num_parents < num_v);
            ct.set_num_parents(num_parents);
            const cmxReal alpha_count =
                alpha_trans_t::trans(alpha, ct.num_cols());
            cmxAssert(alpha_count > CMX_TO_REAL(0.0));
            ct.fill(alpha_count);
        }
    }

    /**
     * Interface expected by Mh_preamble. Update pseudo-counts with any
     * changes in edge connectivity, pseudo-population, and/or alpha parameter
     * changes. By incorporating the pseudo-population counts, this allows
     * direct sampling from the CPT posteriors.
     * @param cpt_counts - output buffer for storing tabular counts
     * @param s - Metropolis-Hastings state for which to update counts
     * @param block_index - location identifier for this distribution
     * @param model - parent model
     * @param sim - outer simulation
     */
    static void tally_cpt_counts(std::vector<cmxBnetBoolCt>& cpt_counts,
                                 const state_t& s,
                                 const cmxUInt block_index,
                                 const model_t& model,
                                 const sim_t& sim) {
        // Initialize with alpha counts
        fill_cpt_alpha(cpt_counts, s, model, sim);

        // Mapping from destination (child) node to source (parent) node
        const cmxBnetDagAdjMap_t& dst2src = s.bnet.get_dag().get_dst2src_map();
        const cmxUInt num_v = CMX_TO_UINT(dst2src.size());

        // Get indices for associated pseudo-population samples
        const cmxPseudopopIndices_t& i_subpop =
            model.get_Clust().get_pseudopop_indices(block_index, sim);
        const cmxUInt num_subpop = CMX_TO_UINT(i_subpop.size());

        // Obtain handle to entire pseudo-population
        const cmxPseudopopFrame_t& pseudopop =
            model.get_Pseudopop().get_pseudopop_frame();
        cmxAssert(num_subpop <= pseudopop.n_cols);
        cmxAssert(pseudopop.n_cols > 0);
        cmxAssertPosEq(pseudopop.n_rows, num_v);

        // Buffer for storing parent states
        std::vector<cmxBool> parents;

        // Process associated pseudo-population counts
        for (cmxUInt itr_subp = 0; itr_subp < num_subpop; ++itr_subp) {
            const cmxUInt i_col = i_subpop[itr_subp];
            cmxAssertBounds(i_col, 0, pseudopop.n_cols);
            const cmxUInt* const bits = pseudopop.colptr(i_col);
            // Process each boolean node
            for (cmxUInt itr_v = 0; itr_v < num_v; ++itr_v) {
                const cmxUInt num_parents = CMX_TO_UINT(dst2src[itr_v].size());
                parents.resize(num_parents);
                // Determine 'observed' state of parents
                for (cmxUInt itr_parent = 0; itr_parent < num_parents;
                     ++itr_parent) {
                    const cmxUInt i_parent = dst2src[itr_v][itr_parent];
                    cmxAssertBounds(i_parent, 0, num_v);
                    cmxAssert((bits[i_parent] == cmxFALSE) ||
                              (bits[i_parent] == cmxTRUE));
                    parents[itr_parent] = bits[i_parent];
                }  // end iteration over parents
                // Update count
                cmxBnetBoolCt& ct = cpt_counts[itr_v];
                cmxBnetBoolCt::handle counts = ct(parents);
                cmxAssert((bits[itr_v] == cmxFALSE) ||
                          (bits[itr_v] == cmxTRUE));
                counts(static_cast<cmxBnetBoolCt::eRowIx>(bits[itr_v])) +=
                    CMX_TO_REAL(1.0);
            }  // end iteration over nodes
        }      // end iteration over associated pseudo-population
    }

    /**
     * Common Metropolis-Hastings preamble. The template argument
     * t_StateContainer refers to either a cmxBnetBoolMoco or a
     * cmxBnetBoolRandomizer type. The only difference is the call to
     * t_StateContainer::compute_cpt_counts(...). If t_StateContainer is of type
     * cmxBnetBoolRandomizer, then only the prior alpha counts are used, else if
     * t_StateContainer is cmxBnetBoolMoco, then the full posterior pseudo-
     * counts are used.
     * @param sc - [Metropolis-Hastings] state container, either
     *  cmxBnetBoolRandomizer or cmxBnetBoolMoco
     * @param mh_args - Metropolis-Hastings arguments
     */
    template <typename t_StateContainer>
    static void Mh_preamble(t_StateContainer& sc,
                            const Mh_args_t mh_args CMX_THREAD_ID_PARAM) {

        // Recover block index, parent model, and outer simulation handles
        const cmxUInt block_index = mh_args.block_index;
        model_t& model = *mh_args.p_model;
        sim_t& sim = *mh_args.p_sim;

        ////////////////////////////////////////////////////////////
        // Select an edge move

        const state_t& curr = sc.get_curr();
        cmxAssert(!curr.dag_moves.empty());
        cmxRand& rng = cmxRng;
        const cmxUInt ix_move = rng.unif_int(
            CMX_TO_UINT(0), CMX_TO_UINT(curr.dag_moves.size() - 1));
        cmxAssertBounds(ix_move, 0, curr.dag_moves.size());

        ////////////////////////////////////////////////////////////
        // Generate candidate DAG

        // Read-only edge data assumed stored in sim-level shared
        const cmxBnetThinEdgeStatesRo& dag_ro =
            sim.get_shared_ro().get_Bnet_dag().get_ero();
        cmxAssertBoundsInc(curr.dag_num_edges, 0, dag_ro.get_e_num());
        const cmxUInt key_move = curr.dag_moves[ix_move];
        cmxAssertBounds(key_move, 0, dag_ro.get_e_num());

        // Overwrite candidate with current state
        state_t& cand = sc.get_cand();
        cand = curr;

        // Determine edge move type
        cmxBnetThinEdgeRemovalInfo_t eri;
        if (cand.dag_mu.has_edge(key_move)) {
            // Edge exists, so propose removing it
            cmxAssert(cand.dag_num_edges > 0);
            cmxBnetThinEdgePostRemovalInfo_t epri;
            cand.dag_mu.get_eri(key_move, eri);
            cand.bnet.get_dag().remove_edge(
                dag_ro.get_v_src(key_move), eri, epri);
            cand.dag_mu.on_remove_edge(key_move, epri, dag_ro);
            --cand.dag_num_edges;
        } else {
            // Edge does not exist, so propose adding it
            cand.bnet.get_dag().add_edge(
                dag_ro.get_v_src(key_move), dag_ro.get_v_dst(key_move), eri);
            cand.dag_mu.on_add_edge(key_move, eri);
            ++cand.dag_num_edges;
            cmxAssert(cand.dag_num_edges <= dag_ro.get_e_num());
        }

        // Update topological sort. Note, ideally, we would only topologically
        // sort accepted candidates; however, our misc prior may need to
        // evaluate the probability of a negative corpus, in which case the
        // candidate network must be topologically sorted
        topo_sort_state(cand, sim CMX_THREAD_ID_ARG);

        ////////////////////////////////////////////////////////////
        // Generate candidate CPTs

        // Update pseudo-counts
        t_StateContainer::tally_cpt_counts(
            sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                .get_Bnet_cpt()
                .get_cpt_counts_buf(),
            cand,
            block_index,
            model,
            sim);
        const std::vector<cmxBnetBoolCt>& cpt_counts =
            sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                .get_Bnet_cpt()
                .get_cpt_counts_buf();

        // Sample CPT parameters from Beta posterior
        const cmxUInt num_cpt = CMX_TO_UINT(cpt_counts.size());
        std::vector<cmxBnetBoolCt>& cpts = cand.bnet.get_cpts();
        cmxAssertPosEq(num_cpt, cand.bnet.get_dag().get_v_num());
        cmxAssertPosEq(num_cpt, CMX_TO_UINT(cpts.size()));
        for (cmxUInt itr_cpt = 0; itr_cpt < num_cpt; ++itr_cpt) {
            const cmxBnetBoolCt& counts = cpt_counts[itr_cpt];
            cmxBnetBoolCt& cpt = cpts[itr_cpt];
            const cmxUInt num_cols = counts.num_cols();
            cpt.set_num_cols(num_cols);
            for (cmxUInt itr_col = 0; itr_col < num_cols; ++itr_col) {
                const cmxReal* const p_count = counts.colptr(itr_col);
                cmxReal* const p_distrib = cpt.colptr(itr_col);
                // Sample from Beta posterior
                cmxDirichletDistribution::sample(
                    rng, cmxBnetBoolCt::num_rows, p_count, p_distrib);
                cmxAssert(cmxBnetBool::check_is_pmf(
                    p_distrib[cmxBnetBoolCt::row_false],
                    p_distrib[cmxBnetBoolCt::row_true]));
            }
        }

        ////////////////////////////////////////////////////////////
        // Compute bias and target prob

        // Compute edge move bias
        cmxBnetThinDagMoveFinderSerial::get_moves(
            cand.bnet.get_dag(),
            cand.dag_mu,
            dag_ro,
            cand.dag_moves,
            sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                .get_Bnet_dag()
                .get_in_deg_buf(),
            sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                .get_Bnet_dag()
                .get_stack_buf());

        // Note: log(1/N) = log(1) - log(N) = 0 - log(N) = -log(N)
        cand.logp_pro = -log(CMX_TO_REAL(cand.dag_moves.size()));
        // Verify current state has properly cached its proposal bias
        cmxAssert(curr.logp_pro == -log(CMX_TO_REAL(curr.dag_moves.size())));

        // Note that we sampled directly from the CPTs posterior; therefore
        // the CPT Dirichlet terms and pseudo-population likelihood cancel
        // from both the proposal and target probabilities!

        // Compute miscellaneous prior score for candidate (e.g. edge prior,
        // prior on infeasible samples, etc.)
        cand.logp_tar =
            misc_prior_t::logscore(cand.bnet, cand.dag_num_edges, model, sim);
        // Conditionally update prior score for current state if miscellaneous
        // prior has random components which also undergo Gibbs updating
        if (misc_prior_t::is_random) {
            sc.get_curr().logp_tar = misc_prior_t::logscore(
                curr.bnet, curr.dag_num_edges, model, sim);
        } else {
            // Verify current state has properly cached its target score;
            // caching is only possible because of direct sampling from CPT
            // posterior and deterministic prior parameters
            cmxAssert(!misc_prior_t::is_random);
            cmxAssert(curr.logp_tar ==
                      misc_prior_t::logscore(
                          curr.bnet, curr.dag_num_edges, model, sim));
        }
    }

    /**
     * Performs topological sort of state's Bayesian network nodes according
     * to DAG edge state
     * @param s - MCMC state's Bayesian network will be sorted
     * @param sim - Owning simulation
     */
    static void topo_sort_state(state_t& s, sim_t& sim CMX_THREAD_ID_PARAM) {
        s.bnet.get_dag().topo_sort(s.bnet.get_order(),
                                   sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                                       .get_Bnet_dag()
                                       .get_in_deg_buf(),
                                   sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                                       .get_Bnet_dag()
                                       .get_stack_buf());
    }

    /**
     * Allow mixin access to private init_state(...) and randomize_state(..)
     * members
     */
    friend cmxMcmcMh4gMetroMixin<glue_t, state_t>;
    friend cmxMcmcMh4gMhastMixin<glue_t, state_t>;

    /**
     * Randomizer type also needs privileged access
     */
    friend randomizer_t;
};

#endif  // cmxBnetBoolMoco_h
