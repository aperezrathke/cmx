//****************************************************************************
// cmxAppMpopCanon.h
//****************************************************************************

/**
 * Canonical multi-population simulation
 *
 * Usage:
 * -app_mpop_canon
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxAutoTimer.h"
#include "cmxCmdOptsMap.h"
#include "cmxConfig.h"
#include "cmxExitCodes.h"
#include "cmxGlobals.h"
#include "cmxLogf.h"
#include "cmxThread.h"
#include "cmxTypes.h"

// Glue: Core
#include "cmxModel.h"
#include "cmxSharedMu.h"
#include "cmxSharedRo.h"
#include "cmxSim.h"

// Glue: Controller
#include "cmxController.h"
#include "cmxMbrParallel.h"

// Glue: Observer
#include "cmxObs.h"

// Glue: Model components
#include "cmxBnetBoolMoco.h"
#include "cmxBnetBoolMpopMoco.h"
#include "cmxBnetCptAlphaDetrMoco.h"
#include "cmxBnetDagKappaDetrMoco.h"
#include "cmxBnetInfeasLambdaDetrMoco.h"
#include "cmxClustMpopMoco.h"
#include "cmxContactFreqsAlphaDetrMoco.h"
#include "cmxContactFreqsMoco.h"
#include "cmxDpStickAlphaGammaMoco.h"
#include "cmxDpStickMpopMoco.h"
#include "cmxPseudopopMoco.h"

// Glue: Read-only shared components
#include "cmxBnetInfeasSharcoRo.h"
#include "cmxBnetMpopSharcoRo.h"
#include "cmxBnetThinDagSharcoRo.h"
#include "cmxClustMpopSharcoRo.h"
#include "cmxContactFreqsSharcoRo.h"
#include "cmxPseudopopSharcoRo.h"

// Glue: Mutable shared components
#include "cmxBnetBoolCptSharcoMu.h"
#include "cmxBnetMpopSharcoMu.h"
#include "cmxBnetThinDagSharcoMu.h"
#include "cmxClustMpopSharcoMu.h"
#include "cmxPseudopopSharcoMu.h"

// Primer
#include "cmxPrimerCorrHic.h"

namespace cmxAppMpopCanon {

/**
 * Read-only shared
 */
class GlueSharedRo {
public:
    typedef cmxDpStickAlphaGammaSharcoRo Dp_stick_alpha_t;
    typedef cmxBnetDagKappaDetrSharcoRo Bnet_dag_kappa_t;
    typedef cmxBnetCptAlphaDetrSharcoRo Bnet_cpt_alpha_t;
    typedef cmxBnetInfeasLambdaDetrSharcoRo Bnet_infeas_lambda_t;
    typedef cmxBnetInfeasSharcoRo Bnet_infeas_t;
    typedef cmxBnetThinDagSharcoRo Bnet_dag_t;
    typedef cmxBnetMpopSharcoRo Bnet_mpop_t;
    typedef cmxClustMpopSharcoRo Clust_t;
    typedef cmxPseudopopSharcoRo Pseudopop_t;
    typedef cmxContactFreqsAlphaDetrSharcoRo Contact_freqs_alpha_t;
    typedef cmxContactFreqsSharcoRo Contact_freqs_t;
};

/**
 * Mutable shared
 */
class GlueSharedMu {
public:
    typedef cmxBnetThinDagSharcoMu Bnet_dag_t;
    typedef cmxBnetBoolCptSharcoMu Bnet_cpt_t;
    typedef cmxBnetMpopSharcoMu Bnet_mpop_t;
    typedef cmxClustMpopSharcoMu Clust_t;
    typedef cmxPseudopopSharcoMu Pseudopop_t;
};

/**
 * Master
 */
class GlueMaster {
public:
    // Core simulation utilities
    typedef cmxSim<GlueMaster> sim_t;
    typedef cmxSharedRo<GlueSharedRo> shared_ro_t;
    typedef cmxSharedMu<GlueSharedMu> shared_mu_t;
    typedef cmxModel<GlueMaster> model_t;
    // Controller simulation utilities
    typedef cmxController<GlueMaster> controller_t;
    typedef cmxMbrParallel<GlueMaster> model_batch_runner_t;
    // Observer simulation utilities
    typedef cmxObs<GlueMaster> obs_t;
    // Model components
    typedef cmxDpStickAlphaGammaMoco<GlueMaster> Dp_stick_alpha_t;
    typedef cmxDpStickMpopMoco<GlueMaster> Dp_stick_t;
    typedef cmxBnetDagKappaDetrMoco<GlueMaster> Bnet_dag_kappa_t;
    typedef cmxBnetCptAlphaDetrMoco<GlueMaster> Bnet_cpt_alpha_t;
    typedef cmxBnetInfeasLambdaDetrMoco<GlueMaster> Bnet_infeas_lambda_t;
    typedef cmxBnetBoolMpopMoco<GlueMaster> Bnet_t;
    typedef cmxClustMpopMoco<GlueMaster> Clust_t;
    typedef cmxPseudopopMoco<GlueMaster> Pseudopop_t;
    typedef cmxContactFreqsAlphaDetrMoco<GlueMaster> Contact_freqs_alpha_t;
    typedef cmxContactFreqsMoco<GlueMaster> Contact_freqs_t;
};

}  // namespace cmxAppMpopCanon

//****************************************************************************
// Main
//****************************************************************************

/**
 * Entry point for app
 */
int cmxAppMpopCanonMain(const cmxCmdOptsMap& cmd_opts) {
    // Auto-timer for capturing total-exec time
    cmxAutoTimer profiler;
    cmxLogf("Running multi-population simulation.\n");
    // Simulation type
    typedef cmxAppMpopCanon::GlueMaster::sim_t sim_t;
    // Primer type
    typedef cmxPrimerCorrHic<cmxAppMpopCanon::GlueMaster> primer_t;
    // Create configuration
    cmxSpConfig_t config = cmxSmartPtr::make_shared<cmxConfig>();
    // Parse command line and any INI configuration
    config->init(cmd_opts);
    // Output configuration
    config->print();
    // Initialize globals
    cmxG::init(config);
    // Report thread count
    CMX_LOG_NUM_WORKER_THREADS;
    // Initialize simulation
    sim_t sim;
    sim.init(config);
    cmxLogf("Finished initializing %d models.\n", (int)sim.get_num_models());
    sim.observers_force_update(CMX_TO_UINT(0));
    // Prime simulation
    primer_t::prime(sim, config);
    // Run simulation
    const cmxBool result = sim.run();
    cmxLogf("Simulation completed with status: %d\n", (int)result);
    // Report stats
    CMX_STATS_REPORT;
    // Destroy globals
    cmxG::teardown();
    // Return exit status
    return cmxExitCode_normal;
}
