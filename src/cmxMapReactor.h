//****************************************************************************
// cmxMapReactor.h
//****************************************************************************

/**
 * @brief MAP reactors respond to whenever a new maximum a posteriori score
 *  is encountered
 */

#ifndef cmxMapReactor_h
#define cmxMapReactor_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxGlue.h"
#include "cmxObs.h"

//****************************************************************************
// cmxMapReactor
//****************************************************************************

/**
 * Base class for 'MAP Reactors' - a special class of observers which are only
 * updated when a new MAP estimate is reached. Child classes must overload
 * the new reactor update_react(...) method and initialize themselves by
 * overloading init_react(...) method.
 */
template <typename t_Glue,
          typename t_ObsLogscoreMap /*= cmxObsLogscoreMapg<t_Glue>*/>
class cmxMapReactor : public cmxObs<t_Glue> {
public:
    CMX_GLUE_INJECT_SIM_TYPEDEFS(t_Glue);

    /**
     * Log-score MAP observer type
     */
    typedef t_ObsLogscoreMap obs_logscore_map_t;

    /**
     * Default constructor
     */
    cmxMapReactor() : mp_logscore_map(NULL) {}

    /**
     * Reactor update managed by logscore MAP observer, when a new MAP
     * estimate is encountered, this method will be called
     * @param iter - Typically post burn-in iteration number (0-based)
     * @param sim - Outer simulation
     * @param i_model - Index of MAP model
     * @param logscore - new MAP logscore associated to MAP model
     */
    virtual void update_react(const cmxUInt iter,
                              sim_t& sim,
                              const size_t i_model,
                              const cmxReal logscore) = 0;

    /**
     * Initialize from user configuration, perform initialization which may be
     * dependent on other observers. Dependencies are assumed to be acyclic
     * with parent observers occurring earlier in parameter observers array
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within reactors array
     */
    virtual void init(cmxSpConstConfig_t config,
                      sim_t& sim,
                      const size_t index) {
        // Register reactor
        mp_logscore_map = NULL;
        if (!obs_t::find_raw(&mp_logscore_map, sim)) {
            cmxLogf(
                "Error: unable to register MAP reactor with MAP observer. "
                "Exiting.\n");
            exit(cmxExitCode_error);
        }
        cmxAssert(mp_logscore_map);
        mp_logscore_map->add_reactor(*this);
        this->init_react(config, sim, index);
    }
    /**
     * Provide default (non-functional) base observer update
     */
    virtual void update(const cmxUInt iter, sim_t& sim) {}

protected:
    /**
     * Hook for initializing a MAP reactor after registration with log-score
     * MAP observer
     * @param config - user configuration
     * @param sim - parent simulation
     * @param index - index of this observer within reactors array
     */
    virtual void init_react(cmxSpConstConfig_t config,
                            sim_t& sim,
                            const size_t index) {}

    /**
     * @return Const handle to owning log-score MAP observer
     */
    inline const obs_logscore_map_t& get_logscore_map() const {
        cmxAssert(mp_logscore_map);
        return *mp_logscore_map;
    }

private:
    /**
     * Handle to owning log-score MAP observer
     */
    obs_logscore_map_t* mp_logscore_map;
};

#endif  // cmxMapReactor_h
