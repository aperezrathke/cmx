//****************************************************************************
// cmxBnetBoolMpopMoco.h
//****************************************************************************

/**
 * @brief Model component for mixture distribution of Bayesian networks.
 */

#ifndef cmxBnetBoolMpopMoco_h
#define cmxBnetBoolMpopMoco_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetBool.h"
#include "cmxBnetBoolMoco.h"
#include "cmxConfigUtils.h"
#include "cmxGlue.h"
#include "cmxMcmcMh4g.h"
#include "cmxMoco.h"
#include "cmxSelectorReservoir.h"
#include "cmxThread.h"

#include <vector>

//****************************************************************************
// cmxBnetBoolMpopMoco
//****************************************************************************

/**
 * Mixture of boolean Bayesian networks. Mpop == Multiple population.
 */
template <typename t_Glue, typename t_BnetSpopMoco = cmxBnetBoolMoco<t_Glue> >
class cmxBnetBoolMpopMoco : public cmxMoco<t_Glue> {
public:
    // Typedefs
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Metropolis argument
     */
    typedef struct {
        model_t* p_model;
        sim_t* p_sim;
        cmxUIVecCol* p_block_indices;
    } Mh_args_t;

    /**
     * Underlying Bayesian network model component for individual
     *  distributions defining the multi-population mixture model
     */
    typedef t_BnetSpopMoco bnet_spop_moco_t;

    /**
     * Single-population Metropolis argument
     */
    typedef typename bnet_spop_moco_t::Mh_args_t Mh_args_spop_t;

    // Enums

    /**
     * Determine if this component is random
     */
    enum { is_random = bnet_spop_moco_t::is_random };

    // Model component interface

    /**
     * Initialization from user configuration. Model component should seek to
     * initialize to single population, independent null model where possible.
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void init(cmxSpConstConfig_t config,
                      model_t& model,
                      sim_t& sim CMX_THREAD_ID_PARAM) {
        // Determine number of mixture components
        const cmxUInt num_bnets = cmxConfigUtils::get_mix_size(config);
        cmxAssert(num_bnets > 0);
        // Allocate mixture components
        m_bnets.clear();
        m_bnets.resize(num_bnets);
        cmxAssert(this->get_mix_size() == num_bnets);
        // Initialize mixture components
        this->mcmc_reset();
        for (cmxUInt i = 0; i < num_bnets; ++i) {
            bnet_spop_moco_t& bnet = m_bnets[i];
            bnet.init(config, model, sim CMX_THREAD_ID_ARG);
            this->mcmc_update(bnet);
        }
    }

    /**
     * Initialize to a random state, should be called after init()
     * @param config - user options
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void randomize(cmxSpConstConfig_t config,
                           model_t& model,
                           sim_t& sim CMX_THREAD_ID_PARAM) {
        const cmxUInt num_bnets = this->get_mix_size();
        cmxAssert(this->get_mix_size() == cmxConfigUtils::get_mix_size(config));
        // Randomize each mixture component
        this->mcmc_reset();
        for (cmxUInt i = 0; i < num_bnets; ++i) {
            bnet_spop_moco_t& bnet = m_bnets[i];
            bnet.randomize(config, model, sim CMX_THREAD_ID_ARG);
            this->mcmc_update(bnet);
        }
    }

    /**
     * Gibbs posterior update, should only be called if model component is
     * random! Generates a sample from the posterior distribution P(Xi | X-i)
     * where Xi is i-th random variable and X-i is set of of all random
     * variables except for Xi in the model.
     * @param model - owning model
     * @param sim - outer simulation
     */
    virtual void Gibbs_posterior(model_t& model,
                                 sim_t& sim CMX_THREAD_ID_PARAM) {
        cmxAssert(cmxBnetBoolMpopMoco::is_random);
        cmxAssert(this->check_mix_size(sim));
        // To satisfy detailed balance, perform single Metropolis step
        cmxUIVecCol& block_indices_buf = sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                                             .get_Bnet_mpop()
                                             .get_mpop_block_indices_buf();
        Mh_args_t mh_args = {&model, &sim, &block_indices_buf};
        cmxMcmcMh4g::Mh_step(*this, mh_args CMX_THREAD_ID_ARG);
    }

    /**
     * Intended to assess Gibbs sampler convergence ideally showing that chain
     * is exploring high probability regions. This may be an expensive call to
     * make and should be used sparingly at regular intervals.
     * @param model - owning model
     * @param sim - outer simulation
     * @return a log score that is proportional to the Bayesian posterior.
     */
    virtual cmxReal logscore(model_t& model, sim_t& sim) {
        cmxAssert(this->check_mix_size(sim));
        cmxReal score = CMX_TO_REAL(0.0);
        const cmxUInt num_bnets = this->get_mix_size();
        for (cmxUInt i = 0; i < num_bnets; ++i) {
            bnet_spop_moco_t& bnet = m_bnets[i];
            score += bnet.logscore(model, sim);
        }
        return score;
    }

    // Metropolis-Hastings interface

    /**
     * Proposes new candidate state and updates target probabilities of
     * current and candidate states given the current state of parent model
     */
    void Mh_prime_cand(const Mh_args_t mh_args CMX_THREAD_ID_PARAM) {
        // Recover handles to model and simulation
        model_t& model = *mh_args.p_model;
        sim_t& sim = *mh_args.p_sim;

        // Handle to thread-specific block indices buffer
        cmxUIVecCol& block_indices_buf = *mh_args.p_block_indices;

        // Handle to thread-specific block weights buffer
        cmxVecCol& block_weights_buf = sim.get_shared_mu(CMX_THREAD_ID_0_ARG)
                                           .get_Bnet_mpop()
                                           .get_mpop_block_weights_buf();

        // Block size
        const cmxUInt block_size =
            sim.get_shared_ro().get_Bnet_mpop().get_bnet_mpop_block_size();
        cmxAssertBoundsInc(block_size, CMX_TO_UINT(1), this->get_mix_size());

        // Block blend
        const cmxReal block_weighted_blend =
            sim.get_shared_ro()
                .get_Bnet_mpop()
                .get_bnet_mpop_block_weighted_blend();
        cmxAssertRealBoundsInc(
            block_weighted_blend, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));

        // Raw mixture weights
        const cmxVecCol& mix_pmf = model.get_Dp_stick().get_mix_pmf();
        cmxAssertPosEq(CMX_TO_UINT(mix_pmf.n_elem), this->get_mix_size());

        // Determine selection weights - blend between weighted and uniform
        block_weights_buf = mix_pmf;
        block_weights_buf *= block_weighted_blend;
        const cmxReal w_unif = (CMX_TO_REAL(1.0) - block_weighted_blend) /
                               CMX_TO_REAL(mix_pmf.n_elem);
        block_weights_buf += w_unif;

        // Select random block
        cmxSelectorReservoir::rand_select_indices(
            block_indices_buf, block_size, block_weights_buf, cmxRng);
        cmxAssertPosEq(block_indices_buf.n_elem, block_size);

        // Generate proposal among selected block
        mcmc_reset();
        Mh_args_spop_t mh_args_spop;
        mh_args_spop.p_model = mh_args.p_model;
        mh_args_spop.p_sim = mh_args.p_sim;
        for (cmxMatSz_t i = CMX_TO_MAT_SZ_T(0); i < CMX_TO_MAT_SZ_T(block_size);
             ++i) {
            mh_args_spop.block_index = block_indices_buf.at(i);
            cmxAssertBounds(
                mh_args_spop.block_index, CMX_TO_UINT(0), this->get_mix_size());
            bnet_spop_moco_t& bnet = m_bnets[mh_args_spop.block_index];
            bnet.Mh_prime_cand(mh_args_spop CMX_THREAD_ID_ARG);
            mcmc_update(bnet);
        }
    }

    /**
     * @return log probability of candidate under target distribution
     */
    inline cmxReal Mh_get_logp_tar_cand() const { return m_logp_tar_cand; }

    /**
     * @return log probability of incumbent (i.e. current) under target
     *  distribution
     */
    inline cmxReal Mh_get_logp_tar_curr() const { return m_logp_tar_curr; }

    /**
     * @return log probability of proposing incumbent (current) sample given
     *  the chain is at candidate sample
     */
    inline cmxReal Mh_get_logp_pro_curr_giv_cand() const {
        return m_logp_pro_curr_giv_cand;
    }

    /**
     * @return log probability of proposing candidate sample given the chain
     *  is at incumbent (current) sample
     */
    inline cmxReal Mh_get_logp_pro_cand_giv_curr() const {
        return m_logp_pro_cand_giv_curr;
    }

    /**
     * Callback for when current state is swapped by MCMC
     */
    inline void Mh_on_accept_cand(const Mh_args_t mh_args) {
        // Inform block
        const cmxUIVecCol& block_indices_buf = *mh_args.p_block_indices;
        Mh_args_spop_t mh_args_spop;
        mh_args_spop.p_model = mh_args.p_model;
        mh_args_spop.p_sim = mh_args.p_sim;
        for (cmxMatSz_t i = CMX_TO_MAT_SZ_T(0); i < block_indices_buf.n_elem;
             ++i) {
            mh_args_spop.block_index = block_indices_buf.at(i);
            cmxAssertBounds(
                mh_args_spop.block_index, CMX_TO_UINT(0), this->get_mix_size());
            bnet_spop_moco_t& bnet = m_bnets[mh_args_spop.block_index];
            bnet.Mh_on_accept_cand(mh_args_spop);
        }

#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
        // Update running acceptance probability tracker
        m_stats(CMX_TO_REAL(1.0));
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    }

    /**
     * Callback for when current state is retained by MCMC
     */
    inline void Mh_on_reject_cand(const Mh_args_t mh_args) {
        // Inform block
        const cmxUIVecCol& block_indices_buf = *mh_args.p_block_indices;
        Mh_args_spop_t mh_args_spop;
        mh_args_spop.p_model = mh_args.p_model;
        mh_args_spop.p_sim = mh_args.p_sim;
        for (cmxMatSz_t i = CMX_TO_MAT_SZ_T(0); i < block_indices_buf.n_elem;
             ++i) {
            mh_args_spop.block_index = block_indices_buf.at(i);
            cmxAssertBounds(
                mh_args_spop.block_index, CMX_TO_UINT(0), this->get_mix_size());
            bnet_spop_moco_t& bnet = m_bnets[mh_args_spop.block_index];
            bnet.Mh_on_reject_cand(mh_args_spop);
        }

#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
        // Update running acceptance probability tracker
        m_stats(CMX_TO_REAL(0.0));
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    }

    // Accessor(s)

    /**
     * @param block_index - block index parameter
     * @return Handle to underlying single-population dependency model
     */
    inline const cmxBnetBool& get_bnet(const cmxUInt block_index) const {
        cmxAssertBounds(block_index, CMX_TO_UINT(0), this->get_mix_size());
        return m_bnets[block_index].get_bnet();
    }

    /**
     * @return const handle to tabulated stats
     */
#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    inline const cmxRunStat_t& get_stats() const { return m_stats; }
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

private:
    /**
     * NOTE: THIS IS PRIVATE ACCESSOR. IN GENERAL, CLIENT CODE SHOULD DEFER TO
     * SIMULATION-LEVEL Clust SHARED OR ConfigUtils FOR DETERMINING MIX SIZE
     * @return maximum number of mixture components
     */
    inline cmxUInt get_mix_size() const { return CMX_TO_UINT(m_bnets.size()); }

#ifdef CMX_BUILD_ENABLE_ASSERT
    /**
     * @return cmxTRUE if view of mixture size is consistent with rest of
     *  model, cmxFALSE o/w
     */
    inline cmxBool check_mix_size(const sim_t& sim) const {
        return sim.get_shared_ro().get_Clust().get_mix_size() ==
               this->get_mix_size();
    }
#endif  // CMX_BUILD_ENABLE_ASSERT

    /**
     * Reset MCMC probabilities
     */
    inline void mcmc_reset() {
        m_logp_tar_cand = m_logp_pro_curr_giv_cand = m_logp_tar_curr =
            m_logp_pro_cand_giv_curr = CMX_TO_REAL(0.0);
    }

    /**
     * Update MCMC probabilities according to parameter Bayesian network
     */
    inline void mcmc_update(const bnet_spop_moco_t& bnet) {
        m_logp_tar_cand += bnet.get_cand().logp_tar;
        m_logp_pro_curr_giv_cand += bnet.get_cand().logp_pro;
        m_logp_tar_curr += bnet.get_curr().logp_tar;
        m_logp_pro_cand_giv_curr += bnet.get_curr().logp_pro;
    }

    /**
     * MCMC - log probability of candidate under target distribution
     */
    cmxReal m_logp_tar_cand;

    /**
     * MCMC - log probability of proposing incumbent (current) sample given
     *  the chain is at candidate sample
     */
    cmxReal m_logp_pro_curr_giv_cand;

    /**
     * MCMC - log probability of incumbent (i.e current) under target
     *  distribution
     */
    cmxReal m_logp_tar_curr;

    /**
     * MCMC - log probability of proposing candidate sample given the chain
     *  is at incumbent (current) sample
     */
    cmxReal m_logp_pro_cand_giv_curr;

    /**
     * MCMC - Running stats tracker
     */
#ifdef CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK
    cmxRunStat_t m_stats;
#endif  // CMX_BUILD_ENABLE_MCMC_MHAST_ACCEPT_TRACK

    /**
     * Underlying mixture components
     */
    std::vector<bnet_spop_moco_t> m_bnets;
};

#endif  // cmxBnetBoolMpopMoco_h
