//****************************************************************************
// cmxHicScorer.h
//****************************************************************************

/**
 * @brief Utility class(es) for scoring empirical Hi-C data. Hi-C scorer must
 *  provide stateless interface (used by contact frequencies model component):
 *      ::logscore_using(frame, freqs, model, sim)
 *      ::logscore_using(frame, freqs, corr, model, sim)
 */

/**
 * Some possible scorers include:
 *  cmxHicScorerBeta => log-likelihood is sum of independent Betas for each
 *      observed contact frequency, where each Beta is centered at Pseudo-
 *      population frequency for that contact
 *  cmxHicScorerCorr => log-likelihood is proportional to a correlative
 *      similarity score. E.g.:
 *          loglike = alpha * Corr(Pseudopop, Hi-C) where Corr(...) can be
 *          any sort of similarity function such as:
 *          - negative [squared] Euclidean distance
 *          - RBF kernel
 *              https://en.wikipedia.org/wiki/Radial_basis_function_kernel
 *          - Pearson or Spearman correlation
 *          - Any SVM kernel:
 *              http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/
 */

#ifndef cmxHicScorer_h
#define cmxHicScorer_h

//****************************************************************************
// Includes
//****************************************************************************

#include "cmxBuild.h"
#include "cmxAssert.h"
#include "cmxBnetBoolCt.h"
#include "cmxDirichletDistribution.h"
#include "cmxGlue.h"
#include "cmxModelTypes.h"
#include "cmxPearsonCorr.h"
#include "cmxTypes.h"

#include <algorithm>

//****************************************************************************
// cmxHicScorerBeta
//****************************************************************************

/**
 * Log-likelihood assumes each empirical frequency is from homogeneous Beta
 * distribution centered at corresponding pseudo-population frequency. Hence,
 *  log-likelihood = sum{log[Beta(HiC_i | Pseudopop_i, alpha)]} where 'i'
 *  indexes the model contact pairs.
 */
template <typename t_Glue>
class cmxHicScorerBeta {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Computes log-likelihood of empirical Hi-C frequencies assuming Beta
     * distributions centered at corresponding pseudo-population frequencies
     * @param pseudo_frame - [ignored] Pseudo-population samples
     * @param pseudo_freqs - Pseudo-population aggregated contact frequencies
     * @param model - owning model
     * @param sim - outer simulation
     * @return log-likelihood of empirical Hi-C given latent pseudo-population
     */
    static cmxReal logscore_using(const cmxPseudopopFrame_t& pseudo_frame,
                                  const cmxContactFreqs_t& pseudo_freqs,
                                  const model_t& model,
                                  const sim_t& sim) {
        // Handle to empirical Hi-C
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();
        cmxAssertPosEq(hic.n_elem, pseudo_freqs.n_elem);
        // Dirichlet pseudo-count hyper-parameter
        const cmxReal ALPHA = model.get_Contact_freqs_alpha().get_scalar(sim);
        cmxAssert(ALPHA > CMX_TO_REAL(0.0));
        // Buffers
        cmxReal alphas[cmxBnetBoolCt::num_rows];
        cmxReal thetas[cmxBnetBoolCt::num_rows];
        cmxAssert(cmxBnetBoolCt::num_rows == 2);
        // Compute log-likelihood
        cmxReal score = CMX_TO_REAL(0.0);
        for (cmxMatSz_t i = 0; i < hic.n_elem; ++i) {
            // Fill empirical frequencies buffer
            const cmxReal p_hic = hic.at(i);
            cmxAssertBoundsInc(p_hic, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            thetas[cmxBnetBoolCt::row_true] = p_hic;
            thetas[cmxBnetBoolCt::row_false] = CMX_TO_REAL(1.0) - p_hic;
            // Fill pseudo-counts buffer
            const cmxReal p_pseudo = pseudo_freqs.at(i);
            cmxAssertBoundsInc(p_pseudo, CMX_TO_REAL(0.0), CMX_TO_REAL(1.0));
            alphas[cmxBnetBoolCt::row_true] = ALPHA * p_pseudo;
            alphas[cmxBnetBoolCt::row_false] =
                ALPHA - alphas[cmxBnetBoolCt::row_true];
            cmxAssertRealEq(ALPHA,
                            alphas[cmxBnetBoolCt::row_true] +
                                alphas[cmxBnetBoolCt::row_false]);
            // Make sure alpha counts are positive (required range for
            // Dirichlet distribution)
            alphas[cmxBnetBoolCt::row_true] = std::max(
                alphas[cmxBnetBoolCt::row_true], CMX_DIRICHLET_MIN_ALPHA);
            alphas[cmxBnetBoolCt::row_false] = std::max(
                alphas[cmxBnetBoolCt::row_false], CMX_DIRICHLET_MIN_ALPHA);
            cmxAssert(alphas[cmxBnetBoolCt::row_false] > CMX_TO_REAL(0.0));
            cmxAssert(alphas[cmxBnetBoolCt::row_true] > CMX_TO_REAL(0.0));
            // Update score
            score += cmxDirichletDistribution::lnpdf(
                cmxBnetBoolCt::num_rows, alphas, thetas);
        }
        // Emit final log-likelihood
        cmxAssert(score <= CMX_TO_REAL(0.0));
        return score;
    }

    /**
     * Overload with correlation parameter (not used)
     * @return log-likelihood of empirical Hi-C given latent pseudo-population
     */
    static cmxReal logscore_using(const cmxPseudopopFrame_t& pseudo_frame,
                                  const cmxContactFreqs_t& pseudo_freqs,
                                  const cmxReal corr,
                                  const model_t& model,
                                  const sim_t& sim) {
        // Correlation not used
        return logscore_using(pseudo_frame, pseudo_freqs, model, sim);
    }
};

//****************************************************************************
// cmxHicScorerPearson
//****************************************************************************

/**
 * Log-likelihood is assumed to be proportional to Pearson correlation
 */
template <typename t_Glue>
class cmxHicScorerPearson {
public:
    CMX_GLUE_INJECT_CORE_TYPEDEFS(t_Glue);

    /**
     * Computes log-likelihood of empirical Hi-C frequencies assuming
     * score is proportional to Pearson correlation
     * @param pseudo_frame - [ignored] Pseudo-population samples
     * @param pseudo_freqs - Pseudo-population aggregated contact frequencies
     * @param model - owning model
     * @param sim - outer simulation
     * @return log-likelihood of empirical Hi-C given latent pseudo-population
     */
    static cmxReal logscore_using(const cmxPseudopopFrame_t& pseudo_frame,
                                  const cmxContactFreqs_t& pseudo_freqs,
                                  const model_t& model,
                                  const sim_t& sim) {
        // Handle to empirical Hi-C
        const cmxContactFreqs_t& hic =
            sim.get_shared_ro().get_Contact_freqs().get_hic();
        cmxAssertPosEq(hic.n_elem, pseudo_freqs.n_elem);
        // Compute correlation between empirical and pseudo Hi-C
        const cmxReal corr = cmxPearsonCorr::get(
            hic.memptr(), pseudo_freqs.memptr(), hic.n_elem);
        return logscore_using(pseudo_frame, pseudo_freqs, corr, model, sim);
    }

    /**
     * Overload which leverages an existing correlation - computes
     *  log-likelihood of empirical Hi-C frequencies assuming score is
     *  proportional to Pearson correlation
     * @param pseudo_frame - [ignored] Pseudo-population samples
     * @param pseudo_freqs - Pseudo-population aggregated contact frequencies
     * @param corr - Pearson correlation
     * @param model - owning model
     * @param sim - outer simulation
     * @return log-likelihood of empirical Hi-C given latent pseudo-population
     */
    static cmxReal logscore_using(const cmxPseudopopFrame_t& pseudo_frame,
                                  const cmxContactFreqs_t& pseudo_freqs,
                                  const cmxReal corr,
                                  const model_t& model,
                                  const sim_t& sim) {
        cmxAssertRealBoundsInc(corr, CMX_TO_REAL(-1.0), CMX_TO_REAL(1.0));
        // Scale hyper-parameter
        const cmxReal ALPHA = model.get_Contact_freqs_alpha().get_scalar(sim);
        // Allow ALPHA to be in [0, inf) as 0 corresponds to a null model
        // where we can sample Bayes net edge entropies without influence from
        // the empirical Hi-C data
        cmxAssert(ALPHA >= CMX_TO_REAL(0.0));
        // Compute log-likelihood
        const cmxReal score = ALPHA * corr;
        // Emit final log-likelihood
        return score;
    }
};

#endif  // cmxHicScorer_h
